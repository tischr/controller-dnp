classdef GapCrossingLogic < PlatformLogic & VideoLogic %& AnalogInLogic

  properties
    AvailableStates = {'Starting','Stopping','Ready','Active','Busy','Waiting','TrialActive','Saving'};
    DistanceSetByUser = 0;
    AnalogInAvailable = 0;
  end
    
  methods
    % CONSTRUCTOR
    function O = GapCrossingLogic(varargin)
      global CG;
                
      % Call Superclass constructors
      O@PlatformLogic;
      O@VideoLogic;
      %O@AnalogInLogic('Parameters','CG.Parameters.Setup.Audio');
      
      O = setName(O,'GapCrossing');
      P = parsePairs(varargin);
      O.ParametersFull = {...
        'ControlInterval', 0.1,'Numeric',inf;...
        'TrialDurationMax',     60,            'Numeric',inf;...
        'DistMean',                35,            'Numeric',inf;...
        'DistSD',                    4,            'Numeric',inf;...
        'DistMax',                  60,          'Numeric',inf;...
        'RewardAmount',0.02,'Numeric',inf;...
        'PositionFeedback','None','String',{'None','Linear','Local'};...
        'FeedbackSignal','Optical','String',{'Optical','Audio'};...
        'StimulationFrequency',20,'Numeric',inf;... % AT THE MOMENT ONLY USED FOR LOCAL STIMULATION
        'StimulationDuration',10,'Numeric',inf;... % AT THE MOMENT ONLY USED FOR LOCAL STIMULATION
        'FeedbackRanges',[30,15;15,0],'Numeric',inf }; % ONLY WORKS FOR LOCAL STIMULATION
    
      % HW RELATED
      if O.VideoAvailable
        O.HW.ArduinoIDs = [O.HW.IDs.ArduinoPlat,O.HW.IDs.ArduinoCam];
      else
        O.HW.ArduinoIDs = O.HW.IDs.ArduinoPlat;
      end
      O.HW.NArduinos = length(O.HW.ArduinoIDs);
      O.HW.PlatformIDs = struct('Guest',1,'Host',2); % Contains the current status of the platforms    
      
      % ANIMAL TRACKING (Animal Position on the grid: [-3,-2.5,-2,-1.5,-1,0,1,1.5,2,2.5,3];)
      O.States.AnimalPositionY = [-9:0];
      O.States.AnimalPositionX = [linspace(-3,-1,O.HW.NAnimalSensors-1),0,linspace(1,3,O.HW.NAnimalSensors-1)];
      O.States.AnimalPosition = zeros(length(O.States.AnimalPositionY),length(O.States.AnimalPositionX)); % Tracks the animal position over time
      O.States.EstAnimalPosition = -1;
      O.States.AnimalPositionByPType = 'Host';
      O.States.InputStates = zeros(1,length(O.HW.Inputs));
      
      % LIVE POSITION ESTIMATE (=> NIDAQ)
      VideoID = CG.Parameters.Setup.Camera.CameraID;
      CG.Sessions.Video(VideoID).ExtractPosition.Switch = [0,0];
      for iC=1:length(CG.Parameters.Setup.DAQ.DistanceChannels)
        cChannel = O.setDOutputChannel('NI',CG.Parameters.Setup.DAQID,CG.Parameters.Setup.DAQ.DistanceChannels{iC},['Distance',num2str(iC)]);
        CG.Sessions.Video(VideoID).ExtractPosition.DistanceChannels(iC) = cChannel;
      end
      ModuleTypes = {'Arduino'};
      ModuleNames = {'ArduinoFeedback'};
      ModulePars = {[]};
      IDs = addModules(O,ModuleTypes,ModuleNames,ModulePars);
      CG.Sessions.Video(VideoID).ExtractPosition.OldStates = zeros(1,iC);
      CG.Sessions.Video(VideoID).ExtractPosition.Function = 'Linear';
      O.HW.IDs.ArduinoFeedback = IDs;
      O.setInputChannel('NI',CG.Parameters.Setup.DAQID,'ai15',NaN,['Feedback1'],'Digital');
      
      % DISPLAY
      O.Display.LastFigureUpdate = now*CG.Misc.DateNum2SecFactor; % Time when the last figure was updated

      O.assignParameters;
       
    end
    
    % START THE PARADIGM
    function start(O)
      global CG;
      C_setDigitalNI(O.HW.IDs.DAQ,{'Trial','Recording'},[0,0]);
      if O.VideoAvailable C_setDigitalNI(O.HW.IDs.DAQ,'Camera',0); end
      start@Logic(O) % can be used to use the subclass and the class function
      % INITIALIZE THE SYSTEM
      O.assignParameters;
      
      % ACTIVATE POSITION ESTIMATION IF NECESSARY
      VideoID = CG.Parameters.Setup.Camera.CameraID;
      FeedbackActive = ~strcmp(O.Parameters.PositionFeedback,'None');
      CG.Sessions.Video(VideoID).ExtractPosition.Switch = repmat(FeedbackActive,1,2);
      CG.Sessions.Video(VideoID).ExtractPosition.Function = O.Parameters.PositionFeedback; 
      % SET STIMULATION PARAMETERS BASED ON FEEDBACK MODE
      if FeedbackActive
        switch O.Parameters.FeedbackSignal
          case 'Optical'; FeedbackMode = 'opticalfeedback';
          case 'Audio'; FeedbackMode = 'audiofeedback';
        end
        switch O.Parameters.PositionFeedback
          case 'Linear'; Parameters = {0,0};
          case 'Local';  
            DutyCycle = round(100*O.Parameters.StimulationFrequency/(1000/O.Parameters.StimulationFrequency));
            if DutyCycle > 100 DutyCycle =100; warning('Stimulation Duration longer then Period of chosen Frequency'); end
            Parameters = {O.Parameters.StimulationFrequency,DutyCycle};
        end
        % SWITCH ARDUINO MODE TO AUDIO OR OPTICAL FEEDBACK
        %C_startArduino(O.HW.IDs.ArduinoFeedback);
        C_sendMessageArduino(O.HW.IDs.ArduinoFeedback,FeedbackMode,Parameters);
      end
      
      O.States.AnimalPosition(:) = 0;
      O.startAcquisition(O.HW.IDs.DAQ);
  
      % SHOW FIGURES
       for iM=1:length(O.Modules)
         eval(['C_show',O.Modules(iM).Type,'(',n2s(O.Modules(iM).IDType),',0);']);
       end
       
       O.HW.LastStopTrial = -1;
       
      %INITIALIZE SETUP
      pause(1.5); % wait for Arduino to boot
      O.moveGates([1,2],'closed');
      O.prepareDisplay; O.showDisplay(1);
      O.updateStates;
      
      global INITIALIZE;
      if isempty(INITIALIZE) INITIALIZE = 1; end
      if INITIALIZE == 1
        O.initializePlatforms;
        if O.VideoAvailable O.initializeCamera; end
        INITIALIZE = 0;
      else
        disp('Skipping Initialization ...');
      end
      O.createTrialTimer;
      O.DistanceSetByUser = 0;
      
      % PREPARE FOR FIRST TRIAL
      O.prepareTrial;
      
      if INITIALIZE == 1
        Choice = questdlg('Make sure animal is not on the platform (Threshold estimation)',...
          'Animal Dialog','Estimate','Use Old','Estimate');
        switch Choice
          case 'Estimate';  C_setThresholdsNI;
        end
      end
      
      Choice = questdlg('Please put the animal on the left platform',...
        'Animal Dialog','Continue','Break','Continue');
      switch Choice;  case 'Break'; O.stop; end
      
      % START RECORDING (ESPECIALLY FOR EPHYS)
      C_setDigitalNI(O.HW.IDs.DAQ,'Recording',1); pause(0.5);
      
      O.startTrial;
    end
    
    function stop(O)
      C_setDigitalNI(O.HW.IDs.DAQ,'Trial',0);
      
      O.stopTrial;
      
      % STOP THE RECORDING (ESPECIALLY FOR EPHYS)
      C_setDigitalNI(O.HW.IDs.DAQ,'Recording',0);
      
      O.moveGates([1,2],'closed');
      O.deleteTimers;
    
      stop@Logic(O);
    end

    % PREPARE THE NEXT TRIAL
    function prepareTrial(O)
      global EVENTPROCESSING;
      EVENTPROCESSING = 1;
      global CG;
      
      if ~get(CG.GUI.Main.Paradigm.StartButton,'Value') return; end
      
      % DELETE THE TIMERS THAT COULD HAVE STARTED THE TRIAL
    
      % PREPARE FILES AND INCREASE TRIAL
      prepareTrial@Logic(O);
       
      % PRECOMPUTE PROPERTIES
      O.updatePlatformStatus;      % ASSIGN HOST AND GUEST PLATFORM
      O.computeTrialParameters; % NEW DISTANCE & CAMERA POSITIONS
      
      % RECONFIGURE SPACE
      Distance =  abs(O.Trials(O.Trial).GuestPosition - O.HW.Platforms(O.HW.PlatformIDs.Guest).Position);
      cPausePlatform = Distance*O.HW.Platforms(O.HW.PlatformIDs.Guest).TimePerMM;
      cPause = cPausePlatform;
      
      if O.VideoAvailable
        Distance =  abs(O.Trials(O.Trial).CameraPosition - O.HW.Camera(1).Position);
        cPauseCamera = Distance*O.HW.Camera(1).TimePerMM;
        cPause = max([cPausePlatform,cPauseCamera]);
      end
      
      O.TimeToContinue = now*86400 + cPause  + 0.5;
      
      Distance =  O.Trials(O.Trial).GuestPosition - O.HW.Platforms(O.HW.PlatformIDs.Guest).Position;
      O.movePlatform(O.HW.PlatformIDs.Guest,Distance);
      
      if O.VideoAvailable
        Distance = O.Trials(O.Trial).CameraPosition - O.HW.Camera.Position;
        O.moveCamera(O.HW.Camera.CameraID,Distance);
      end
      
      O.moveGates(O.HW.PlatformIDs.Guest,'open');
      pauseUntil(O.TimeToContinue);
 
      % RESTART FEEDBACK
      VideoID = CG.Parameters.Setup.Camera.CameraID;
      if CG.Sessions.Video(VideoID).ExtractPosition.Switch(2)
        CG.Sessions.Video(VideoID).ExtractPosition.Switch(1) = CG.Sessions.Video(VideoID).ExtractPosition.Switch(2);
        switch O.Parameters.PositionFeedback
          case 'Linear';  FeedbackState = 1;
          case 'Local'; FeedbackState = 0;
            % RANDOMLY INITIALIZE FEEDBACK RANGE PER TRIAL
            NRanges = size(O.Parameters.FeedbackRanges,1);
            iFeedback = ceil(NRanges*rand);
            O.Trials(O.Trial).Feedback.Range = O.Parameters.FeedbackRanges(iFeedback,:);
            CG.Sessions.Video(VideoID).ExtractPosition.Range = O.Trials(O.Trial).Feedback.Range;
        end
        C_setFeedbackState(FeedbackState);
      end
      
      if O.VideoAvailable         O.prepareVideo(O.HW.IDs.Camera); end
      if O.AnalogInAvailable   O.prepareAnalogIn(O.HW.IDs.AnalogIn); end
      
      disp('prepareTrial finished.')
    end
     
    function startTrial(O)
      global CG;
      
      if ~get(CG.GUI.Main.Paradigm.StartButton,'Value') return; end

      O.Trials(O.Trial).Attempt = 0;
      O.updateDisplay('TrialStart');

      if O.AnalogInAvailable O.startAnalogIn(O.HW.IDs.AnalogIn); end
      
      % WAIT FOR ANIMAL TO LEAVE FRONT SENSOR
      FrontSensor  = O.getInputByName(['AniPosP',num2str(O.HW.PlatformIDs.Host),'S3']);
      fprintf('Waiting for up to 5s for animal to leave front sensor...');
      TTotal = 0; dT = 0.05; 
      while TTotal<5;  
        pause(dT); TTotal = TTotal + dT;  
        try 
          [States,~,DeltaT] = O.updateStates; 
          if DeltaT < 0.5 && States(FrontSensor); break; end;
        catch
          disp('Error likely in updateStates');
        end
      end; disp(' Starting');
      O.updateAnimalPosition;

      startTrial@Logic(O);
      
      O.startTrialTimer;
      C_setDigitalNI(O.HW.IDs.DAQ,'Trial',1);
      fprintf(['\n\n> Starting Trial ',num2str(O.Trial),'\n']);
      
      if O.VideoAvailable       O.startVideo(O.HW.IDs.Camera); end
      O.moveGates(O.HW.PlatformIDs.Host,'open');
      pause(0.5);
    end
    
    % STOP THE CURRENT TRIAL
    function stopTrial(O)
      global CG
      O.TrialActive = 0; O.changeState('Busy');
      O.stopTrialTimer;
      
      % PAUSE VIDEO ACQUISITION
      SaveVideo = O.VideoActive; 
      if O.VideoActive;      O.pauseVideo(O.HW.IDs.Camera);     end    
      
      % STOP AUDIO ACQUISITION
      if O.AnalogInAvailable
        SaveAnalogIn = O.AnalogInActive;
        if O.AnalogInActive O.stopAnalogIn(O.HW.IDs.AnalogIn); end
      end
      
      % STOP FEEDBACK AT END OF TRIAL
      VideoID = CG.Parameters.Setup.Camera.CameraID;
      if CG.Sessions.Video(VideoID).ExtractPosition.Switch(2)
        CG.Sessions.Video(VideoID).ExtractPosition.Switch(1) = 0
        C_setFeedbackState(0);
      end
      
      % CLOSE HOST GATE
      FrontSensor  = O.getInputByName(['AniPosP',num2str(O.HW.PlatformIDs.Guest),'S3']);
      fprintf('Waiting for up to 10s for animal to leave front sensor on guest platform...');
      TTotal = 0; dT = 0.05; 
      while TTotal<0;
        pause(dT); TTotal = TTotal + dT;  
        [States,~,DeltaT] = O.updateStates;
        if DeltaT < 0.2 && States(FrontSensor); break; end;
      end; disp(' Starting');
      O.moveGates([O.HW.PlatformIDs.Guest],'closed'); 
      
      % CLOSE HOST GATE
      FrontSensor  = O.getInputByName(['AniPosP',num2str(O.HW.PlatformIDs.Host),'S3']);
      fprintf('Waiting for up to 10s for animal to leave front sensor on host platform...');
      TTotal = 0; dT = 0.05;
      while TTotal<0;
        pause(dT); TTotal = TTotal + dT;  
        [States,~,DeltaT] = O.updateStates;
        if DeltaT < 0.2 && States(FrontSensor); break; end;
      end; disp(' Starting');
      O.moveGates([O.HW.PlatformIDs.Host],'closed');  
      
      % SELECT FRAME FOR VIDEO TO SAVE
      if O.VideoAvailable O.selectFramesForSaving(SaveVideo); end
    
      % WRAP UP TRIAL
      O.updatePerformance;
      stopTrial@Logic(O);
      disp('stopTrial finished.')
      global EVENTPROCESSING;
      EVENTPROCESSING = 0;
    end
    
    % COMPUTE NEW POSITIONS OF PLATFORMS & CAMERA
    function computeTrialParameters(O)
      % COMPUTE POSITIONS FOR PLATFORMS
      DistanceFound = 0;
      HostID = O.HW.PlatformIDs.Host;
      GuestID = O.HW.PlatformIDs.Guest;

      % CURRENT LIMITS OF POSITIONING
      HostPos = O.HW.Platforms(HostID).Position;
      MinDist = HostPos + sum([O.HW.Platforms.PositionOffset]);
      RangeMaxGuest =  O.HW.Platforms(O.HW.PlatformIDs.Guest).RangeMax;
      
      % CHECK FOR USER ENTERED DISTANCE
      NewDistance = [];
      if O.DistanceSetByUser
        NewDistance = str2num(get(O.Display.Handles.DistanceEdit,'String'));
        if ~isempty(NewDistance) % ELSE COVERED IN THE COMPUTATION BELOW
          if NewDistance<MinDist
            NewDistance = MinDist; warning(['Distance set to current minimal distance : ',num2str(NewDistance)]);
          end
          if NewDistance> MinDist+RangeMaxGuest
            NewDistance = RangeMaxGuest; warning(['Distance set to current maximal distance : ',num2str(NewDistance)]);
          end
        else
          if O.Trial>1
            warning('New value of distance could not be converted to a number');
          end
        end
      end
      
      % COMPUTE NEW DISTANCE FROM DISTRIBUTION
      if isempty(NewDistance)
        while ~DistanceFound
          NewDistance = randn*O.Parameters.DistSD + O.Parameters.DistMean;
          if NewDistance >= MinDist && NewDistance <= MinDist+RangeMaxGuest;
            DistanceFound = 1;
          end
        end
      end
      
      % ROUND DISTANCE TO 0.1MM
      NewDistance = round(10*NewDistance)/10;
      NewDistance = max([NewDistance,0]);
      fprintf(['Distance for Trial ',num2str(O.Trial),' :  ',num2str(NewDistance),'mm\n']);
      
      GuestPos = NewDistance - MinDist;
      if GuestPos < 0 fprintf('Guest platform position below lower limit!\n'); keyboard; end
      
      HostPos = O.HW.Platforms(HostID).Position;
      if HostPos < 0 fprintf('Host platform position below lower limit!\n'); keyboard; end      
      O.Trials(O.Trial).Distance = NewDistance;
      O.Trials(O.Trial).GuestPosition = GuestPos;
      O.Trials(O.Trial).HostPosition = HostPos;
      
      if O.VideoAvailable
        % COMPUTE NEW POSITION FOR THE CAMERA
        switch GuestID
          case 1; % Target on the left
            NewCamPos = O.HW.Camera.DistanceToCenter + ...
              (GuestPos + O.HW.Platforms(GuestID).PositionOffset - O.HW.Camera.DistanceToPlatform ) ;
          case 2; %Target on the right
            NewCamPos = O.HW.Camera.DistanceToCenter - ...
              (GuestPos + O.HW.Platforms(GuestID).PositionOffset - O.HW.Camera.DistanceToPlatform );
        end
        if NewCamPos < 0 fprintf('Camera position below lower limit!\n'); keyboard; end
        if NewCamPos > O.HW.Camera(1).RangeMax;
          fprintf('Camera position above upper range limit!\n'); keyboard; end
        O.Trials(O.Trial).CameraPosition = NewCamPos;
      end
      
      O.Trials(O.Trial).Attempt = 0;
    end
    
    % UPDATE THE PERFORMANCE OF THE ANIMAL OVER TRIALS
    function updatePerformance(O)
      iT = O.Trial;
      if iT>0
        Outcome = double(strcmp(O.States.AnimalPositionByPType,'Guest'));
        O.Performance(iT).Outcome = Outcome;
      end
    end
        
    % UPDATE ANIMAL POSITION
    function updateAnimalPosition(O)
      InputStates = O.States.InputStates(O.HW.AnimalSensorsPhys);
      InputStates = ~InputStates;
      if sum(InputStates) % IF AT LEAST ONE SENSOR IS ACTIVE
        O.States.AnimalPosition(end+1,[1:2:end]) = InputStates/sum(InputStates);
      else % NO SENSOR ACTIVE
        if max(O.States.AnimalPosition(end,:))==1 % 
          T = zeros(1,size(O.States.AnimalPosition,2));
          for i=1:size(O.States.AnimalPosition,2)
            C1 = 0; if i>1; C1 = O.States.AnimalPosition(end,i-1); end
            C2 = 0; if i<size(O.States.AnimalPosition,2);  C2 = O.States.AnimalPosition(end,i+1); end
            T(i) = C1 + C2;
          end
          O.States.AnimalPosition(end+1,:) = T/sum(T);
        end
      end
      O.States.EstAnimalPosition = sum(O.States.AnimalPositionX.*O.States.AnimalPosition(end,:));
      if O.States.EstAnimalPosition<0; PID = 1; end
      if O.States.EstAnimalPosition>0; PID = 2; end;
      if O.States.EstAnimalPosition==0;  PID = 0; end;
      if O.HW.PlatformIDs.Host == PID; PType = 'Host'; else PType = 'Guest'; end
      if O.States.EstAnimalPosition==0; PType = 'Middle'; end;
      O.States.AnimalPositionByPType = PType;
    end
    
    % UPDATE PLATFORM STATUS
    function updatePlatformStatus(O)
      if O.States.EstAnimalPosition<=0 || O.Trial==1 % ANIMAL ON THE LEFT OR FIRST TRIAL
        O.HW.PlatformIDs.Host = 1; O.HW.PlatformIDs.Guest = 2;
      else % ANIMAL ON THE RIGHT
        O.HW.PlatformIDs.Host = 2; O.HW.PlatformIDs.Guest = 1;
      end
    end
    
    % EVENT PROCESSING (CORE FUNCTION)
    function processEvent(O,SourceName,Event)
      global CG;
      %fprintf([SourceName,' Event : ',Event.Name,' ',num2str(Event.Data),'\n']);
      if ~O.ParadigmActive return; end
      switch SourceName
        case 'Keyboard';
          processEvent@Logic(O,SourceName,Event);
          
        case 'PointGrey';
          switch Event.Name
            case 'MemoryFull';
              fprintf('Maximal Memory reached, flushing image buffer!\n');
              % WAIT, lets first get the background pictures
              VideoID = O.HW.IDs.Camera;
              tic
              BackgroundImages = getdata(CG.Sessions.Video(VideoID).S,500);
              toc
              flushdata(CG.Sessions.Video(VideoID).S);
              CG.Sessions.Video(VideoID).MemoryFullNotified = 0;
              toc
          end
          
        case 'NIDAQ';
          %           RelEventTime =  (now - CG.Sessions.NI(1).TriggerTime)*86400 - Event.Time(1);
          %           if  RelEventTime > 1;
          %             disp(['Ignoring Event ',num2str(RelEventTime)]);
          %             return;
          %           else
          %             disp(['NIDAQ Event received at relative time ',num2str(RelEventTime)]);
          %           end          
          InputStates = O.updateStates(Event.Time(1));
          O.updateAnimalPosition;
          O.updateDisplay('Update');
          HostID = O.HW.PlatformIDs.Host;
          GuestID = O.HW.PlatformIDs.Guest;
          ChannelName = O.HW.Inputs(Event.Data).Name;
          switch Event.Name
            case 'AI_to_High'; % Thresholded change of analog channels
              switch ChannelName % Channel Number that has changed
                case {'AniPosP1S3','AniPosP2S3'}; % Inner End
                  switch O.States.AnimalPositionByPType,
                      case {'Guest'}; 
                        VideoID = CG.Parameters.Setup.Camera.CameraID;
                        CG.Sessions.Video(VideoID).ExtractPosition.Switch(1) = 0; 
                  end
                
              end % END SWITCH EVENT NUMBER
              
            case 'AI_to_Low';  % Thresholded change of analog channels
              switch ChannelName % Channel Number that has changed
                case {'AniPosP1S2','AniPosP2S2'}; % Middle
                  if O.TrialActive
                    switch O.States.AnimalPositionByPType,
                      case {'Guest','Middle'}; % ANIMAL HAS BROKEN BEAM ON OTHER SIDE
                        if  (Event.Time(1) - O.HW.LastStopTrial > 5) % SUCCESSFUL TRIAL
                          disp('Animal Crossed (signalled by Arrival at Guest)!');
                          
                          O.TrialActive = 0; O.HW.LastStopTrial = Event.Time(1);
                          if O.VideoAvailable && ~O.VideoActive  C_setDigitalNI(O.HW.IDs.DAQ,{'Camera'},1); pause(0.01); end; % STRANGE CASE, WHERE VIDEO NOT TRIGGERED
                          if O.VideoAvailable C_setDigitalNI(O.HW.IDs.DAQ,{'Trial','Camera'},[0,0]); else C_setDigitalNI(O.HW.IDs.DAQ,{'Trial'},[0]); end
                          disp('Setting Camera & Trial Trig to 0');
                          % GIVE REWARD
                         if strcmp(ChannelName,'AniPosP1S2') PlatformID = 1; 
                         elseif strcmp(ChannelName,'AniPosP2S2') PlatformID = 2; end
                         O.provideReward(PlatformID,O.Parameters.RewardAmount);
                         
                         % ERROR HERE
                         evalin('base',...
                           ['T= timer(''TimerFcn'','...
                           '[''global CG; '...
                           'CG.Paradigm.stopTrial; '...
                           'CG.Paradigm.prepareTrial; ' ...
                           'pauseUntil(CG.Paradigm.TimeToContinue); '...
                           'CG.Paradigm.startTrial;''],'...
                           '''StartDelay'',0.3,''Name'',''AnimalCrossedGuest''); '...
                           'start(T);'])
                        end
                    end
                  end
                  
                case {'AniPosP1S3','AniPosP2S3'}; % InnerEnd
                  if O.TrialActive
                    switch O.States.AnimalPositionByPType,
                      case 'Host'; % ANIMAL ATTEMPTS TO CROSS
                        %if O.VideoAvailable && ~O.VideoActive && O.VideoReady
                          %O.startVideo(O.HW.IDs.Camera);
                          %O.Trials(O.Trial).Attempt = O.Trials(O.Trial).Attempt + 1;
                          %disp('Video Starting!');
                       % end
                    end
                  end
              end
          end % END SWITCH EVENT TYPE
      end
    end
     
    % PREPARE DISPLAY BEFORE FIRST PLOT
    function prepareDisplay(O)
      if ~isfield(O.Display,'Handles') || ~ishandle(O.Display.Handles.FIG)
        prepareDisplay@Logic(O); 
      else
        figure(O.Display.Handles.FIG); 
      end
      clf;
      DC = axesDivide(1,[1.2,1,1],[0.1,0.2,0.8,0.7],[],1);
      colormap(HF_colormap({[1,1,1],[1,0,0]},[0,1]));
      % ANIMAL POSITION
      FontSize = 8;
      O.Display.Handles.AnimalAxis = axes('Pos',DC{1},'FontSize',FontSize);
      O.Display.Handles.AnimalPosPlot = imagesc(O.States.AnimalPositionX,O.States.AnimalPositionY,O.States.AnimalPosition(end-9:end,:));
      set(O.Display.Handles.AnimalAxis,'CLim',[0,1],'XLim',[O.States.AnimalPositionX([1,end])],'YLim',O.States.AnimalPositionY([1,end]));
      O.Display.Handles.AnimalTitle = get(O.Display.Handles.AnimalAxis,'Title');
      set(O.Display.Handles.AnimalTitle,'String',['Animal Position']);
      
      % PERFORMANCE STATISTICS
      % successful crossings and time until crossing over the history
      O.Display.Handles.AnimalPerfAxis = axes('Pos',DC{2},'FontSize',FontSize);
      O.Display.Handles.PerfH  = plot(0,0,'.-r');
      %O.Display.Handles.TimeH = plot(Time,zeros(size(Time)),'-.r');
      set(O.Display.Handles.AnimalPerfAxis,'YLim',[-0.1,1.1]);
      O.Display.Handles.PerfTitle = get(O.Display.Handles.AnimalPerfAxis,'Title');
      set(O.Display.Handles.PerfTitle,'String',['Performance']);
      
      % DISTANCE STATISTICS
      O.Display.Handles.AnimalHistAxis = axes('Pos',DC{3},'FontSize',FontSize);
      Bins = [0:5:100];
      O.Display.Handles.HistH = bar(Bins,zeros(size(Bins)));
      O.Display.DistBins = Bins;
      set(O.Display.Handles.AnimalHistAxis,'YLim',[0,5],'XLim',Bins([1,end]));
      O.Display.Handles.HistTitle = get(O.Display.Handles.AnimalHistAxis,'Title');
      set(O.Display.Handles.HistTitle,'String',['Distance Distribution']);
     
      % CURRENT TRIAL INFORMATION
      % text fields which contain the information 
      Fields = {'Dist','Host','Guest'};
      O.Display.Handles.DistanceEdit =  uicontrol('style','edit','units','norm','pos',[0.11,0.05,0.17,0.07],'String','','Callback','CG.Paradigm.DistanceSetByUser = 1;');
      for iF=1:length(Fields)
        O.Display.Handles.([Fields{iF},'Text']) = text(-0.1+(iF-1)*0.4,-0.9,[Fields{iF}],'Units','norm','FontSize',8,'Horiz','left');
      end
      O.Display.LastFigureUpdate = now;
      set(O.Display.Handles.FIG,'Visible','off');
    end
    
    % UPDATE FIGURE WHENEVER NECESSARY
    function updateDisplay(O,Opt)
      global CG
      
      if ~exist('Opt','var') Opt = 'Update'; end
      
      cTime = now*CG.Misc.DateNum2SecFactor;
      % LIMIT UPDATE RATE TO CONTROLINTERVAL
      if get(CG.GUI.Main.Paradigm.ShowButton,'Value') ...
          && cTime - O.Display.LastFigureUpdate > O.Parameters.ControlInterval;
        
        % ANIMAL POSITION
        set(O.Display.Handles.AnimalPosPlot,'CData',O.States.AnimalPosition(end-9:end,:));
        set(O.Display.Handles.AnimalTitle,'String',['Animal Position : ',O.States.AnimalPositionByPType]);
        
        % PERFORMANCE STATISTICS
        if length(O.Performance)>0 && isfield(O.Performance,'Outcome')
          Trials = [1:length(O.Performance)]; Crossings = [O.Performance.Outcome];
          cInd = [1:length(Crossings)];
          set(O.Display.Handles.PerfH,'XData',Trials(cInd),'YData',Crossings);
        end
        
        % HISTOGRAM OF GAP DISTANCES
        if O.Trial && ~isempty(O.Trials)
          H = hist([O.Trials.Distance],O.Display.DistBins);
          set(O.Display.Handles.HistH,'YData',H);
        end
        
        % UPDATE POSITION INFORMATION
        if O.Trial  && ~isempty(O.Trials) && length(O.Trials)==O.Trial && strcmp(Opt,'TrialStart')
          set(O.Display.Handles.DistanceEdit,'String',n2s(O.Trials(O.Trial).Distance));
        end
        set(O.Display.Handles.HostText,'String',  ['Host : ',n2s(O.HW.PlatformIDs.Host),' ( ',O.HW.Platforms(O.HW.PlatformIDs.Host).Location,' )']);
        set(O.Display.Handles.GuestText,'String',['Guest : ',n2s(O.HW.PlatformIDs.Guest),' ( ',O.HW.Platforms(O.HW.PlatformIDs.Guest).Location,' )']);
        
        O.Display.LastFigureUpdate = cTime;
      end
     % catch exception
      %  fprintf(['ERROR : (while updateDisplay) : ',exception.stack(1).name,' ',num2str(exception.stack(1).line),': ',exception.message,'\n']);
    %end
    end
  end
end
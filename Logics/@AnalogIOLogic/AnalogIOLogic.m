% 'internal' analog input channels : http://digital.ni.com/public.nsf/allkb/6C77B1884F5BBEE8862569E3005BE30E

classdef AnalogIOLogic < Logic

  % DEFINE PROPERTIES
  properties (SetAccess = public)
    
    AnalogIOAvailable = 1;
    AnalogIOActive = 0;
    
    analogInputSampleRate = 0;
    device = 0;
    
  end
  
  methods
    % CONSTRUCTOR
    function O = AnalogIOLogic(varargin)
      
      global CG;
      
      O.AnalogIOAvailable = isfield(CG.Parameters.Setup, 'AnalogIO');
      if ~O.AnalogIOAvailable return; end
      P = parsePairs(varargin);
      checkField(P, 'Parameters', [])
      if ~isempty(P.Parameters)        
        
        O.analogInputSampleRate = eval( [P.Parameters, '.SRAI'] );
        O.device = eval( [ P.Parameters, '.Device{2}' ] ); 

        ModulePars = { { ...
          'Device', eval( [ P.Parameters, '.Device{2}' ] ), ...
          'SRAI', eval( [P.Parameters, '.SRAI'] ), ...
          'ChAI', eval( [P.Parameters, '.ChAI'] ), ...
          'AINames', eval( [P.Parameters, '.AINames'] ),... % the module require names setting
          'ChAO', eval( [P.Parameters, '.ChAO'] ), ...
          'AONames', eval( [P.Parameters, '.AONames'] ),... % the module require names setting
          'SRAO', eval( [P.Parameters, '.SRAO'] ), ...
          'ChDO', eval( [P.Parameters, '.ChDO'] ), ...
          'TerminalConfig', 'SingleEnded', ...
          'ControlInterval', 0.25, ...
          'Name', 'AnalogIO', ...
          'Saving', 'All' } };
        
      else
        
        ModulePars = {};
        
      end
      
      O = setName(O,'AnalogIO');
           
      ModuleTypes = {'NI'};
      ModuleNames = {'AnalogIO'};
     
      ID = addModules(O, ModuleTypes, ModuleNames, ModulePars);
      
      O.HW.IDs.AnalogIO = ID;
      
      C_prepareNI(O.HW.IDs.AnalogIO);
      
    end    
          
    function startAnalogIO(O)
      
      C_startNI(O.HW.IDs.AnalogIO);
      O.AnalogIOActive = 1;
      
    end

    function stopAnalogIO(O)
      
      C_stopNI(O.HW.IDs.AnalogIO, 0); 
      O.AnalogIOActive = 0;
      
    end    

  end
end
classdef AnalogInLogic < Logic

  % DEFINE PROPERTIES
  properties (SetAccess = public)
    AnalogInAvailable = 1;
    AnalogInActive = 0;
  end
  
  methods
    % CONSTRUCTOR
    function O = AnalogInLogic(varargin)
      global CG;
      O.AnalogInAvailable = isfield(CG.Parameters.Setup,'Audio');
      if ~O.AnalogInAvailable return; end
      P = parsePairs(varargin);
      checkField(P,'Parameters',[])
      if ~isempty(P.Parameters)
        ModulePars = {{...
        'Device',eval([P.Parameters,'.Device{2}']),...
        'SRAI',eval([P.Parameters,'.SRAI']),...
        'ChAI',eval([P.Parameters,'.ChAI']),...
        'ChDO',[],'ChAO',[],...
        'TerminalConfig','SingleEnded',...
        'AINames',{'MicLeft','MicRight'},...
        'ControlInterval',0.25,...
        'Name','Audio',...
        'Saving','All'}};
      else
        ModulePars = {};
      end
      
      O = setName(O,'AnalogIn');
           
      ModuleTypes = {'NI'};
      ModuleNames = {'AnalogIn'};
     
      ID = addModules(O,ModuleTypes,ModuleNames,ModulePars);
      
      O.HW.IDs.AnalogIn = ID;
      
      % ESTABLISH TRIGGER CONNECTION BETWEEN TRIAL TRIGGGER
      C_setTriggerNI(O.HW.IDs.AnalogOut,'Direction','AnalogIn','Source','External','Type','Start',...
        'Target',[CG.Parameters.Setup.Audio.Device{2},'/',CG.Parameters.Setup.Audio.TriggerChannel])
    end
      
    function prepareAnalogIn(O,AnalogInID)
      C_prepareNI(AnalogInID);
    end
    
    function startAnalogIn(O,AnalogInID)
      C_startNI(AnalogInID);
      O.AnalogInActive = 1;
    end

    function stopAnalogIn(O,AnalogInID)
      disp('AnalogIn stopping ...')
      C_stopNI(AnalogInID,0); % DONT SAVE DATA YET
      O.AnalogInActive = 0;
      disp('AnalogIn stopped.')      
    end    

  end
end
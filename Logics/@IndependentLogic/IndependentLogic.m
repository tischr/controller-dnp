%% DEFINE THE INDEPENDENT CLASS
% This logic is used mostly for testing individual modules
% since they work independently in this case (i.e. events are not routed, but shown!!)

classdef IndependentLogic < Logic
  properties (Hidden)
    
  end
  
  properties (SetAccess = private)
    
  end
  
  events
    
  end
  
  
  methods
    % CONSTRUCTOR
    function O = IndependentLogic(varargin)
      O = setName(O,'Independent');
      P = parsePairs(varargin);
      O.Parameters = {...
        'ControlInterval',0.1,'Numeric',inf;...
        };
      %ModuleTypes = {'Keyboard','Arduino','Video'};
      %ModuleNames = {'Keyboard','ArduinoDAQ','Camera'};
      %ModuleTypes = {'Video'};
      %ModuleNames = {'Camera'};
      %ModuleTypes = {'Keyboard','Phidget'};
      %ModuleNames = {'Keyboard','Phidget'};
      ModuleTypes = {'Arduino'};
      ModuleNames = {'ArduinoDAQ'};
      %ModuleTypes = {'Keyboard','NI'};
      %ModuleNames = {'Keyboard','NIDAQ'};
      ModulePars = {[],[]};
      O.addModules(ModuleTypes,ModuleNames,ModulePars);
    end
    
    % START THE MODULE
    function start(O)
      start@Logic(O) % can be used to use the subclass and the class function
    end
    
    % EVENT PROCESSING (CORE FUNCTION)
    function O = processEvent(O,SourceName,Event);
      
      switch SourceName
        case 'Keyboard';  
          processEvent@Logic(O,SourceName,Event);
          
        case 'ArduinoDAQ';
      
        case 'Camera';
     
      end
    end
    
    function O = updateDisplay()
      
    end
    
    function O = saveData(O)
      % SAVE MODULE AND GENERAL PROPERTIES
      saveData@Logic(O);
      % NOW SAVE SPECIFIC INFORMATION OF THE PARADIGM
      
    end
    
  end
  
end
classdef AudioPassiveLogic < MonitorLogic &  AnalogOutLogic
  
  properties (SetAccess = public)
    AvailableStates = {'Starting','Stopping','Ready','Active','Busy','Waiting','TrialActive','Saving'};
    Stimulus = []; % Stimulus SoundObject
    Repetition = []; % Current Repetition
    Repetitions = []; % Holds the Repetition related information
    SRAudio = []; % Audio Sampling Rate
    StopParadigm = 0; % Flag to stop paradigm gracefully
    Speaker = []; % Speaker Calibration
  end
  
  methods
    % CONSTRUCTOR
    function O = AudioPassiveLogic(varargin)
      global CG;
      % CALL SUPERCLASS CONSTRUCTORS
      O@MonitorLogic('Parameters','CG.Parameters.Setup.DAQ');
      O@AnalogOutLogic('Parameters','CG.Parameters.Setup.Audio');
     
      O = setName(O,'AudioPassive');
      P = parsePairs(varargin);
      O.ParametersFull = {...
        'Stimulus',            'PureTones',      'String',       'TMP = C_getStimuli(''Module'',''Audio'');';...
        'Speaker',              'FostexT250D',   'String',      'TMP = C_getSpeakers;';...
        'ControlInterval',  0.1,                     'Numeric',  inf;...
        'Repetitions',        10,                       'Numeric',  inf};
           
      % HW Related
      O.SRAudio = CG.Parameters.Setup.Audio.SRAO;

      O.assignParameters;
    end
    
    % START THE PARADIGM
    function start(O)
      global CG;
      C_setDigitalNI(O.HW.IDs.DAQ,{'Trial','Recording'},[0,0]);  O.StopParadigm = 0;
      start@Logic(O); set(CG.GUI.Main.Paradigm.StartButton,'Enable','off'); 

      % INITIALIZE THE SYSTEM
      O.assignParameters; O.startAcquisition(O.HW.IDs.DAQ);
     
      O.getSpeaker;  % LOAD SPEAKER EQUALIZATION
       
      % INITIALIZE STIMULUS
      O.Stimulus = eval(O.Parameters.Stimulus);
      O.Stimulus.SR = O.SRAudio;
      
      % SHOW FIGURES
       for iM=1:length(O.Modules)
        eval(['C_show',O.Modules(iM).Type,'(',n2s(O.Modules(iM).IDType),',1);']);
       end
       O.prepareDisplay;  O.showDisplay(1);
      
       % START EPHYS RECORDING
      C_setDigitalNI(O.HW.IDs.DAQ,'Recording',1); pause(0.5);

      % LOOP OVER REPETITIONS
      iR = 0;
      while iR < O.Parameters.Repetitions
        iR = iR+1;
        O.Repetition = iR;
        MaxIndex = O.Stimulus.MaxIndex;
        O.Repetitions(iR).Indices = O.Stimulus.Randomize;
        
        % LOOP OVER TRIALS WITHIN REPETITION
        for iT = 1:MaxIndex % iT just looks over internal repetitions
          O.Trial = O.Trial + 1;
          O.Trials(O.Trial).Repetition = iR;
          O.Trials(O.Trial).Index =  O.Repetitions(iR).Indices(iT);
          fprintf(['\n====== Repetition ',num2str(iR),' - Trial ',num2str(iT),' =======\n']);
  
          O.prepareTrial; % PREPARE STIMULUS
          O.startTrial; % PLAY STIMULUS
          
          % UPDATE DISPLAY WHILE STIMULUS IS PLAYING
          while O.getAnalogOutState
            O.updateDisplay; pause(O.Parameters.ControlInterval);
          end
          O.stopTrial;
          if O.StopParadigm;  break;  end
        end
        
        % IF USER REQUESTED PARADIGM TO STOP
        if O.StopParadigm; break;
        else
          if iR == O.Parameters.Repetitions
            % CHECK WHETHER TO CONTINUE
            O.Parameters.Repetitions = O.Parameters.Repetitions + isequal(questdlg('Continue another repetition?','','Yes','No','Yes'),'Yes');
          end
        end
      end % END REPETITION LOOP
      O.stop;
    end
    
    % STOP PARADIGM
    function stop(O)
      C_setDigitalNI(O.HW.IDs.DAQ,'Recording',0);
      stop@Logic(O,0); % STOP ENTIRE PARADIGM, DONT SAVE FINAL DATA
    end
    
    % CALLBACK FROM GUI BUTTON TO BREAK THE EXECUTION
    function terminate(O,H,Event)
      O.StopParadigm = ~O.StopParadigm;
    end
    
    % PREPARE THE NEXT TRIAL
    function prepareTrial(O)
      
      % PREPARE FILES AND INCREASE TRIAL
      prepareTrial@Logic(O,0);
 
      Waveform = O.Stimulus.Waveform(O.Trials(O.Trial).Index);
      Waveform = C_correctSpeaker(Waveform,'SR',O.SRAudio,'Calibration',O.Speaker);
      
      % SAVE STIMULUS PROPERTIES OF CURRENT TRIAL
      O.Trials(O.Trial).Stimulus = struct('StartPos',O.Stimulus.StartPos,'StopPos',O.Stimulus.StopPos,...
        'ParSequence',O.Stimulus.ParSequence,'Permutation',O.Stimulus.Permutation);
      
      % CREATE PRECISELY TIMED TRIGGERS FOR EPHYS (NOT NEEDED AT THE MOMENT)
      Triggers = ones(size(Waveform)); Triggers(end)=0;
      
      O.prepareAnalogOut(O.HW.IDs.AnalogOut,[ Waveform , Triggers]);
    end
     
    % START TRIAL
    function startTrial(O)
      O.changeState('TrialActive'); O.TrialActive = 1;
      O.startAnalogOut(O.HW.IDs.AnalogOut);
    end
    
    % STOP THE CURRENT TRIAL
    function stopTrial(O)
      O.TrialActive = 0; O.changeState('Stopping');  

      % STOP AUDIO OUTPUT
      O.stopAnalogOut(O.HW.IDs.AnalogOut);
      C_setDigitalNI(O.HW.IDs.DAQ,'Trial',0); pause(0.1); % WAIT UNTIL IT IS RECORDING IN NIDAQ;
      
      stopTrial@Logic(O); 
      O.changeState('Ready');
    end
    
    % CHANGE THE OVERALL NUMBER OF REPETITIONS
    function changeRepetition(O,H,Event,Step)
      O.Parameters.Repetitions = O.Parameters.Repetitions + Step;
      O.updateDisplay;
    end
    
    % LOAD THE SELECTED SPEAKER
    function getSpeaker(O)
      [Speakers,Files] = C_getSpeakers;
      Pos = find(strcmp(O.Parameters.Speaker,Speakers));
      tmp = load(Files{Pos}); O.Speaker = tmp.R;
    end
    
    % EVENT PROCESSING : NO EVENTS IN PASSIVE CASE!
    function processEvent(O,SourceName,Event);  end
     
    % PREPARE DISPLAY BEFORE FIRST PLOT
    function prepareDisplay(O)
      global CG
      
      if ~isfield(O.Display,'Handles') || ~ishandle(O.Display.Handles.FIG)
        prepareDisplay@Logic(O); 
      else
        figure(O.Display.Handles.FIG); 
      end
      clf;
            
      %  ARRANGEMENT :
      %  RepMins         RepMax       RepPlus
      %  TrialInRep      Repetition    Index
      %  CurrentTime  TotalTime    Break 
      %  Parameters:         ---------------Axis For Display-------------
      %  Par 1
      %  Par 2
      %  Par 3

      set(O.Display.Handles.FIG,'Visible','on') 
      
      DC1 = axesDivide(1,[0.3,0.7],[0.01,0.02,0.98,0.95],[],0.03);
      DC2 = axesDivide([0.5,0.5],1,DC1{2},0.03,[]);
      DC = [DC1(1);DC2(:)];
      % CREATE PANELS
      Panels = {'General','Parameters','Results'};
      for iP=1:length(DC)
        O.Display.Handles.([Panels{iP},'Panel']) = uipanel(O.Display.Handles.FIG,...
          'Pos',DC{iP},'Title',Panels{iP},'BackgroundColor',CG.Colors.GUI.Panel,'TitlePosition','CenterTop');
      end
      
      % ADD PARAMETERS
      FN = fieldnames(O.Stimulus.Parameters); Parent = O.Display.Handles.ParametersPanel;
      DCP = axesDivide(1,length(FN),[0.05,0.05,0.9,0.9],[],[0.5]);
      for iP = 1:length(FN)
        cPar = O.Stimulus.Parameters.(FN{iP});
        O.Display.Handles.([FN{iP},'Par']) = uicontrol(Parent,'style','text',...
          'Units','norm','Pos',DCP{iP},'String',[FN{iP},' : ',num2str(cPar.Value)],...
          'FontSize',7,'Horiz','left','ForegroundColor',[0,0,0],'BackGroundColor',CG.Colors.GUI.Panel);
      end
      
      % CURRENT TRIAL INFORMATION
      Parent = O.Display.Handles.GeneralPanel;
      DCP = axesDivide(3,3,[0.02,0.01,0.96,0.9],[0.3],[0.3]);

      Fields = {'RepMax','RepCurr','Trial','TimeCurr','TimeMax','Index'};
      DCPInd = [4,5,2,3,6,8];
      for iF=1:length(Fields)
        switch Fields{iF}
          case 'RepMax'; cValue = O.Parameters.Repetitions;
          case 'RepCurr'; cValue = O.Repetition;
          case 'Trial'; cValue = O.Trial;
          case 'Index'; cValue = 0; %O.Trials(O.Trial).Stimulus.Index;
          case 'TimeCurr'; cValue = 0;
          case 'TimeMax'; cValue = 0;
          otherwise error('Field not assigned!');
        end
        O.Display.Handles.([Fields{iF},'Text']) = uicontrol(Parent,'style','text',...
          'Units','norm','Pos',DCP{DCPInd(iF)},'String',[Fields{iF},' : ',num2str(cValue)],...
          'FontSize',7,'Horiz','left','ForegroundColor',[0,0,0],'BackGroundColor',CG.Colors.GUI.Panel);
      end
      O.Display.Fields = Fields;
      
      % CONTROL BUTTONS
      Buttons = {'RepMinus','Break','RepPlus'}; 
      DCPInd = [1,9,7];
      for iB=1:length(Buttons)
        switch Buttons{iB}
          case 'RepMinus'; Callback = {@O.changeRepetition,-1}; ButtonType = 'PushButton';
          case 'Break'; Callback = @O.terminate;  ButtonType = 'ToggleButton';
          case 'RepPlus'; Callback = {@O.changeRepetition,1};  ButtonType = 'PushButton';
          otherwise error('Field not assigned!');
        end
        O.Display.Handles.([Buttons{iB},'Button']) = uicontrol(Parent,'style',ButtonType,...
          'Units','norm','Pos',DCP{DCPInd(iB)},'String',Buttons{iB},...
          'FontSize',8,'Horiz','left','ForegroundColor',[0,0,0],'BackGroundColor',CG.Colors.GUI.Panel,...
          'Callback',Callback);
      end
        
      set(O.Display.Handles.FIG,'Visible','off');
     
    end
    
    % UPDATE FIGURE WHENEVER NECESSARY
    function updateDisplay(O)
      global CG
      
      % SHOW THE STIMULUS OUTPUT IN THE MODULE FIGURE 
      C_updateNIDisplay(O.HW.IDs.AnalogOut);
      
      % UPDATE THE PARADIGM FIGURE
      Fields = O.Display.Fields;
       for iF=1:length(Fields)
        switch Fields{iF}
          case 'RepMax'; cValue = O.Parameters.Repetitions;
          case 'RepCurr'; cValue = O.Repetition;
          case 'Trial'; cValue = O.Trial;
          case 'Index'; try cValue = O.Trials(O.Trial).Stimulus.Index; catch cValue = 0; end
          case 'TimeCurr'; cValue = 0;
          case 'TimeMax'; cValue = 0;
          otherwise error('Field not assigned!');
        end
        set(O.Display.Handles.([Fields{iF},'Text']),'String',[Fields{iF},' : ',num2str(cValue)]);
      end
      

    end
    
  end
end
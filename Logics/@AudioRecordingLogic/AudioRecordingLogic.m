classdef AudioRecordingLogic < AudioInLogic

  properties
    AvailableStates = {'Starting','Stopping','Ready','Active','Busy','Waiting','TrialActive','Saving'};
  end
    
  methods
    % CONSTRUCTOR
    function O = AudioRecordingLogic(varargin)
      global CG;
      O@AudioInLogic;
      O = setName(O,'AudioRecording');
      P = parsePairs(varargin);
      O.ParametersFull = {...
        'MaxTrialDur',              1,             'Numeric',inf};
      
      ModuleTypes = {'Keyboard'};
      ModuleNames = {'Keyboard'};
      ModulePars = {[],[]};
      O = addModules(O,ModuleTypes,ModuleNames,ModulePars);    
          
      % DISPLAY
      O.assignParameters;
    end
    
    % START THE PARADIGM
    function start(O)
      global CG;
      start@Logic(O) % can be used to use the subclass and the class function
      % INITIALIZE THE SYSTEM
      O.assignParameters;

      % SHOW FIGURES
       for iM=1:length(O.Modules)
        eval(['C_show',O.Modules(iM).Type,'(',n2s(O.Modules(iM).IDType),',1);']);
       end
       
      %INITIALIZE SETUP
      O.prepareDisplay;
      O.showDisplay(1);
      C_startKeyboard(1);
      
      % PREPARE FOR FIRST TRIAL
      O.prepareTrial;
    end
   
    % PREPARE THE NEXT TRIAL
    function cPause = prepareTrial(O)
      global CG;
      % PREPARE FILES AND INCREASE TRIAL
      prepareTrial@Logic(O);           
    end
    
    % START THE TRIAL
    function startTrial(O)
      O.updateDisplay;
      O.prepareAudioIn;
      startTrial@Logic(O);
      O.startAudioIn;
    end
    
    % STOP THE CURRENT TRIAL
    function stopTrial(O)
      O.TrialActive = 0;
      O.changeState('Busy');
      if O.AudioActive  O.stopAudioIn; end
      
      stopTrial@Logic(O);
    end
    
    % EVENT PROCESSING (CORE FUNCTION)
    function processEvent(O,SourceName,Event)
      if ~O.ParadigmActive return; end
      switch SourceName
        case 'Keyboard';
          switch Event.Name
            case 'Key_s';
              if ~O.TrialActive
                O.startTrial;
              else
                O.stopTrial;
                O.prepareTrial;
              end
          end
      end
    end
     
    % PREPARE DISPLAY BEFORE FIRST PLOT
    function prepareDisplay(O)
      if ~isfield(O.Display,'Handles') || ~ishandle(O.Display.Handles.FIG)
        prepareDisplay@Logic(O); 
      else
        figure(O.Display.Handles.FIG); 
      end
      clf;
      DC = axesDivide(1,[1.2,1,1],[0.1,0.2,0.8,0.7],[],1);
      set(O.Display.Handles.FIG,'Visible','off');
    end
    
    % UPDATE FIGURE WHENEVER NECESSARY
    function updateDisplay(O)
      global CG
      % LIMIT UPDATE RATE TO CONTROLINTERVAL
      if get(CG.GUI.Main.Paradigm.ShowButton,'Value') 
      end
    end
  end
end
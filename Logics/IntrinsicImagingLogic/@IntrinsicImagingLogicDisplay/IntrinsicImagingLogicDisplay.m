 % intrinsic optical imaging logic gui

classdef IntrinsicImagingLogicDisplay < handle
  
  properties
    
    % host logic
    
    logic = 0;
    
    % enumeration of the possible logic states, the logic notifies the
    % display about state change, the display updates its state,
    % for example figure title etc
    lsIdle = 0;
    lsStimulation = 1;
    lsIntertrialDelay = 2;
    lsCameraPreview = 3;
    
    logicState = 0;
                 
    % ui elements

    % note: I name ui controls using their class prefix - panel<Name>,
    % button<Name>, check<Name> etc
    
    % redefined in constructor
    figureId = 100501;
    figureDeleted = 0;

    initialFigureWidth = 1100;
    initialFigureHeight = 700;    
    initialFigureX = 100;
    initialFigureY = 100;
    
    defaultBgColor = 0;
    colorMap = 0;
    
    % some ui layout constants    
    panelBorderWidth = 1;            
    trialsPanelHeight = 300;
    leftPanelsWidth = 160;
    leftPanelsMargin = 5;   
    previewListExtraHeight = 100;
    % these are reset in the contructor - taken from logic perameters
    previewFrameWidth = 200;
    previewFrameHeight = 150;
    previewListHeight = 250;
    
    % frame currently viewed
        
    viewedFrameIndex = 1;
    viewedFrameImage = 0;
    viewedFrameAxes = 0;
    viewedFrameLegendAxes = 0;
    
    cameraPreviewImage = 0;
    
    % left side panels & controls
    panelTrials = 0;
    panelTrialsInt = 0;
    panelFrameView = 0;
    panelFrameViewInt = 0;
    
    % trials panel controls
    checkAutoRepeatTrials = 0;
    buttonStartTrial = 0;
    buttonClearData = 0;
    buttonCaptureBackground = 0;
    buttonTestStimulation = 0;
    buttonCameraPreview = 0;
    buttonLoadData = 0;

    % view panel controls
    buttonPrevFrame = 0;
    buttonNextFrame = 0;
    checkDisplayRaw = 0;       
    checkApplyGaussian = 0;            
    checkDivideByMean = 0;       
    checkFlipSides = 0;       
    checkNormalizeColors = 0;    
    checkShowBackground = 0;
    labelViewedRange = 0;    
    editViewedRangeMin = 0;
    editViewedRangeMax = 0;
    buttonUpdateViewedRange = 0;
    viewedRangeMax = 0;
    viewedRangeMin = 0;
    checkViewSelectedTrials = 0;
    editTrialsToView = 0;
    buttonIncreaseViewedTrialNumber = 0;
    buttonDecreaseViewedTrialNumber = 0;
    buttonUpdateTrialsToView = 0;

    textViewedFrameLegend = 0;    
    
    % bottom panel

    panelPreviewList = 0;
    viewedFrameSelectionRect = 0;
    previewListImages = [];
    previewListOuterAxes = [];
    previewListImageAxes = [];
    previewListLegendTexts = [];
    
    % preview list image containers
    
    previewAxes = [];
    
    % preview frames slider
    
    sliderFrames = 0;
    firstPreviewFrameIndex = 0;
    lastPreviewFrameIndex = 0;    
    previewFramesToDisplay = 0;
    
    % calculated data
        
    % relative disparities = dF/F, double
    framesInMeanDelta = []       
    meanDelta = {};
    finalDelta = {}; % with filters applied etc
    
    % raw frames, 16 bit grayscale, double
    meanRaw = {}; 
    framesInMeanRaw = [];

    % cached images to speed up the interface a bit
    cachedImages = containers.Map('KeyType', 'int64', 'ValueType', 'any');
    
    % the last 'displayed' trial
    lastTrialIndex = 0;
    
    % ...
    lastLoadDataParams = 0;
    
    %...
    viewSettingsChangedWhileDoingCameraPreview = 0;
    
    %...
    displayOneMore = 0;
    % should be sorted
    selectedTrials = [];
    
  end
    
  methods

    % constructor
                 
    function O = IntrinsicImagingLogicDisplay(logic, figureId)
      
      global CG;
      
      O.logic = logic;
      O.figureId = figureId;

      O.previewFrameWidth = logic.Parameters.DisplayPreviewListFrameWidth;
      O.previewFrameHeight = logic.Parameters.DisplayPreviewListFrameHeight;
      O.previewListHeight = O.previewFrameHeight + O.previewListExtraHeight;
      
      figureSize = [O.initialFigureX, O.initialFigureY, O.initialFigureWidth, O.initialFigureHeight];
      
      figure(O.figureId); 
      set(O.figureId, 'Position', figureSize, ...
         'NumberTitle', 'off', 'Color', CG.Colors.GUI.Background, ...
         'Toolbar', 'none', 'Menubar', 'none', 'Resize', 'on', 'ResizeFcn', @O.onFigureResized, ...
          'DeleteFcn', @O.onFigureDelete);        
 %        'DeleteFcn', @O.onFigureDelete, 'CloseRequestFcn', @O.onFigureCloseRequest);        
    
       O.setFigureTitle(O.lsIdle);
       
       menu = uimenu('Label', 'Misc');
       uimenu(menu, 'Label', 'Make a movie...', 'Callback',  @O.onMakeMovie);

       %{ 
       doesnt work
       % limit figure minimum size ...
       jFrame = get(handle(O.figureId), 'JavaFrame');
       try
         jProx = jFrame.fFigureClient.getWindow();
       catch
         jProx = jFrame.fHG1Client.getWindow();
       end
       jProx.setMinimumSize(java.awt.Dimension(600, 400));
       %}

      % initialize some constants
      O.defaultBgColor = get(0, 'DefaultUIControlBackgroundColor');          
            
      % color map for disparity values
      % note: red corresponds to _low_ intensity
      r =  [ linspace(1,  0, 256) zeros(1, 256) ];
      g =  zeros(1, 512);
      b =  [ zeros(1, 256)  linspace(0,  1, 256) ];
      
      O.colorMap = [r' g' b'];
      
      O.arrangeUiControls();            
      
    end
    
    % destructor
    function delete(O)
       %if O.figureDeleted ~= 1
       %  close(O.figureId); 
       %end
       % no  need to destroy the figure - controller will delete it
    end    
    
    % just for syntax clarity
    function update(O)
      if O.figureDeleted == 1
        return;
      end
      figure(O.figureId);
      O.calculateDisplayedData();
      O.arrangeUiControls();
    end

    function enableViewControls(O, state)

      val = 'off';
      if state == 1
        val = 'on';
      end      
      set( findall(O.panelFrameViewInt, '-property', 'Enable'), 'Enable', val) 
      
      O.setViewSelectedTrialsDependentControlsEnabledState();
      
    end;
    
    function enableTrialControls(O, state)      
      val = 'off';
      if state == 1
        val = 'on';
      end
      set( findall(O.panelTrialsInt, '-property', 'Enable'), 'Enable', val)      
    end;
    
    function setFigureTitle(O, state)
      figureTitlePrefix = 'Intrinsic optical imaging';
      newTitle = [ figureTitlePrefix ' : ' ] ;
      switch state
        case O.lsIdle
          newTitle = [ newTitle 'ready to start a trial' ];
        case O.lsIntertrialDelay
          newTitle = [ newTitle 'intertrial delay... ' ];
        case O.lsStimulation
          newTitle = [ newTitle 'performing a trial... ' ];          
        case O.lsCameraPreview
          newTitle = [ newTitle 'Camera preview' ];          
      end      
      set(O.figureId, 'name', newTitle, 'NumberTitle', 'off');
    end    
    
    function onLogicStateChanged(O, newState)
      
      % if user closed the window...
      if O.figureDeleted == 1
        return;
      end
      
      % if doing auto repeat...
      autoRepeat = get(O.checkAutoRepeatTrials, 'Value');            

      oldState = O.logicState;
      O.logicState = newState;
      
      switch newState
          
        case O.lsIdle                     
          if oldState ~= O.lsCameraPreview
            O.calculateDisplayedData();
            O.arrangeUiControls();
            O.enableTrialControls(1);           
            set(O.buttonStartTrial, 'String', 'Start trial');            
          else              
            O.enableViewControls(1);
            O.enableTrialControls(1);
            O.arrangeUiControls();
          end
          
        case O.lsStimulation            
          O.enableTrialControls(0);
          if autoRepeat == 1
            set(O.buttonStartTrial, 'Enable', 'on');
          end
          
        case O.lsIntertrialDelay  
          O.calculateDisplayedData();            
          O.arrangeUiControls();
          
        case O.lsCameraPreview
          O.enableViewControls(0);
          O.enableTrialControls(0);
          set(O.buttonCameraPreview, 'Enable', 'on');                      
          set(O.checkFlipSides, 'Enable', 'on');                      
          
      end
      O.setFigureTitle(newState);
    end

    function calculateDisplayedData(O)

        viewSelectedTrials = get(O.checkViewSelectedTrials, 'Value');
      
        % if there were no new trials, then nothing to calculate
        if viewSelectedTrials == 0 && O.lastTrialIndex >= numel(O.logic.trialFrameBins)
          return
        end
      
   
        % clear cached
        remove(O.cachedImages, keys(O.cachedImages));

        applyGaussian = get(O.checkApplyGaussian, 'Value');       
        divideByMean = get(O.checkDivideByMean, 'Value');
    
        totalBins = (O.logic.Parameters.StimulationDuration + O.logic.Parameters.StimulationStopDelay) / ...
            O.logic.Parameters.AveragingInterval;        
                                        
        height = O.logic.frameHeight;
        width = O.logic.frameWidth;
  
        if isempty(O.meanRaw)
        
            O.meanRaw = cell(1, totalBins);
            O.meanRaw(:) = { zeros(height, width, 'double') };
            O.framesInMeanRaw = cell(1, totalBins);
            O.framesInMeanRaw(:) = { double(0) };
                
            O.meanDelta = cell(1, totalBins);
            O.meanDelta(:) = { zeros(height, width, 'double') };   
            O.finalDelta = cell(1, totalBins);
            O.finalDelta(:) = { zeros(height, width, 'double') };               
            O.framesInMeanDelta = cell(1, totalBins);
            O.framesInMeanDelta(:) = { double(0) };
            
        end
    
        if viewSelectedTrials == 0
          trialList =  [(O.lastTrialIndex + 1):numel(O.logic.trialFrameBins)];
        else
          if O.displayOneMore == 1
            trialList = [ O.selectedTrials(end) ];
            O.displayOneMore = 0;
          else
            trialList =  O.selectedTrials;
          end
        end
                
        for k = 1:numel(trialList)
                
            j = trialList(k);
                        
            if divideByMean == 1
              bg = O.logic.trialNormalizedBackgrounds{j};
            else
              bg = O.logic.trialBackgrounds{j};
            end

            gaussian = fspecial('gaussian', 8, 8);
                        
            for bin = 1:min(totalBins, numel(O.logic.trialFrameBins{j}))
                
                frameNum = O.logic.frameNumberInFrameBins{j}{bin};
                
                frameBins = O.logic.trialFrameBins{j};
                
                O.meanRaw{bin} = (O.meanRaw{bin} * double(O.framesInMeanRaw{bin}) + ...
                    frameBins{bin} * double(frameNum)) / double(O.framesInMeanRaw{bin} + frameNum);
                O.framesInMeanRaw{bin} = O.framesInMeanRaw{bin} + frameNum;

                if divideByMean == 1
                  frame = O.logic.trialNormalizedFrameBins{j}{bin};
                else
                  frame = frameBins{bin};
                end
                
                delta = (frame - bg);
                delta = double(delta) ./ ( 1e-6 + double(bg) );

                if applyGaussian == 1
                    delta = imfilter(delta, gaussian, 'replicate');            
                end
                
                O.meanDelta{bin} = (O.meanDelta{bin} * double(O.framesInMeanDelta{bin}) + delta) / ...
                    double(O.framesInMeanDelta{bin} + 1);
                O.framesInMeanDelta{bin} = O.framesInMeanDelta{bin} + 1;

            end                          
            
        end
        
        gaussian = fspecial('gaussian', 8, 8);                
        for i = 1:numel(O.meanDelta)
            if applyGaussian == 1
                O.finalDelta{i} = imfilter(O.meanDelta{i}, gaussian, 'replicate');                        
            else
                O.finalDelta{i} = O.meanDelta{i};                        
            end
        end
        
        O.lastTrialIndex = numel(O.logic.trialFrameBins);
        
    end
    
    % create/rearrange ui contols
    function arrangeUiControls(O)
                  
      figure(O.figureId);
      
      figurePosition = get(O.figureId, 'Position');
      
      figureWidth = figurePosition(3);
      figureHeight = figurePosition(4);

      % some ui layout constants
      viewedFrameBorder = 15;
      
      previewListSliderHeight = 20;
      previewFrameLegendHeight = 30;
      previewListFreeVerticalSpace = O.previewListHeight - O.previewFrameHeight ...
          - previewFrameLegendHeight - previewListSliderHeight;
      previewListSliderBottom = previewListFreeVerticalSpace / 3;
      previewListFramesBottom = previewListSliderBottom ...
          + previewListSliderHeight + previewListFreeVerticalSpace / 3;
            
      previewListBorderWidth = 35;
      previewListSliderBorderWidth = 25;
      defaultPreviewInterframeSpace = 0;      
      previewInterframeSpace = defaultPreviewInterframeSpace;
                  
      % create/reposition preview panel
      
      previewListPanelPosition = [ O.leftPanelsWidth + 2*O.leftPanelsMargin, O.leftPanelsMargin, ...
        figureWidth - O.leftPanelsWidth - 3*O.leftPanelsMargin, O.previewListHeight ];
      
      spaceLeftForPreviewList = previewListPanelPosition(3) - 2*previewListBorderWidth;
      
      O.previewFramesToDisplay = floor((spaceLeftForPreviewList + previewInterframeSpace) / (O.previewFrameWidth + previewInterframeSpace));
      
      if O.previewFramesToDisplay > size(O.meanDelta, 2)
        O.previewFramesToDisplay = size(O.meanDelta, 2);
      end         
      
      if O.panelPreviewList == 0 | ~isvalid(O.panelPreviewList)
        O.panelPreviewList = uipanel('Parent', O.figureId, 'Title', ' Captured sequence ', 'Units', 'Pixels', 'Position', ...
          previewListPanelPosition, 'Visible', 'on');        
      else
          set(O.panelPreviewList, 'Position', previewListPanelPosition);
      end
      
      % create/reposition the preview list slider

      sliderPosition = [ previewListSliderBorderWidth, previewListSliderBottom, ...
        previewListPanelPosition(3) - 2*previewListSliderBorderWidth, previewListSliderHeight];
      sliderMax = size(O.meanDelta, 2) - O.previewFramesToDisplay + 1;
      sliderStep = 1 / max(1, sliderMax - 1);
      
      if O.sliderFrames == 0 || ~isvalid(O.sliderFrames)
        O.sliderFrames = uicontrol(O.panelPreviewList, 'Style', 'slider', 'Min', 1, 'Max', sliderMax, 'Value', 1, ...
          'Position', sliderPosition, 'SliderStep', [sliderStep, sliderStep], ...
          'Callback', @O.onPreviewListSliderChanged);
      else
        currentSliderValue = get(O.sliderFrames, 'Value');
        if currentSliderValue > sliderMax
          set(O.sliderFrames, 'Value', sliderMax);
        end
        set(O.sliderFrames, 'Position', sliderPosition);
        set(O.sliderFrames, 'Max', sliderMax);
        set(O.sliderFrames, 'SliderStep', [sliderStep, sliderStep]);
      end
                     
      if sliderMax == 1
        set(O.sliderFrames, 'Visible', 'off');
      else
        set(O.sliderFrames, 'Visible', 'on');
      end
            
      O.firstPreviewFrameIndex = 1;
      
      if O.sliderFrames ~= 0  && isvalid(O.sliderFrames)      
        O.firstPreviewFrameIndex = round(get(O.sliderFrames, 'Value'));
      end
                        
      % create/rarrange left side panels      
      panelY = O.leftPanelsMargin;    
      panelY = O.arrangeViewPanel(panelY, figureHeight);
      panelY = panelY + O.leftPanelsMargin;      
      O.arrangeTrialsPanel(panelY, figureHeight);      
            
      % create/reposition the viewed image
      % you need fake axis to position an image in matlab...
      viewedFramePosition = [ O.leftPanelsWidth + viewedFrameBorder, O.previewListHeight + viewedFrameBorder, ...
        figureWidth - viewedFrameBorder*2 - O.leftPanelsWidth, figureHeight - viewedFrameBorder*2 - O.previewListHeight];
            
      if O.viewedFrameAxes == 0 || ~isvalid(O.viewedFrameAxes)
        
        O.viewedFrameAxes = axes('Units', 'pixels', 'Position', viewedFramePosition, 'Parent', O.figureId, ...
          'Visible', 'off');
        white = ones(O.logic.frameHeight, O.logic.frameWidth, 'uint8') * 255;
        O.viewedFrameImage = imshow(white);                
                          
      else
        
        set(O.viewedFrameAxes, 'Position', viewedFramePosition); 
                
      end                     

      % display 'viewed frame' content      
      O.updateViewedPicture()
            
      % do interframe space correction      
      if O.previewFramesToDisplay > 1
        spaceLeft = spaceLeftForPreviewList - O.previewFramesToDisplay * O.previewFrameWidth;
        previewInterframeSpace = spaceLeft / (O.previewFramesToDisplay - 1);
        % redistribute space if needed
        if previewInterframeSpace > defaultPreviewInterframeSpace
          total = previewInterframeSpace * (O.previewFramesToDisplay - 1) + previewListBorderWidth*2;
          totalSpaces = O.previewFramesToDisplay - 1 + 2; 
          spaceWidth = total / totalSpaces;
          previewInterframeSpace = spaceWidth;
          previewListBorderWidth = spaceWidth;          
        end
      end

      pos = previewListBorderWidth;

      % delete current container axes
      arrayfun(@cla, O.previewAxes);
      O.previewAxes = [];
      O.previewListOuterAxes = [];
      O.previewListImageAxes = [];
      O.previewListLegendTexts = [];
      O.previewListImages = [];
      
      if O.previewFramesToDisplay > size(O.meanDelta, 2)
        O.previewFramesToDisplay = size(O.meanDelta, 2);
      end
      
      O.lastPreviewFrameIndex = O.firstPreviewFrameIndex + O.previewFramesToDisplay - 1;
      
      for i = O.firstPreviewFrameIndex:O.lastPreviewFrameIndex
        
        % todo: should use less magic constants! move such values to a named
        % constants
        
        % you need fake axis to position an image in matlab...       
        outerAxesPosition = [pos - 3, previewListFramesBottom - 5, O.previewFrameWidth + 6, O.previewFrameHeight + 30];
        outerAxes = axes('Units', 'pixels', 'Position', outerAxesPosition, 'Parent', O.panelPreviewList, 'Visible', 'off', ...
          'XLim', [1, outerAxesPosition(3)], 'YLim', [1, outerAxesPosition(4)] );
        set(O.figureId, 'CurrentAxes', outerAxes);
      
        O.previewListOuterAxes = [ O.previewListOuterAxes, outerAxes ];
                
        t = text(10, O.previewFrameHeight + 20, '', 'Parent', outerAxes, ...
                 'BackgroundColor', O.defaultBgColor, 'Visible', 'on');
               
        O.previewListLegendTexts = [O.previewListLegendTexts, t];                                     
        
        O.previewAxes = [O.previewAxes, outerAxes];        
        
        imageAxes = axes('Units', 'pixels', 'Position', [pos, previewListFramesBottom, O.previewFrameWidth, O.previewFrameHeight], ...
          'Parent', O.panelPreviewList,  'Visible', 'off');

        O.previewListImageAxes = [O.previewListImageAxes, imageAxes];
        
        set(O.figureId, 'CurrentAxes', imageAxes);
        
        O.previewAxes = [O.previewAxes, imageAxes];                
                
        white = ones(O.logic.frameHeight, O.logic.frameWidth, 'uint8') * 255;
        
        axis off;
        imageHandle = imshow(white);
        axis image; % imagesc doesnt keep aspect ratio
        set(imageAxes, 'Visible', 'off'); % imagesc makes axes visible again!
        set(outerAxes, 'Visible', 'off'); % imagesc makes axes visible again!
        
        set(imageHandle, 'ButtonDownFcn', {@O.onPreviewFrameClicked, i - O.firstPreviewFrameIndex});

        O.previewListImages = [O.previewListImages, imageHandle];                
        
        pos = pos + O.previewFrameWidth + previewInterframeSpace;
                
      end
      
      O.updatePreviewList();      
      O.updateSelectionRect();
      
    end

    % create/update view panel controls    
    function newPanelY = arrangeViewPanel(O, panelY, figureHeight)

      % manage frame view panel
            
      panelY = O.leftPanelsMargin;
            
      frameViewPanelPosition = [ O.leftPanelsMargin, panelY, O.leftPanelsWidth, ...
          figureHeight - O.trialsPanelHeight - 3*O.leftPanelsMargin];
      
      % we need this 'Internal' panel to keep controls positions on resize        
      
      internalPanelHeight = 380;
      internalPanelWidth = frameViewPanelPosition(3) - O.panelBorderWidth * 2 - 1;
      
      frameViewIntPanelPosition = [ 0, frameViewPanelPosition(4) - internalPanelHeight - 2*O.panelBorderWidth, ...
        internalPanelWidth, internalPanelHeight ];
            
      if O.panelFrameView == 0 | ~isvalid(O.panelFrameView)
        
        O.panelFrameView = uipanel('Parent', O.figureId, 'Title', '  View  ', 'Units', 'Pixels', 'Position', ...
          frameViewPanelPosition, 'Visible', 'on');
        O.panelFrameViewInt = uipanel('Parent', O.panelFrameView, 'Units', 'Pixels', 'Position', frameViewIntPanelPosition, ...
          'BorderType', 'none', 'Visible', 'on');        

        % create buttons                
                
        buttonHeight = 25;
        editHeight = 20;
        checkHeight = 25;
        legendHeight = 30;
        labelHeight = 13;
        buttonHorzMargin = 5;
        buttonVertMargin = 5;
        buttonWidth = O.leftPanelsWidth - 2*buttonHorzMargin - 2*O.panelBorderWidth;
                        
        y = 5;
        
        O.buttonPrevFrame = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'pushbutton', 'String', '<< View prev <<', ...
          'Position', [buttonHorzMargin, y, buttonWidth, buttonHeight], ...
          'Callback', @O.onViewPrevPressed);
        
        y = y + buttonHeight + buttonVertMargin;
        
        O.buttonNextFrame = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'pushbutton', 'String', '>> View next >>', ...
          'Position', [buttonHorzMargin, y, buttonWidth, buttonHeight], ...
          'Callback', @O.onViewNextPressed);

        y = y + buttonHeight + buttonVertMargin;
        
        O.textViewedFrameLegend = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'text', 'String', 'Stub', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, legendHeight]);                

        y = y + legendHeight + 2*buttonVertMargin;
        
        panelSeparator = uipanel('Parent', O.panelFrameViewInt, 'Units', 'Pixels', 'BorderType', 'none', ...          
          'BackgroundColor', 'black', 'Position', [buttonHorzMargin, y, buttonWidth, 1 ] );
        
        y = y + 2*buttonVertMargin;                                 

        incDecViewedTrialsButtonWidth = 20;
        trialsToViewEditWidth = 44;

        viewedTrialsHorzSpace = 4;
        
        x = 2*buttonHorzMargin;
                        
        O.buttonDecreaseViewedTrialNumber = uicontrol(O.panelFrameViewInt, 'Style', 'pushbutton', 'String', '<', ...
          'Enable', 'off', 'Position', [ x, y, incDecViewedTrialsButtonWidth, editHeight ], 'Callback', @O.onSelectPrevTrialToView);
        
        x = x + incDecViewedTrialsButtonWidth + viewedTrialsHorzSpace;
        
        O.editTrialsToView = uicontrol(O.panelFrameViewInt, 'Style', 'edit', 'String', '1', ...
          'Enable', 'on', 'Position', [ x, y, trialsToViewEditWidth, editHeight ], 'Callback', @O.onUpdateSelectedTrials);
        
        x = x + trialsToViewEditWidth + viewedTrialsHorzSpace;

        O.buttonIncreaseViewedTrialNumber = uicontrol(O.panelFrameViewInt, 'Style', 'pushbutton', 'String', '>', ...
          'Enable', 'off', 'Position', [ x, y, incDecViewedTrialsButtonWidth, editHeight ], 'Callback', @O.onSelectNextTrialToView);
        
        x = x + incDecViewedTrialsButtonWidth + viewedTrialsHorzSpace;
        
        updateViewedTrialsButtonPosition = [ x, y, O.leftPanelsWidth - x - 2*buttonHorzMargin - O.panelBorderWidth, editHeight ];
        
        O.buttonUpdateTrialsToView = uicontrol(O.panelFrameViewInt, 'Style', 'pushbutton', 'String', 'Update', ...
          'Enable', 'off', 'Position', updateViewedTrialsButtonPosition, 'Callback', @O.onUpdateSelectedTrials);
                 
        y = y + editHeight;        

        O.checkViewSelectedTrials = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'checkbox', 'String', 'View selected trials:', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, checkHeight], ...
          'Callback', @O.onViewSelectedTrialsClicked);                

        y = y + checkHeight;        
        
        O.checkFlipSides = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'checkbox', 'String', 'Flip sides', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, checkHeight], ...
           'Value', 1, 'Callback', @O.onFlipSidesClicked);                
        
        y = y + checkHeight;        
        
        O.checkDivideByMean = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'checkbox', 'String', 'Divide by mean', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, checkHeight], ...
           'Value', 1, 'Callback', @O.onDivideByMeanClicked);                
        
        y = y + checkHeight;        
        
        O.checkApplyGaussian = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'checkbox', 'String', 'Apply gaussian', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, checkHeight], ...
          'Value', 1, 'Callback', @O.onApplyGaussianClicked);                

        y = y + checkHeight + buttonVertMargin;        
      
        viewedRangeEditWidth = 45;
        viewedRangeEditSpace = 4;
        
        x = 2*buttonHorzMargin;

        O.viewedRangeMax = O.logic.Parameters.DisplayViewedRangeMax;
        O.viewedRangeMin = O.logic.Parameters.DisplayViewedRangeMin;
        
        viewedRangeMinPosition = [ x, y, viewedRangeEditWidth, editHeight ];
        
        O.editViewedRangeMin = uicontrol(O.panelFrameViewInt, 'Style', 'edit', 'String', num2str(O.viewedRangeMin), ...
          'Enable', 'on', 'Position', viewedRangeMinPosition, 'Callback', @O.onViewedRangeChanged);
        
        x = x + viewedRangeEditWidth + viewedRangeEditSpace;
        
        viewedRangeMaxPosition = [ x, y, viewedRangeEditWidth, editHeight ];
        
        O.editViewedRangeMax = uicontrol(O.panelFrameViewInt, 'Style', 'edit', 'String', num2str(O.viewedRangeMax), ...
          'Enable', 'on', 'Position', viewedRangeMaxPosition, 'Callback', @O.onViewedRangeChanged);
        
        x = x + viewedRangeEditWidth + viewedRangeEditSpace;

        buttonUpdateViewedRangePosition = [ x, y, O.leftPanelsWidth - x - 2*buttonHorzMargin - O.panelBorderWidth, editHeight ];
        
        O.buttonUpdateViewedRange = uicontrol(O.panelFrameViewInt, 'Style', 'pushbutton', 'String', 'Update', ...
          'Enable', 'on', 'Position', buttonUpdateViewedRangePosition, 'Callback', @O.onViewedRangeChanged);
      
        y = y + editHeight + buttonVertMargin;                

        O.labelViewedRange = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'text', 'String', 'Viewed range', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, labelHeight], ...
          'Enable', 'on', 'HorizontalAlignment', 'left');                

        y = y + labelHeight + buttonVertMargin;                
      
        O.checkNormalizeColors = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'checkbox', 'String', 'Auto normalize colors', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, checkHeight], ...
          'Value', 0, 'Callback', @O.onNormalizeColorsClicked);                

        y = y + checkHeight;        
      
        O.checkShowBackground = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'checkbox', 'String', 'Show background', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, checkHeight], ...
          'Value', 1, 'Callback', @O.onShowBackgroundClicked);                

        y = y + checkHeight;        

        O.checkDisplayRaw = uicontrol('Parent', O.panelFrameViewInt, 'Style', 'checkbox', 'String', 'Raw frames', ...
          'Position', [2*buttonHorzMargin, y, buttonWidth - buttonHorzMargin, checkHeight], ...
          'Callback', @O.onDisplayRawClicked);                        
      
      else
        
        set(O.panelFrameView, 'Position', frameViewPanelPosition);
        set(O.panelFrameViewInt, 'Position', frameViewIntPanelPosition);
        
      end
      
      O.updateViewedFrameLegend();
            
      newPanelY = panelY + frameViewPanelPosition(4);
        
    end

    % create/update trials panel controls
    function newPanelY = arrangeTrialsPanel(O, panelY, figureHeight)
             
      trialsPanelPosition = [ O.leftPanelsMargin, panelY, O.leftPanelsWidth, O.trialsPanelHeight];

      % we need this 'Internal' panel to keep controls positions on resize        
      
      internalPanelHeight = trialsPanelPosition(4) - O.panelBorderWidth * 2;
      internalPanelWidth = trialsPanelPosition(3) - O.panelBorderWidth * 2 - 1;
      
      trialsIntPanelPosition = [ 0, O.panelBorderWidth, ...
        internalPanelWidth, internalPanelHeight ];
      
      if O.panelTrials == 0 | ~isvalid(O.panelTrials)

        O.panelTrials = uipanel('Parent', O.figureId, 'Title', ' Trials ', 'Units', 'Pixels', ...
          'Position', trialsPanelPosition, 'Visible', 'on');                
        O.panelTrialsInt = uipanel('Parent', O.panelTrials, 'Units', 'Pixels', 'Position', trialsIntPanelPosition, ...
          'BorderType', 'none', 'Visible', 'on');        
        
        buttonHeight = 30;
        bigButtonHeight = 38;
        checkHeight = 30;
        buttonHorzMargin = 5;
        buttonVertMargin = 5;        
        buttonWidth = O.leftPanelsWidth - 2*buttonHorzMargin - 2*O.panelBorderWidth;
        
        controlY = 10;
        
        O.buttonStartTrial = uicontrol('Parent', O.panelTrialsInt, 'Style', 'pushbutton', 'String', 'Start trial', ...
          'Position', [buttonHorzMargin, controlY, buttonWidth, buttonHeight], ...
          'Callback', @O.onStartTrialPressed);        
        
        controlY = controlY + buttonHeight + buttonVertMargin;

        O.buttonClearData = uicontrol('Parent', O.panelTrialsInt, 'Style', 'pushbutton', 'String', 'Clear data', ...
          'Position', [buttonHorzMargin, controlY, buttonWidth, buttonHeight], ...
          'Callback', @O.onClearDataPressed);        
        
        controlY = controlY + buttonHeight + buttonVertMargin;
        
        O.checkAutoRepeatTrials = uicontrol('Parent', O.panelTrialsInt, 'Style', 'checkbox', 'String', 'Auto repeat trials', ...
          'Position', [2*buttonHorzMargin, controlY, buttonWidth, checkHeight], 'Value', 1);                

        controlY = controlY + checkHeight + buttonVertMargin;

        panelSeparator = uipanel('Parent', O.panelTrialsInt, 'Units', 'Pixels', 'BorderType', 'none', ...          
          'BackgroundColor', 'black', 'Position', [buttonHorzMargin, controlY, buttonWidth, 1 ] );
        
        controlY = controlY + 2*buttonVertMargin + 1;
        
        O.buttonCaptureBackground = uicontrol('Parent', O.panelTrialsInt, 'Style', 'pushbutton', 'String', 'Capture background', ...
          'Position', [buttonHorzMargin, controlY, buttonWidth, buttonHeight], ...
          'Callback', @O.onCaptureBackgroundPressed);
        
        controlY = controlY + buttonHeight + 2*buttonVertMargin;
        
        panelSeparator = uipanel('Parent', O.panelTrialsInt, 'Units', 'Pixels', 'BorderType', 'none', ...          
          'BackgroundColor', 'black', 'Position', [buttonHorzMargin, controlY, buttonWidth, 1 ] );

        controlY = controlY + 2*buttonVertMargin + 1;
        
        O.buttonTestStimulation = uicontrol('Parent', O.panelTrialsInt, 'Style', 'pushbutton', 'String', 'Stimulation test', ...
          'Position', [buttonHorzMargin, controlY, buttonWidth, buttonHeight], ...
          'Callback', @O.onTestStimulationPressed);
        
        controlY = controlY + buttonHeight + buttonVertMargin;

        O.buttonCameraPreview = uicontrol('Parent', O.panelTrialsInt, 'Style', 'pushbutton', 'String', 'Camera preview', ...
          'Position', [buttonHorzMargin, controlY, buttonWidth, buttonHeight], ...
          'Callback', @O.onCameraPreviewPressed);

        controlY = controlY + buttonHeight + buttonVertMargin;

        O.buttonLoadData = uicontrol('Parent', O.panelTrialsInt, 'Style', 'pushbutton', 'String', 'Load data', ...
          'Position', [buttonHorzMargin, controlY, buttonWidth, buttonHeight], ...
          'Callback', @O.onLoadDataPressed);
        
      else
      
        set(O.panelTrials, 'Position', trialsPanelPosition);
        set(O.panelTrialsInt, 'Position', trialsIntPanelPosition); 
        
      end
      
      newPanelY = panelY + trialsPanelPosition(4);
        
    end

    function updateViewedFrameLegend(O)
      time = (O.viewedFrameIndex - 1) * O.logic.Parameters.AveragingInterval;              
      legendString = { 'Viewed image time: ',  [num2str(time, '%.2f') ' s'] };
      set(O.textViewedFrameLegend, 'String', legendString);
    end
    
    function updateViewedPicture(O)      
      % switch focus to this axes
      set(O.figureId, 'CurrentAxes', O.viewedFrameAxes);

      if O.logicState ~= O.lsCameraPreview
        if ~isempty(O.meanDelta)
          image = O.composePicture(O.viewedFrameIndex);
          set(O.viewedFrameImage, 'CData', image);
        else
          showBackground = get(O.checkShowBackground, 'Value');
          if ~isscalar(O.logic.backgroundPicture) && showBackground == 1
            image = uint8(O.logic.backgroundPicture / 256);
          else       
            image = ones(O.logic.frameHeight, O.logic.frameWidth, 'uint8') * 255;
          end
          set(O.viewedFrameImage, 'CData', image);
        end              
      end      
    end

    function updatePreviewList(O)     
      
      O.firstPreviewFrameIndex = round(get(O.sliderFrames, 'Value'));

      O.lastPreviewFrameIndex = O.firstPreviewFrameIndex + O.previewFramesToDisplay - 1;
      
      for i = O.firstPreviewFrameIndex:O.lastPreviewFrameIndex
                
        j = i - O.firstPreviewFrameIndex + 1;
        
        image = O.previewListImages(j);
        data = O.composePicture(i);
        set(image, 'CData', data);

        t = O.previewListLegendTexts(j);    
        
        time = (i - 1) * O.logic.Parameters.AveragingInterval;
        
        if time < O.logic.Parameters.StimulationDuration
          set(t, 'Color', [0.5, 0, 0]);
        else
          set(t, 'Color', [0, 0, 0]);
        end 
        
        legendString = ['t = ' num2str(time, '%.2f') ' s'];        
        t.String = legendString;
        
      end
      
    end
    
    function updateSelectionRect(O)
      
      if O.viewedFrameSelectionRect ~= 0
        delete(O.viewedFrameSelectionRect)
        O.viewedFrameSelectionRect = 0;
      end
      
      if ~isempty(O.meanDelta) && (O.viewedFrameIndex >= O.firstPreviewFrameIndex) && (O.viewedFrameIndex <= O.lastPreviewFrameIndex)
          a = O.previewListOuterAxes(O.viewedFrameIndex - O.firstPreviewFrameIndex + 1);
          ap = get(a, 'Position');
          O.viewedFrameSelectionRect = rectangle('Position', [1, 1, ap(3) - 1, O.previewFrameHeight + 15], 'Parent', a, ...
            'LineWidth', 1, 'LineStyle', '--');          
          uistack(O.viewedFrameSelectionRect, 'bottom');
      end      
      
    end
  
    function image = composePictureNoCache(O, sourceIndex)

      displayRaw = get(O.checkDisplayRaw, 'Value');

      if displayRaw == 1                
        
        image = uint8(O.meanRaw{sourceIndex} / 256.);
        
      else

        showBackground = get(O.checkShowBackground, 'Value');
        
        if (~isscalar(O.logic.backgroundPicture)) && (showBackground == 1)
          image = uint8(O.logic.backgroundPicture / 256);
        else
          image = zeros(O.logic.frameHeight, O.logic.frameWidth, 'uint8');
        end
        
        normalizeColors = get(O.checkNormalizeColors, 'Value');
        
        delta = O.finalDelta{sourceIndex};
        
        if normalizeColors == 0
          
          mx = max(O.viewedRangeMin, O.viewedRangeMax);
          mn = min(O.viewedRangeMin, O.viewedRangeMax);
          
          if mn == mx
            deltaTransparency = delta;
            if mn >= 0
              val = 1;
            else
              val = -1;
            end
            deltaTransparency(deltaTransparency == mn) = val;
          else
            
            if mn >= 0
              amp = 1 / (mx - mn);
              deltaTransparency = amp * (delta - mn);
              deltaTransparency(deltaTransparency < 0) = 0;
            elseif mx <= 0
              amp = 1 / (mx - mn);
              deltaTransparency = amp * (delta - mx);
              deltaTransparency(deltaTransparency > 0) = 0;
            else
              deltaTransparency = delta;
              negAmp = 1 / abs(mn);
              posAmp = 1 / mx;
              deltaTransparency(deltaTransparency < 0) = negAmp * deltaTransparency(deltaTransparency < 0);
              deltaTransparency(deltaTransparency > 0) = posAmp * deltaTransparency(deltaTransparency > 0);
            end
          end
          
        else
          amp = 0.5 / abs(mean(mean(abs(delta))));
          deltaTransparency = amp * delta;
        end
        
        deltaTransparency(deltaTransparency > 1.) = 1.;
        deltaTransparency(deltaTransparency < -1.) = -1.;
        
        deltaColors = int64(deltaTransparency*256 + 256);
        deltaColors(deltaColors < 1) = 1;
        deltaColors(deltaColors > 512) = 512;
        
        deltaColors = ind2rgb(deltaColors, O.colorMap);
        
        deltaTransparency = abs(deltaTransparency);
        
        rgbImage = cat(3, image, image, image);
        deltaTransparency = cat(3, deltaTransparency, deltaTransparency, deltaTransparency);
        
        image = (double(rgbImage) ./ 255) .* (1 - deltaTransparency) + deltaColors .* deltaTransparency;
        
      end
      
      flipSides = get(O.checkFlipSides, 'Value');
      
      if flipSides == 1
        image = rot90(image, 2);
      end
           
    end
    
    function image = composePicture(O, sourceIndex)

      if O.cachedImages.isKey(sourceIndex)
        image = O.cachedImages(sourceIndex);
        return
      end
      
      image = O.composePictureNoCache(sourceIndex);
      
      O.cachedImages(sourceIndex) = image;
      
    end 
        
    % ui event handlers             
    function onFigureDelete(O, figureObject, ~)
       
       % todo: we have to stop the paradigm on window close... 
        O.figureDeleted = 1;
        
    end
    
    % don't allow user to close the window - just hide it? ( fixme: it
    % confuses the controller...)
    %{
    function onFigureCloseRequest(O, figureObject, ~)
       
       %set(O.figureId, 'Visible', 'off');
       delete(O.figureId);
        
    end
    %}
    
    function onFigureResized(O, figureObject, ~)
                  
      O.arrangeUiControls();
      
    end

    function onPreviewListSliderChanged(O, sliderObject, ~)
      
      O.updatePreviewList();      
      O.updateSelectionRect();      
      
    end
    
    function onViewedRangeChanged(O, sliderObject, ~)
      
      % clear cached
      remove(O.cachedImages, keys(O.cachedImages));
      
      rangeMin = get(O.editViewedRangeMin, 'String');
      rangeMax = get(O.editViewedRangeMax, 'String');

      O.viewedRangeMin = str2double(rangeMin);
      if isnan(O.viewedRangeMin)
        O.viewedRangeMin = O.logic.Parameters.DisplayViewedRangeMin;
        warndlg('Viewed range minimum must be numerical', 'Warning', 'modal');
        set(O.editViewedRangeMin, 'String', O.logic.Parameters.DisplayViewedRangeMin);
      end
      
      O.viewedRangeMax = str2double(rangeMax);
      if isnan(O.viewedRangeMax)
        O.viewedRangeMax = O.logic.Parameters.DisplayViewedRangeMax;
        warndlg('Viewed range maximum must be numerical', 'Warning', 'modal');
        set(O.editViewedRangeMax, 'String', O.logic.Parameters.DisplayViewedRangeMax);
      end
      
      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end
        
    % function onPreviewFrameClicked(O, eventdata, previewFrameIndex)
    function onPreviewFrameClicked(O, imageObject, ~, previewFrameIndex)

      O.viewedFrameIndex = O.firstPreviewFrameIndex + previewFrameIndex;
      O.updateViewedPicture();
      O.updateViewedFrameLegend();
      O.updateSelectionRect();
      
    end

    % 'trials' controls handlers

    function onLoadDataPressed(O, buttonObject, ~)
      IntrinsicImagingLogicLoadDataDialog(O, O.lastLoadDataParams);
    end
    
    % dialog callback
    function onLoadData(O, params)
      h = waitbar(0, 'Please wait while loading data...');
      O.lastLoadDataParams = params;
      O.logic.loadData(params, h);      
    end
        
    function updatePreviewFunction(O, object, event, hImage)
      % display image data.
      
      %O.viewedFrameImage.CData = event.Data;
      
      flipSides = get(O.checkFlipSides, 'Value');
      
      if flipSides == 1
        event.Data = rot90(event.Data, 2);
      end
      
      hImage.CData = event.Data;
      drawnow; % drawnow here is nesessary to keep the interface responsive
      
    end
    
    function onCameraPreviewPressed(O, buttonObject, ~)
      
      global CG;
    
      if O.logicState ~= O.lsCameraPreview

        set(O.buttonCameraPreview, 'String', 'Stop camera preview');                      
    
        O.viewSettingsChangedWhileDoingCameraPreview = 0;
        
        videoObj = CG.Sessions.Video(O.logic.HW.IDs.Camera).S;      
      
        % caxis(O.viewedFrameAxes, [0,65536]); doesn't help to speed things up
        set(O.figureId, 'CurrentAxes', O.viewedFrameAxes);

        % don't allow matlab delete underlying image
        hold on;
        axis off;
      
        res = videoObj.VideoResolution; 
        bands = videoObj.NumberOfBands;
        img = image( zeros(res(2), res(1), bands) );
        %img = imagesc( zeros(res(2), res(1), bands) );
        
        % set up the update preview window function.
        setappdata(img, 'UpdatePreviewWindowFcn', @O.updatePreviewFunction);
        
        O.cameraPreviewImage = preview(videoObj, img);
      
        axis image;
        
        O.onLogicStateChanged(O.lsCameraPreview);
        
      else
          
        videoObj = CG.Sessions.Video(O.logic.HW.IDs.Camera).S;
        closepreview(videoObj);
        delete(O.cameraPreviewImage);
        set(O.buttonCameraPreview, 'String', 'Camera preview');                      

        if O.viewSettingsChangedWhileDoingCameraPreview ~= 0
          % clear cached
          remove(O.cachedImages, keys(O.cachedImages));
          O.updatePreviewList();        
        end
        
        O.updateViewedPicture();
        O.onLogicStateChanged(O.lsIdle);
          
      end
      
    end
        
    function onTestStimulationPressed(O, buttonObject, ~)  
      set(O.buttonTestStimulation,'Enable','off')
      O.logic.testStimulation();
      set(O.buttonTestStimulation,'Enable','on')
      end
    
    function onCaptureBackgroundPressed(O, buttonObject, ~)
      
      O.logic.captureBackground();

      % clear cached
      remove(O.cachedImages, keys(O.cachedImages));

      O.updatePreviewList();
      O.updateViewedPicture();
      
    end
    
    function onStartTrialPressed(O, buttonObject, ~)

      if O.logicState ~= O.lsIdle
        O.logic.abortIOITrial();
        return;
      end
        
      autoRepeat = get(O.checkAutoRepeatTrials, 'Value');      
      
      if autoRepeat == 1
        set(O.buttonStartTrial, 'String', 'Stop trials');
      else
        set(O.buttonStartTrial, 'String', 'Abort trial');
      end
       
      O.logic.startIOITrial(autoRepeat);
      
    end

    function onClearDataPressed(O, buttonObject, ~)
      
      % let the engine to clear all it  needs
      O.logic.clearData();      
      % do gui state reset
      O.viewedFrameIndex = 1;
      O.lastTrialIndex = 0;      
      O.framesInMeanDelta = [];       
      O.meanDelta = {};
      O.finalDelta = {};
      O.meanRaw = {}; 
      O.framesInMeanRaw = [];
      % clear cached
      remove(O.cachedImages, keys(O.cachedImages));
      % rebuild the gui
      O.arrangeUiControls();
      
    end
    
    % 'view' controls handlers

    function onDisplayRawClicked(O, buttonObject, ~)

      % clear cached
      remove(O.cachedImages, keys(O.cachedImages));

      state = get(O.checkDisplayRaw, 'Value');
        
      val = 'off';
      if state == 0
        val = 'on';
      end
      
      set(O.checkNormalizeColors, 'Enable', val);         
      set(O.labelViewedRange, 'Enable', val);
      set(O.editViewedRangeMin, 'Enable', val);
      set(O.editViewedRangeMax, 'Enable', val);
      set(O.buttonUpdateViewedRange, 'Enable', val);
      set(O.checkShowBackground, 'Enable', val);
      set(O.checkDivideByMean, 'Enable', val);
      set(O.checkApplyGaussian, 'Enable', val);
        
      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end

    function onDivideByMeanClicked(O, buttonObject, ~)
     
      O.lastTrialIndex = 0;      
      O.meanRaw = {};
      O.calculateDisplayedData();
      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end

    function onFlipSidesClicked(O, buttonObject, ~)

      if O.logicState == O.lsCameraPreview
        O.viewSettingsChangedWhileDoingCameraPreview = bitxor(O.viewSettingsChangedWhileDoingCameraPreview, 1);
      else
        % clear cached
        remove(O.cachedImages, keys(O.cachedImages));
        O.updatePreviewList();
        O.updateViewedPicture();
      end
      
    end
    
    function parseSelectedTrials(O)
      O.selectedTrials = eval(['[' O.editTrialsToView.String ']']);      
      O.selectedTrials = O.selectedTrials(O.selectedTrials <= numel(O.logic.trialFrameBins));
      O.selectedTrials = sort(O.selectedTrials);
    end

    % why the name is so long ? nobody knows...
    function setViewSelectedTrialsDependentControlsEnabledState(O)

      state = get(O.checkViewSelectedTrials, 'Value');
        
      val = 'off';
      if state == 1
        val = 'on';
        O.parseSelectedTrials();
      end

      set(O.buttonIncreaseViewedTrialNumber, 'Enable', val);         
      set(O.buttonDecreaseViewedTrialNumber, 'Enable', val);
      set(O.buttonUpdateTrialsToView, 'Enable', val);

    end
    
    function onViewSelectedTrialsClicked(O, ~, ~)

      O.setViewSelectedTrialsDependentControlsEnabledState();
      
      O.lastTrialIndex = 0;      
      O.meanRaw = {};
      O.calculateDisplayedData();
      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end

    function onUpdateSelectedTrials(O, buttonObject, ~)
    
      O.parseSelectedTrials();      
      
      O.lastTrialIndex = 0;      
      O.meanRaw = {};
      O.calculateDisplayedData();
      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end

    % compact string representation of short lists
    function r = list2str(O, x)
      x = sort(x);
      r = '';
      if numel(x) == 0
        return;
      end
      start = 0;
      last = 0;
      for i = 1:numel(x)
        if i == 1          
          start = x(i);          
          r = num2str(x(i));
        elseif x(i) ~= last + 1            
            if(last > start)
              r = [r ':' num2str(last)];
            end            
            r = [r ',' num2str(x(i))];
            start = x(i);
        end
        last = x(i);
      end
      if start ~= x(end)
        r = [r ':' num2str(x(end))];
      end
    end

    
    function onSelectNextTrialToView(O, buttonObject, ~)

      O.parseSelectedTrials();
      
      updated = 0;
      clearData = 0;
      
      if max(O.selectedTrials) < numel(O.logic.trialFrameBins)
        if numel(O.selectedTrials) == 1
          O.selectedTrials(1) = O.selectedTrials(1) + 1;
          clearData = 1;
        else
          O.selectedTrials = [O.selectedTrials (max(O.selectedTrials) + 1)];
          O.displayOneMore = 1;
        end
        O.editTrialsToView.String = O.list2str(O.selectedTrials);
        updated = 1;
      end
      
      if updated == 1
        
        if clearData == 1
          O.lastTrialIndex = 0;
          O.meanRaw = {};
        end
      
        O.calculateDisplayedData();
        O.updatePreviewList();
        O.updateViewedPicture();
        
      end
        
    end

    function onSelectPrevTrialToView(O, buttonObject, ~)

      O.parseSelectedTrials();

      updated = 0;
      
      if max(O.selectedTrials) > 1
        if numel(O.selectedTrials) == 1
          O.selectedTrials(1) = O.selectedTrials(1) - 1;
        else
          O.selectedTrials = O.selectedTrials(1:end - 1);
        end
        O.editTrialsToView.String = O.list2str(O.selectedTrials);
        updated = 1;
      end

      if updated == 1
      
        O.lastTrialIndex = 0;
        O.meanRaw = {};
        O.calculateDisplayedData();
        O.updatePreviewList();
        O.updateViewedPicture();
        
      end
      
    end
    
    function onShowBackgroundClicked(O, buttonObject, ~)
      
      % clear cached
      remove(O.cachedImages, keys(O.cachedImages));

      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end
    
    function onNormalizeColorsClicked(O, ~, ~)
    
      % clear cached
      remove(O.cachedImages, keys(O.cachedImages));

      state = get(O.checkNormalizeColors, 'Value');
        
      val = 'off';
      if state == 0
        val = 'on';
      end
      
      set(O.labelViewedRange, 'Enable', val);
      set(O.editViewedRangeMin, 'Enable', val);
      set(O.editViewedRangeMax, 'Enable', val);
      set(O.buttonUpdateViewedRange, 'Enable', val);
              
      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end
    
    % todo: move all the uniform call sequence to separate methods like
    % "recalculateDisplayedDataAndUpdate" and so on
    
    function onApplyGaussianClicked(O, buttonObject, ~)
      
      O.lastTrialIndex = 0;      
      O.meanRaw = {};
      O.calculateDisplayedData();
      O.updatePreviewList();      
      O.updateViewedPicture();
      
    end
    
    function onViewNextPressed(O, buttonObject, ~)
      
      if O.viewedFrameIndex >= numel(O.meanDelta)
        return;
      end
      O.viewedFrameIndex = O.viewedFrameIndex + 1;
      O.updateViewedPicture();
      O.updateViewedFrameLegend();
      O.updateSelectionRect();
      
    end
    
    function onViewPrevPressed(O, buttonObject, ~)
      
      if O.viewedFrameIndex == 1
        return;
      end
      O.viewedFrameIndex = O.viewedFrameIndex - 1;
      O.updateViewedPicture();
      O.updateViewedFrameLegend();
      O.updateSelectionRect();
      
    end

    function onMakeMovie(O, ~,  ~)
      IntrinsicImagingLogicMovieMaker(O);
    end
    
  end % methods

end
% the document: https://docs.google.com/document/d/11z2v4PLL0nJ0xiqnd_iDK7OQn6wHZsdVHTpaRDc-e6s/edit?usp=sharing

classdef IntrinsicImagingLogic < AnalogIOLogic

  properties
    
    AvailableStates = { 'Starting', 'Stopping', 'Ready', 'Active', 'Busy', 'Waiting', 'TrialActive', 'Saving' };
    
    VideoAvailable = 1;
    VideoReady = 0;
    VideoActive = 0;
    
    % acquired data    
    
    % background (vascularity) picture, 2D matrix, 16 bit grayscale
    backgroundPicture = 0;
    
    % averaging data and the results 

    % trials data, 16 bit images, uint32, [trials]    
    frameWidth = 0;
    frameHeight = 0; 
    trialBackgrounds = {}; % {trial number} -> trial background 2D array, int16 grayscale
    trialNormalizedBackgrounds = {};
    trialFrameBins = {}; % {trial number} -> (averaging bin -> averaged frame 2D array, int16 grayscale)
    trialNormalizedFrameBins = {};
    frameNumberInFrameBins = {}; % how many frames were averaged in each bin for each trial
                                 % trial number -> array of frame number in
                                 % each bin
    
    % daq sessions
    triggerSession = 0;
    stimulatorSession = 0;
    
    % trial control timer    
    trialTimer = 0;
    intertrialTimer = 0;

    autoRepeatTrials = 0;

    % = 1 if performing trial abort
    aborting = 0;
    
    % results viewer 
    gui = 0;
            
  end
    
  methods
    
    % constructor    
    function O = IntrinsicImagingLogic(varargin)
      
      global CG;

      % superclass 
      O@AnalogIOLogic('Parameters', 'CG.Parameters.Setup.AnalogIO');
      
      % setup parameters
      O = setName(O, 'IntrinsicImaging');
      
      O.ParametersFull = { ...
        'TrialDurationMax', 150, 'Numeric', inf;... % TrialDurationMax is used by Logic so it's obligaory
        'ShutterBugWorkaroundDelay',0.5,'Numeric',inf;...
        'StimulationDelay',2,'Numeric',inf;...
        'StimulationDuration',1, 'Numeric', inf;...  
        'StimulationStopDelay', 7, 'Numeric', inf;...        
        'IntertrialDelay', 0.1, 'Numeric', inf;...        
        'AveragingInterval', 0.5, 'Numeric', inf;...     % frames bin size used for averaging, seconds
        'StimulationType','Tactile','String',{'Audio','Tactile'};...
        'StimulationShape','Sinusoid','String',{'Sinusoid','Pulses'};...
        'StimulationFrequency', 10, 'Numeric', inf;...  
        'StimulationSampleRate', 5000, 'Numeric', inf;...
        'StimulationAmplitude', 7, 'Numeric', inf;...        
        'StimulationTestDuration',1, 'Numeric', inf;...
        'TriggerInChannel1', 'Dev1/PFI0', 'String', '';...        
        'TriggerInChannel2', 'Dev1/PFI1', 'String', '';...        
        'DisplayPreviewListFrameWidth', 100, 'Numeric', inf;...
        'DisplayPreviewListFrameHeight', 50, 'Numeric', inf;...        
        'DisplayViewedRangeMin', -0.0005, 'Numeric', inf;...       
        'DisplayViewedRangeMax', 0.0005, 'Numeric', inf;...       
      };
        
      % give user a chance to assign parameters in the configuration file
      for i = 1:size(O.ParametersFull, 1)          
        if isfield(CG.Parameters.Setup.IntrinsicImaging, O.ParametersFull{i, 1})
            O.ParametersFull{i, 2} = CG.Parameters.Setup.IntrinsicImaging.(O.ParametersFull{i, 1});
        end        
      end
      
      % Video module requires the existance of CG.Data that is
      % created in Arduino module only. So I create a fake one here:
  
      CG.Data.foo = 1;
      
      % 'EPhysDataPath' is used in Logic.stop, so we have to define it...
      CG.Files.EPhysDataPath = '';      
      
      O.assignParameters;
            
      % setup camera
      ModuleTypes = { 'Video' };
      ModuleNames = { 'PointGrey' };
      
      % actually, it doesn't matter what parameters we pass to video
      % module... because they will be overriden in startVideo ->
      % prepareVideo, i.e. taken from CG.Parameters.Setup.Camera.      
      % It seems to be just a design flaw.
      ModulePars = { { 'VideoMode', 'F7_Mono16_640x512_Mode1', 'SR', 100 } };      
      % note: camera trigger is software('internal') by default
      ID = addModules(O, ModuleTypes, ModuleNames, ModulePars);
      O.HW.IDs.Camera = ID;      
      
      % make video module keep the data
      CG.Sessions.Video(O.HW.IDs.Camera).KeepInMemory = 1;

      % setup daq
      % todo/discuss: should we move this code to AnalogIOLogic ?
      aiSession = CG.Sessions.NI(O.HW.IDs.AnalogIO).SAI;      
      % setup trigger for shutter channel
      aiSession.addTriggerConnection('external', O.Parameters.TriggerInChannel1, 'StartTrigger');
      % create session for output channel
      O.stimulatorSession = CG.Sessions.NI(O.HW.IDs.AnalogIO).SAO;
      % setup trigger for stimulation session
      O.stimulatorSession.addTriggerConnection('external', O.Parameters.TriggerInChannel2, 'StartTrigger');
      % the session for trigger activation
      O.triggerSession = CG.Sessions.NI(O.HW.IDs.AnalogIO).SD;
                        
    end
        
    function delete(O)
        
      delete(O.triggerSession); 
      
      if O.gui ~= 0
          delete(O.gui)
      end
       
    end    
    
    function clearData(O)
      % clear all trials data - gui calls it
      O.trialBackgrounds = {};
      O.trialFrameBins = {};
      O.trialNormalizedBackgrounds = {};
      O.trialNormalizedFrameBins = {};
      O.frameNumberInFrameBins = {};
      % don't clear the backgound
      % O.backgroundPicture = 0;
    end

    function images = readLabviewImages(O, filename, width, height)
        % read labview image file
      fid = fopen(filename); %open binary file for reading
              
      fseek(fid, 0, 'eof');
      size = ftell(fid);
      fseek(fid, 0, 'bof');
      
      frameSize = width*height*2;      
      totalFrames = size / frameSize;
      
      inter = fread(fid, height * width * totalFrames, 'int16', 'ieee-be'); % read value from binary file
      images = reshape(inter, width, height, totalFrames);
      fclose(fid);
      
    end    
    
    function loadChaosData(O, params, waitBar)      

      filesPrefix = [params.path '/' params.fileNamePrefix]; 

      O.backgroundPicture = imread([filesPrefix, '_bg.tiff' ]);
      
      O.frameWidth = 320;
      O.frameHeight = 240;

      O.Parameters.StimulationDelay = 3.0; % hardcoded in Chao tool
      O.Parameters.StimulationDuration = 2.0; % I have no idea what was the actual value
      O.Parameters.StimulationStopDelay = 12 - O.Parameters.StimulationDuration;
      O.Parameters.AveragingInterval = 0.5;

      O.ParametersFull{strcmp(O.ParametersFull, 'StimulationDelay'), 2} = O.Parameters.StimulationDelay;
      O.ParametersFull{strcmp(O.ParametersFull, 'StimulationDuration'), 2} = O.Parameters.StimulationDuration;          
      O.ParametersFull{strcmp(O.ParametersFull, 'StimulationStopDelay'), 2} = O.Parameters.StimulationStopDelay;          
      O.ParametersFull{strcmp(O.ParametersFull, 'AveragingInterval'), 2} = O.Parameters.AveragingInterval;
      
      dir_files = dir([filesPrefix '*_rep*.']);
      files = { dir_files.name };
      files = natsort(files);
      
      trials = numel(files);

      progressNumerator = 1;
      progressDenominator = double(trials);
      
      for k = 1:trials
          
        fileName = fullfile(params.path, files(k));
        fileName = fileName{1};
          
        metaData = load(fileName);
        whisker = metaData(1, 3);
        angle = metaData(1, 4);
          
        if whisker ~= params.whisker || angle ~= params.angle
          continue;
        end
          
        timestamps = metaData(:, 1);
           
        frames = readLabviewImages(O, [fileName, '.dat'], O.frameWidth, O.frameHeight);
        frames = permute(frames, [2, 1, 3]);

        O.calculateTrialData(frames, timestamps);
        
        waitbar(progressNumerator / progressDenominator, waitBar);
        progressNumerator = progressNumerator + 1;        
                                                 
      end
                           
      close(waitBar);
      
      O.gui.update();            
      
    end

    function loadData(O, params, waitBar)      
      
      if(params.chaosData == 1)
        O.loadChaosData(params, waitBar);
        return
      end
      
      fprintf('loading paradigm data... ');
      load([params.path '/General.mat'], 'CGSave');
      O.backgroundPicture = CGSave.Paradigm.backgroundPicture;
      O.frameWidth = CGSave.Paradigm.frameWidth;
      O.frameHeight = CGSave.Paradigm.frameHeight;      

      % probably we don't want to overwrite Parameters completely...
      O.Parameters.StimulationDelay = CGSave.Paradigm.Parameters.StimulationDelay;
      O.Parameters.StimulationDuration = CGSave.Paradigm.Parameters.StimulationDuration;
      O.Parameters.StimulationStopDelay = CGSave.Paradigm.Parameters.StimulationStopDelay;
      % load original averaging interval ?

      % the paradigm parameters dialog will reset the parameters to their
      % original values if we don't do these assignments.
      % fixme: assign these values and do 'assignParameters' instead of
      % assigning to O.Parameters first
      O.ParametersFull{strcmp(O.ParametersFull, 'StimulationDelay'), 2} = O.Parameters.StimulationDelay;
      O.ParametersFull{strcmp(O.ParametersFull, 'StimulationDuration'), 2} = O.Parameters.StimulationDuration;          
      O.ParametersFull{strcmp(O.ParametersFull, 'StimulationStopDelay'), 2} = O.Parameters.StimulationStopDelay;          

      if params.recalculateTrialsData ~= 1

        fromTrial = max(params.fromTrial, 1);
        toTrial = min(params.toTrial, numel(CGSave.Paradigm.trialFrameBins));
        if toTrial == 0
          toTrial = numel(CGSave.Paradigm.trialFrameBins);
        end

        totalTrialsToLoad = toTrial - fromTrial + 1;
        progressNumerator = 1;
        progressDenominator = double(totalTrialsToLoad);
        
        for i = fromTrial:toTrial
          O.trialBackgrounds{end + 1} = CGSave.Paradigm.trialBackgrounds{i};
          O.trialNormalizedBackgrounds{end + 1} = CGSave.Paradigm.trialNormalizedBackgrounds{i};
          O.trialFrameBins{end + 1} = CGSave.Paradigm.trialFrameBins{i};
          O.trialNormalizedFrameBins{end + 1} = CGSave.Paradigm.trialNormalizedFrameBins{i};
          O.frameNumberInFrameBins{end + 1} = CGSave.Paradigm.frameNumberInFrameBins{i};
          waitbar(progressNumerator / progressDenominator, waitBar);
          progressNumerator = progressNumerator + 1;
        end
        
      end
      
      clear CGSave;
      
      disp('done.');      

      if params.recalculateTrialsData == 1      
      
        fprintf('processing raw data... \n');
        
        dir_files = dir([params.path '/PointGrey' '/Data_*.dat']);
        files = { dir_files.name };
        files = natsort(files);
        
        disp([num2str(numel(files)) ' trials found']);
        
        fromTrial = max(params.fromTrial, 1);
        toTrial = min(params.toTrial, numel(files));
        if toTrial == 0
          toTrial = numel(files);
        end
        
        totalTrialsToLoad = toTrial - fromTrial + 1;
        fprintf('Loading selected trials : ');
        
        progressNumerator = 1;
        progressDenominator = double(totalTrialsToLoad * 3);
        
        for i = fromTrial:toTrial
          fprintf([num2str(i),' ']);
          fn = fullfile(params.path, 'PointGrey', files(i));
          
          fid = fopen(fn{1});
          
          fseek(fid, 0, 'eof');
          size = ftell(fid);
          fseek(fid, 0, 'bof');
          
          width = O.frameWidth;
          height = O.frameHeight;
          
          frameSize = width*height*2;
          
          totalFrames = size / frameSize;
          
          inter = fread(fid, height * width * totalFrames, 'uint16=>single');
          
          waitbar(progressNumerator / progressDenominator, waitBar);
          progressNumerator = progressNumerator + 1;
          
          frames = reshape(inter, height, width, totalFrames);
          
          clear inter;
          
          fclose(fid);
          
          totalDuration = O.Parameters.StimulationStopDelay + O.Parameters.StimulationDelay + O.Parameters.StimulationDuration;
          timestamps = linspace(0, totalDuration, totalFrames);
          
          % O.backgroundPicture = frames(:, :, 3);
          
          waitbar(progressNumerator / progressDenominator, waitBar);
          progressNumerator = progressNumerator + 1;
          
          O.calculateTrialData(frames, timestamps);
          
          clear frames;
          clear timestamps;
          
          waitbar(progressNumerator / progressDenominator, waitBar);
          progressNumerator = progressNumerator + 1;
          
        end
        disp(' Done.');
      
      end 
      
      close(waitBar);

      O.gui.update();            
      
    end
    
    function captureBackground(O)
      % capture the 'background' ( ~ vascularity ) picture
      global CG;
            
      mbh = msgbox('Capturing background picture...', 'Please wait');      
      
      C_prepareVideo(O.HW.IDs.Camera, 'internal');
      C_startVideo(O.HW.IDs.Camera);
      % move pause time to parameters
      pause(2);
      
      C_stopVideo(O.HW.IDs.Camera, 0); % 0 = don't save video

      videoSession = CG.Sessions.Video(O.HW.IDs.Camera).S;      
      framesAvailable = get(videoSession, 'FramesAvailable');  
      [frames, cameraFrameTimes, metaData] = getdata(videoSession, framesAvailable);
      
      O.frameHeight = size(frames, 1);
      O.frameWidth = size(frames, 2);
      
      % average frames
           
      O.backgroundPicture = mean(frames(:, :, 1, :), 4);
      
      delete(mbh);
                 
    end   
      
    function startIOITrial(O, autoRepeat)      
      O.aborting = 0;
      O.autoRepeatTrials = autoRepeat;
      O.prepareTrial;
      O.startTrial;      
    end
    
    function abortIOITrial(O)
      O.aborting = 1;
      if O.intertrialTimer ~= 0 && strcmp(get(O.intertrialTimer, 'Running'), 'on')
        stop(O.intertrialTimer);
        O.gui.onLogicStateChanged(O.gui.lsIdle);        
      end
    end
 
    % Logic methods implementation
    
    function start(O)
      
      global CG;
 
      start@Logic(O)
      O.assignParameters;    
      O.prepareDisplay;       
      O.updateDisplay;
    end
    
    function stop(O) 

      if O.TrialActive
        O.aborting = 1;
        % fixme: should wait until the trial is actually stopped?
      end
      
      % save paradigm data 
      O.saveData('Paradigm', 1, 'Raw', 0);
      
      O.deleteTimers;     
      
      if O.gui ~= 0
          delete(O.gui);
          O.gui = 0;
      end
      
      O.clearData();
      
      stop@Logic(O);
      
    end
    
    function prepareTrial(O)
      
      % prepare files and increase trial
      prepareTrial@Logic(O);
      C_prepareVideo(O.HW.IDs.Camera,'external','FramesPerTrigger',1);
    end
    
    function startTrial(O)
      global CG;
      O.Trials(O.Trial).Attempt = 0;
      
      O.gui.onLogicStateChanged(O.gui.lsStimulation);
       
      SR = O.Parameters.StimulationSampleRate;
      StimulationDelay = O.Parameters.StimulationDelay;
      StimulationDuration = O.Parameters.StimulationDuration;
      StimulationStopDelay = O.Parameters.StimulationStopDelay;
      AcquisitionDuration = StimulationDelay + StimulationDuration + StimulationStopDelay;
      AfterTriggerDelay = 2;       % last samples bug workaround
      TrialDuration = AcquisitionDuration + AfterTriggerDelay;
      
      % PREPARE TRIGGER FOR CAMERA SHUTTER
      TriggerAmplitude = 3.5;
      FrameRate = CG.Sessions.Video(O.HW.IDs.Camera).SR;
      ShutterSignal =  TriggerAmplitude/2*(1+square(linspace(0, AcquisitionDuration * FrameRate * pi * 2, AcquisitionDuration * SR)));
      ZeroSteps = round(0.5*SR/FrameRate);
      ShutterSignal = [zeros(1,ZeroSteps),ShutterSignal(1:end-ZeroSteps)];
      TriggerSteps = length(ShutterSignal);
    
      % PREPARE STIMULATION SIGNAL
      StimulationSignal = O.prepareStimulationSignal(StimulationDuration);
      
      % EMBED STIMULATION SIGNAL IN ZEROS    
      PreZeros = zeros(1,round(StimulationDelay*SR));
      PostZeros = zeros(1,TriggerSteps - length(StimulationSignal) - length(PreZeros));
      StimulationSignal = [PreZeros,StimulationSignal,PostZeros];
      
      % APPEND SAFETY MARGIN FOR CORRECT FRAMES
      AfterTriggerZeros =  zeros(1, round(SR * AfterTriggerDelay));
      StimulationSignal = [StimulationSignal,AfterTriggerZeros]';
      ShutterSignal = [ShutterSignal, AfterTriggerZeros]';
 
      % ACCOUNT FOR HIGH FREQUENCY AUDIO STIMULATION
      if  O.Parameters.StimulationFrequency > 2500 && isequal(O.Parameters.StimulationType,'Audio')
        AudioStimulator = 1; SRAudio = 100000;
        AudioSignal = O.prepareStimulationSignal(StimulationDuration,SRAudio);
        PreZeros = zeros(1,round(StimulationDelay*SRAudio));
        PostZeros = zeros(1,round(0.5*SRAudio));
        AudioPlayer = audioplayer([PreZeros,AudioSignal,PostZeros],SRAudio);
        StimulationSignal(:) = 0;
      else
        AudioStimulator = 0;
      end
      
      
      queueOutputData(O.stimulatorSession, [StimulationSignal, ShutterSignal] );      
      
 
      % send trial start signal
      disp(['Starting Trial ',num2str(O.Trial)]);
      startTrial@Logic(O);
      
      % prepare the trigger
      O.triggerSession.outputSingleScan(0);
      
      O.startAnalogIO;   

      % trigger the stimulation and shutter pulses recording
      O.triggerSession.outputSingleScan(1);
            
      % TRIGGER AUDIO DIRECTLY IF NECESSARY
      if AudioStimulator;  AudioPlayer.play;    end
      
      % let camera and stimulator do their job
      pause(TrialDuration);  

      O.stopTrial;    
      
    end
    
    function stopTrial(O)
        
      global CG;
           
      % STOP RECORDING SESSIONS 
      
      % extra delay to catch all strobes
      pause(1);
      
      O.stopAnalogIO; pause(0.1);
      C_stopVideo(O.HW.IDs.Camera, 0); % 0 = don't save video     
      
      % SEND STOP TRIGGER (DOESN'T  DO ANYTHING HERE)
      O.triggerSession.outputSingleScan(0);
            
      % save modules data('raw data') only
      stopTrial@Logic(O, 'SaveParadigm', 0);
            
      O.updateIOI;
      
      if O.autoRepeatTrials == 1 && O.aborting ~= 1
        O.intertrialTimer = timer('TimerFcn',  @O.onStopIntertrialTimer, 'StartDelay', O.Parameters.IntertrialDelay);
        start(O.intertrialTimer);
        O.gui.onLogicStateChanged(O.gui.lsIntertrialDelay);
      else      
        O.gui.onLogicStateChanged(O.gui.lsIdle);
      end
      
    end

    function testStimulation(O)
      SR = O.Parameters.StimulationSampleRate;
      TestDuration = O.Parameters.StimulationTestDuration;
      StimulationSignal = O.prepareStimulationSignal(TestDuration);
      ShutterSignal = zeros(1, TestDuration * SR);
      
      % ACCOUNT FOR HIGH FREQUENCY AUDIO STIMULATION
      if  O.Parameters.StimulationFrequency > 2500 && isequal(O.Parameters.StimulationType,'Audio')
        AudioStimulator = 1;
        AudioSignal = O.prepareStimulationSignal(TestDuration,100000);
        AudioPlayer = audioplayer([zeros(1,5000),AudioSignal,zeros(1,5000)],100000);
        StimulationSignal(:) = 0;
      else
        AudioStimulator = 0;
      end
      
      queueOutputData(O.stimulatorSession, [StimulationSignal', ShutterSignal'] );
      
      % prepare & trigger the session
      O.triggerSession.outputSingleScan(0);
      O.startAnalogIO;
      O.triggerSession.outputSingleScan(1);
      
      % TRIGGER AUDIO DIRECTLY IF NECESSARY
      if AudioStimulator;  AudioPlayer.play;    end
      
      pause(TestDuration); 
      O.stopAnalogIO;      
    end
   
    
    function StimulationSignal = prepareStimulationSignal(O,Duration,SR)
      % PREPARE STIMULATION SIGNAL
      Frequency = O.Parameters.StimulationFrequency;
      Amplitude = O.Parameters.StimulationAmplitude;
      if ~exist('SR','var')   SR = O.Parameters.StimulationSampleRate;  end
      Time = [1:round(Duration*SR)]/SR;
      switch O.Parameters.StimulationShape
        case 'Sinusoid';
          StimulationSignal = cos(2*pi*Frequency*Time);
        case 'Pulses'
          StimulationSignal = zeros(size(Time));
          Period = 1/Frequency;
          for iT=1:length(Time)
            if mod(Time(iT),Period) <= 0.5/SR
              StimulationSignal(iT) = 1;
              end
          end
      end

      switch O.Parameters.StimulationType
        case 'Audio'; % All positive, since also through a piezo %%Centered around 0
        %  StimulationSignal = Amplitude*StimulationSignal;
          StimulationSignal = Amplitude/2*(1-StimulationSignal);
        case 'Tactile'; % All Positive 
          StimulationSignal = Amplitude/2*(1-StimulationSignal);
      end
    end
    
    % trial timer callback
    function onStopTrialTimer(O, object, event)
            
      % stop(O.trialTimer);                
       % O.stopTrial();

    end
    
    % intertrial timer callback
    function onStopIntertrialTimer(O, object, event)
      
      if O.aborting ~= 1      
        O.prepareTrial;
        O.startTrial;
      else
        O.gui.onLogicStateChanged(O.gui.lsIdle);
      end
      
    end
    
    % aqcuire the data and calculate the results
    function updateIOI(O)

      global CG;
 
      % get analog input data : relating to the shutter
      totalSamples = CG.Sessions.NI(O.HW.IDs.AnalogIO).PacketsAcquired;
      shutter = CG.Data.NI(O.HW.IDs.AnalogIO).Analog(1:totalSamples);
       
      % debug stub : 10 ms pulses
      
      %{
      totalTime = totalSamples / O.analogInputSampleRate;
      totalFrames = totalTime / 0.01;
      amplitude = 4;
      shutter = amplitude * 0.5 * (square(linspace(0, 2*pi*totalFrames, totalSamples)) + 1);
      %}
      
      % get the data from camera
      
      % 640x512 by default
      % videoSession = CG.Sessions.Video(O.HW.IDs.Camera).S;
      % framesAvailable = get(videoSession, 'FramesAvailable');  
      % note : we're not interested in camera frame times
      % [ frames, cameraFrameTimes, metaData ] = getdata(videoSession, framesAvailable);
      
      frames = CG.Sessions.Video(O.HW.IDs.Camera).Frames;
      
      %{
      if O.aborting == 1            
        return;
      end
      %}
      
      % collect pulses - taken from Roachalyzer
      
      shutterPulseThreshold = 1.5; % volts      
      
      ind = find(shutter > shutterPulseThreshold);
      pulses = [];
      if length(ind) > 1
        dind = diff(ind); 
        ind2 = find(dind > 1);
        pulses = zeros(1, length(ind2) + 1); 
        pulses(1) = ind(1);  
        pulses(2 : end) = ind(ind2 + 1);
      end
            
      % calculate frame times
      samplingPeriod = 1. /  O.analogInputSampleRate;
      timestamps = pulses * samplingPeriod;
      totalStimulationFrames = size(pulses, 2);    
      
      O.frameHeight = size(frames, 1);
      O.frameWidth = size(frames, 2);
      totalFrames = size(frames, 4);

      disp(['Frame Triggers recorded : ',num2str(totalStimulationFrames)]);
      disp(['Frames received : ',num2str(totalFrames)]);
    
      if totalFrames < totalStimulationFrames
        disp('totalFrames< totalStimulationFrames, probably something went wrong');
        totalStimulationFrames = totalFrames;
      end
      
       if totalStimulationFrames == 0
        disp('no frame tiggers were recorded!');
        keyboard
      end
      
      % average frames over averaging bin durations
      
      % the first trial frame index            
      % firstFrameIndex = totalFrames - totalStimulationFrames + 1;
      
      firstFrameIndex = 1;
      
      frames = squeeze(frames(:, :, 1, ...
          firstFrameIndex:(firstFrameIndex + totalStimulationFrames - 1)));
      
      O.calculateTrialData(frames, timestamps);
      
    end
     
    function calculateTrialData(O, frames, timestamps)
      
      % frame pixels come as integers if gotten from camera
      frames = single(frames);
      
      bgInterval = O.Parameters.StimulationDelay;
      bgIndex = min(find(timestamps >= bgInterval, 1), numel(frames));                
      bgSkipIndex = min(find(timestamps >= O.Parameters.ShutterBugWorkaroundDelay, 1), numel(frames));                
      
      bgFrames = frames(:, :, bgSkipIndex:bgIndex);
      
      O.trialBackgrounds{end + 1} = mean(bgFrames, 3);
      O.trialNormalizedBackgrounds{end + 1} = mean(bsxfun(@rdivide, bgFrames, mean(mean(bgFrames, 2), 1)), 3);
      
      totalBins = (O.Parameters.StimulationDuration + O.Parameters.StimulationStopDelay) / ...
            O.Parameters.AveragingInterval;  
        
      maxIndex = min(numel(timestamps), size(frames, 3)); 
      
      O.trialFrameBins{end + 1} = {};
      O.trialNormalizedFrameBins{end + 1} = {};
      O.frameNumberInFrameBins{end + 1} = {};      
      
      bin = 1;
      startIndex = bgIndex;
      startTime = O.Parameters.StimulationDelay;
      
      for i = 1:maxIndex
                
        if bin > totalBins
            break;
        end
                
        timestampIsInTheNextBin = timestamps(i) - startTime > O.Parameters.AveragingInterval;                
        if timestampIsInTheNextBin == 1 || i == maxIndex
                                        
            endIndex = i - 1;
            if timestampIsInTheNextBin ~= 1
                endIndex = i;
            end
    
            binFrames = frames(:, :, startIndex:endIndex);

            O.trialFrameBins{end}{end + 1} = mean(binFrames, 3);
            O.trialNormalizedFrameBins{end}{end + 1} = mean(bsxfun(@rdivide, binFrames, mean(mean(binFrames,2), 1)),  3);            
            
            frameNum = endIndex - startIndex + 1;
            O.frameNumberInFrameBins{end}{end + 1} = frameNum;            
                
            startIndex = i;
            startTime = startTime + O.Parameters.AveragingInterval;
            bin = bin + 1;
                
        end 
        
      end                
        
    end
        
    % event processing
    function processEvent(O,SourceName,Event)

      if ~O.ParadigmActive return; end
      
      switch SourceName
        case 'Keyboard';
          processEvent@Logic(O,SourceName,Event);
          
        % since 'thresholds' are not set, we dont expect any events from daq
        % case 'NIDAQ';
        
      end
    end
      
    function prepareDisplay(O)
      
      if ~isfield(O.Display,'Handles') || ~ishandle(O.Display.Handles.FIG)
        
        % let basic class generate figure id
        prepareDisplay@Logic(O);
          
      else
        
        figure(O.Display.Handles.FIG); 
        
      end
      
    end
    
    function updateDisplay(O)
      
      global CG
      
      % create 'display' if not yet created
      if O.gui == 0
        O.gui = IntrinsicImagingLogicDisplay(O, O.Display.Handles.FIG);
        O.showDisplay(1);
      else
        O.gui.update;
      end
            
    end
    
  end % methods
  
end
 % intrinsic optical imaging load data dialog

classdef IntrinsicImagingLogicLoadDataDialog < handle
  
  properties
    
    % host display
    
    host = 0;
            
    dialog = 0;
    
    figureDeleted = 0;

    initialFigureWidth = 640;
    initialFigureHeight = 340;    
    initialFigureX = 300;
    initialFigureY = 300;
    
    editDataPath = 0;
    editFromTrial = 0;
    editToTrial = 0;
    checkRecalculateTrialsData = 0;
    
    % Chao's data controls
    checkChaosData = 0;
    editFileNamePrefix = 0;
    labelFileNamePrefix = 0;
    labelAngle = 0;
    editAngle = 0;
    labelWhisker = 0;
    editWhisker = 0;
    
    defaultBgColor = 0;
    
    initialParams = 0;
    
  end
    
  methods

    % constructor
                 
    function O = IntrinsicImagingLogicLoadDataDialog(host, initialParams)
      
      global CG;
      
      O.host = host;
      O.initialParams = initialParams;
      
      position = [O.initialFigureX, O.initialFigureY, O.initialFigureWidth, O.initialFigureHeight];      
    
      O.dialog = dialog('Position', position, 'Name', 'Load data to analyse');
      
      topMargin = 15;
      leftMargin = 13;
      horzSpace = 10;
      vertSpace = 5;
      editHeight = 20;
      checkHeight = 20;
      textHeight = 20;
      
      y = O.initialFigureHeight - editHeight - topMargin;
      x = leftMargin;      
      
      dataPathLabelPosition = [ x,  y, 100, textHeight ];
      
      labelPath = uicontrol(O.dialog, 'Style', 'text', 'String', 'Data path: ', ...
        'Position', dataPathLabelPosition, 'HorizontalAlignment', 'left');
      
      y = y - textHeight - vertSpace;
      
      selectDataWidth = 60;
      
      selectDataPathPosition = [ x,  y, selectDataWidth, editHeight ];
      
      buttonSelectDataPath = uicontrol(O.dialog, 'Style', 'pushbutton', 'String', 'Browse', ...
          'Enable', 'on', 'Position', selectDataPathPosition, 'Callback', @O.onSelectDataPathPressed);

      x = x + selectDataWidth + horzSpace;

      startPath = '';
      if isstruct(O.initialParams)
        startPath = O.initialParams.path;
      end
      
      dataPathPosition = [ x, y, O.initialFigureWidth - x - horzSpace, editHeight ];
        
      O.editDataPath = uicontrol(O.dialog, 'Style', 'edit', 'String', startPath, ...
          'Enable', 'on', 'Position', dataPathPosition);
        
      y = y - textHeight - vertSpace;
      x = leftMargin;

      fromTrial = 0;
      toTrial = 0;
      recalculateTrialsData = 0;
      if isstruct(O.initialParams)
        fromTrial = O.initialParams.fromTrial;
        toTrial = O.initialParams.toTrial;
        recalculateTrialsData = O.initialParams.recalculateTrialsData;
      end
      
      fromTrialLabelWidth = 80;
      fromTrialLabelPosition = [ x,  y, fromTrialLabelWidth, textHeight ];
      
      labelFromTrial = uicontrol(O.dialog, 'Style', 'text', 'String', 'From trial ', ...
        'Position', fromTrialLabelPosition, 'HorizontalAlignment', 'left');
      
      x = x + fromTrialLabelWidth + horzSpace;
    
      fromTrialEditWidth = 60;
      fromTrialEditPosition = [ x, y, fromTrialEditWidth, editHeight ];
          
      O.editFromTrial = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(fromTrial), ...
          'Enable', 'on', 'Position', fromTrialEditPosition);
        
      x = x + fromTrialEditWidth + horzSpace;

      toTrialLabelWidth = 60;
      toTrialLabelPosition = [ x,  y, fromTrialLabelWidth, textHeight ];
      
      labelToTrial = uicontrol(O.dialog, 'Style', 'text', 'String', 'To trial ', ...
        'Position', toTrialLabelPosition, 'HorizontalAlignment', 'left');
      
      x = x + toTrialLabelWidth + horzSpace;

      toTrialEditWidth = 60;
      toTrialEditPosition = [ x, y, toTrialEditWidth, editHeight ];
          
      O.editToTrial = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(toTrial), ...
          'Enable', 'on', 'Position', toTrialEditPosition);

      y = y - editHeight - vertSpace;
      x = leftMargin;

      controlWidth = 200;
      
      O.checkRecalculateTrialsData = uicontrol('Parent', O.dialog, 'Style', 'checkbox', 'String', 'Recalculate trials data', ...
          'Position', [x, y, controlWidth, checkHeight], ...
          'Value', recalculateTrialsData);                            
        
      % Chao's data controls
            
      chaosDataEnabled = 0;
      chaosDataEnabledOnOff = 'off';
      if isstruct(O.initialParams) && O.initialParams.chaosData == 1
        chaosDataEnabled = 1;
        chaosDataEnabledOnOff = 'on';
      end

      y = 150;
      initialX = 400;
      
      x = initialX;
      
      angleLabelWidth = 80;
      angleLabelPosition = [ x,  y, angleLabelWidth, textHeight ];
      
      O.labelAngle = uicontrol(O.dialog, 'Style', 'text', 'String', 'Angle ', ...
        'Position', angleLabelPosition, 'HorizontalAlignment', 'left', 'Enable', chaosDataEnabledOnOff);
      
      x = x + angleLabelWidth + horzSpace;
    
      angleEditWidth = 100;
      angleEditPosition = [ x, y, angleEditWidth, editHeight ];
      defaultAngle = 16;
      
      if chaosDataEnabled == 1
        defaultAngle = O.initialParams.angle;
      end      
          
      O.editAngle = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(defaultAngle), ...
          'Enable', chaosDataEnabledOnOff, 'Position', angleEditPosition);
      
      y = y + editHeight + vertSpace;        
      x = initialX;
      
      whiskerLabelWidth = 80;
      whiskerLabelPosition = [ x,  y, whiskerLabelWidth, textHeight ];
      
      O.labelWhisker = uicontrol(O.dialog, 'Style', 'text', 'String', 'Whisker ', ...
        'Position', whiskerLabelPosition, 'HorizontalAlignment', 'left', 'Enable', chaosDataEnabledOnOff);
      
      x = x + whiskerLabelWidth + horzSpace;
    
      whiskerEditWidth = 100;
      whiskerEditPosition = [ x, y, whiskerEditWidth, editHeight ];
      defaultWhisker = 1;
      
      if chaosDataEnabled == 1
        defaultWhisker = O.initialParams.whisker;
      end
                
      O.editWhisker = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(defaultWhisker), ...
          'Enable', chaosDataEnabledOnOff, 'Position', whiskerEditPosition);
      
      y = y + editHeight + vertSpace;        
      x = initialX;
      
      fileNamePrefixLabelWidth = 80;
      fileNamePrefixLabelPosition = [ x,  y, fileNamePrefixLabelWidth, textHeight ];
      
      O.labelFileNamePrefix = uicontrol(O.dialog, 'Style', 'text', 'String', 'File name prefix ', ...
        'Position', fileNamePrefixLabelPosition, 'HorizontalAlignment', 'left', 'Enable', chaosDataEnabledOnOff);
      
      x = x + fileNamePrefixLabelWidth + horzSpace;
    
      fileNamePrefixEditWidth = 100;
      fileNamePrefixEditPosition = [ x, y, fileNamePrefixEditWidth, editHeight ];
      defaultFileNamePrefix = 'recording2';
      
      if chaosDataEnabled == 1
        defaultFileNamePrefix = O.initialParams.fileNamePrefix;
      end
          
      O.editFileNamePrefix = uicontrol(O.dialog, 'Style', 'edit', 'String', defaultFileNamePrefix, ...
          'Enable', chaosDataEnabledOnOff, 'Position', fileNamePrefixEditPosition);

      y = y + editHeight + vertSpace;
        
      O.checkChaosData = uicontrol('Parent', O.dialog, 'Style', 'checkbox', 'String', 'Chao''s data', ...
          'Position', [x, y, 100, checkHeight], ...
          'Value', chaosDataEnabled, 'Callback', @O.onChaosDataClicked);                
      
        
      % load button
      
      loadButtonPosition = [ 10, 10, 80, 30 ];
        
      buttonLoad = uicontrol(O.dialog, 'Style', 'pushbutton', 'String', 'Load data', ...
          'Enable', 'on', 'Position', loadButtonPosition, 'Callback', @O.onLoadButtonPressed);            
      
    end
    
    % destructor
    function delete(O)
       %if O.figureDeleted ~= 1
       %  close(O.figureId); 
       %end
       % no  need to destroy the figure - controller will delete it
    end    
            
    % ui event handlers             
    function onFigureDelete(O, figureObject, ~)
       
       % todo: we have to stop the paradigm on window close... 
        O.figureDeleted = 1;
        
    end
    
    % don't allow user to close the window - just hide it? ( fixme: it
    % confuses the controller...)
    %{
    function onFigureCloseRequest(O, figureObject, ~)
       
       %set(O.figureId, 'Visible', 'off');
       delete(O.figureId);
        
    end
    %}
    
    function onSelectDataPathPressed(O, buttonObject, ~)
      folder = uigetdir(O.editDataPath.String, 'Select video data folder');
      O.editDataPath.String = folder;
      %{
      filename, pathname] = uigetdir('Select video data folder');
      if ~isequal(filename,0)
        O.editDataPath.String = fullfile(pathname, filename);
      end      
      %}
    end
    
    function onLoadButtonPressed(O, buttonObject, ~)
        params = struct();        
        params.path = O.editDataPath.String;
        params.fromTrial = str2num(O.editFromTrial.String);
        params.toTrial = str2num(O.editToTrial.String);
        params.recalculateTrialsData = O.checkRecalculateTrialsData.Value;
        params.chaosData = O.checkChaosData.Value;        

        if params.chaosData == 1
          params.fileNamePrefix = O.editFileNamePrefix.String;
          params.angle = str2num(O.editAngle.String);
          params.whisker = str2num(O.editWhisker.String);
        end
        
        delete(O.dialog);
        O.host.onLoadData(params);
        
    end
    
    function onChaosDataClicked(O, ~, ~)
    
      state = get(O.checkChaosData, 'Value');
        
      val = 'off';
      if state == 1
        val = 'on';
      end
          
      set(O.editFileNamePrefix, 'Enable', val);
      set(O.labelFileNamePrefix, 'Enable', val);
      set(O.labelAngle, 'Enable', val);
      set(O.editAngle, 'Enable', val);
      set(O.labelWhisker, 'Enable', val);
      set(O.editWhisker, 'Enable', val);
                    
    end
    
  end % methods

end
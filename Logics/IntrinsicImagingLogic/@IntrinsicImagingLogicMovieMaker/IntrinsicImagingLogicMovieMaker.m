 % intrinsic optical imaging movie maker dialog

classdef IntrinsicImagingLogicMovieMaker < handle
  
  properties
    
    % host display
    
    host = 0;
            
    dialog = 0;
    
    figureDeleted = 0;

    initialFigureWidth = 640;
    initialFigureHeight = 340;    
    initialFigureX = 300;
    initialFigureY = 300;
    
    editDataPath = 0;
    editFromTrial = 0;
    editToTrial = 0;
    
    % Chao's data controls
    checkChaosData = 0;
    editFileNamePrefix = 0;
    labelFileNamePrefix = 0;
    labelAngle = 0;
    editAngle = 0;
    labelWhisker = 0;
    editWhisker = 0;
    
    defaultBgColor = 0;
    
    initialParams = 0;
    
    showBackground = 0;
    normalizeColors = 0;
    flipSides = 0;
    viewedRangeMin = 0;
    viewedRangeMax = 0;
    backgroundPicture = 0;
    
    frameWidth = 0;
    frameHeight = 0;
    
    colorMap = 0;
    
  end
    
  methods

    % constructor
                 
    function O = IntrinsicImagingLogicMovieMaker(host)
      
      global CG;
      
      O.host = host;
      
      position = [O.initialFigureX, O.initialFigureY, O.initialFigureWidth, O.initialFigureHeight];      
    
      O.dialog = dialog('Position', position, 'Name', 'Make a movie');
      
      topMargin = 15;
      leftMargin = 13;
      horzSpace = 10;
      vertSpace = 5;
      editHeight = 20;
      checkHeight = 20;
      textHeight = 20;
      
      y = O.initialFigureHeight - editHeight - topMargin;
      x = leftMargin;      
      
      dataPathLabelPosition = [ x,  y, 100, textHeight ];
      
      labelPath = uicontrol(O.dialog, 'Style', 'text', 'String', 'Data path: ', ...
        'Position', dataPathLabelPosition, 'HorizontalAlignment', 'left');
      
      y = y - textHeight - vertSpace;
      
      selectDataWidth = 60;
      
      selectDataPathPosition = [ x,  y, selectDataWidth, editHeight ];
      
      buttonSelectDataPath = uicontrol(O.dialog, 'Style', 'pushbutton', 'String', 'Browse', ...
          'Enable', 'on', 'Position', selectDataPathPosition, 'Callback', @O.onSelectDataPathPressed);

      x = x + selectDataWidth + horzSpace;

      startPath = '';
      if isstruct(O.initialParams)
        startPath = O.initialParams.path;
      end
      
      dataPathPosition = [ x, y, O.initialFigureWidth - x - horzSpace, editHeight ];
        
      O.editDataPath = uicontrol(O.dialog, 'Style', 'edit', 'String', startPath, ...
          'Enable', 'on', 'Position', dataPathPosition);
        
      y = y - textHeight - vertSpace;
      x = leftMargin;

      fromTrial = 0;
      toTrial = 0;
      recalculateTrialsData = 0;
      if isstruct(O.initialParams)
        fromTrial = O.initialParams.fromTrial;
        toTrial = O.initialParams.toTrial;
        recalculateTrialsData = O.initialParams.recalculateTrialsData;
      end
      
      fromTrialLabelWidth = 80;
      fromTrialLabelPosition = [ x,  y, fromTrialLabelWidth, textHeight ];
      
      labelFromTrial = uicontrol(O.dialog, 'Style', 'text', 'String', 'From trial ', ...
        'Position', fromTrialLabelPosition, 'HorizontalAlignment', 'left');
      
      x = x + fromTrialLabelWidth + horzSpace;
    
      fromTrialEditWidth = 60;
      fromTrialEditPosition = [ x, y, fromTrialEditWidth, editHeight ];
          
      O.editFromTrial = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(fromTrial), ...
          'Enable', 'on', 'Position', fromTrialEditPosition);
        
      x = x + fromTrialEditWidth + horzSpace;

      toTrialLabelWidth = 60;
      toTrialLabelPosition = [ x,  y, fromTrialLabelWidth, textHeight ];
      
      labelToTrial = uicontrol(O.dialog, 'Style', 'text', 'String', 'To trial ', ...
        'Position', toTrialLabelPosition, 'HorizontalAlignment', 'left');
      
      x = x + toTrialLabelWidth + horzSpace;

      toTrialEditWidth = 60;
      toTrialEditPosition = [ x, y, toTrialEditWidth, editHeight ];
          
      O.editToTrial = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(toTrial), ...
          'Enable', 'on', 'Position', toTrialEditPosition);

      y = y - editHeight - vertSpace;
      x = leftMargin;

      controlWidth = 200;
              
      % Chao's data controls
            
      chaosDataEnabled = 0;
      chaosDataEnabledOnOff = 'off';
      if isstruct(O.initialParams) && O.initialParams.chaosData == 1
        chaosDataEnabled = 1;
        chaosDataEnabledOnOff = 'on';
      end

      y = 150;
      initialX = 400;
      
      x = initialX;
      
      angleLabelWidth = 80;
      angleLabelPosition = [ x,  y, angleLabelWidth, textHeight ];
      
      O.labelAngle = uicontrol(O.dialog, 'Style', 'text', 'String', 'Angle ', ...
        'Position', angleLabelPosition, 'HorizontalAlignment', 'left', 'Enable', chaosDataEnabledOnOff);
      
      x = x + angleLabelWidth + horzSpace;
    
      angleEditWidth = 100;
      angleEditPosition = [ x, y, angleEditWidth, editHeight ];
      defaultAngle = 16;
      
      if chaosDataEnabled == 1
        defaultAngle = O.initialParams.angle;
      end      
          
      O.editAngle = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(defaultAngle), ...
          'Enable', chaosDataEnabledOnOff, 'Position', angleEditPosition);
      
      y = y + editHeight + vertSpace;        
      x = initialX;
      
      whiskerLabelWidth = 80;
      whiskerLabelPosition = [ x,  y, whiskerLabelWidth, textHeight ];
      
      O.labelWhisker = uicontrol(O.dialog, 'Style', 'text', 'String', 'Whisker ', ...
        'Position', whiskerLabelPosition, 'HorizontalAlignment', 'left', 'Enable', chaosDataEnabledOnOff);
      
      x = x + whiskerLabelWidth + horzSpace;
    
      whiskerEditWidth = 100;
      whiskerEditPosition = [ x, y, whiskerEditWidth, editHeight ];
      defaultWhisker = 1;
      
      if chaosDataEnabled == 1
        defaultWhisker = O.initialParams.whisker;
      end
                
      O.editWhisker = uicontrol(O.dialog, 'Style', 'edit', 'String', num2str(defaultWhisker), ...
          'Enable', chaosDataEnabledOnOff, 'Position', whiskerEditPosition);
      
      y = y + editHeight + vertSpace;        
      x = initialX;
      
      fileNamePrefixLabelWidth = 80;
      fileNamePrefixLabelPosition = [ x,  y, fileNamePrefixLabelWidth, textHeight ];
      
      O.labelFileNamePrefix = uicontrol(O.dialog, 'Style', 'text', 'String', 'File name prefix ', ...
        'Position', fileNamePrefixLabelPosition, 'HorizontalAlignment', 'left', 'Enable', chaosDataEnabledOnOff);
      
      x = x + fileNamePrefixLabelWidth + horzSpace;
    
      fileNamePrefixEditWidth = 100;
      fileNamePrefixEditPosition = [ x, y, fileNamePrefixEditWidth, editHeight ];
      defaultFileNamePrefix = 'recording2';
      
      if chaosDataEnabled == 1
        defaultFileNamePrefix = O.initialParams.fileNamePrefix;
      end
          
      O.editFileNamePrefix = uicontrol(O.dialog, 'Style', 'edit', 'String', defaultFileNamePrefix, ...
          'Enable', chaosDataEnabledOnOff, 'Position', fileNamePrefixEditPosition);

      y = y + editHeight + vertSpace;
        
      O.checkChaosData = uicontrol('Parent', O.dialog, 'Style', 'checkbox', 'String', 'Chao''s data', ...
          'Position', [x, y, 100, checkHeight], ...
          'Value', chaosDataEnabled, 'Callback', @O.onChaosDataClicked);                
      
        
      % load button
      
      makeMovieButtonPosition = [ 10, 10, 80, 30 ];
        
      buttonMakeAMovie = uicontrol(O.dialog, 'Style', 'pushbutton', 'String', 'Make a movie', ...
          'Enable', 'on', 'Position', makeMovieButtonPosition, 'Callback', @O.onMakeMoviePressed);            
        
      % color map for disparity values
      % note: red corresponds to _low_ intensity
      r =  [ linspace(1,  0, 256) zeros(1, 256) ];
      g =  zeros(1, 512);
      b =  [ zeros(1, 256)  linspace(0,  1, 256) ];
      
      O.colorMap = [r' g' b'];        
      
    end
    
    % destructor
    function delete(O)
       %if O.figureDeleted ~= 1
       %  close(O.figureId); 
       %end
       % no  need to destroy the figure - controller will delete it
    end    
            
    % ui event handlers             
    function onFigureDelete(O, figureObject, ~)
       
       % todo: we have to stop the paradigm on window close... 
        O.figureDeleted = 1;
        
    end
    
    % don't allow user to close the window - just hide it? ( fixme: it
    % confuses the controller...)
    %{
    function onFigureCloseRequest(O, figureObject, ~)
       
       %set(O.figureId, 'Visible', 'off');
       delete(O.figureId);
        
    end
    %}
    
    function onSelectDataPathPressed(O, buttonObject, ~)
      folder = uigetdir(O.editDataPath.String, 'Select video data folder');
      O.editDataPath.String = folder;
      %{
      filename, pathname] = uigetdir('Select video data folder');
      if ~isequal(filename,0)
        O.editDataPath.String = fullfile(pathname, filename);
      end      
      %}
    end
    
    function image = composePicture(O, delta)
        
      if O.showBackground == 1
        image = uint8(O.backgroundPicture / 256);
      else
        image = zeros(O.frameHeight, O.frameWidth, 'uint8');
      end
                        
      if O.normalizeColors == 0
          
        mx = max(O.viewedRangeMin, O.viewedRangeMax);
        mn = min(O.viewedRangeMin, O.viewedRangeMax);
          
        if mn == mx
          deltaTransparency = delta;
          if mn >= 0
            val = 1;
          else
            val = -1;
          end
          deltaTransparency(deltaTransparency == mn) = val;
        else
            
          if mn >= 0
            amp = 1 / (mx - mn);
            deltaTransparency = amp * (delta - mn);
            deltaTransparency(deltaTransparency < 0) = 0;
          elseif mx <= 0
            amp = 1 / (mx - mn);
            deltaTransparency = amp * (delta - mx);
            deltaTransparency(deltaTransparency > 0) = 0;
          else
            deltaTransparency = delta;
            negAmp = 1 / abs(mn);
            posAmp = 1 / mx;
            deltaTransparency(deltaTransparency < 0) = negAmp * deltaTransparency(deltaTransparency < 0);
            deltaTransparency(deltaTransparency > 0) = posAmp * deltaTransparency(deltaTransparency > 0);
          end
        end
        
      else
        amp = 0.5 / abs(mean(mean(abs(delta))));
        deltaTransparency = amp * delta;
      end
      
      deltaTransparency(deltaTransparency > 1.) = 1.;
      deltaTransparency(deltaTransparency < -1.) = -1.;
        
      deltaColors = int64(deltaTransparency*256 + 256);
      deltaColors(deltaColors < 1) = 1;
      deltaColors(deltaColors > 512) = 512;
      
      deltaColors = ind2rgb(deltaColors, O.colorMap);
        
      deltaTransparency = abs(deltaTransparency);
      
      rgbImage = cat(3, image, image, image);
      deltaTransparency = cat(3, deltaTransparency, deltaTransparency, deltaTransparency);
      
      image = (double(rgbImage) ./ 255) .* (1 - deltaTransparency) + deltaColors .* deltaTransparency;
      
      if O.flipSides == 1
        image = rot90(image, 2);
      end
                 
    end
    
    
    function r = lowPassFilter(O, frames)
      
      r = frames;
      alpha = 0.01;
  
      c = r(:, :, 1);
      for i = 1:size(r, 3)
        v = c * (1 - alpha) + alpha * r(:, :, i);
        r(:, :, i) = v;
        c = v;
      end
            
    end
    
    function onMakeMoviePressed(O, buttonObject, ~)
      
      O.showBackground = get(O.host.checkShowBackground, 'Value');
      O.normalizeColors = get(O.host.checkNormalizeColors, 'Value');
      O.flipSides = get(O.host.checkFlipSides, 'Value');      
      O.viewedRangeMin = O.host.viewedRangeMin;
      O.viewedRangeMax = O.host.viewedRangeMax;            
      
      path = O.editDataPath.String
      fromTrial = str2num(O.editFromTrial.String);
      toTrial = str2num(O.editToTrial.String);
      
      load([path '/General.mat'], 'CGSave');
      
      width = CGSave.Paradigm.frameWidth;
      height = CGSave.Paradigm.frameHeight;      
      O.frameWidth = width;
      O.frameHeight = height;
      O.backgroundPicture = CGSave.Paradigm.backgroundPicture;
      
      dir_files = dir([path '/PointGrey' '/Data_*.dat']);
      files = { dir_files.name };
      files = natsort(files);
        
      disp([num2str(numel(files)) ' trials found']);
        
      fromTrial = max(fromTrial, 1);
      toTrial = min(toTrial, numel(files));
      if toTrial == 0
        toTrial = numel(files);
      end
        
      totalTrialsToLoad = toTrial - fromTrial + 1;      
        
      progressNumerator = 1;
      progressDenominator = double(totalTrialsToLoad * 3);
      waitBar = waitbar(0, 'Loading frames and computing delta images...');
  
      movieFrames =  0;

      gaussian = fspecial('gaussian', 8, 8);
      
      for i = fromTrial:toTrial
        
        fprintf([num2str(i),' ']);
        fn = fullfile(path, 'PointGrey', files(i));
          
        fid = fopen(fn{1});
          
        fseek(fid, 0, 'eof');
        fileSize = ftell(fid);
        fseek(fid, 0, 'bof');
                    
        frameSize = width*height*2;
          
        totalFrames = fileSize / frameSize;
          
        inter = fread(fid, height * width * totalFrames, 'uint16=>single');
          
        waitbar(progressNumerator / progressDenominator, waitBar);
        progressNumerator = progressNumerator + 1;
          
        frames = reshape(inter, height, width, totalFrames);
          
        clear inter;
          
        fclose(fid);
          
        totalDuration = CGSave.Paradigm.Parameters.StimulationStopDelay + CGSave.Paradigm.Parameters.StimulationDelay + CGSave.Paradigm.Parameters.StimulationDuration;
        timestamps = linspace(0, totalDuration, totalFrames);
                    
        waitbar(progressNumerator / progressDenominator, waitBar);
        progressNumerator = progressNumerator + 1;

        frames = bsxfun(@rdivide, frames, mean(mean(frames, 2), 1));
        
        bgInterval = CGSave.Paradigm.Parameters.StimulationDelay;
        bgIndex = min(find(timestamps >= bgInterval, 1), numel(frames));
        bgSkipIndex = min(find(timestamps >= CGSave.Paradigm.Parameters.ShutterBugWorkaroundDelay, 1), numel(frames));
              
        bgFrames = frames(:, :, bgSkipIndex:bgIndex);      
        background = single(mean(bgFrames, 3));

        mx = size(frames, 3);
        frames = frames(:, :, bgSkipIndex:mx);        
        frames = O.lowPassFilter(frames);
        mx = size(frames, 3);
        frames = frames(:, :, (bgIndex - bgSkipIndex):mx);
        
        frames = bsxfun(@minus, frames, background);
        background = single(background) + 1e-6;
        frames = bsxfun(@rdivide, frames, background);
        
        frames = imfilter(frames, gaussian, 'replicate');
               
        if isscalar(movieFrames)
          movieFrames = frames;
        else
          minSize = min(size(frames, 3), size(movieFrames, 3));
          movieFrames = movieFrames(:, :, 1:minSize);
          frames = frames(:, :, 1:minSize);
          movieFrames = bsxfun(@plus, movieFrames, frames);
        end
          
        clear frames;
        clear timestamps;
          
        waitbar(progressNumerator / progressDenominator, waitBar);
        progressNumerator = progressNumerator + 1;
          
      end
      
      movieFrames = bsxfun(@rdivide, movieFrames, totalTrialsToLoad);
      movieFrames = imfilter(movieFrames, gaussian, 'replicate');

      close(waitBar);            

      waitBar = waitbar(0, 'Composing movie frames...');
      progressNumerator = 1;
      progressDenominator = double(size(movieFrames, 3));
      
      resultingFrames = zeros(O.frameHeight, O.frameWidth, 3, size(movieFrames, 3), 'uint8');
      
      for i = 1:size(movieFrames, 3)
        
        resultingFrames(:, :, :, i) = uint8(O.composePicture(movieFrames(:, :, i)) .* 255);

        waitbar(progressNumerator / progressDenominator, waitBar);
        progressNumerator = progressNumerator + 1;

      end

      clear movieFrames;
      
      close(waitBar);                          
      
      movieFileName = 'movie.dat';
      fid = fopen(movieFileName, 'w');
      fwrite(fid, resultingFrames, class(resultingFrames));
      fclose(fid);
      
      VideoViewer('Data', resultingFrames);
        
    end
    
    function onChaosDataClicked(O, ~, ~)
    
      state = get(O.checkChaosData, 'Value');
        
      val = 'off';
      if state == 1
        val = 'on';
      end
          
      set(O.editFileNamePrefix, 'Enable', val);
      set(O.labelFileNamePrefix, 'Enable', val);
      set(O.labelAngle, 'Enable', val);
      set(O.editAngle, 'Enable', val);
      set(O.labelWhisker, 'Enable', val);
      set(O.editWhisker, 'Enable', val);
                    
    end
    
  end % methods

end
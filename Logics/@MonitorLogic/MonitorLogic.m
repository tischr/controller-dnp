classdef MonitorLogic < Logic
    
  methods
    % CONSTRUCTOR
    function O = MonitorLogic(varargin)
      global CG;          
      O = setName(O,'Monitor');
      
      P = parsePairs(varargin);
      checkField(P,'Parameters',[])
      if ~isempty(P.Parameters)
        ModulePars = {{...
          'Device',eval([P.Parameters,'.Device{2}']),...
          'SRAI',eval([P.Parameters,'.SRAI']),...
          'ChAI',[],...
          'ChAO',[],...
          'ChDO',[],...
          'TerminalConfig','SingleEnded',...
          'AINames',[],...
          'ControlInterval',0.1,...
          'Name','Monitor',...
          'Saving','Trial'}};
      else
        ModulePars = {};
      end
      ModuleTypes = {'NI'};
      ModuleNames = {'NIDAQ'};
      O.HW.IDs.DAQ = addModules(O,ModuleTypes,ModuleNames,ModulePars);
    
      O.setInputChannel('NI',CG.Parameters.Setup.DAQID,CG.Parameters.Setup.DAQ.TrialSensor,NaN,['Trial'],'Digital');
      O.setDOutputChannel('NI',CG.Parameters.Setup.DAQID,CG.Parameters.Setup.DAQ.TrialChannel,'Trial');
      O.setDOutputChannel('NI',CG.Parameters.Setup.DAQID,CG.Parameters.Setup.DAQ.RecordingChannel,'Recording');

      % TRIAL AND CAMERA TRIGGER
      C_setDigitalNI(O.HW.IDs.DAQ,{'Trial','Recording'},[0,0]);
    end
  
  end
end
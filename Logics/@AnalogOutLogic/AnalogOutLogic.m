classdef AnalogOutLogic < Logic
  
  % DEFINE PROPERTIES
  properties (SetAccess = public)
    AnalogActive = 0;
    AnalogOutAvailable = [];
  end
  
  methods
    % CONSTRUCTOR
    function O = AnalogOutLogic(varargin)
      global CG;
      O = setName(O,'AnalogOut');
 
      O.AnalogOutAvailable = isfield(CG.Parameters.Setup,'Audio');
      if ~O.AnalogOutAvailable return; end
      P = parsePairs(varargin);
      checkField(P,'Parameters',[])
      if ~isempty(P.Parameters)
        ModulePars = {{...
          'Device',eval([P.Parameters,'.Device{2}']),...
          'SRAO',eval([P.Parameters,'.SRAO']),...
          'ChAO',eval([P.Parameters,'.ChAO']),...
          'ChDO',[],'ChAI',[],...
          'TerminalConfig','SingleEnded',...
          'AONames',{'AudioOut1','AudioOut2'},...
          'ControlInterval',0.1,...
          'Name','AudioOut',...
          'Saving','All'}};
      else
        ModulePars = {};
      end
      ModuleTypes = {'NI'};
      ModuleNames = {'AnalogOut'};
      ID = addModules(O,ModuleTypes,ModuleNames,ModulePars);
      O.HW.IDs.AnalogOut = ID;
      
      % ESTABLISH TRIGGER CONNECTION BETWEEN TRIAL TRIGGGER
      C_setTriggerNI(O.HW.IDs.AnalogOut,'Direction','AnalogOut','Source','External','Type','Start',...
        'Target',[CG.Parameters.Setup.Audio.Device{2},'/',CG.Parameters.Setup.Audio.TriggerChannel])
    end
      
    function prepareAnalogOut(O,ID,Stimulus)
      C_prepareNI(ID,'DataOut',Stimulus);
      C_startNI(ID);
    end
    
    function startAnalogOut(O,ID)
      global CG
      C_setDigitalNI(CG.Parameters.Setup.DAQID,'Trial',1);
    end

    function stopAnalogOut(O,ID)
      C_stopNI(ID);
      O.AnalogActive = 0;
    end
    
    function State = getAnalogOutState(O)
      global CG
      State = ~(CG.Sessions.NI(CG.Parameters.Setup.AudioID).SAO.IsDone);
    end
    

  end
end
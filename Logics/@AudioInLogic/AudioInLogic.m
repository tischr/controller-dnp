classdef AudioInLogic < Logic

  % DEFINE PROPERTIES
  properties (SetAccess = public)
    AudioActive = 0;
  end
  
  methods
    % CONSTRUCTOR
    function O = AudioInLogic(varargin)
      global CG;
      O = setName(O,'AudioIn');
           
      ModuleTypes = {'AudioIn'};
      ModuleNames = {'OctaCapture'};
      ModulePars = {[]};
      O = addModules(O,ModuleTypes,ModuleNames,ModulePars);
      
      O.HW.AudioInID = 1;
    end
      
    function prepareAudioIn(O,AudioInID)
      C_prepareAudioIn(AudioInID);
    end
    
    function startAudioIn(O,AudioInID)
      C_startAudioIn(AudioInID);
      O.AudioActive = 1;
    end

    function stopAudioIn(O,AudioInID)
      C_stopAudioIn(AudioInID);
      O.AudioActive = 0;
    end    

  end
end
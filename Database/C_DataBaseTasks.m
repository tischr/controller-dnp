function C_DataBaseTasks
  
global CG
if ~C_checkDatabase return; end

% LIST OPTIONS
Tasks = {'Add a new animal.','Add a new User','Add a measurement for an animal'};
Commands = {'C_addAnimal','C_addUser','C_addMeasurement'};

disp('Available Tasks : ');
for iT=1:length(Tasks)
  fprintf(['\t',num2str(iT),' : \t',Tasks{iT}],'\n');
end; endl;

% GET RESPONSE 
R = input('Please choose a task to execute : ');

% EVALUATE CHOICE
eval(Commands{R});


function DI = C_addMeasurement(varargin)
  
global CG
C_checkDatabase;

% PARSE ARGUMENTS
P = parsePairs(varargin);
checkField(P,'Animal','');
checkField(P,'Date','');
% checkField(P,'Date',datestr(now,31));
checkField(P,'DataType','');
checkField(P,'Value','');
 % checkField(P,'Certainty','');

% P.Date = datestr(datenum(P.Date),31);

% COLLECT ALL REQUIRED FIELDS, UNLESS THEY HAVE BEEN SET DIRECTLY
FN = fieldnames(P);
for i=1:length(FN)
  if isempty(P.(FN{i}))
    P.(FN{i}) = input(['Please enter the value of "',FN{i},'" : '],'s');
  end
end
R = mysql(['SELECT id FROM subjects WHERE name=''',P.Animal,'''']);
P.SubjectID = R.id;

% BUILD SQL STRING
SQL = ['INSERT INTO measurements SET '];
SQL = [SQL,'subjectid=',num2str(P.SubjectID),', '];
SQL = [SQL,'subject=''',num2str(P.Animal),''', '];
SQL = [SQL,'date=''',P.Date,''', '];
SQL = [SQL,'datatype=''',P.DataType,''', '];
SQL = [SQL,'value=''',P.Value,''';'];
% SQL = [SQL,'certainty=''',num2str(P.Certainty),''';'];

% EVALUTE SQL
disp('Inserting new animal into database...');
disp(SQL);
[R,A,ID] = mysql(SQL);
disp('completed successfully.');

 
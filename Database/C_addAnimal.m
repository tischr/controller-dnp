function DI = C_addAnimal(varargin)
  
global CG
C_checkDatabase;

% GET COLUMN NAMES
R = mysql('SELECT * FROM subjects');
CNames = fieldnames(R);
CNames = setdiff(CNames,'id');
% CHECK IF ALL FIELDS HAVE BEEN FILLED
P = parsePairs(varargin);
for i=1:length(CNames)
  checkField(P,CNames{i},'');
  % QUERY UNFILLED FIELDS
  if isempty(P.(CNames{i}))
    P.(CNames{i}) = input(['Please enter the value of "',CNames{i},'" : '],'s');
    tmp = str2num(P.(CNames{i}));
    if ~isempty(tmp) && isnumeric(tmp)
      P.(CNames{i}) = tmp;
    end
  end
end

% PREPARE SQL COMMAND FOR INSERTION
SQL = ['INSERT INTO subjects SET '];
for i=1:length(CNames)
  clear String;
  cData = P.(CNames{i});
  switch CNames{i}
    case 'birthday';
      cData = datenum(cData,'dd-mm-yyyy');
      String = ['''',datestr(cData,31),''''];
    otherwise
      if ischar(cData) String = ['''',cData,'''']; end;
      if isnumeric(cData) String = [num2str(cData)]; end
  end
  SQL = [SQL,' ',CNames{i},'=',String];
  if i<length(CNames)   SQL = [SQL,', ']; end
end

% EVALUTE SQL
disp('Inserting new animal into database...');
disp(SQL);
[R,A,ID] = mysql(SQL);
disp('completed successfully.');

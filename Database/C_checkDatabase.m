function State = C_checkDatabase(varargin)
% SETUP AND CHECK DATABASE ACCESS
  
global CG

P = parsePairs(varargin);
checkField(P,'Force',0);

% SET DEFAULT DATA BASE PARAMETERS UNLESS IT HAS BEEN SET BEFORE
if ~isfield(CG,'Misc') ||  ~isfield(CG.Misc,'Database')
  CG.Misc.Database.Server = 'dnp-backup.science.ru.nl';
  CG.Misc.Database.User = 'experimenter';
  CG.Misc.Database.Share = 'ControllerData';
  CG.Misc.Database.DBName = 'data';
  CG.Misc.Database.Password = input('Enter Password for Database : ','s');
  
  switch architecture
    case 'PCWIN';  CG.Misc.Database.MappedFolder = 'S:';
    case 'MAC';     CG.Misc.Database.MappedFolder = '/Volumes/Backup/';
    case 'UNIX';   CG.Misc.Database.MappedFolder = '/auto/Backup';
  end
end

% CHECK CONNECTION
if P.Force || ~isfield(CG.Misc.Database,'Available') || ~CG.Misc.Database.Available
  try
    switch architecture
      case 'PCWIN';
        R = mysql('SELECT * FROM users');
      otherwise
        mysql('open',CG.Misc.Database.Server,CG.Misc.Database.User,CG.Misc.Database.Password);
    end
    mysql(['use data']);
    State = 1;
  catch Exception
    fprintf(['  Error: \tC_checkDataBase (mysql) : ',Exception.message,'\n'...
      '  Solutions : \tcheck Network, VPN, Server?\n\n']);
    State = 0;
  end  
  CG.Misc.Database.Available = State;
else 
  State = CG.Misc.Database.Available;
end


function C_listRecordings(varargin)
  
global CG
C_checkDatabase;

% PARSE ARGUMENTS
P = parsePairs(varargin);
checkField(P,'Animal','');
checkField(P,'N',25);
checkField(P,'StartDate','');

Opts = '';
if ~isempty(P.Animal)
  AnimalOpt = ['animal=''',P.Animal,''' '];
else
  AnimalOpt = '';
end

if ~isempty(P.StartDate)
  DateOpt = [' date>=STR_TO_DATE(''',P.StartDate,''', ''%Y-%m-%d %H:%i:%s'')'];
  if ~isempty(AnimalOpt) DateOpt = ['AND ',DateOpt]; end
else 
  DateOpt = '';
end
Opts = [Opts,AnimalOpt,DateOpt];

if ~isempty(Opts) Opts = ['WHERE ',Opts]; end
% COLLECT ALL REQUIRED FIELDS, UNLESS THEY HAVE BEEN SET DIRECTLY
QUERY = ['SELECT * FROM recordings ',Opts];
R = mysql(QUERY);
iStart = max([1,length(R)-P.N]);
iStop = length(R);

for iR=iStart:iStop
  cR = R(iR);
  fprintf(['ID: ',num2str(cR.id),'\t ',cR.animal,'   \t\t R',num2str(cR.recording),' (Trials : ',num2str(cR.trials),') \t :\t',cR.paradigm,' \t(Light : ',num2str(cR.light),')\n']);
end

 
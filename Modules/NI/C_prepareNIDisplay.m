function C_prepareNIDisplay(ID,Force)

global CG; if nargin < 1 ID = 1; end

if exist('Force','var') && Force == 1 
  try; close(CG.Display.NI(ID).FIG); end
end

% CHECK IF FIGURE ALREADY EXISTS
if ~isfield(CG.Display,'NI') | ID>length(CG.Display.NI) | ~isfield(CG.Display.NI(ID),'FIG') | ...
    isempty(CG.Display.NI(ID).FIG) | ~ishandle(CG.Display.NI(ID).FIG)
   
  % COMPUTE POSITION OF FIGURE
  FigureSize = [300,500];
  cFIG = C_createModuleFigure('NI',ID,FigureSize);
  set(cFIG,'Toolbar','Figure')
  NAnalogIn = CG.Sessions.NI(ID).NAnalogIn;
  NAnalogOut = CG.Sessions.NI(ID).NAnalogOut;
  NDigitalOut = CG.Sessions.NI(ID).NDigitalOut;

  % PREPARE PLOTS ON FIGURE
  if NDigitalOut
    Division = [0.1,0.9]; Sep = 0.2;
  else
    Division = [0,1]; Sep = 0;
  end
  
  DC1 = axesDivide(1,Division,[0.32,0.09,0.65,0.875],[],Sep);
  DC2 = axesDivide(1,[NAnalogIn,NAnalogOut],...
    DC1{2},[],[(NAnalogIn>0)*(NAnalogOut>0)]);
  
  DC = [DC1(1);DC2(:)];
  
  FontSize = 7;
  TextOpt = {'Horiz','Right','FontSize',FontSize};
  SeparatorColor = [0.5,0.5,0.5];
  SpacingFactor = 1.2;
  CG.Display.NI(ID).DigitalColors = {[1,0,0],[0,1,0]};
  
  % DIGITAL
  if NDigitalOut > 0
    AH(1) = axes('Position',DC{1}); hold on; box off;
    for iP=1:NDigitalOut
      CG.Display.NI(ID).DigitalPH(iP) = ...
        plot(AH(1),iP,0,'Color',CG.Display.NI(ID).DigitalColors{1},'Marker','.','MarkerSize',30,...
        'ButtonDownFcn',{@LF_changeDOState,ID,iP,CG.Sessions.NI(ID).SD.Channels(iP).Name});
    end
    text(0,0,'DIO','FontSize',FontSize,'Horiz','right');
    set(gca,'XTick',[],'YTick',[],'XLim',[0.5,NDigitalOut+0.5]);
    for iC = 1:NDigitalOut
      text(iC,-1,CG.Sessions.NI(ID).SD.Channels(iC).Name,'Rotation',90,'FontSize',FontSize,'Horiz','right');
    end
    CG.Display.NI(ID).AH.DataDigital = AH(1);
    box on;
  end
  

  % ANALOG IN
  if NAnalogIn > 0
    CG.Display.NI(ID).PacketsDisplayed = round(CG.Sessions.NI(ID).SAI.Rate*CG.Display.NI(ID).DisplayDuration);
    Packets = [ -CG.Display.NI(ID).PacketsDisplayed : -1];
    Data0 = zeros(size(Packets));
    AH(2) = axes('Position',DC{2}); hold on; box on;
    
    for iP=NAnalogIn:-1:1
      % PLOT MINIMAL AND MAXIMAL LINES
      CG.Display.NI(ID).AnalogOffset(iP) = iP*CG.Parameters.NI(ID).AnalogInRange(2)*SpacingFactor;
      plot(AH(2),Packets([1,end]),repmat(CG.Display.NI(ID).AnalogOffset(iP),1,2),'Color',SeparatorColor);
      plot(AH(2),Packets([1,end]),repmat(CG.Display.NI(ID).AnalogOffset(iP)+CG.Parameters.NI(ID).AnalogInRange(2),1,2),'Color',SeparatorColor);
      
      % PLOT THRESHOLDS
      cThreshold = CG.Sessions.NI(ID).AnalogThresholds(iP);
      plot(AH(2),Packets([1,end]),repmat(CG.Display.NI(ID).AnalogOffset(iP) +cThreshold ,1,2),'Color',[1,0,0]);
      
      % PREPARE PLOTS FOR ANALOG
      CG.Display.NI(ID).AnalogPH(iP) = plot(AH(2),Packets,Data0+CG.Display.NI(ID).AnalogOffset(iP),'Color',hsv2rgb([0.6,1,1]));
      
      % PREPARE TEXT ON FIGURE
      CG.Display.NI(ID).AnalogLabel(iP,1) = ...
        text(1.05*Packets(1),CG.Display.NI(ID).AnalogOffset(iP)+0.5*CG.Parameters.NI(ID).AnalogInRange(2) ,...
        upper([CG.Sessions.NI(ID).SAI.Channels(iP).ID]),TextOpt{:});
      CG.Display.NI(ID).AnalogLabel(iP,2) = ...
        text(1.05*Packets(1),CG.Display.NI(ID).AnalogOffset(iP)-0*CG.Parameters.NI(ID).AnalogInRange(2) ,...
        CG.Sessions.NI(ID).SAI.Channels(iP).Name,TextOpt{:});
      CG.Display.NI(ID).AnalogVal(iP) = ...
        text(Packets(end)-5,CG.Display.NI(ID).AnalogOffset(iP)+0.5*CG.Parameters.NI(ID).AnalogInRange(2) ,['0V'],TextOpt{:},'FontSize',6);
    end
    set(AH(2),'YTick',[],'YLim',[CG.Parameters.NI(ID).AnalogInRange(2)/2,CG.Display.NI(ID).AnalogOffset(end)+0.6*CG.Parameters.NI(ID).AnalogInRange(2)],...
      'XLim',[Packets([1]),0],'XTick',[Packets([1,round(length(Packets)/2)]),0],...
      'XTickLabel',[Packets([1,round(length(Packets)/2+1)]),0]/CG.Sessions.NI(ID).SAI.Rate,...
      'FontSize',FontSize);
    CG.Display.NI(ID).AH.DataAnalog = AH(2);
  end
   
  if NAnalogOut > 0
    CG.Display.NI(ID).ScansDisplayed = round(CG.Sessions.NI(ID).SAO.Rate*CG.Display.NI(ID).DisplayDuration);
    AH(3) = axes('Position',DC{3}); hold on; box on;
    Packets = [ -CG.Display.NI(ID).ScansDisplayed : -1]';
    Data0 = zeros(size(Packets));
    for iP=NAnalogOut:-1:1
      % PLOT MINIMAL AND MAXIMAL LINES
      CG.Display.NI(ID).AnalogOffset(iP) = iP*CG.Parameters.NI(ID).AnalogOutRange(2)*SpacingFactor;
     plot(AH(3),Packets([1,end]),repmat(CG.Display.NI(ID).AnalogOffset(iP),1,2),'Color',SeparatorColor);
     plot(AH(3),Packets([1,end]),repmat(CG.Display.NI(ID).AnalogOffset(iP)+CG.Parameters.NI(ID).AnalogOutRange(2),1,2),'Color',SeparatorColor);
            
      % PREPARE PLOTS FOR ANALOG
      CG.Display.NI(ID).AnalogOutPH(iP) = plot(AH(3),Packets,Data0+CG.Display.NI(ID).AnalogOffset(iP),'Color',hsv2rgb([0.6,1,1]));
      
      % PREPARE TEXT ON FIGURE
      CG.Display.NI(ID).AnalogOutLabel(iP,1) = ...
        text(1.05*Packets(1),CG.Display.NI(ID).AnalogOffset(iP)+10*CG.Parameters.NI(ID).AnalogOutRange(2) ,...
        upper([CG.Sessions.NI(ID).SAO.Channels(iP).ID]),TextOpt{:});
      CG.Display.NI(ID).AnalogOutLabel(iP,2) = ...
        text(1.05*Packets(1),CG.Display.NI(ID).AnalogOffset(iP)-0*CG.Parameters.NI(ID).AnalogOutRange(2) ,...
        CG.Sessions.NI(ID).SAO.Channels(iP).Name,TextOpt{:});
      CG.Display.NI(ID).AnalogOutVal(iP) = ...
        text(Packets(end)-5,CG.Display.NI(ID).AnalogOffset(iP)+0.5*CG.Parameters.NI(ID).AnalogOutRange(2) ,['0V'],TextOpt{:},'FontSize',6);
    end
    set(AH(3),'YTick',[],'YLim',[CG.Parameters.NI(ID).AnalogOutRange(2)/2,CG.Display.NI(ID).AnalogOffset(end)+0.6*CG.Parameters.NI(ID).AnalogOutRange(2)],...
      'XLim',[Packets([1]),0],'XTick',[Packets([round(length(Packets)/2)],1),0],...
      'XTickLabel',[Packets([round(length(Packets)/2+1)],1),0]/CG.Sessions.NI(ID).SAO.Rate,...
      'FontSize',FontSize);
    CG.Display.NI(ID).AH.DataAnalog = AH(2);
  end
  
  % PREPARE TEXT ON FIGURE
  CG.Display.NI(ID).TimeH = uicontrol('style','text','String',['Time Acquired = 0s'],'Units','n','Pos',[0.05,0.02,0.9,0.02],'Horiz','Right','FontSize',FontSize,'BackgroundColor',[1,1,1]);  
end

function LF_changeDOState(H,E,ID,iDO,Name)
global CG;
C_setDigitalNI(ID,Name,~CG.Sessions.NI(ID).DOState(iDO));
C_updateNIDisplay(ID);

function C_setThresholdsNI(ID,varargin)

global CG;
if nargin<1 ID = 1; end;
P = parsePairs(varargin);
checkField(P,'ThreshDist',0.3);
checkField(P,'SensorNames',{'AniPosP1S1','AniPosP1S2','AniPosP1S3','AniPosP2S1','AniPosP2S2','AniPosP2S3'});

SensorNames = {CG.Paradigm.HW.Inputs.Name};
cPos = CG.Sessions.NI(ID).PacketsAcquired;
for iS = 1:length(P.SensorNames)
  Ind = strcmp(P.SensorNames{iS},SensorNames);
  Values(Ind) = mean(CG.Data.NI(ID).Analog(cPos-100:cPos,Ind));
  CG.Sessions.NI(ID).AnalogThresholds(Ind) = Values(Ind)-P.ThreshDist;
end

C_prepareNIDisplay(ID,1)
function C_saveNI(ID)

global CG;

if nargin<1 ID = 1; end

Parameters = CG.Parameters.NI;
Session =  rmfield(CG.Sessions.NI(ID),{'SAI','SAO','SD','CallbackAvail'});

switch CG.Sessions.NI(ID).Saving
  case 'Trial';
    StartPacket   = CG.Sessions.NI(ID).LastSavedPacket;
    StartEvent     = CG.Sessions.NI(ID).LastSavedEvent;
  case 'All';
    StartPacket   = 1;
    StartEvent     = 1;
  otherwise error(['Saving option "',P.Selection,'" not known.']);
end
StopPacket    = min([size(CG.Data.NI(ID).Analog,1),size(CG.Data.NI(ID).Time,1),CG.Sessions.NI(ID).PacketsAcquired]);
StopEvent      = length(CG.Events.NI(ID).Events);

% SELECT A SUBSET OF THE DATA FOR SAVING
% Indices = [CG.Paradigm.HW.Inputs.Index];
% Save = [CG.Paradigm.HW.Inputs.Save];
% SaveInd = Indices(Save);

% DIFFERENTIATE WHAT INFORMATION IS SAVED PER CHANNEL
% Digital : only save the time when up and down occurred
% Analog : downsample by a certain rate if desired (e.g. factor 10 for PosSensors)

Data.Analog = CG.Data.NI(ID).Analog(StartPacket+1:StopPacket,:);
Data.Time      = CG.Data.NI(ID).Time(StartPacket+1:StopPacket,:);
Events = CG.Events.NI(ID).Events(StartEvent+1:StopEvent);
save(CG.Files.NI(ID).FileNameTmp,'Events','Data','Parameters','Session','-v7.3');

CG.Sessions.NI(ID).LastSavedPacket = StopPacket;
CG.Sessions.NI(ID).LastSavedEvent   = StopEvent;

C_Logger('C_saveNI',['NI ',num2str(ID),' data saved (',num2str(StopPacket - StartPacket-1),' samples)\n']);
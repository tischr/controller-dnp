function C_discoverNI(Force)

global CG

if ~exist('Force','var') Force = 0; end

% CHECK IF AUDIO ALREADY DISCOVERED
if isfield(CG,'Devices') && isfield(CG.Devices,'NI') && ~Force
  % DEVICES ALREADY DISCOVERED
else % DISCOVER DEVICES
  switch architecture
    case 'PCWIN';
      AllD = daq.getDevices; k=0;
      for iD = 1:length(AllD)
        switch AllD(iD).Vendor.ID
          case 'ni'; k=k+1;
            D(k) = AllD(iD);
        end
      end
      iI = 0; iO = 0;
      ModelsToCheck = {'PCI-6259','PCIE-6321','PCIE-6351'};
      ModelNamesInternal = {'PCI6259','PCIE6321','PCIE6351'};
      Models = get(D,'Model'); if ~iscell(Models) Models = {Models}; end
      KnownTypes = {'AnalogInput','AnalogOutput','DigitalIO'};
      Directions = {'AI','AO','DIO'};
      for iD = 1:length(D) % LOOP OVER THE DEVICES
        DeviceName = D(iD).ID;
        Types = {D(iD).Subsystems.SubsystemType};
        for iT = 1:length(Types) % LOOP OVER INPUT AND OUTPUT
          clear DevicesByChannel Channels;
          cType = Types{iT};
          cInd = strcmp(cType,KnownTypes);
          if ~sum(cInd) continue; end
          ChNames = D(iD).Subsystems(iT).ChannelNames;
          for iC=1:length(ChNames)
            DevicesByChannel(iC).ChNum = str2num(ChNames{iC}(3:end));
            DevicesByChannel(iC).ID =  DeviceName;
            DevicesByChannel(iC).LocalChannel =  ChNames{iC};
          end
          CG.Devices.NI.(DeviceName).(Directions{cInd}).DevicesByChannel = DevicesByChannel;
        end
      end
      
    otherwise  error('Computer type not implemented for NI!');
  end
end
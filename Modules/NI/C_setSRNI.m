function C_setSRNI(ID,SR,Kind)

global CG; if nargin==1 ID = 1; end

CG.Sessions.NI(ID).(Kind).Rate = SR;
if CG.Sessions.NI(ID).(Kind).Rate~=SR error('Sampling Rate cannot be used exactly!'); end
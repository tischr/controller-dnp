function C_stopNI(ID,Save)

global CG;

if nargin<1 ID = 1; end
if nargin<2 Save = 0; end

% STOPPING ANALOG IN
%fprintf('C_stopNI : stopping ... ');
if ~isempty(CG.Sessions.NI(ID).SAI.Channels) 
  stop(CG.Sessions.NI(ID).SAI);
end; %fprintf(' stopped.\n');

% STOPPING ANALOG OUT
%fprintf('C_stopNI : stopping Analog Out... ');
if ~isempty(CG.Sessions.NI(ID).SAO.Channels) 
  stop(CG.Sessions.NI(ID).SAO);
end; % fprintf(' stopped.\n');

C_Logger('C_stopNI',['NI ',num2str(ID),' stopping...\n']);
set(CG.GUI.Main.Modules.NI(ID).StartButton,'Value',0);
try set(CG.GUI.Main.Modules.NI(ID).StartButton,'Value',0,'BackgroundColor',[1,1,1]); end
if Save C_saveNI(ID); end;
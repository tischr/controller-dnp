function C_resetNI

global CG;

if isfield(CG,'Sessions') && isfield(CG.Sessions,'NI') && isfield(CG.Sessions.NI,'S')
  for i=1:length(CG.Sessions.NI.S)
    try stop(CG.Sessions.NI.S(i)); end
    try delete(CG.Sessions.NI.S(i)); end
  end
  CG.Sessions.NI = rmfield(CG.Sessions.NI,'S');
end
daqreset;

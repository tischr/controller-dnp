function C_startNI(ID)

global CG;

if CG.Sessions.NI(ID).SAI.IsRunning == 0
  C_prepareNI(ID); 
end

% TRY TO START ANALOG IN
if ~isempty(CG.Sessions.NI(ID).SAI.Channels) 
  startBackground(CG.Sessions.NI(ID).SAI);
end
%  TRY TO START ANALOG OUT
if ~isempty(CG.Sessions.NI(ID).SAO.Channels) && CG.Sessions.NI(ID).SAO.ScansQueued
  startBackground(CG.Sessions.NI(ID).SAO); 
end  
C_Logger('C_startNI','NI triggered...\n');
try set(CG.GUI.Main.Modules.NI(ID).StartButton,'Value',1,'BackgroundColor',[1,0,0]); end

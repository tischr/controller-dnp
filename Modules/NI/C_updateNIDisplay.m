function C_updateNIDisplay(ID)
% UPDATE PLOTTING FOR THE ARDUINO
  
global CG; if nargin < 1 ID = 1; end

try 
  if isempty(CG.Display.NI(ID).FIG) | ~ishandle(CG.Display.NI(ID).FIG)
    C_prepareNIDisplay(ID); end
  
  NDigitalOut = CG.Sessions.NI(ID).NDigitalOut;
  NAnalogIn = CG.Sessions.NI(ID).NAnalogIn;
  NAnalogOut = CG.Sessions.NI(ID).NAnalogOut;
  
  ThreshColors = {[0,0,0],[1,0,0]};
  
  % UPDATE THE DIGITAL OUTPUT
  for iP = 1:NDigitalOut
    set(CG.Display.NI(ID).DigitalPH(iP),'Color',CG.Display.NI(ID).DigitalColors{CG.Sessions.NI(ID).DOState(iP)+1});
  end
  
  % DISPLAY THE MOST RECENT ANALOG IN DATA
  for iP = 1:NAnalogIn
    % COMPUTE THE CURRENT TIME-INDICES TO USE
    PacketsAcquired = CG.Sessions.NI(ID).PacketsAcquired;
    PacketsDisplayed =  CG.Display.NI(ID).PacketsDisplayed ;
    if PacketsAcquired > PacketsDisplayed
      cTimeInd  = [PacketsAcquired - PacketsDisplayed+1:PacketsAcquired];
      PreData = [];
    else
      cTimeInd = [1:PacketsAcquired];
      PreData = zeros(PacketsDisplayed - PacketsAcquired,1);
    end
    
    % DISPLAY DATA
    cData = CG.Data.NI(ID).Analog(cTimeInd,iP);
    if ~isempty(PreData) cData = [PreData;cData]; end
    set(CG.Display.NI(ID).AnalogPH(iP),'YData',cData + CG.Display.NI(ID).AnalogOffset(iP));
    IsGreater = CG.Sessions.NI(ID).AnalogThresholds(iP)>cData(end);
    set(CG.Display.NI(ID).AnalogVal(iP),'String',sprintf('%1.2fV',cData(end)),'Color',ThreshColors{IsGreater+1});
  end
  
  % DISPLAY THE MOST RECENT ANALOG OUT DATA
  for iP = 1:NAnalogOut
    % COMPUTE THE CURRENT TIME-INDICES TO USE
    ScansSent = CG.Sessions.NI(ID).SAO.ScansOutputByHardware;
    ScansDisplayed =  CG.Display.NI(ID).ScansDisplayed;
    if ScansSent > ScansDisplayed
      cTimeInd  = [ScansSent - ScansDisplayed+1:ScansSent];
      PreData = [];
    else
      cTimeInd = [1:ScansSent];
      PreData = zeros(ScansDisplayed - ScansSent,1);
    end
    
    % DISPLAY DATA
    if isfield(CG.Data.NI(ID),'AnalogOut')
      cData = CG.Data.NI(ID).AnalogOut(cTimeInd,iP);
      if ~isempty(PreData) cData = [PreData;cData]; end
      set(CG.Display.NI(ID).AnalogOutPH(iP),'YData',cData + CG.Display.NI(ID).AnalogOffset(iP));
      set(CG.Display.NI(ID).AnalogOutVal(iP),'String',sprintf('%1.2fV',cData(end)),'Color',[0,0,0]);
    end
    
    if NAnalogIn == 0 && ~isempty(cTimeInd)
      CG.GUI.Main.Modules.NI(ID).StartButton.String = sprintf('%i s',cTimeInd(end)/CG.Sessions.NI(ID).SAO.Rate);
    end
  end
  
  % UPDATE TIME DISPLAY
  if NAnalogIn > 0
    cDiff = diff(CG.Data.NI(ID).Time([1,CG.Sessions.NI(ID).PacketsAcquired],1));
    cSR = (CG.Sessions.NI(ID).PacketsThisIteration)/CG.Sessions.NI(ID).TimeThisIteration;
  elseif NAnalogOut > 0
    cSR = CG.Sessions.NI(ID).SAO.Rate;   
    cDiff = double(ScansSent)/double(cSR);
  end
  
  set(CG.Display.NI(ID).TimeH,'String',sprintf('Time = %3.2f s , SR = %4.0f Hz ',cDiff,cSR));
catch ME
  fprintf(['Error in C_updateDisplayNI : ',ME.message]);
end
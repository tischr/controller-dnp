function C_setDigitalNI(ID,Channels,States)

global CG;

if ischar(Channels) Channels = {Channels}; end
Names = '';
if iscell(Channels) % MATCH CHANNELS BY NAME
  ChannelNames = Channels;
  Names =[ChannelNames{:}];
  Channels = zeros(size(Channels));
  for i=1:length(ChannelNames)
    cInd = find(strcmp(ChannelNames{i},{CG.Sessions.NI(ID).SD.Channels.Name}));
    if ~isempty(cInd)
      Channels(i) = cInd;
    else
      disp(['Channel "',ChannelNames{i},'" does not exist.']);
    end
  end
end

% RECORD STATE
CG.Sessions.NI(ID).DOState(Channels) = States;
%disp(['Setting ',Names,' (Ch. ',num2str(Channels),') to ',num2str(States)]);
% WRITE STATES TO DEVICE OUTPUT
outputSingleScan(CG.Sessions.NI(ID).SD,CG.Sessions.NI(ID).DOState);
 C_Logger('C_setDigitalNI',['Set Digital Outputs\n']);
function C_deleteArduino(ID)
%% REMOVE AN ARDUINO COMMUNICATIOn

global CG; if nargin < 1 ID = 1; end

try
  delete(CG.Sessions.Arduino(ID).S);
  CG.Sessions.Arduino(ID).S = [ ];
end
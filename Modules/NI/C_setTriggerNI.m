function C_setTriggerNI(ID,varargin)

global CG;
if nargin<1 ID = 1; end
P = parsePairs(varargin);
checkField(P,'Direction')
checkField(P,'Source');
checkField(P,'Target');
checkField(P,'Type','Start');
checkField(P);

switch P.Direction
  case 'AnalogOut'; Direction = 'O';
  case 'AnalogIn';    Direction = 'I';
end

% ADD THE TRIGGER
addTriggerConnection(CG.Sessions.NI(ID).(['SA',Direction]),P.Source,P.Target,[P.Type,'Trigger']);

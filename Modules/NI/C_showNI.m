function C_showNI(ID,State)

global CG; if nargin==1 ID = 1; end


if isempty( CG.Display.NI(ID).FIG) | ~ishandle(CG.Display.NI(ID).FIG) 
  C_prepareNIDisplay(ID); 
end

CG.Display.NI(ID).State = State;
StateString = CG.Misc.StateStrings{State+1};
set(CG.Display.NI(ID).FIG,'Visible',StateString);
set(CG.GUI.Main.Modules.NI(ID).ShowButton,'Value',State);
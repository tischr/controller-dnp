function C_prepareNI(ID,varargin)

global CG;

if nargin<1 ID = 1; end;
P = parsePairs(varargin);
checkField(P,'InitDuration',60);
checkField(P,'DataOut',[])

if ~isfield(CG,'Sessions') | ~isfield(CG.Sessions,'NI')
  C_createNI;
end

CG.Events.NI(ID).Events = struct('Name',{},'Data',{},'Time',{});

% PREPARE ANALOG IN
if ~isempty(CG.Sessions.NI(ID).SAI.Channels) 
  NSteps = P.InitDuration*get(CG.Sessions.NI(ID).SAI,'Rate');
  NChannels =  length(get(CG.Sessions.NI(ID).SAI,'Channels'));
  
  % REMOVE THE DATA OF THE CURRENT NI ACQUISITION
  CG.Data.NI(ID).Time = zeros([NSteps,1],'double');
  CG.Data.NI(ID).Analog = zeros([NSteps,NChannels],'single');
  CG.Sessions.NI(ID).Iteration = 0;
  CG.Sessions.NI(ID).LastSavedPacket = 0;
  CG.Sessions.NI(ID).LastSavedEvent = 0;
  CG.Sessions.NI(ID).PacketsAcquired = 0;
  CG.Sessions.NI(ID).CurrentSize = 0;
  CG.Sessions.NI(ID).LastTime = 0;
end

% PREPARE ANALOG OUT, QUEUE THE SIGNAL
if  ~isempty(CG.Sessions.NI(ID).SAO.Channels) &&  ~isempty(P.DataOut)
  CG.Sessions.NI(ID).ScansSent = 0;
  CG.Data.NI(ID).AnalogOut = P.DataOut; % KEEP DATA FOR DISPLAY
  queueOutputData(CG.Sessions.NI(ID).SAO, P.DataOut);
end

if CG.Sessions.NI(ID).SAI.IsRunning | CG.Sessions.NI(ID).SAO.IsRunning 
  C_stopNI(ID,0); % Stop without saving
end

% UPDATE PARAMETERS
[Pars,PS] = C_getParametersModule('ModuleType','NI','IDType',ID);
if ~isempty(CG.Sessions.NI(ID).SAI.Channels) 
  C_setSRNI(ID,PS.SRAI,'SAI');
  ControlSamples = floor(PS.SRAI*PS.ControlInterval);
  CG.Display.NI(ID).DisplayDuration = PS.DisplayDuration;
  set(CG.Sessions.NI(ID).SAI,'NotifyWhenDataAvailableExceeds',ControlSamples);
end
if ~isempty(CG.Sessions.NI(ID).SAO.Channels) 
  C_setSRNI(ID,PS.SRAO,'SAO');
end

C_Logger('C_prepareNI','NI starting...\n');
try set(CG.GUI.Main.Modules.NI(ID).StartButton,'Value',0,'BackgroundColor',[0,1,0]); end
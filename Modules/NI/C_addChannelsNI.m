function [ID,Pars] = C_addChannelNI(ID,varargin)
%% Setup NI Acquisition Session

global CG;

P = parsePairs(varargin);
checkField(P,'ChannelType');
checkField(P,'Channels');
checkField(P,'Threshold',NaN);
checkField(P,'TerminalConfig','SingleEnded');
checkField(P,'Names','');
checkField(P,'UpdateFigure',1);

if ischar(P.Channels) P.Channels = {P.Channels}; end
if ischar(P.Names) P.Names = {P.Names}; end

P.Device = CG.Sessions.NI(ID).Device;

switch P.ChannelType
  case 'AI';
    ChNames = {CG.Devices.NI.(P.Device).AI.DevicesByChannel.LocalChannel};
    if length(P.Threshold) == 1 P.Threshold = repmat(P.Threshold,length(P.Channels)); end
    for iC = 1:length(P.Channels)
      cChannel = P.Channels(iC);
      if ~isnumeric(cChannel)
        cChannel = find(strcmp(cChannel,ChNames));
      end
      cID = CG.Devices.NI.(P.Device).AI.DevicesByChannel(cChannel).ID;
      cLocalChannel = CG.Devices.NI.(P.Device).AI.DevicesByChannel(cChannel).LocalChannel;
      C = addAnalogInputChannel(CG.Sessions.NI(ID).SAI,cID,cLocalChannel,'Voltage');
      C.TerminalConfig = P.TerminalConfig;
       NAI = length(CG.Sessions.NI(ID).SAI.Channels);
       CG.Sessions.NI(ID).NAnalogIn = NAI;
       CG.Sessions.NI(ID).SAI.Channels(NAI).Name = P.Names{iC};
       CG.Sessions.NI(ID).AnalogThresholds(NAI) = P.Threshold(iC);
    end
    
  case 'AO';
    % ADD OUTPUT CHANNELS
     ChNames = {CG.Devices.NI.(P.Device).AO.DevicesByChannel.LocalChannel};
    for iC = 1:length(P.Channels)
      cChannel = P.Channels(iC);
      if ~isnumeric(cChannel)
        cChannel = find(strcmp(cChannel,ChNames));
      end
      cID = CG.Devices.NI.(P.Device).AO.DevicesByChannel(cChannel).ID;
      cLocalChannel = CG.Devices.NI.(P.Device).AO.DevicesByChannel(cChannel).LocalChannel;
      addAnalogOutputChannel(CG.Sessions.NI(ID).SAO,cID,cLocalChannel,'Voltage');
      NAO = length(CG.Sessions.NI(ID).SAO.Channels);
      CG.Sessions.NI(ID).NAnalogOut = NAO;
      CG.Sessions.NI(ID).SAO.Channels(NAO).Name = P.Names{iC};
    end
    
  case 'DO';
    % ADD DIGITAL OUTPUT CHANNELS
    ChNames = {CG.Devices.NI.(P.Device).DIO.DevicesByChannel.LocalChannel};
    for iC = 1:length(P.Channels)
       cChannel = P.Channels(iC);
      if ~isnumeric(cChannel)
        cChannel = find(strcmp(cChannel,ChNames));
      end
      cLocalChannel = CG.Devices.NI.(P.Device).DIO.DevicesByChannel(cChannel).LocalChannel;
      addDigitalChannel(CG.Sessions.NI(ID).SD,P.Device,cLocalChannel,'OutputOnly');
      NDO = length(CG.Sessions.NI(ID).SD.Channels);
      CG.Sessions.NI(ID).NDigitalOut = NDO;
      CG.Sessions.NI(ID).SD.Channels(NDO).Name = P.Names{iC};
    end
    CG.Sessions.NI(ID).DOState = zeros(1,CG.Sessions.NI(ID).NDigitalOut);
    C_setDigitalNI(ID,[1:CG.Sessions.NI(ID).NDigitalOut],CG.Sessions.NI(ID).DOState);
end

if P.UpdateFigure; C_prepareNIDisplay(ID,1); end
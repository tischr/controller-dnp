function C_saveAudioIn(ID)

global CG;

if nargin<1 ID = 1; end

if isprop(CG.Paradigm,'ParadigmActive') && CG.Paradigm.ParadigmActive
  Data.Analog = CG.Data.AudioIn(ID).Data;
  Data.Time = CG.Data.AudioIn(ID).Time;
  save(CG.Files.AudioIn(ID).FileNameTmp,'Data','-v6'); % -v6 for fastest writing
end
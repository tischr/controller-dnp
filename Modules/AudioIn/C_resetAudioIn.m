function C_resetAudioIn

global CG;

if isfield(CG,'Sessions') && isfield(CG.Sessions,'AudioIn') && isfield(CG.Sessions.AudioIn,'S')
  for i=1:length(CG.Sessions.AudioIn.S)
    try stop(CG.Sessions.AudioIn.S(i)); end
    try delete(CG.Sessions.AudioIn.S(i)); end
  end
  CG.Sessions.AudioIn = rmfield(CG.Sessions.AudioIn,'S');
end

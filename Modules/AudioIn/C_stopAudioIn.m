function C_stopAudioIn(ID,Save)

global CG;

if nargin<1 ID = 1; end
if nargin<2 Save = 1; end

stop(CG.Sessions.AudioIn.S(ID));
C_Logger('C_stopAudioIn','AudioIn stopping...\n');
set(CG.GUI.Main.Modules.AudioIn(ID).StartButton,'Value',0);
try set(CG.GUI.Main.Modules.AudioIn(ID).StartButton,'Value',0,'BackgroundColor',[1,1,1]); end
if Save C_saveAudioIn(ID); end
function C_prepareAudioInDisplay(ID)

global CG;

if nargin < 1; ID = 1; end

% CHECK IF FIGURE ALREADY EXISTS
if isempty(CG.Display.AudioIn(ID).FIG) | ~ishandle(CG.Display.AudioIn(ID).FIG)

  % COMPUTE POSITION OF FIGURE
  cRes = [500,300];
  cFIG = C_createModuleFigure('AudioIn',ID,cRes);
  [DC,AH] = axesDivide(1,1,'C');
  CG.Display.AudioIn(ID).AH.Data = AH(1);
  CG.Display.AudioIn(ID).DataH = plot(0,0);
  set(AH(1),'YLim',[-1e-3,1e-3]);
end
function C_callbackAudioInIn(Src,Event,ID)

global CG;

ModuleType = 'AudioIn';
ModuleName = CG.Sessions.AudioIn(ID).Name;

% MOVE DATA TO INTERNAL VARIABLE
CG.Data.AudioIn(ID).Data(end+1:end+length(Event.Data),1) = Event.Data';
CG.Data.AudioIn(ID).Time(end+1:end+length(Event.TimeStamps),1) = Event.TimeStamps';

% UPDATE AudioIn DISPLAY
if CG.Display.AudioIn(ID).State  C_updateAudioInDisplay(ID); end

% UPDATE AudioIn OUTPUT
%if CG.Output.AudioIn(ID).State  C_updateAudioInOutput(ID); end

% DETECT EVENTS
Events = {}; EventData = {};

% PROCESS EVENTS
if ~isempty(Events)
  % PROCESS EVENTS
  for iE=1:length(Events)
    CG.Paradigm.processEvent(ModuleName,Events(iE));
  end
  
  % RECORD TIMING AND EVENTS
  C_addEvents(ModuleType,ID,Events);
end
function C_startAudioIn(ID)

global CG;

if CG.Sessions.AudioIn(ID).S.IsRunning == 0
  C_prepareAudioIn(ID); 
end

startBackground(CG.Sessions.AudioIn.S(ID));
C_Logger('C_startAudioIn','AudioIn triggered...\n');
try set(CG.GUI.Main.Modules.AudioIn(ID).StartButton,'Value',1,'BackgroundColor',[1,0,0]); end

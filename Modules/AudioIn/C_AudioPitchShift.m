function Data = C_AudioPitchShift(Data,Pars)
% Pars should be a structure with the fields:
% - Method : Resample/PhaseVocoder
% - Factor [number] : >1 = transfer to higher freqs, <1 = transfer to lower freqs
  
% PITCH SHIFT AUDIO
switch Pars.Method
  case '';
    P = length(Data); 
    Q = Pars.Factor
    Data = resample(Data,P,Q);
  case 'PhaseVocoder';
    cData = pvoc(cData,Pars{:});
end

 
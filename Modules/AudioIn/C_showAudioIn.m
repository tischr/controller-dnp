function C_showAudioIn(ID,State)

global CG;

if nargin==1 ID = 1; end

if isempty( CG.Display.AudioIn(ID).FIG) | ~ishandle(CG.Display.AudioIn(ID).FIG) 
  C_prepareAudioInDisplay(ID); 
end

CG.Display.AudioIn(ID).State = State;
StateString = CG.Misc.StateStrings{State+1};
set(CG.Display.AudioIn(ID).FIG,'Visible',StateString);
set(CG.GUI.Main.Modules.AudioIn(ID).ShowButton,'Value',State);
function C_prepareAudioIn(ID,Trigger)

global CG;

if nargin<2 Trigger = 'internal'; end
if nargin<1 ID = 1; end

% REMOVE THE DATA OF THE CURRENT AUDIOIN ACQUISITION
FN = fieldnames(CG.Data.AudioIn);
for iF=1:length(FN) CG.Data.AudioIn(ID).(FN{iF}) = []; end
CG.Events.AudioIn(ID).Events = struct('Name',{},'Data',{},'Time',{});

if ~isfield(CG,'Sessions') | ~isfield(CG.Sessions,'AudioIn')
  C_createAudioIn;
end

if CG.Sessions.AudioIn(ID).S.IsRunning
  C_stopAudioIn(ID,0); % Stop without saving
end

% UPDATE PARAMETERS
[Pars,PS] = C_getParametersModule('ModuleType','AudioIn','IDType',ID);
CG.Sessions.AudioIn(ID).SR = PS.SR; % SAMPLING RATE
ControlSamples = floor(PS.SR*PS.ControlInterval);
CG.Display.AudioIn(ID).DisplayDuration = PS.DisplayDuration;
set(CG.Sessions.AudioIn(ID).S,'NotifyWhenDataAvailableExceeds',ControlSamples);

C_Logger('C_prepareAudioIn','AudioIn starting...\n');
try set(CG.GUI.Main.Modules.AudioIn(ID).StartButton,'Value',1,'BackgroundColor',[0,1,0]); end

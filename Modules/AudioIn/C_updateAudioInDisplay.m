function C_updateAudioInDisplay(ID)
%% Setup an AudioIn streamer for a certain set of channels 
% This requires to discover devices, check that channels are not in use
% already
% 
global CG;

DisplaySamples = 1+round(CG.Display.AudioIn(ID).DisplayDuration * CG.Sessions.AudioIn(ID).SR);
Samples = length(CG.Data.AudioIn(ID).Time);
if Samples < DisplaySamples
  Inds = [1:Samples];
  cData = CG.Data.AudioIn(ID).Data(Inds);
  cTime  = CG.Data.AudioIn(ID).Time(Inds);
  cData = [zeros(DisplaySamples-Samples,1);cData];
  cTime = [linspace(-(DisplaySamples-Samples)/(CG.Sessions.AudioIn(ID).SR),...
    0,DisplaySamples-Samples)';cTime];
else
  Inds = [Samples-DisplaySamples+1:Samples];
  cData = CG.Data.AudioIn(ID).Data(Inds);
  cTime  = CG.Data.AudioIn(ID).Time(Inds);
end
cTime = cTime + 1/CG.Sessions.AudioIn(ID).SR;
set(CG.Display.AudioIn(ID).DataH,'YData',cData,'XData',cTime-cTime(1));
MAX =  max(abs(cData));
YLim = get(CG.Display.AudioIn(ID).AH.Data,'YLim');
if MAX > YLim(2); YLim = [-MAX,MAX]; end;

set(CG.Display.AudioIn(ID).AH.Data,'XLim',cTime([1,end])-cTime(1),...
  'XTick',cTime([1,end])-cTime(1),...
  'XTickLabel',cTime([1,end]),...
  'YLim',YLim);

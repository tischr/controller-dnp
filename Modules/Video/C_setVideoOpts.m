function C_setVideoOpts(ID,Trigger,varargin)

global CG;

if nargin<1 ID = 1; end

if length(varargin)>0
  P = parsePairs(varargin);
  checkField(P,'FramesPerTrigger',50);
end

switch CG.Sessions.Video.Device
  case 'PointGrey';
    % Setting everything to manual before starting to tweak parameters
    CG.Sessions.Video(ID).Source.TriggerDelayMode = 'Manual';
    CG.Sessions.Video(ID).Source.ExposureMode = 'Manual';
    CG.Sessions.Video(ID).Source.ShutterMode = 'Manual';
    CG.Sessions.Video(ID).Source.GainMode = 'Manual';
    CG.Sessions.Video(ID).Source.FrameRatePercentageMode = 'Manual';
    CG.Sessions.Video(ID).Source.Shutter = CG.Sessions.Video(ID).ShutterTime;

    % SAMPLING RATE
    FRP = 100 * CG.Sessions.Video(ID).SR / CG.Sessions.Video(ID).SRMax;

    switch Trigger
      case 'internal';
        CG.Sessions.Video(ID).Source.FrameRatePercentage = FRP;
        
        triggerconfig(CG.Sessions.Video(ID).S, 'manual', 'none', 'none');
        
      case 'external';
        set(get(CG.Sessions.Video(ID).S,'Source'),'FrameRatePercentage',FRP);
        if FRP == 100 % PRECISE CONTROL OVER FRAMERATE
          FramesPerTrigger = 1;
        else  % OPTMIZED FOR FAST IMAGING
          FramesPerTrigger = P.FramesPerTrigger;
        end
        CG.Sessions.Video(ID).Source.TriggerParameter = FramesPerTrigger;
        CG.Sessions.Video(ID).Source.TriggerParameter = 1;
        triggerconfig(CG.Sessions.Video(ID).S,'hardware','risingEdge','externalTriggerMode15-Source0');
        % triggerconfig(CG.Sessions.Video(ID).S,'hardware','risingEdge','externalTriggerMode14');

        %CG.Sessions.Video(ID).SourceInstant.Shutter = CG.Sessions.Video(ID).ShutterTime;
        %CG.Sessions.Video(ID).SourceInstant.FrameRatePercentage = FRP;
    end
    
    CG.Sessions.Video(ID).Source.TriggerDelayMode = 'Manual';
    CG.Sessions.Video(ID).Source.ExposureMode = 'Manual';
    CG.Sessions.Video(ID).Source.ShutterMode = 'Manual';
    CG.Sessions.Video(ID).Source.GainMode = 'Manual';
   % CG.Sessions.Video(ID).Source.FrameRatePercentageMode = 'Off';
    %CG.Sessions.Video(ID).Source.FrameRatePercentageMode = 'Manual';
          
    CG.Sessions.Video(ID).Source.Brightness = 0;
    CG.Sessions.Video(ID).Source.TriggerDelay = 0;
    CG.Sessions.Video(ID).Source.Exposure = 0.3;
    CG.Sessions.Video(ID).Source.Gain = 0; % Should probably stay low
    CG.Sessions.Video(ID).Source.Strobe2Duration = 1; % peak width, ms
    CG.Sessions.Video(ID).Source.Strobe2Polarity = 'High'; % STROBE IS GOING HIGH TO INDICATE SIGNAL 
    CG.Sessions.Video(ID).Source.Shutter = CG.Sessions.Video(ID).ShutterTime;

    CG.Sessions.Video(ID).SourceInstant.Strobe2 = 'off';
    CG.Sessions.Video(ID).Source.Strobe2 = 'on';
    
    % A frame read as a bug workaround - due to some bug the set parameters become
    % actualized only after the first frame reading. We encountered this
    % bug in R2014, not sure if we still need this read in R2015b
    %notUsed = getsnapshot(CG.Sessions.Video(ID).S);

    
  case 'MacVideo';
    triggerconfig(CG.Sessions.Video(ID).S,'manual','none','none');
end

function C_prepareVideoDisplay(ID)

global CG; if nargin < 1; ID = 1; end

% CHECK IF FIGURE ALREADY EXISTS
if ~isfield(CG.Display.Video(ID),'FIG')  | isempty(CG.Display.Video(ID).FIG) | ~ishandle(CG.Display.Video(ID).FIG)
  
  % COMPUTE POSITION OF FIGURE
  cRes = CG.Sessions.Video(ID).Resolution;
  cFIG = C_createModuleFigure('Video',ID,cRes/2);
  
  % PREPARE PLOTS ON FIGURE
  [DC,AH] = axesDivide(1,1,[0.05,0.075,0.9,0.875],'C');
  CG.Display.Video(ID).AH.Data = AH(1);
  CG.Display.Video(ID).DataH = imagesc(zeros(cRes(2),cRes(1)));
  colormap('gray');
  hold on;
  
  CG.Display.Video(ID).SnoutH = plot([cRes(1),cRes(1)],[1,cRes(2)],'Color',[0,1,0]);
  CG.Display.Video(ID).DataType = '';
  
  set(AH(1),'XLim',[1,cRes(1)],'YLim',[1,cRes(2)],'XTick',[],'YTick',[],'CLim',[0,255],...
    'CLimMode','M','XLimMode','M','YLimMode','M','XTickMode','Manual','YTickMode','M');
  
  % PREPARE TEXT ON FIGURE
  CG.Display.Video(ID).FrameH = text(1.05,-0.05,['FR = 0 Hz |  DR = 0 Hz'],'Units','n','Horiz','Right','FontSize',10);
end
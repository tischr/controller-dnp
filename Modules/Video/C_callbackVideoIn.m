function C_callbackVideoIn(Src,Event,ID)

global CG;

% MOVE DATA TO INTERNAL VARIABLE
C_Logger('C_callbackVideoIn','Getting frame...\n');
LoggingState = get(Src,'Logging');

if strcmp(LoggingState,'on') 
   Events = struct('Name',{},'Data',{},'Time',{});
   
  % CHECK CONTENTS OF FRAME BUFFER
  FramesAvailable = get(CG.Sessions.Video(ID).S,'FramesAvailable');
  if ~CG.Sessions.Video(ID).MemoryFullNotified && ...
      (FramesAvailable > CG.Sessions.Video(ID).FramesMax)
    
    Events(end+1).Name = 'MemoryFull';
    Events(end).Data = FramesAvailable;
    Events(end).Time = [now,Event.Data.AbsTime];
    CG.Sessions.Video(ID).MemoryFullNotified = 1;
  end
  
  if (CG.Display.Video(ID).State || CG.Sessions.Video(ID).ExtractPosition.Switch(1))
      
  % KEEP TRACK OF TIME ETC
    CG.Sessions.Video(ID).Iteration = CG.Sessions.Video(ID).Iteration + 1;
    
    % TRACK FRAMES
    LastFrames =  CG.Sessions.Video(ID).FramesAcquired;
    ThisFrames = FramesAvailable;
    CG.Sessions.Video(ID).FramesThisIteration = ThisFrames-LastFrames;
    CG.Sessions.Video(ID).FramesAcquired = ThisFrames;
    
    % TRACK TIME
    LastTime =  CG.Sessions.Video(ID).Time;
    ThisTime = datenum(Event.Data.AbsTime);
    CG.Sessions.Video(ID).TimeThisIteration = (ThisTime-LastTime)*(24*60*60);
    CG.Sessions.Video(ID).Time = ThisTime;
 
    LastFrame = peekdata(CG.Sessions.Video(ID).S,1);
    if isempty(LastFrame)
      return; 
    end
    
    if FramesAvailable % Turning off Audio Out for Position Estimate (Chao's Project)
      
      if CG.Sessions.Video(ID).ExtractPosition.Switch(1)
        
        EP = CG.Sessions.Video(ID).ExtractPosition;
        % ESTIMATE FRONT OF ANIMAL POSITION
        CG.Data.Video(ID).FrontPositionTime = ThisTime;
        DistProfile = mean(LastFrame(:,10:10:end),2);
        switch CG.Paradigm.HW.PlatformIDs.Guest
          case 1; cPos = 1; % Going to the left
          case 2; cPos = length(DistProfile); % Going to the right
        end
        GlobalBrightnessRef = DistProfile(cPos);
        DistProfile = DistProfile / GlobalBrightnessRef;
        MaxBrightness = double(CG.Sessions.Video(ID).ImageProperties.UpperBrightness)/GlobalBrightnessRef;
        cThreshold = MaxBrightness * CG.Sessions.Video(ID).BrightnessThreshold;
        
        switch CG.Paradigm.HW.PlatformIDs.Guest
          case 1; % Going to the left
            FrontPosition = find(DistProfile(end:-1:1)>cThreshold,1,'first');
          case 2; % Going to the right
            FrontPosition = find(DistProfile(1:end)>cThreshold,1,'first');
        end
        if isempty(FrontPosition)  FrontPosition = inf; end
        if FrontPosition == 1 FrontPosition = -inf; end
        PlatformEdgePosition = 65;
        CG.Data.Video(ID).FrontPosition = CG.Sessions.Video.Resolution(1)-FrontPosition;
        Distance = (CG.Data.Video(ID).FrontPosition - PlatformEdgePosition)/10; % about 0.1mm resolution
        Distance = max([Distance,0]);
        
        if Distance >= 0
          switch CG.Sessions.Video(ID).ExtractPosition.Function
            case 'Linear';
              MaxDistance = 40;
              Index = Distance/MaxDistance;
            case 'Local';
              StimulationRange = CG.Sessions.Video(ID).ExtractPosition.Range;
              if Distance < StimulationRange(1)  && Distance > StimulationRange(2)
                Index = 1;
              else
                Index = 0;
              end
          end
          %fprintf(['FrontPos : ',num2str(CG.Data.Video(ID).FrontPosition),' | Distance : ',num2str(Distance),'\n']);
          
          C_setFeedbackState(Index);
          
        end
      end
      % UPDATE VIDEO DISPLAY
      if CG.Display.Video(ID).State
        cFrame = flipud(LastFrame'); % ACCOUNT FOR ORIENTATION OF CAMERA
        C_updateVideoDisplay(ID,cFrame);
      end
    end
  end
end

% PROCESS EVENTS
if ~isempty(Events)
  for iE = 1:length(Events)    
    CG.Paradigm.processEvent(CG.Sessions.Video(ID).Name,Events(iE));   
  end
end
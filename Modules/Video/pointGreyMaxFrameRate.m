function maxFrameRate = pointGreyMaxFrameRate(videoMode)

    % Unfortunately, there is no way to 'calculate' PointGray maximum frame rate 
    % using IMA, and we need this data to correctly set the frame rate. Here I
    % hardcode maximum frame rate values for FL3-U3-13Y3M model taken from its
    % technical reference...
       
    switch videoMode
    case 'F7_Mono12_1280x1024_Mode0'
        maxFrameRate = 92;
    case 'F7_Mono12_640x512_Mode1'
        maxFrameRate = 230;
    case 'F7_Mono16_1280x1024_Mode0'
        maxFrameRate = 76;
    case 'F7_Mono16_640x512_Mode1'
        maxFrameRate = 230;
    case 'F7_Mono8_1280x1024_Mode0'
        maxFrameRate = 92;
    case 'F7_Mono8_640x512_Mode1'
        maxFrameRate = 230;
    case 'F7_Raw8_1280x1024_Mode0'
        maxFrameRate = 150;
    case 'F7_Raw8_640x512_Mode1'
        maxFrameRate = 480;
    case 'Mono16_1280x960'
        maxFrameRate = 81;
    case 'Mono8_1280x960'
        maxFrameRate = 97;                
    otherwise
        error('Unknown video mode - can''t set maximum frame rate value')
    end
    
    


function C_startVideo(ID)

global CG;

if nargin<1 ID = 1; end

if strcmp(get(CG.Sessions.Video(ID).S,'Running'),'off'); 
  C_prepareVideo(ID); 
end

if strcmp(get(CG.Sessions.Video(ID).S,'TriggerType'),'hardware') 
  C_stopVideo(ID); C_prepareVideo(ID); 
end

trigger(CG.Sessions.Video(ID).S);
C_Logger('C_startVideo','Video triggered...\n');
set(CG.GUI.Main.Modules.Video(ID).StartButton,'Value',1);

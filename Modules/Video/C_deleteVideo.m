function C_deleteVideo(ID)
%% REMOVE a Video streamer

global CG;

try 
  delete(CG.Sessions.Video.S);
  CG.Sessions.Video.S = [ ];
end
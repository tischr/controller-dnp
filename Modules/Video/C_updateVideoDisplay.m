function C_updateVideoDisplay(ID,Frame)

global CG;

% CHECK WHETHER FIGURE IS OPEN
if isempty(CG.Display.Video(ID).FIG) | ~ishandle(CG.Display.Video(ID).FIG)   
  C_prepareVideoDisplay(ID); end

% UPDATE VIDEO DISPLAY
set(CG.Display.Video(ID).DataH,'CData',Frame);

switch CG.Sessions.Video(ID).ImageProperties.DataType
  case 'uint8'; caxis(CG.Display.Video(ID).AH.Data,[0,255])
  case 'uint16'; caxis(CG.Display.Video(ID).AH.Data,[0,65536]);
  otherwise error('DataType not implemented.');
end

% SHOW SNOUT POSITION
% FrontPos = CG.Data.Video(ID).FrontPosition(end);
% set(CG.Display.Video(ID).SnoutH,'XData',repmat(FrontPos,1,2));

% UPDATE FRAME RATE INFORMATION
if CG.Sessions.Video(ID).Iteration>1
  set(CG.Display.Video(ID).FrameH,'String',...
    sprintf(['Frames = %i  |  FR = %3.1f Hz '],...
    CG.Sessions.Video(ID).FramesAcquired,...
    CG.Sessions.Video(ID).FramesThisIteration/CG.Sessions.Video(ID).TimeThisIteration));
end
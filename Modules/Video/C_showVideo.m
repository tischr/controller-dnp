function C_showVideo(ID,State)

global CG;

if nargin==1 ID = 1; end

CG.Display.Video(ID).State = State;
StateString = CG.Misc.StateStrings{State+1};
if isempty(CG.Display.Video(ID).FIG) || ~ishandle(CG.Display.Video(ID).FIG)   
  C_prepareVideoDisplay(ID); end
set(CG.Display.Video(ID).FIG,'Visible',StateString);
set(CG.GUI.Main.Modules.Video(ID).ShowButton,'Value',State);
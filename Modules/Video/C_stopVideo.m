function C_stopVideo(ID,Save)

global CG;

if nargin<1 ID = 1; end
if nargin<2 Save = 0; end

fprintf('Stopping Video...');
stop(CG.Sessions.Video(ID).S);
try  CG.Sessions.Video(ID).SourceInstant.Strobe2 = 'on'; end
try CG.Sessions.Video(ID).SourceInstant.Strobe2 = 'off'; end
fprintf('stopped.\n');

C_Logger('C_stopVideo','Video stopping...\n');
set(CG.GUI.Main.Modules.Video(ID).StartButton,'Value',0);
if Save C_saveVideo(ID); end

function C_saveVideo(ID)

global CG;

if nargin<1 ID = 1; end

FramesAvailable = get(CG.Sessions.Video(ID).S, 'FramesAvailable');

if FramesAvailable && isprop(CG.Paradigm,'ParadigmActive') && CG.Paradigm.ParadigmActive
  fprintf('Saving Video Data : starting ...');
  
  % REDUCE TO A CERTAIN AMOUNT OF FRAMES TO BE SAVED
  FramesToGet = FramesAvailable;
  
  % ACTUALLY GET THE DATA
  
  [Frames,Time,MetaData] = getdata(CG.Sessions.Video(ID).S,FramesToGet);
  
  fprintf([' ',num2str(size(Frames,4)),' Frames retrieved ...']);
  % SET FIRST TIME TO 0 (CAN BE NON-ZERO, IF BACKGROUND IMAGES ACQUIRED FIRST)

  
  % REMOVE UNWANTED FRAMES
  cInd = ones(FramesToGet,1);
  if length(CG.Data.Video(ID).RegionsToExclude) > 0  % INNER EXCLUSION EXISTS
    RTE = CG.Data.Video(ID).RegionsToExclude;
    for iR=1:length(RTE)
      cInd = cInd.*((Time-Time(1))<RTE{iR}(1))+((Time-Time(1))>RTE{iR}(2));
    end
  end
  SaveInd = find(cInd);
  
  % SAVE TIME AND METAINFORMATION ON THE FILE
  AT = reshape([MetaData.AbsTime],6,length(Time))';
  Time(:,2) = datenum(AT);
  Data.TimeAll = Time; % SAVE ALL FRAME TIMES
  Data.Time = Time(SaveInd,:);
  Data.SaveInd = SaveInd;
  Data.MetaData = MetaData;
  % ADD DIMENSIONS
  Data.NFrames = length(SaveInd);
  Data.Dims = size(Frames);
  Data.Dims(4) = Data.NFrames;
  Data.Resolution = Data.Dims([1:2]);
  cFileNameRoot = CG.Files.Video(ID).FileNameTmp(1:end-4);
  save(CG.Files.Video(ID).FileNameTmp,'Data','-v6');
  
  % SAVE VIDEO ITSELF
  cFileNameFrames= [cFileNameRoot,'.dat'];
  FID = fopen(cFileNameFrames,'w');
  fwrite(FID,Frames(:,:,:,SaveInd), class(Frames)); 
  fclose(FID);
  cFileNameFrames = escapeMasker(cFileNameFrames);
  C_Logger('C_saveVideo',...
    ['Video ',num2str(ID),' data saved (',num2str(Data.NFrames),' frames, ',cFileNameFrames,')\n']);
  fprintf(' done\n');
  
  if isfield(CG.Sessions.Video(ID), 'KeepInMemory') && CG.Sessions.Video(ID).KeepInMemory == 1
    CG.Sessions.Video(ID).Frames = Frames;
    CG.Sessions.Video(ID).Time = Time;
    CG.Sessions.Video(ID).MetaData = MetaData;
  end
  
end
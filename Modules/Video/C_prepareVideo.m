function C_prepareVideo(ID,Trigger,varargin)

global CG;

if nargin<2 Trigger = 'internal'; end
if nargin<1 ID = 1; end
P = parsePairs(varargin);
checkField(P,'FramesPerTrigger',255);

switch architecture
 case 'MAC'; 
   Name = CG.Sessions.Video(ID).Name; 
   C_deleteVideo(ID); C_createVideo('Name',Name,'ID',ID); C_showVideo(ID,1); % OTHERWISE SR SCREWED UP
end

if ~isfield(CG,'Sessions') | ~isfield(CG.Sessions,'Video')
  C_createVideo;
end

if  strcmp(get(CG.Sessions.Video(ID).S,'Running'),'on')
  C_stopVideo(ID,0); % Stop without saving
end

% INITIALIZE PARAMETERS
CG.Sessions.Video(ID).Time = 0;
CG.Sessions.Video(ID).FramesAcquired = 0;
CG.Sessions.Video(ID).FramesThisIteration = 0;
CG.Sessions.Video(ID).TimeThisIteration = 0;
CG.Sessions.Video(ID).Iteration = 0;
CG.Sessions.Video(ID).MemoryFullNotified = 0;
if ~isfield(CG.Data,'Video')   
  CG.Data.Video(ID) = struct('Time',[],'RegionsToExclude',[]); 
else
  FN = fieldnames(CG.Data.Video); for iF=1:length(FN) CG.Data.Video(ID).(FN{iF}) = []; end
end

% UPDATE PARAMETERS
[Pars,PS] = C_getParametersModule('ModuleType','Video','IDType',ID);
CG.Sessions.Video(ID).SR = PS.SR; % SAMPLING RATE
CG.Sessions.Video(ID).ControlInterval = PS.ControlInterval;
ControlSamples = floor(PS.SR*PS.ControlInterval);
set(CG.Sessions.Video(ID).S,'FramesAcquiredFcnCount',ControlSamples);
if ~isfield(CG.Sessions.Video(ID).ExtractPosition,'Switch')
  CG.Sessions.Video(ID).ExtractPosition.Switch = [0,0];
end
CG.Sessions.Video(ID).BrightnessThreshold = PS.BrightnessThreshold;
CG.Sessions.Video(ID).ImageProperties.UpperBrightness = NaN;

% ivanenko: added shutter time parameter
if isfield(PS,'ShutterTime')
    CG.Sessions.Video(ID).ShutterTime = PS.ShutterTime; 
end

%% CONFIGURE TRIGGER
C_setVideoOpts(ID,Trigger,'FramesPerTrigger',P.FramesPerTrigger);

%% EMPTY THE RAM OF THE VIDEO
flushdata(CG.Sessions.Video(ID).S);

%% ACQUIRE SOME IMAGE STATISTICS
cFrame = getsnapshot(CG.Sessions.Video(ID).S);
CG.Sessions.Video(ID).ImageProperties.DataType = class(cFrame);
CG.Sessions.Video(ID).ImageProperties.UpperBrightness = prctile(cFrame(:),[99]);
CG.Sessions.Video(ID).ImageProperties.Snapshot = cFrame;

%% MAXIMAL NUMBER OF FRAMES
[~,Tmp] = memory; BytesAvail = Tmp.PhysicalMemory.Available;
Tmp = whos('cFrame'); BytesImage = Tmp.bytes;
CG.Sessions.Video(ID).FramesMax = 0.4*BytesAvail/BytesImage;

%% START ENGINE
C_Logger('C_prepareVideo','Video starting...\n');
start(CG.Sessions.Video(ID).S);
function C_discoverVideo(Force)

global CG

if ~exist('Force','var') Force = 0; end

% CHECK IF AUDIO ALREADY DISCOVERED
if isfield(CG,'Devices') && isfield(CG.Devices,'Video') && ~Force
  % DEVICES ALREADY DISCOVERED
else % DISCOVER DEVICES
 Adaptors = imaqhwinfo;
 for iA=1:length(Adaptors.InstalledAdaptors)
   cAdaptor = Adaptors.InstalledAdaptors{iA};
   switch cAdaptor
     case 'pointgrey';
       Devices = imaqhwinfo(cAdaptor);
       CG.Devices.Video.PointGrey.Handles = Devices.DeviceInfo;
       CG.Devices.Video.PointGrey.DevicesByChannel = Devices.DeviceIDs;
     case 'macvideo';
       Devices = imaqhwinfo(cAdaptor);
       CG.Devices.Video.MacVideo.Handles = Devices.DeviceInfo;
       CG.Devices.Video.MacVideo.DevicesByChannel = Devices.DeviceIDs;
     case 'dcam'
   end
 end
end

function C_callbackKeyboard(Src,Event,ID)

global CG;
ModuleType = 'Keyboard';
ModuleName = CG.Sessions.Keyboard(ID).Name;

% MOVE DATA TO INTERNAL VARIABLE
C_Logger('C_callbackKeyboard','Getting keyboard input...\n');
CG.Data.Keyboard(ID).LastInput = get(Src,'CurrentCharacter');
CG.Data.Keyboard(ID).LastInputTime = now;

% UPDATE Keyboard DISPLAY
if CG.Display.Keyboard(ID).State C_updateKeyboardDisplay(ID); end

Events = struct(...
  'Name',['Key_',CG.Data.Keyboard(ID).LastInput],...
  'Data',[],...
  'Time',CG.Data.Keyboard(ID).LastInputTime);

if ~isempty(Events)
  % PROCESS EVENTS
  for iE = 1:length(Events) CG.Paradigm.processEvent(ModuleName,Events(iE)); end
  
  % RECORD TIMING AND EVENTS
  C_addEvents(ModuleType,ID,Events);
end
function C_prepareKeyboardDisplay(ID)

global CG;

if nargin < 1; ID = 1; end

% CHECK IF FIGURE ALREADY EXISTS
if isempty(CG.Display.Keyboard(ID).FIG) | ~ishandle(CG.Display.Keyboard(ID).FIG)
  
  cFIG = C_createModuleFigure('Keyboard',ID,[250,100]);
  
  set(cFIG,'KeyPressFcn',{@C_callbackKeyboard,ID},'Toolbar','none');
  
  % PREPARE TEXT ON FIGURE
  CG.Display.Keyboard(ID).CharH = text(0.5,0.5,['Last Input : '],'Units','n','Horiz','Center','FontSize',16);
  axis off;
  C_showKeyboard(ID,0);
end
function C_updateKeyboardDisplay(ID)

global CG;

if isempty(CG.Display.Keyboard(ID).FIG) | ~ishandle(CG.Display.Keyboard(ID).FIG)   
  C_prepareKeyboardDisplay(ID); end

set(CG.Display.Keyboard(ID).CharH,'String',['Last Input : ',CG.Data.Keyboard(ID).LastInput]);
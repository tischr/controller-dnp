function C_resetKeyboard

global CG;

if isfield(CG,'Sessions') && isfield(CG.Sessions,'Keyboard')
  CG.Sessions = rmfield(CG.Sessions,'Keyboard');
end

function C_showKeyboard(ID,State)

global CG;

if nargin==1 ID = 1; end

CG.Display.Keyboard(ID).State = State;
StateString = CG.Misc.StateStrings{State+1};
if ~ishandle(CG.Display.Keyboard(ID).FIG)   C_prepareKeyboardDisplay(ID); end
set(CG.Display.Keyboard(ID).FIG,'Visible',StateString);

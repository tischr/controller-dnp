function C_startKeyboard(ID)

global CG;

if nargin<1 ID = 1; end

if ~isfield(CG,'Events') | ~isfield(CG.Events,'Keyboard')
  C_prepareKeyboard(ID);
end

C_Logger('C_startKeyboard','Keyboard triggered...\n');
try set(CG.GUI.Main.Modules.Keyboard(ID).StartButton,'Value',1,'BackgroundColor',[1,0,0]); end


function ID = C_createKeyboard(varargin)
%% Setup a Keyboard listener

global CG;

%% CHECK FOR LAST SESSION ID
ID = C_getModuleID('ModuleType','Keyboard');

%% PARSE INITIALIZATION ARGUMENTS
P = parsePairs(varargin);
% PARAMETERS
Pars = C_getParametersModule('ModuleType','Keyboard','IDType',ID);
for iP = 1:size(Pars,1) checkField(P,Pars{iP}.Name,Pars{iP}.Value); end
% OPTIONS
checkField(P,'Name',['Keyboard',num2str(ID)]);
checkField(P,'Start',0);
checkField(P,'Reset',0);

%% INITIALIZE KEYBOARD IF NECESSARY
if P.Reset C_resetKeyboard; end;

%% GET NEW KEYBOARD SESSION
CG.Sessions.Keyboard(ID).S = 1; % Placeholder;
CG.Sessions.Keyboard(ID).Name = P.Name;
CG.Sessions.Keyboard(ID).Device = 'Keyboard';
CG.Sessions.Keyboard(ID).ModuleName = 'Keyboard';

%% PREPARE OUTPUTs
CG.Display.Keyboard(ID).State = 0;
CG.Display.Keyboard(ID).FIG = [];
C_prepareKeyboardDisplay(ID);

%% START Keyboard
if P.Start C_prepareKeyboard(ID); C_startKeyboard(ID); end

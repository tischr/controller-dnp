function C_prepareKeyboard(ID)

global CG;

if nargin<1 ID = 1; end

CG.Events.Keyboard(ID).Events = struct('Name',{},'Data',{},'Time',{});

if ~isfield(CG,'Sessions') | ~isfield(CG.Sessions,'Keyboard')
  C_createKeyboard;
end

C_Logger('C_prepareKeyboard','Keyboard starting...\n');

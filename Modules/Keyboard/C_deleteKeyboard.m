function C_deleteKeyboard(ID)
%% REMOVE A KEYBOARD INPUT

global CG;

try 
  CG.Sessions.Keyboard(ID).S = [ ];
end
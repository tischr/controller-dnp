function C_saveKeyboard(ID)

global CG;

if nargin<1 ID = 1; end

Parameters =  [];
Session =  rmfield(CG.Sessions.Keyboard(ID),'S'); 
Data = [];
Events = CG.Events.Keyboard;
save(CG.Files.Keyboard.FileNameTmp,'Events','Data','Session','Parameters','-v6');

C_Logger('C_saveKeyboard','Keyboard data saved.\n');
function C_stopKeyboard(ID,Save)

global CG;

if nargin<1 ID = 1; end

C_Logger('C_stopKeyboard','Keyboard stopping...\n');
try set(CG.GUI.Main.Modules.Keyboard(ID).StartButton,'Value',0,'BackgroundColor',[1,1,1]); end

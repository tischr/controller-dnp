function C_sendMessageArduino(ID,Command,Parameters)
% Interface to outgoing communication with the Arduino 
% Parameters:
%  ID              : ID of the Arduino in Controller
%  Command  : Defines Type of Action to perform (see list in switch below)
%  Parameters : Add the parameters for each command (see list in switch below for each)
% 
% With these a message to the Arduino is constructed which has the following format:
% 11 Chars/Uint8
% [ b {command char} {parameters} {zeros to fill to 11} ] 
% e.g. "bd000000000"  for starting (=[98,100,48,48,48,48,48,48,48,48,48], as uint8)   
% or   "bm1" for moving a motor backward

global CG;

%if strcmp(get(CG.Sessions.Arduino(ID).S,'status'),'closed') C_startArduino(ID); end

switch lower(Command) % ADD PARAMETERS FOR EACH
  case 'start'; % begin           
    Command = 'a'; % No Parameters
  case 'stop'; % halt     
    Command = 'h'; % No Parameters
  case 'setdo'; % digital          
    Command = 'd';
    PinChar      = sprintf('%02d',Parameters{1});
    ValueChar   = sprintf('%1d',Parameters{2});
    Command   = [Command,PinChar,ValueChar];
  case 'generatepulses'; % digital          
    Command = 'p'; 
    PulseDurationChar     = sprintf('%06d',Parameters{1}); % pulse duration in microseconds 
    Command = [Command,PulseDurationChar,'2','10'];
  case 'triggersend'; 
    Command = 't';
  case 'movemotor';             
    Command = 'm';
    ActuatorID   = num2str(Parameters{1});
    Direction     = num2str((-Parameters{2}+1)/2);
    StepsChar   = sprintf('%05d',round(Parameters{3}));
    Command =[Command,ActuatorID,Direction,StepsChar];
  case 'moveservo';              
    Command = 's'; %parameters : servo# (1 or 2), end angle, velocity (0-50)
    ActuatorID   = sprintf('%01d',Parameters{1});
    StepsChar    = sprintf('%03d',Parameters{2});
    DelayChar    = sprintf('%03d',Parameters{3}); % in ms
    Command =[Command,ActuatorID,StepsChar,DelayChar];    
  case 'custom';            
    Command = Parameters;
  case 'audiofeedback'
    Command = 'a';
  case 'opticalfeedback'
    Command = 'o';
    Default = exist('Parameters','var')>0;
    if ~Default Parameters = {0,0}; end
    DefaultChar         = sprintf('%d',Default);
    FrequencyChar    = sprintf('%05d',Parameters{1});
    DutyCycleChar    = sprintf('%03d',Parameters{2});
    Command =[Command,DefaultChar,FrequencyChar,DutyCycleChar];    
  otherwise
    error('Command not known.');
end

% COMPLETE MESSAGE
BytesPerCommand = CG.Parameters.Arduino.BytesPerCommand;
CommandFinal = ['b',Command,repmat('0',1,BytesPerCommand-length(Command)-1)];

% SEND MESSAGE
%disp(CommandFinal);
fprintf(CG.Sessions.Arduino(ID).S,CommandFinal);
C_Logger('C_sendMessageArduino',['Sent Command ',CommandFinal,' to Arduino ',num2str(ID),'\n']);
function C_closeArduino(ID)
  
global CG

if nargin<1 ID = 1; end

fclose(CG.Sessions.Arduino(ID).S);


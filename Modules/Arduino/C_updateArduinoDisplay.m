function C_updateArduinoDisplay(ID)
% UPDATE PLOTTING FOR THE ARDUINO
  
global CG; if nargin < 1 ID = 1; end

if isempty(CG.Display.Arduino(ID).FIG) | ~ishandle(CG.Display.Arduino(ID).FIG)   
  C_prepareArduinoDisplay(ID); end

NDigital =  CG.Sessions.Arduino(ID).NDigital;
NAnalog = CG.Sessions.Arduino(ID).NAnalog;
ThreshColors = {[0,0,0],[1,0,0]};

% COMPUTE THE CURRENT TIME-INDICES TO USE
PacketsAcquired = CG.Sessions.Arduino(ID).PacketsAcquired;
PacketsDisplayed = CG.Parameters.Arduino.PacketsDisplayed;
if PacketsAcquired > PacketsDisplayed
  cTimeInd  = [PacketsAcquired - PacketsDisplayed+1:PacketsAcquired];
  PreData = [];
else 
  cTimeInd = [1:PacketsAcquired];
  PreData = zeros(PacketsDisplayed - PacketsAcquired,1);
end

% DISPLAY THE MOST RECENT  DIGITAL DATA
for iP = 1:NDigital
  cData = bitget(CG.Data.Arduino(ID).Digital(cTimeInd),iP);
  if ~isempty(PreData) cData = [PreData;cData]; end
  set(CG.Display.Arduino(ID).DigitalPH(iP),'YData',cData + CG.Display.Arduino(ID).DigitalOffset(iP));
end

% DISPLAY THE MOST RECENT ANALOG DATA
for iP = 1:NAnalog
  cData = CG.Data.Arduino(ID).Analog(cTimeInd,iP);
  if ~isempty(PreData) cData = [PreData;cData]; end
  set(CG.Display.Arduino(ID).AnalogPH(iP),'YData',cData + CG.Display.Arduino(ID).AnalogOffset(iP));
  IsGreater = CG.Sessions.Arduino(ID).AnalogThresholds(iP)>cData(end);
  set(CG.Display.Arduino(ID).AnalogVal(iP),'String',sprintf('%1.2fV',cData(end)),'Color',ThreshColors{IsGreater+1});
end
cDiff = diff(CG.Data.Arduino(ID).Time([1,CG.Sessions.Arduino(ID).PacketsAcquired],1));
cSR = (CG.Sessions.Arduino(ID).PacketsThisIteration)/CG.Sessions.Arduino(ID).TimeThisIteration;
set(CG.Display.Arduino(ID).TimeH,'String',sprintf('Time = %3.2f s , SR = %4.0f Hz ',cDiff,cSR));
function C_showArduino(ID,State)

global CG; if nargin==1 ID = 1; end


if isempty( CG.Display.Arduino(ID).FIG) | ~ishandle(CG.Display.Arduino(ID).FIG) 
  C_prepareArduinoDisplay(ID); 
end

CG.Display.Arduino(ID).State = State;
StateString = CG.Misc.StateStrings{State+1};
set(CG.Display.Arduino(ID).FIG,'Visible',StateString);
set(CG.GUI.Main.Modules.Arduino(ID).ShowButton,'Value',State);
//to do: try to setup the datapins as interrupt pin as well

// define which pins to use
#define PULSEPIN 13 
#define TRIGGERPIN 22
#define NUMDATAPINS 5
#define TESTPIN 53
#define NUMLIGHTPINS 5
#define DEVICENAME "Due_AOF"
#define MAXAUDIOFREQUENCY 100000
#define MINAUDIOFREQUENCY 200
#define MAXLIGHTDUTY 85
#define CLOCKTOUSE TC_CMR_TCCLKS_TIMER_CLOCK1

// Main variables
char mode = 'x';
char oldmode = 'x';
boolean deviceRunning = false;
char para[9];

// frequencies to be used
const long Frequency_List[32] = {0,1000,1055,1113,1175,1239,1308,1380,1456,1536,1621,1710,1804,1904,2009,2119,2236,2359,2489,2627,2771,2924,3085,3255,3435,3624,3824,4034,4257,4491,4739,5000 };
// digital pins used to read current sound frequency
const int DataPins[NUMDATAPINS] = {33, 34, 35, 36, 37};
// these pins are C1-5 in Arduino Due; corresponding mask is 0bx111110, which is 62; for 4 pins (C1-C4) it is 30
// note: int type in Arduino Due is 32 bit by default
const unsigned int DataPinMask = 62;
// need to right shift the register value by 1 bit because pin PC0 is not in use
const int BitShift = 1;
// the port of the data pins; assuming all pins are on the same port
const Pio* DataPinPort = g_APinDescription[DataPins[1]].pPort;
// wait time between new pulsegeneration start; in microseconds
unsigned long waitTime = 0;
// state of pulse pin
int PulsePinState = LOW;

// pins used to deliver light pulses
// use PMW pins on the DUE
const int LightPins[NUMLIGHTPINS] = {2, 4, 6, 8};
int NumUsedLightPins = 2;
int DutysPin[2][16] = {{37, 73, 109, 145, 181, 217, 181, 145, 109,  73,  37,   0,   0,   0,   0,  0}, { 0,  0,   0,   0,   0,   0,  37,  73, 109, 145, 181, 217, 181, 145, 109, 73}};
int LPFreq = 200; // frequency of the light pulses
//const unsigned int LigthPinsMask = 126976;
//const int BitShiftLightPins = 12;
//const Pio* LightPinPort = g_APinDescription[LightPins[1]].pPort;


// constants for TimerCounter
const uint32_t channelToChNo[] = { 0, 0, 1, 1, 2, 2, 0, 0, 1, 1, 2, 2, 0, 0, 1, 1, 2, 2 };
const uint32_t channelToAB[]   = { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 };
Tc *channelToTC[] = {
      TC0, TC0, TC0, TC0, TC0, TC0,
      TC1, TC1, TC1, TC1, TC1, TC1,
      TC2, TC2, TC2, TC2, TC2, TC2 };
const uint32_t channelToId[] = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8 };
uint32_t pPinChannel= g_APinDescription[PULSEPIN].ulTCChannel; // channel No. on arduino board
Tc *pPinTC = channelToTC[pPinChannel];
uint32_t pPinTCCh = channelToChNo[pPinChannel];                // channel No. of the TC instance
static uint8_t PWMEnabled = 0;
static uint8_t pinEnabled[PINS_COUNT];
static uint8_t TCChanEnabled[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};


// variables need to be shared between main and ISR programs
volatile int FreqID = 0;
volatile boolean PGFlag = false;
volatile boolean testPinState = false;

// test with software trigger
//boolean OutputState = true;

void setup() {
  // setup different lines
  pinMode(PULSEPIN, OUTPUT);
  for (int i=0; i<NUMDATAPINS; i++){
    pinMode(LightPins[i], OUTPUT);
    }
  // not nesessary, but just to be safe
  pinMode(TRIGGERPIN, INPUT);
  for (int i=0; i<NUMDATAPINS; i++){
    pinMode(DataPins[i], INPUT);
    }
    
  // hardware interupt
  attachInterrupt(TRIGGERPIN, ReadPositionChange, CHANGE); 
  FreqID = 0;
  
  // use serial ouput to debug
  Serial.begin(115200);
  pinMode(TESTPIN, OUTPUT);
  
  //Serial.println(FreqID);
}

void loop() {
  if(Serial.available()>10) { 
    // Check which command has arrived    
    commandread();
  }
  // write device name to serial port
  if (mode=='i')  { 
    Serial.write(DEVICENAME); 
    Serial.write('\n'); 
    mode = 'x';
  }
  // audio feedback
  if (mode=='A')  { 
    AudioFeedBack();
  }
  // optogenetics
  if (mode=='O')  { 
    OptoStim();  
  }
}

// Read the command from the serial line
void commandread(){
  char bcheck= (char) Serial.read();
  if (bcheck=='b') {
    mode = (char) Serial.read();
    oldmode = mode;
    for (int ii=0; ii<9; ii++) para[ii] = (char) Serial.read();
  }
}

void newcommandcheck(){
  if (Serial.available()>10) { 
    char bcheck= (char) Serial.read();
    if (bcheck=='b') {
      mode = (char) Serial.read();
      //need to switch to new mode
      deviceRunning = false;
      PGFlag = false;
      oldmode = mode;
      for (int ii=0; ii<9; ii++) para[ii] = (char) Serial.read();
    }
  }
}

void AudioFeedBack(){
  AudioSetup();
  deviceRunning = true;
  while(deviceRunning){
    pulsegenerator(Frequency_List[FreqID]);
  }
  // stop pulse generation
  TC_Stop(pPinTC, pPinTCCh);
}

void OptoStim(){
  deviceRunning = true;
  while(deviceRunning){
    LightPulseGenerator();
  }
}

void AudioSetup(){
  // configure timer for tone generation
  ToneTimerSetup(pPinTC, pPinChannel);
}

void OptoSetup(){
  
  
}

// generate pulse with different frequency; used for auditory feedback
void pulsegenerator(int Freq)  {
  //unsigned long period;
 // unsigned long modTime;
  //unsigned long tstart;
  //unsigned long pulsedur; 
  PGFlag = true;

  //period = 1./Freq*1000000; //in microseconds
  //pulsedur = 1./Freq*1000000/2;

  // wait until old cycle end
  //if (waitTime > 8)
    //delayMicroseconds(waitTime - 8);

  //start timer to generate tone
  ToneTimerStart(pPinTC, pPinTCCh, Freq);
  
  // empty loop to hold the device here
  while (PGFlag){  
    newcommandcheck();
  };
  
}

// generate pulse for optogenetics
// analogwrite on PWM pins directly generate PWM pulses; however frequency will be fixed at 1000Hz (need to confirm)
void LightPulseGenerator(){
  //PGFlag = false;
  if (PGFlag == false){
    Serial.println(FreqID);
    for (int i = 0; i < NumUsedLightPins; i++){
      PMWWrite(LightPins[i], DutysPin[i][FreqID], LPFreq);
    }
    PGFlag = true;
  }
  while(PGFlag){
    newcommandcheck();
  }
}

// digital line interupt call back
// if triggered, read all lines to determine current frequency
void ReadPositionChange(){
  FreqID = 0;
  // direct read pin status from port register
  // assume all the pins are from the same port; in this case only need to read once
  // with Arduino Due each register is 32 bit
  int PortStatus = DataPinPort -> PIO_PDSR & DataPinMask;
  FreqID = PortStatus >> BitShift;
  // flag stop the pulsegenerator
  PGFlag = false;

  // change the state of test pin to check the trigger delay
  /*if (testPinState == HIGH){
    digitalWriteDirect(TESTPIN, HIGH);
    testPinState = LOW;
    } else {
    digitalWriteDirect(TESTPIN, LOW);
    testPinState = HIGH;
    }*/
}


// digital write with direct port manipulation, should be > 7x faster than digitalwrite
inline void digitalWriteDirect(int pin, boolean val){
  if(val) g_APinDescription[pin].pPort -> PIO_SODR = g_APinDescription[pin].ulPin;
  else    g_APinDescription[pin].pPort -> PIO_CODR = g_APinDescription[pin].ulPin;
}

inline int digitalReadDirect(int pin){
  return !!(g_APinDescription[pin].pPort -> PIO_PDSR & g_APinDescription[pin].ulPin);
}

int getPinMask(int imThePin[]){
  int imTheMask = 0;
  for (int i = 0; i < sizeof(imThePin)/sizeof(imTheMask); i++){
    imTheMask = imTheMask|g_APinDescription[imThePin[i]].ulPin;
  }
  return imTheMask;
}

void ToneTimerSetup(Tc *tc, uint32_t channel){
  pmc_set_writeprotect(false);
  uint32_t chNo = channelToChNo[channel];
  uint32_t chA  = channelToAB[channel];
  uint32_t interfaceID = channelToId[channel];
  pmc_enable_periph_clk(TC_INTERFACE_ID + interfaceID);
  
  // configure TimerCounter
  TC_Configure(tc, chNo, 
        CLOCKTOUSE |           // which clock to use
        TC_CMR_WAVE |         // Waveform mode
        TC_CMR_WAVSEL_UP_RC | // Counter running up and reset when equals to RC
        TC_CMR_EEVT_XC0 |     // Set external events from XC0 (this setup TIOB as output)
        TC_CMR_ACPA_CLEAR | 
        TC_CMR_ACPC_SET |
        TC_CMR_BCPB_CLEAR | 
        TC_CMR_BCPC_SET);

  // try to directly output TIOA or TIOB line signal
  PIO_Configure(g_APinDescription[PULSEPIN].pPort, 
                g_APinDescription[PULSEPIN].ulPinType,
                g_APinDescription[PULSEPIN].ulPin,
                g_APinDescription[PULSEPIN].ulPinConfiguration);
}

void ToneTimerStart(Tc *tc, uint32_t TCchannel, uint32_t freq)
{
   uint32_t rc = VARIANT_MCK / 2 / freq;
   TC_SetRA(tc, TCchannel, rc / 2); // 50% duty cycle square wave
   TC_SetRB(tc, TCchannel, rc / 2); // 50% duty cycle square wave
   TC_SetRC(tc, TCchannel, rc);
   TC_Start(tc, TCchannel);
}

// modified version of the builtin analogWrite; current version only works for PMW pins (2-13), but output frequency can be set
void PMWWrite(uint32_t ulPin, uint32_t ulValue, uint32_t freq) {
  uint32_t attr = g_APinDescription[ulPin].ulPinAttribute;

  if ((attr & PIN_ATTR_PWM) == PIN_ATTR_PWM) {
    //ulValue = mapResolution(ulValue, _writeResolution, PWM_RESOLUTION);

    if (!PWMEnabled) {
      // PWM Startup code
        pmc_enable_periph_clk(PWM_INTERFACE_ID);
        PWMC_ConfigureClocks(freq * PWM_MAX_DUTY_CYCLE, 0, VARIANT_MCK);
      PWMEnabled = 1;
    }

    uint32_t chan = g_APinDescription[ulPin].ulPWMChannel;
    if (!pinEnabled[ulPin]) {
      // Setup PWM for this pin
      PIO_Configure(g_APinDescription[ulPin].pPort,
          g_APinDescription[ulPin].ulPinType,
          g_APinDescription[ulPin].ulPin,
          g_APinDescription[ulPin].ulPinConfiguration);
      PWMC_ConfigureChannel(PWM_INTERFACE, chan, PWM_CMR_CPRE_CLKA, 0, 0);
      PWMC_SetPeriod(PWM_INTERFACE, chan, PWM_MAX_DUTY_CYCLE);
      PWMC_SetDutyCycle(PWM_INTERFACE, chan, ulValue);
      PWMC_EnableChannel(PWM_INTERFACE, chan);
      pinEnabled[ulPin] = 1;
    }

    PWMC_SetDutyCycle(PWM_INTERFACE, chan, ulValue);
    return;
  }

  if ((attr & PIN_ATTR_TIMER) == PIN_ATTR_TIMER) {
    // We use MCLK/2 as clock.
    const uint32_t TC = VARIANT_MCK / 2 / freq;

    //Serial.println(ulValue);
    // Map value to Timer ranges 0..255 => 0..TC
   // ulValue = mapResolution(ulValue, _writeResolution, TC_RESOLUTION);
    ulValue = ulValue * TC / 255;

    // Setup Timer for this pin
    ETCChannel channel = g_APinDescription[ulPin].ulTCChannel;
    uint32_t chNo = channelToChNo[channel];
    uint32_t chA  = channelToAB[channel];
    Tc *chTC = channelToTC[channel];
    uint32_t interfaceID = channelToId[channel];

    if (!TCChanEnabled[interfaceID]) {
      pmc_enable_periph_clk(TC_INTERFACE_ID + interfaceID);
      TC_Configure(chTC, chNo,
        TC_CMR_TCCLKS_TIMER_CLOCK1 |
        TC_CMR_WAVE |         // Waveform mode
        TC_CMR_WAVSEL_UP_RC | // Counter running up and reset when equals to RC
        TC_CMR_EEVT_XC0 |     // Set external events from XC0 (this setup TIOB as output)
        TC_CMR_ACPA_CLEAR | 
        TC_CMR_ACPC_CLEAR  |
        TC_CMR_BCPB_CLEAR | 
        TC_CMR_BCPC_CLEAR);     
      TC_SetRC(chTC, chNo, TC);
    }
    if (ulValue == 0) {
      if (chA)
        TC_SetCMR_ChannelA(chTC, chNo, TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_CLEAR);
      else
        TC_SetCMR_ChannelB(chTC, chNo, TC_CMR_BCPB_CLEAR | TC_CMR_BCPC_CLEAR);
    } else {
      if (chA) {
        TC_SetRA(chTC, chNo, ulValue);
        TC_SetCMR_ChannelA(chTC, chNo, TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_SET);
      } else {
        TC_SetRB(chTC, chNo, ulValue);
        TC_SetCMR_ChannelB(chTC, chNo, TC_CMR_BCPB_CLEAR | TC_CMR_BCPC_SET);
      }
    }
    if (!pinEnabled[ulPin]) {
      PIO_Configure(g_APinDescription[ulPin].pPort,
          g_APinDescription[ulPin].ulPinType,
          g_APinDescription[ulPin].ulPin,
          g_APinDescription[ulPin].ulPinConfiguration);
      pinEnabled[ulPin] = 1;
    }
    if (!TCChanEnabled[interfaceID]) {
      TC_Start(chTC, chNo);
      TCChanEnabled[interfaceID] = 1;
    }
    return;
  }

  // Defaults to digital write
  pinMode(ulPin, OUTPUT);
  //ulValue = mapResolution(ulValue, _writeResolution, 8);
  if (ulValue < 128)
    digitalWrite(ulPin, LOW);
  else
    digitalWrite(ulPin, HIGH);
}

void TC_SetCMR_ChannelA(Tc *tc, uint32_t chan, uint32_t v)
{
  tc->TC_CHANNEL[chan].TC_CMR = (tc->TC_CHANNEL[chan].TC_CMR & 0xFFF0FFFF) | v;
}

void TC_SetCMR_ChannelB(Tc *tc, uint32_t chan, uint32_t v)
{
  tc->TC_CHANNEL[chan].TC_CMR = (tc->TC_CHANNEL[chan].TC_CMR & 0xF0FFFFFF) | v;
}

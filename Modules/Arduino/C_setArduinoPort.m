function C_setArduinoDevice(ID,SerialPort);

global CG

% EXCEPTION UNTIL ARDUINO DUE WORKS WITH 230400
if strcmp(SerialPort,'COM7')
  cBaudRate = 115200;
else
  cBaudRate = CG.Parameters.Arduino.BaudRate;
end  

CG.Sessions.Arduino(ID).S=serial(SerialPort,...
  'BaudRate',cBaudRate,...
  'InputBufferSize',CG.Parameters.Arduino.InputBufferSize,...
  'BytesAvailableFcnMode',CG.Parameters.Arduino.BytesAvailableFcnMode,...
  'BytesAvailableFcn',{@C_callbackArduinoIn,ID},...
  'TimeOut',0.1);
CG.Devices.Arduino.HandlesAvailable = setdiff(CG.Devices.Arduino.HandlesAvailable,SerialPort);
CG.Sessions.Arduino(ID).SerialPort  = SerialPort;
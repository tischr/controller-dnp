function D = C_discoverArduino(varargin)

global CG

P = parsePairs(varargin);
checkField(P,'ReturnAvailable',0);

% GET ALL SERIAL PORTS DYNAMICALLY
switch architecture
  case 'PCWIN'; 
    DS = winqueryreg('name', 'HKEY_LOCAL_MACHINE', 'HARDWARE\DEVICEMAP\SERIALCOMM'); 
    D = {};
    for (iD = 1:length(DS))
      D{iD} = winqueryreg('HKEY_LOCAL_MACHINE', 'HARDWARE\DEVICEMAP\SERIALCOMM', DS{iD});
    end
  case 'MAC';
    SearchString = '/dev/tty.usbmodem';
    [R,DS] = system(['ls ',SearchString,'*']);
    k=0; 
    while ~isempty(DS)  k=k+1; [D{k},DS] = strtok(DS); end
    if isempty(D{end}) D = D(1:end-1); end
  otherwise error('Computer type not implemented!');
end

% FIND ALL ARDUINO RELATED SERIAL PORTS
D = sort(D);
CG.Devices.Arduino.Handles = D;

if P.ReturnAvailable
  if isfield(CG.Devices.Arduino,'HandlesAvailable')
    D = CG.Devices.Arduino.HandlesAvailable;
  else
    CG.Devices.Arduino.HandlesAvailable = D;
  end
end

function C_saveArduino(ID)

global CG;

if nargin<1 ID = 1; end
disp('Arduino saving ...')
if CG.Sessions.Arduino(ID).PacketsAcquired > 0
  
  Parameters = CG.Parameters.Arduino;
  Session =  rmfield(CG.Sessions.Arduino(ID),'S');
  
  LastSavedPacket = CG.Sessions.Arduino(ID).LastSavedPacket;
  LastSavedEvent   = CG.Sessions.Arduino(ID).LastSavedEvent;
  LastPacket           = CG.Sessions.Arduino(ID).PacketsAcquired;
  LastEvent             = length(CG.Events.Arduino(ID).Events);
  
  Data.Digital  = CG.Data.Arduino(ID).Digital(LastSavedPacket+1:LastPacket,:);
  Data.Analog = CG.Data.Arduino(ID).Analog(LastSavedPacket+1:LastPacket,:);
  Data.Time      = CG.Data.Arduino(ID).Time(LastSavedPacket+1:LastPacket,:);
  Events = CG.Events.Arduino(ID).Events(LastSavedEvent+1:LastEvent);
  save(CG.Files.Arduino(ID).FileNameTmp,'Events','Data','Parameters','Session','-v6');
  
  CG.Sessions.Arduino(ID).LastSavedPacket = LastPacket;
  CG.Sessions.Arduino(ID).LastSavedEvent   = LastEvent;
  disp('Arduino saved')
  C_Logger('C_saveArduino','Arduino data saved.\n');
end
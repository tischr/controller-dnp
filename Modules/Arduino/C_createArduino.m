function ID = C_createArduino(varargin)
%% Setup Arduino Communication

global CG; 

%% CHECK FOR LAST SESSION ID
ID = C_getModuleID('ModuleType','Arduino');

%% CHECK WHETHER CHANNELS ALREADY USED
C_discoverArduino;

%% PARSE INITIALIZATION ARGUMENTS
P = parsePairs(varargin);
% PARAMETERS
Pars = C_getParametersModule('ModuleType','Arduino','IDType',ID);
for iP = 1:length(Pars) checkField(P,Pars{iP}.Name,Pars{iP}.Value); end
% OPTIONS
checkField(P,'Name',['Arduino',num2str(ID)]);
checkField(P,'Start',0);
checkField(P,'Reset',0);

%% CREATE AND CONFIGURE SERIAL PORT CONNECTION TO ARDUINO
C_setArduinoPort(ID,P.SerialPort);

% FOLLOWING SECTION SHOULD BE GENERALIZED FOR DIFFERENT ARDUINOS
CG.Sessions.Arduino(ID).Name  = P.Name;
CG.Sessions.Arduino(ID).Device  = 'ArduinoMega';
CG.Sessions.Arduino(ID).NDigital = 8;
CG.Sessions.Arduino(ID).NAnalog = 8;
CG.Sessions.Arduino(ID).AnalogThresholds = NaN*zeros(1,CG.Sessions.Arduino(ID).NAnalog);

% INITIALIZE BASIC VARIABLES
CG.Display.Arduino(ID).State = 0;
CG.Display.Arduino(ID).FIG = [];
CG.Display.Arduino(ID).PacketsAcquired = 0;
CG.Data.Arduino(ID) = struct('Digital',[],'Analog',[],'Time',[],'UnfinishedPacket','');

%% PREPARE OUTPUTs
C_prepareArduinoDisplay(ID);

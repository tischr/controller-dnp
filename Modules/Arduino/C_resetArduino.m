function C_resetVideo

global CG;

if isfield(CG,'Sessions') && isfield(CG.Sessions,'Video') && isfield(CG.Sessions.Video,'S')
  for i=1:length(CG.Sessions.Video.S)
    try stop(CG.Sessions.Video.S(i)); end
    try delete(CG.Sessions.Video.S(i)); end
  end
  CG.Sessions.Video = rmfield(CG.Sessions.Video,'S');
end
imaqreset;

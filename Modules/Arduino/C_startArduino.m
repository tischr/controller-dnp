function C_startArduino(ID)

global CG; if nargin<1 ID = 1; end

if isempty(CG.Sessions.Arduino(ID).S) | strcmp(get(CG.Sessions.Arduino(ID).S,'Status'),'closed')
  C_prepareArduino(ID);
end

Command = 'Start';
C_sendMessageArduino(ID,Command);
C_Logger('C_startArduino','Arduino started...\n');
try set(CG.GUI.Main.Modules.Arduino(ID).StartButton,'Value',1,'BackgroundColor',[1,0,0]); end

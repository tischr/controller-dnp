function C_prepareArduinoDisplay(ID)

global CG; if nargin < 1 ID = 1; end

% CHECK IF FIGURE ALREADY EXISTS
if ID>length(CG.Display.Arduino) | ...
    isempty(CG.Display.Arduino(ID).FIG) | ~ishandle(CG.Display.Arduino(ID).FIG)
   
  % COMPUTE POSITION OF FIGURE
  FigureSize = [200,500];
  cFIG = C_createModuleFigure('Arduino',ID,FigureSize);

  NDigital = CG.Sessions.Arduino(ID).NDigital;
  NAnalog = CG.Sessions.Arduino(ID).NAnalog;

  % PREPARE PLOTS ON FIGURE
  DC = axesDivide(1,[1,2],[0.18,0.09,0.76,0.875],[],[0.2]);
  
   FontSize = 9;
  TextOpt = {'Horiz','Right','FontSize',FontSize};
  SeparatorColor = [0.5,0.5,0.5];
 
  Packets = [ -CG.Parameters.Arduino.PacketsDisplayed : -1];
  Data0 = zeros(size(Packets));
  
  SpacingFactor = 1.2;
  AH(1) = axes('Position',DC{1}); hold on; box on;
  for iP =NDigital:-1:1
    % PLOT MINIMAL LINE
    CG.Display.Arduino(ID).DigitalOffset(iP) = iP*CG.Parameters.Arduino.DigitalRange(2)*SpacingFactor;
   plot(AH(1),Packets([1,end]),repmat(CG.Display.Arduino(ID).DigitalOffset(iP)-0.1,1,2),'Color',SeparatorColor);
    
    % PREPARE PLOTS FOR DIGITAL
    CG.Display.Arduino(ID).DigitalPH(iP) = plot(AH(1),Packets,Data0+CG.Display.Arduino(ID).DigitalOffset(iP),'Color',hsv2rgb([(iP-1)/NDigital,1,1]));
        
    % PREPARE TEXT ON FIGURE
    if iP<8 cString = ['DI',n2s(iP)]; else cString = 'BUSY'; end
    CG.Display.Arduino(ID).DigitalLabel(iP) =...
      text(1.05*Packets(1),CG.Display.Arduino(ID).DigitalOffset(iP)+CG.Parameters.Arduino.DigitalRange(2)/2,cString,TextOpt{:});
  end
  set(AH(1),'YTick',[],'YLim',[0.5,CG.Display.Arduino(ID).DigitalOffset(end)+CG.Parameters.Arduino.DigitalRange(2)+0.5],...
    'XLim',[Packets([1]),0],'FontSize',FontSize);
    
  AH(2) = axes('Position',DC{2}); hold on; box on;
  for iP=NAnalog:-1:1
    % PLOT MINIMAL AND MAXIMAL LINES
    CG.Display.Arduino(ID).AnalogOffset(iP) = iP*CG.Parameters.Arduino.AnalogRange(2)*SpacingFactor;
    plot(AH(2),Packets([1,end]),repmat(CG.Display.Arduino(ID).AnalogOffset(iP),1,2),'Color',SeparatorColor);
    plot(AH(2),Packets([1,end]),repmat(CG.Display.Arduino(ID).AnalogOffset(iP)+CG.Parameters.Arduino.AnalogRange(2),1,2),'Color',SeparatorColor);
 
    % PLOT THRESHOLDS
    cThreshold = CG.Sessions.Arduino(ID).AnalogThresholds(iP);
    plot(AH(2),Packets([1,end]),repmat(CG.Display.Arduino(ID).AnalogOffset(iP) +cThreshold ,1,2),'Color',[1,0,0]);
    
    % PREPARE PLOTS FOR ANALOG
    CG.Display.Arduino(ID).AnalogPH(iP) = plot(AH(2),Packets,Data0+CG.Display.Arduino(ID).AnalogOffset(iP),'Color',hsv2rgb([(iP-1)/NAnalog,1,1]));
 
    % PREPARE TEXT ON FIGURE
    CG.Display.Arduino(ID).AnalogLabel(iP) = ...
      text(1.05*Packets(1),CG.Display.Arduino(ID).AnalogOffset(iP)+0.5*CG.Parameters.Arduino.AnalogRange(2) ,['AI',n2s(iP)],TextOpt{:});    
    CG.Display.Arduino(ID).AnalogVal(iP) = ...
      text(Packets(end)-5,CG.Display.Arduino(ID).AnalogOffset(iP)+0.5*CG.Parameters.Arduino.AnalogRange(2) ,['0V'],TextOpt{:},'FontSize',6); 
  end
  set(AH(2),'YTick',[],'YLim',[CG.Parameters.Arduino.AnalogRange(2)/2,CG.Display.Arduino(ID).AnalogOffset(end)+1.5*CG.Parameters.Arduino.AnalogRange(2)],...
    'XLim',[Packets([1]),0],'FontSize',FontSize);
  CG.Display.Arduino(ID).AH.DataDigital = AH(1);
  CG.Display.Arduino(ID).AH.DataAnalog = AH(2);
  
    % PREPARE TEXT ON FIGURE
  CG.Display.Arduino(ID).TimeH = text(1.05,-0.12,['Time Acquired = 0s'],'Units','n','Horiz','Right','FontSize',FontSize);  
end

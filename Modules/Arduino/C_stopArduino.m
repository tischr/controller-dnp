function C_stopArduino(ID,Save)
  
global CG; if nargin<1 ID = 1; end

% MAKE SURE ARDUINO'S LEAVE TRIGGER LOOP
try 
  C_sendMessageArduino(ID,'setdo',{CG.Parameters.Setup.Platforms(1).Triggerline,1});
  pause(0.02); C_sendMessageArduino(ID,'setdo',{CG.Parameters.Setup.Platforms(1).Triggerline,0});
end

if strcmp(get(CG.Sessions.Arduino(ID).S,'Status'),'open')
  Command = 'Stop';
  try 
    C_sendMessageArduino(ID,Command);
  catch
    fprintf(['Error: Stop Message could not be sent to Arduino',num2str(ID)]);
  end
  C_Logger('C_stopArduino','Arduino stopping...\n');
  try set(CG.GUI.Main.Modules.Arduino(ID).StartButton,'Value',0,'BackgroundColor',[1,1,1]); end
end
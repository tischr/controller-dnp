function C_preparePhidget(ID)

global CG; if nargin<1 ID = 1; end

% REMOVE THE DATA OF THE CURRENT PHIDGET ACQUISITION

if strcmp(CG.Sessions.Phidget(ID).Status,'open')
  C_closePhidget(ID);
end

CG.Sessions.Phidget(ID).S = ...
  PhidgetInterfaceKitSetup(CG.Parameters.Phidget.BoardType,...
  CG.Parameters.Phidget.BoardSN,CG.Parameters.Phidget.LibName);
CG.Sessions.Phidget(ID).Status = 'open';
C_Logger('C_preparePhidget','Phidget ready to start ...\n');

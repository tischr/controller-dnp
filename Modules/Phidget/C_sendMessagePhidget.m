function C_sendMessagePhidget(ID,Command)
% Interface to outgoing communication with the Phidget 
% Parameters:
%  ID              : ID of the Phidget in Controller
%  Command  : Defines Type of Action to perform (see list in switch below)

global CG; 

if ~strcmp(CG.Sessions.Phidget(ID).Status,'open')
  disp('Phidget not opened. Returning.'); return;
end

switch lower(Command) % ADD PARAMETERS FOR EACH
  case 'start'; % begin      
    Event = 1;
  case 'stop'; % halt
    Event = 2;
  otherwise
    if ~isnumeric(Command)
      error('Command not known.');
    else 
      Event = Command;
    end
end

% SEND MESSAGE
SendCheetahEvent(CG.Sessions.Phidget(ID).S,Event,CG.Parameters.Phidget.LibName,...
  CG.Parameters.Phidget.NumberOfBits);
CG.Sessions.Phidget(ID).LastMessage = Event;
C_updatePhidgetDisplay(ID);
C_Logger('C_sendMessagePhidget',['Sent Command ',num2str(Event),' to Phidget ',num2str(ID),'\n']);
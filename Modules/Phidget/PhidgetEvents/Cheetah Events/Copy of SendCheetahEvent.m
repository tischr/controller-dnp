function SendCheetahEvent(cfg)

if (~isfield(cfg,'event'))      cfg.event=0;            end
if (~isfield(cfg,'phIKhandle'))
    disp('Please pass a handle to the interface kit')
    return;
end

numberOfBits = 8;

handle = cfg.phIKhandle;
for n=0:15
    calllib('phidget21', 'CPhidgetInterfaceKit_setOutputState', handle, n, 0);
end

s=dec2bin(cfg.event);
if(length(s)<numberOfBits)
    for i=1:length(s)
        calllib('phidget21', 'CPhidgetInterfaceKit_setOutputState', handle, length(s)-i, str2num(s(i)));
    end;
    calllib('phidget21', 'CPhidgetInterfaceKit_setOutputState', handle, numberOfBits-1, 1);
else
    disp('Please input an event number smaller than 32768')
end

statePtr = libpointer('int32Ptr',0);
wait=true;
while(wait)
    calllib('phidget21', 'CPhidgetInterfaceKit_getOutputState', handle, numberOfBits-1, statePtr);
    state = get(statePtr,'Value');
    if(state==1)
        wait=false;
    end
end
disp(['Send event' num2str(cfg.event)])

function SendCheetahEvent(H,Event,LibName,NumberOfBits)
% Because the way Jos has connected power to the kits outputs
% there is a signal +5V when the output is 0 and there isn't one 
% when the output is 1

if(Event>2^NumberOfBits)
    disp(['Send an event that is smaller than ' 2^NumberOfBits]);
    return;
end

% Turn the event decimal number to a binary representation as string. 
% Turn the string into a vector.
% Inverse the logic (1->0 and 0->1) because of the kits resersed logic
% Add the vector at the end of a vector full of 1s so that the total
% elements are equal to the number of bits.
% Use this to switch the outputs
input_string=dec2bin(Event);
k=1;
for(i=1:length(input_string))
    input_string_prime(k)=input_string(i);
    input_string_prime(k+1)=blanks(1);
    k=k+2;
end
input_vector=str2num(input_string_prime);
output(1:NumberOfBits-length(input_vector)-1)=0;
output(NumberOfBits-length(input_vector):NumberOfBits-1)=input_vector;
    
for i=1:length(output)
    if(output(i)==1)
    calllib(LibName, 'CPhidgetInterfaceKit_setOutputState', H, length(output)-i, output(i));  
    end
end;
calllib(LibName, 'CPhidgetInterfaceKit_setOutputState', H, NumberOfBits-1, 1);
pause(0.004);
for i=1:length(output)
    if(output(i)==1)
    calllib(LibName, 'CPhidgetInterfaceKit_setOutputState', H, length(output)-i, 0);  
    end
end;
calllib(LibName, 'CPhidgetInterfaceKit_setOutputState', H, NumberOfBits-1, 0);
pause(0.003);


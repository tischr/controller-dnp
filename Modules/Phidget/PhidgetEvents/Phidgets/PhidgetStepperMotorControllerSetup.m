function handle=PhidgetStepperMotorControllerSetup()
%Opens interface kit with serial 155020 (the 8/8/8 one)

loadlibrary libphidget21 phidget21Matlab.h;
ptr = libpointer('int32Ptr',0);
calllib('libphidget21', 'CPhidgetStepper_create', ptr);
handle = get(ptr, 'Value');

calllib('libphidget21', 'CPhidget_open', handle, 92194);
if calllib('libphidget21', 'CPhidget_waitForAttachment', handle, 2500) == 0
    disp('Opened Stepper Motor Controller')
else
    disp('Could not open Stepper Motor Controller')
end


for i=1:4
    calllib('libphidget21', 'CPhidgetStepper_setCurrentLimit', handle, i, 0.26);
end

function H=PhidgetInterfaceKit888Setup()
%Opens interface kit with serial 155020 (the 8/8/8 one)

H = libpointer('int32Ptr',0);
calllib('phidget21', 'CPhidgetInterfaceKit_create', H);
calllib('phidget21', 'CPhidget_open', H, 95086);
if calllib('phidget21', 'CPhidget_waitForAttachment', H, 2500) == 0
    disp('Opened InterfaceKit')
else
    disp('Could not open InterfaceKit')
end



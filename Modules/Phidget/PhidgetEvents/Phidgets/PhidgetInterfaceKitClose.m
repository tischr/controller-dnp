function PhidgetInterfaceKitClose(H,LibName)

calllib(LibName, 'CPhidget_close', H);
calllib(LibName, 'CPhidget_delete', H);

C_Logger('PhidgetInterfaceKitClose','Closed InterfaceKit\n')

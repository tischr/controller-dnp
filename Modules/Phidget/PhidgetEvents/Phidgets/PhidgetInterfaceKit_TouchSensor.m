function PhidgetInterfaceKit_TouchSensor(cfg)

handle = cfg.phIKhandle;
channel=cfg.channel;
value= libpointer('int32Ptr',0);
t=1000;
tic;
while(t>500)
    if length(channel)>1
        for ChNr=1:length(channel)
            calllib('libphidget21', 'CPhidgetInterfaceKit_getSensorValue', handle, channel(ChNr), value);
            t=min(t,value.value);
        end
    else
        calllib('libphidget21', 'CPhidgetInterfaceKit_getSensorValue', handle, channel, value);
        t=value.value;
    end
end
toc


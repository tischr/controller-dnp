function H = PhidgetInterfaceKitSetup(BoardType,BoardSN,LibName)

H = libpointer('int32Ptr',0);
calllib(LibName, 'CPhidgetInterfaceKit_create', H);
calllib(LibName, 'CPhidget_open', H, BoardSN);
BoardString = ['PhidgetBoard (',BoardType,',',num2str(BoardSN),')'];
V = calllib(LibName, 'CPhidget_waitForAttachment', H, 2500);
switch  V
  case 0; C_Logger('PhidgetInterfaceKitSetup',['Opened Connection to ',BoardString,'\n'])
  otherwise; fprintf(['Failed to open connection to ',BoardString,'\n']); 
end

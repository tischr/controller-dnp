function C_closePhidget(ID)
  
global CG

if nargin<1 ID = 1; end
if strcmp(CG.Sessions.Phidget(ID).Status,'open') 
  PhidgetInterfaceKitClose(CG.Sessions.Phidget.S,CG.Parameters.Phidget.LibName);
  CG.Sessions.Phidget(ID).Status = 'closed';
end

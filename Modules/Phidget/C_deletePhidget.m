function C_deletePhidget(ID)
%% REMOVE AN PHIDGET COMMUNICATIOn

global CG; if nargin < 1 ID = 1; end

try
  delete(CG.Sessions.Phidget(ID).S);
  CG.Sessions.Phidget(ID).S = [ ];
end
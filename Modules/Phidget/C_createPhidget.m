function ID = C_createPhidget(varargin)
%% Setup Phidget Communication

global CG;

%% CHECK FOR LAST SESSION ID
ID = C_getModuleID('ModuleType','Phidget');

%% PARSE INITIALIZATION ARGUMENTS
P = parsePairs(varargin);
% PARAMETERS
Pars = C_getParametersModule('ModuleType','Phidget','IDType',ID);
for iP = 1:length(Pars) checkField(P,Pars{iP}.Name,Pars{iP}.Value); end

% LOAD PHIDGETS LIBRARY IF NECESSARY
loadphidget21;

% OPTIONS
checkField(P,'Name',['Phidget',num2str(ID)]);
checkField(P,'Start',0);
checkField(P,'Reset',0);

%% CREATE AND CONFIGURE SERIAL PORT CONNECTION TO PHIDGET
CG.Sessions.Phidget(ID).S = [];
CG.Sessions.Phidget(ID).Status = 'closed';
CG.Sessions.Phidget(ID).Name  = P.Name;
CG.Sessions.Phidget(ID).Device  = ['Phidget ',CG.Parameters.Phidget.BoardType];
CG.Sessions.Phidget(ID).SN  = CG.Parameters.Phidget.BoardSN;
CG.Sessions.Phidget(ID).NDigital = CG.Parameters.Phidget.NumberOfBits;

%% PREPARE OUTPUTs
CG.Display.Phidget(ID).State = 0;
CG.Display.Phidget(ID).FIG = [];
C_preparePhidgetDisplay(ID);

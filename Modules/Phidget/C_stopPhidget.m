function C_stopPhidget(ID)
  
global CG; if nargin<1 ID = 1; end

if strcmp(CG.Sessions.Phidget(ID).Status,'open')
  C_closePhidget(ID);
  C_Logger('C_stopPhidget','Phidget stopping...\n');
  set(CG.GUI.Main.Modules.Phidget(ID).StartButton,'Value',0);
end
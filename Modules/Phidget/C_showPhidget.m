function C_showPhidget(ID,State)

global CG; if nargin==1 ID = 1; end

if isempty( CG.Display.Phidget(ID).FIG) | ~ishandle(CG.Display.Phidget(ID).FIG) 
  C_preparePhidgetDisplay(ID); 
end

CG.Display.Phidget(ID).State = State;
StateString = CG.Misc.StateStrings{State+1};
set(CG.Display.Phidget(ID).FIG,'Visible',StateString);
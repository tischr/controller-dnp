function C_updatePhidgetDisplay(ID)
% UPDATE PLOTTING FOR A PHIDGET BOARD
  
global CG; if nargin < 1 ID = 1; end

if isempty(CG.Display.Phidget(ID).FIG) | ~ishandle(CG.Display.Phidget(ID).FIG)   
  C_preparePhidgetDisplay(ID); end

NDigital =  CG.Sessions.Phidget(ID).NDigital;

% DISPLAY THE MOST RECENT  DIGITAL DATA
CurrentState = dec2bin(CG.Sessions.Phidget(ID).LastMessage,NDigital);
for i=1:NDigital CurrentStateNum(i) = str2num(CurrentState(i)); end
set(CG.Display.Phidget(ID).StateH,'CData',CurrentStateNum);

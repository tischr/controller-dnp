function C_preparePhidgetDisplay(ID)

global CG; if nargin < 1 ID = 1; end

% CHECK IF FIGURE ALREADY EXISTS
if ID>length(CG.Display.Phidget) | ...
    isempty(CG.Display.Phidget(ID).FIG) | ~ishandle(CG.Display.Phidget(ID).FIG)
   
  % COMPUTE POSITION OF FIGURE
  FigureSize = [300,100];
  cFIG = C_createModuleFigure('Phidget',ID,FigureSize);
  colormap(HF_colormap({[1,1,1],[1,0,0]},[0,1])); 
  NDigital = CG.Sessions.Phidget(ID).NDigital;

  % PREPARE PLOTS ON FIGURE
  DC = axesDivide(1,1,[0.05,0.25,0.9,0.55]);
  
  axes('Pos',DC{1});
  CG.Display.Phidget(ID).StateH = imagesc(zeros(1,NDigital));
  set(gca,'XTick',[1:NDigital],'XTickLabel',{[16:-1:1]},'CLim',[0,1],'YTick',[],'FontSize',8);
end

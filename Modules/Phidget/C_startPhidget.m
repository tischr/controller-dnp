function C_startPhidget(ID)

global CG; if nargin<1 ID = 1; end

if isempty(CG.Sessions.Phidget(ID).S) | strcmp(CG.Sessions.Phidget(ID).Status,'closed')
  C_preparePhidget(ID);
end

C_Logger('C_startPhidget','Phidget started...\n');
set(CG.GUI.Main.Modules.Phidget(ID).StartButton,'Value',1);
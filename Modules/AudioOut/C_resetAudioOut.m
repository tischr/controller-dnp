function C_resetAudio

global CG;

if isfield(CG,'Sessions') && isfield(CG.Sessions,'Audio') && isfield(CG.Sessions.Audio,'S')
  for i=1:length(CG.Sessions.Audio.S)
    try stop(CG.Sessions.Audio.S(i)); end
    try delete(CG.Sessions.Audio.S(i)); end
  end
  CG.Sessions.Audio = rmfield(CG.Sessions.Audio,'S');
end

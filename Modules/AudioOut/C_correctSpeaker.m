function Data  = C_correctSpeakers(Data,varargin)

P = parsePairs(varargin);
checkField(P,'SR')
checkField(P,'Calibration'); % created by C_calibrateSpeakers

% CHECK THAT THE SRs ARE MATCHED
if P.SR ~= P.Calibration.SR error('Sampling Rates of Calibration and Data must match.'); end

FN = fieldnames(P.Calibration);
cInd = find(~cellfun(@isempty,strfind(FN,'IIR')));
cIIRName = FN{cInd};
  
% COMPUTE THE LEVEL CORRECTION FACTOR
dB80Factor  = 10^((80-P.Calibration.dBSPLRef)/20);

% CONVOLVE DATA WITH IMPULSE RESONSE
NSteps = length(Data);
Data = conv(Data,P.Calibration.(cIIRName)  * dB80Factor);

% CORRECT SHIFT AND LENGTH OF VECTOR
DelaySteps = P.Calibration.ConvDelay * P.SR;
Data = Data(DelaySteps+1:DelaySteps + NSteps);
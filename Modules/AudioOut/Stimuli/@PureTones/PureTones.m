classdef PureTones < SoundObject
  % PureTones produces random sequences of sinusoidal pure-tones
  % which can be used for estimating a neural tuning of a cell
  % All Stimuli are presented in one sequence and then
  
  properties
    ParametersFull = {...
      'FMin',1000,'numeric','Val>0';...
      'FMax',100000,'numeric','Val<SR/2';... %
      'FSteps',21,'numeric',inf;...
      'AMin',60,'numeric','Val>0';...
      'AMax',90,'numeric','Val<=80';...
      'ASteps',5,'numeric','';...
      'StimDur',0.1,'numeric','';...
      'PauseDur',0.05,'numeric','';...
      'Randomizations',5,'numeric',''...
      };
    LastSequence = [];
  end
  
  methods
    % CONSTRUCTOR
    function O = PureTones(varargin)
      O@SoundObject;
      O.Name = 'PureTones';
      O.assignParameters(varargin{:});
    end

    % CREATE SOUND WAVEFORM (REQUIRED FUNCTION)
    function W = Waveform(O,Index,IsRef,Mode);
      
      % GET PARAMETERS
      Par = O.getParStruct;
      
      O.checkIndex(Index);
          
      % GET PARAMETERS OF CURRENT Index
      % Random stream for drawing the random sequences
      R = RandStream('swb2712','Seed',Index*pi);
      
      % CREATE BASIC SINUSOIDS
      if ~isfield(O.V,'StimsBase')
        NSteps = round(Par.StimDur*O.SR);
        Time = [0:NSteps-1]'/O.SR;
        for iA = 1 : size(O.V.StimPars,1)
          for iF = 1 : size(O.V.StimPars,2)
            cFreq = O.V.StimPars(iA,iF).Freq;
            cAmp = O.V.StimPars(iA,iF).Amp;
            cDur = O.V.StimPars(iA,iF).Duration;
            % LOUDNESS NORMALIZED TO 1 at 80dB, SO THAT SCALLING WITH AMPLITUDE
            % CORRESPONDS TO 80dB
            cStim = C_StimConversion(cAmp,'dB2S')*sqrt(2)*sin(2*pi*cFreq*Time);
            
            % APPLY RAMP TO EACH SINUSOID
            cStim = C_addRamp(cStim,0.005,O.SR);
            O.V.StimsBase{iA,iF} = cStim;
          end
        end
      end
      
      % RANDOMIZE SEQUENCE
      [~,cPerm] = sort(R.rand(O.V.NStimuli,1));
      
      % RECORD WITHIN STIMULUS INFORMATION
      O.ParSequence = O.V.StimPars(cPerm);
      O.Permutation = cPerm;
      O.StartPos = zeros(O.V.NStimuli,1);
      O.StopPos = zeros(O.V.NStimuli,1);
      
      % ASSEMBLE THE CURRENT REALIZATION
      NSteps = round(O.Durations(Index)*O.SR);
      W = zeros(NSteps,1);
      for iP = 1:O.V.NStimuli
        cStim = O.V.StimsBase{cPerm(iP)};
        cStart = round(((iP-1)*Par.StimDur  + sum(O.V.Pauses(1:iP-1)) + O.PreDuration)*O.SR) + 1;
        O.StartPos(iP) = cStart;
        O.StopPos(iP) = cStart + length(cStim) - 1;
        cInds = [O.StartPos(iP):O.StopPos(iP)];
        W(cInds) = cStim;
      end
     
    end
  
    % UPDATE PROPERTIES OF SOUNDOBJECT (REQUIRED FUNCTION
    function O = Update(O);
      Par = O.getParStruct;
      
      % SET MAXIMAL INDEX OF SOUND OBJECT
      O.MaxIndex = Par.Randomizations;
      
      % SET NSTIMULI
      O.V.NStimuli = Par.FSteps * Par.ASteps;
      
      % SET Freq-Amplitude-Matrix
      O.V.Frequencies = logspace(log10(Par.FMin),log10(Par.FMax),Par.FSteps);
      O.V.Amplitudes = linspace(Par.AMin,Par.AMax,Par.ASteps);
      for iF = 1:Par.FSteps
        for iA = 1:Par.ASteps
          O.V.StimPars(iA,iF).Freq = O.V.Frequencies(iF);
          O.V.StimPars(iA,iF).Amp = O.V.Amplitudes(iA);
          O.V.StimPars(iA,iF).Duration = Par.StimDur;
        end
      end
      try; O.V = rmfield(O.V,'StimsBase'); end
      
      % SET PAUSES BETWEEN STIMULI
      O.V.Pauses = repmat(Par.PauseDur,O.V.NStimuli,1);
      
      % SET DURATIONS AND NAMES OF STIMULI
      O.Durations = zeros(O.MaxIndex,1);
      O.StimNames = cell(O.MaxIndex,1);
      NStimuli = O.V.NStimuli; 
      for Index=1:O.MaxIndex
        % COMPUTE STIMULUS LENGTHS FOR DMS AHEAD OF WAVEFORM
        O.Durations(Index) = NStimuli*(Par.StimDur + Par.PauseDur) + O.PreDuration+O.PostDuration;
        O.StimNames{Index} = ['Randomization ',n2s(Index)];
      end
    end
    
  end
end
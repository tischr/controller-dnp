classdef SoundObject < handle
  % BASIC SOUND OBJECT CLASS
  % All other SoundObjects derive from this class
  
  properties
    MaxIndex = NaN;
    Durations = NaN;
    StimNames = {};
    ParSequence = []; % Struct of parameters of the stimuli in sequence
    StartPos = []; % Index of starting of the stimuli (w.r.t. the SR)
    StopPos = []; % Index of stopping of the stimuli (w.r.t. the SR)
    Permutation = []; % Internal permutation of stimuli
    Name = '';
    SR = NaN;
    PreDuration = 0;
    PostDuration = 0;
    Parameters = [];
    V = []; % Internal Variables
  end
  
  methods
    % CONSTRUCTOR
    function O = SoundObject(varargin)
      
    end
  
    % ASSIGN PARAMETERS TO THEIR USUAL LOCATIONS
    function O = assignParameters(O,varargin)
      P = parsePairs(varargin);
      for iP=1:size(O.ParametersFull,1)
        cName = O.ParametersFull{iP,1};
        cType = O.ParametersFull{iP,3};
        cRestrictions = O.ParametersFull{iP,4};
        O.Parameters.(cName) = struct('Value',[],'Type',cType,'Restrictions',cRestrictions);
        if isfield(P,cName)
          cValue = P.cName;
        else
          cValue = O.ParametersFull{iP,2};
        end
        O.set( O.ParametersFull{iP,1} , cValue , 0);
      end
      O = O.Update;
    end
    
    % ASSIGN A SINGLE PARAMETER
    function O = set(O,Name,Value,Update)
      if nargin < 4 Update = 1; end
      % ADD RESTRICTION CHECKING BASED ON 4th ENTRY IN PARAMETERSFULL!
      O.Parameters.(Name).Value = Value;
      if Update O.Update; end
    end
      
    % GET A SINGLE PARAMETER
    function Value = get(O,Name)
      cType = O.Parameters.(Name).Type;
      switch cType
        case 'numeric';
          Value = O.Parameters.(Name).Value;
        case 'cell';  
          Value = O.Parameters.(Name).Options{O.Parameters.(Name).Value};
          Par.(Name) = get(O,Name);
        otherwise error('FieldType not implemented!');
      end
      
    end

    % GET ALL PARAMETERS AS A STRUCTURE
    function  Par = getParStruct(O)
      ParNames = fieldnames(O.Parameters);
      for iP=1:length(ParNames)
        Par.(ParNames{iP}) = O.get(ParNames{iP});
      end
    end
    
    % CHECK WHETHER Index EXCEEDED AVAILABLE INDICES
    function checkIndex(O,Index)
      if Index > O.MaxIndex error('Number of available Stimuli exceeded'); end
    end
    
    % PROVIDE NEW RANDOMIZATION FOR THE CURRENT REPETITION
    function Indices = Randomize(O)
      Indices = randperm(O.MaxIndex);
    end
    
  end
end
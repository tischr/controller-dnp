function Vec = C_addRamp(Vec,RampDur,SR)

NSteps=floor(RampDur*SR);
Time = [0:NSteps-1]'/SR;
FRamp = 1/(2*RampDur);
Ramp = 0.5-0.5*cos(2*pi*FRamp*Time);

Vec(1:NSteps) = Ramp.*Vec(1:NSteps);
Vec(end-NSteps+1:end) = flipud(Ramp).*Vec(end-NSteps+1:end);

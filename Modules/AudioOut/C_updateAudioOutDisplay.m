function C_updateAudioDisplay(ID)
%% Setup an audio streamer for a certain set of channels 
% This requires to discover devices, check that channels are not in use
% already
% 
global CG;

set(CG.Display.Audio(ID).DataH,'YData',CG.Data.Audio(ID).Data,'XData',CG.Data.Audio(ID).Time);
set(CG.Display.Audio(ID).AH.Data,'XLim',CG.Data.Audio(ID).Time([1,end]),'XTick',CG.Data.Audio(ID).Time([1,end/2,end]));
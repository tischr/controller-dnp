function C_startAudio(ID)

global CG;

startBackground(CG.Sessions.Audio.S(ID));
C_Logger('C_startAudio','Audio triggered...\n');
try set(CG.GUI.Main.Modules.Audio(ID).StartButton,'Value',1,'BackgroundColor',[1,0,0]); end
function C_stopAudio(ID)

global CG;

stop(CG.Sessions.Audio.S(ID));
C_Logger('C_stopAudio','Audio stopping...\n');
set(CG.GUI.Main.Modules.Audio(ID).StartButton,'Value',0);
function Stimuli  = C_getStimuli(varargin)

global CG;
if isempty(CG); C_setupEnvironment; end

P = parsePairs(varargin);
checkField(P,'Module','Audio');
P.Path = [CG.Files.CodePath,'Modules',filesep,P.Module,'Out',filesep,'Stimuli',filesep];

Files = dir([P.Path,'@*']);
Directory = [Files.isdir];
Stimuli = {Files(Directory).name};
for iS=1:length(Stimuli) Stimuli{iS} = Stimuli{iS}(2:end); end
function [Speakers,FileNames]  = C_getSpeakers(varargin)

global CG;
if isempty(CG); C_setupEnvironment; end

P = parsePairs(varargin);
checkField(P,'Path',[CG.Files.CodePath,'Modules',filesep,'AudioOut',filesep,'Speakers',filesep]);

Files = dir([P.Path,'*.mat']);
for iF=1:length(Files)
  cName = Files(iF).name;
  Pos = find(cName=='_');
  Speakers{iF} = cName(Pos(1)+1:Pos(2)-1);
  FileNames{iF} = cName;
end



function ID = C_createAudio(varargin)
%% Setup an audio streamer for a certain set of channels 
% This requires to discover devices, check that channels are not in use
% already
% 
global CG;

%% CHECK FOR LAST SESSION ID
ID = C_getModuleID('ModuleType','Video');

%% PARSE INITIALIZATION ARGUMENTS
P = parsePairs(varargin);
% PARAMETERS
Pars = C_getParametersModule('ModuleType','Audio','IDType',ID);
for iP = 1:size(Pars,1) checkField(P,Pars{iP}.Name,Pars{iP}.Value); end
% OPTIONS
checkField(P,'Start',0);
checkField(P,'Reset',0);

%% CHECK WHETHER CHANNELS ALREADY USED
C_discoverAudio; % attach audio to SG if not done before

%% INITIALIZE AUDIO IF NECESSARY
if P.Reset C_resetAudio; end;

%% GET NEW AUDIO SESSION
CG.Sessions.Audio.S(ID) = daq.createSession('directsound');
cS = CG.Sessions.Audio.S(ID);
cS.IsContinuous = true;
CG.Sessions.Audio.Types{ID} = '';

%% SET SAMPLING RATE
Rates = get(cS,'StandardSampleRates');
SR = Rates(Rates==P.SR);
if ~isempty(SR)  cS.Rate = SR; 
else             error(['Specified SR (',num2str(P.SR),') is not available.']); 
end

%% SETUP SESSION (Fuse the two following steps soon)
% ADD INPUT CHANNELS
if ~isempty(P.ChannelsIn)
  CG.Sessions.Audio.Types{ID}(end+1) = 'I';
  
  for iC = 1:length(P.ChannelsIn)
    cChannel = P.ChannelsIn(iC);
    cID = CG.Devices.Audio.(P.Device).Input.DevicesByChannel(cChannel).ID;
    cLocalChannel = CG.Devices.Audio.(P.Device).Input.DevicesByChannel(cChannel).LocalChannel;
    addAudioInputChannel(cS,cID,cLocalChannel);
  end
  
  %% SETUP INPUT CALLBACK FUNCTION
  ControlSamples = lower(P.SR * P.ControlInterval);
  CG.Sessions.Audio(ID).ControlSamples = ControlSamples;
  cS.NotifyWhenDataAvailableExceeds = ControlSamples;
  CG.Sessions.Audio(ID).CallbackAvail =  ...
    addlistener(cS,'DataAvailable',@(src,evt)C_callbackAudioIn(src,evt,ID));
end


% ADD OUTPUT CHANNELS
if ~isempty(P.ChannelsOut)
  CG.Sessions.Audio.Types{ID}(end+1) = 'O';
  for iC = 1:length(P.ChannelsOut)
    cChannel = P.ChannelsOut(iC);
    cID = CG.Devices.Audio.(P.Device).Output.DevicesByChannel(cChannel).ID;
    cLocalChannel = CG.Devices.Audio.(P.Device).Output.DevicesByChannel(cChannel).LocalChannel;
    addAudioOutputChannel(cS,cID,cLocalChannel);
  end
  
  %% SETUP OUTPUT CALLBACK FUNCTION
  ControlSamples = lower(P.SR * P.ControlInterval);
  cS.NotifyWhenScansQueuedBelow = ControlSamples;
  CG.Sessions.Audio(ID).CallbackReq =  ...
    addlistener(cS,'DataRequired',@(src,evt)C_callbackAudioOut(src,evt,ID));
  
  %% QUEUE INITIAL DATA
  C_prepareAudioOutput(ID);
end

%% PREPARE OUTPUTs
% OUTPUT WILL BE UPDATED BASED ON THE DATAAVAIL CALLBACK FUNCTIOn
% there, the state of each output is checked and the appropriate output
% function called. Should these be handles, such that they can be
% exchanged? I.e. a handle which is Part of CG, and can thus be modified by
% the chosen logic?

%% SETUP FIGURE
% Could this be for Output as well? Probably it would make sense, if it can
% adapt to the number of data packets for both in and out.

CG.Display.Audio(ID).State = 0;
CG.Display.Audio(ID).FIG = [];

C_prepareAudioDisplay(ID);

%% SETUP AUDIO OUTPUT
% This is useful for observing the input live,
% e.g. after transforming the frequency range
% C_createAudio('ChannelOut','next','DataSource',);

%% START AUDIO?
if P.Start C_startAudio(ID); end
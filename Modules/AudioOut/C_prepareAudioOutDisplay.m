function C_prepareAudioDisplay(ID)

global CG;

if nargin < 1; ID = 1; end

% CHECK IF FIGURE ALREADY EXISTS
if isempty(CG.Display.Audio(ID).FIG) | ~ishandle(CG.Display.Audio(ID).FIG)

  % COMPUTE POSITION OF FIGURE
  cRes = [500,300];
  cFIG = C_createModuleFigure('Audio',ID,cRes);

  figure(FIG); clf;
  [DC,AH] = axesDivide(1,1,'C');
  CG.Display.Audio(ID).AH.Data = AH(1);
  CG.Display.Audio(ID).DataH = plot(0,0);
  set(AH(1),'YLim',[-1e-3,1e-3]);
end
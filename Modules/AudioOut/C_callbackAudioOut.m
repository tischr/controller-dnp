function C_callbackAudioOut(Src,Event,ID)

global CG;

% UPDATE AUDIO OUTPUT
NSamples = CG.Sessions.AudioOut(ID).ControlSamples;
LastPos = CG.Sessions.AudioOut(ID).LastQueuedSample;

% SELECT THE DATA FOR THE PRESENT STIMULUS
cStart = LastPos+1;
cStop = LastPos+NSamples;
NData = CG.Data.AudioOut(ID).Data;
if NData >= cStart 
  if NData>=cStop
    cInd = cStart:cStop;
  else
    cInd = cStart:NData;
    Event = struct();
  end
  cData = CG.Data.AudioOut(ID).Data(cInd);
  queueOutputData(Src,cData);  
end

% CODE FOR PITCH-SHIFTING A  
% for i=1:length(CG.Sessions.Audio(ID).Filter)
%   switch CG.Sessions.Audio(ID).Filter(i).Type
%     case 'PitchShift';
%       cData = C_AudioPitchShift(cData);
%     case ''
%     otherwise error('Filter not known.');
%   end
% end
function R  = C_calibrateSpeakers(varargin)
% During the execution, the user is asked to visually judge the length of
% the IIR. A typical value is 10ms.
% see also: SpeakerCalib, findAmplitudeAndDelay, VolumeConversion, StimConversion
% 
% This file is part of MANTA licensed under the GPL. See MANTA.m for details.

%%  PARSE INPUT
P = parsePairs(varargin);

% MODE 
checkField(P,'TestMode',0);

% HARD WARE PARAMETERS
checkField(P,'Speaker','FostexT250D')
checkField(P,'Microphone','AvisoftCM16')
checkField(P,'DeviceIn','Dev2')
checkField(P,'DeviceOut','Dev2')
checkField(P,'ChIn',0);
checkField(P,'ChOut',0);
checkField(P,'SpeakerPath',''); % ENTER CONTROLLERPATH

% GENERAL PARAMETERS
checkField(P,'SR',250000)
checkField(P,'Vmax',10)
checkField(P,'InputRange',[-5,5])

% CALIBRATION PARAMETERS 
checkField(P,'LStim',10)
checkField(P,'NoiseStd',0.02)
checkField(P,'LoudnessMethod','MaxLocalStd')
checkField(P,'PreDur',0.5); P.PreSteps = round(P.PreDur*P.SR);
checkField(P,'PostDur',0.05); P.PostSteps = round(P.PostDur*P.SR);

% ANALYSIS / CORRECTION
checkField(P,'LowFreq',1000)
checkField(P,'HighFreq',100000)
checkField(P,'ImpRespDur',0.006)
checkField(P,'NFFT',round(P.ImpRespDur*P.SR))

% TEST PARAMETERS
checkField(P,'TestDur',5)
checkField(P,'dBSPLRef',60)
checkField(P,'VoltageOutAbsMax',5)
checkField(P,'Signal','Noise');
checkField(P,'RampDur',0.005)

% PLOTTING PARAMETERS
checkField(P,'FIG',1)
checkField(P,'Colors',struct('Signal',[0,0,0],'Response',[1,0,0],'Filter',[0,0,1]))

fprintf(['\n === Calibrating Speaker [ ',P.Speaker,' ] on DAQ Devices ',...
  '(IN : ',P.DeviceIn,' Ch. ',n2s(P.ChIn),', OUT : ',P.DeviceOut,' Ch. ',n2s(P.ChOut),' at SR=',n2s(P.SR),') ===\n']);

P.SameDevice = strcmp(P.DeviceIn,P.DeviceOut);

P = LF_loudnessParameters(P);

%% PREPARE NOISE STIMULUS
[Signal,P] = LF_prepareSignal(P); R = [ ];

if ~P.TestMode
  % PREPARE SOUND OUTPUT & INPUT
  AIO = LF_prepareEngines(P);
  
  % SEND AND ACQUIRE DATA
  fprintf(['\n ==== Calibration stimulus playing (',n2s(P.LStim),' s)'])
  D = LF_getData(AIO,Signal,P); fprintf('\n');
else
  % PRODUCE SIMULATION DATA
  D = LF_createData(Signal,P);  
end
  
% DISPLAY OUTPUT VOLUME
[b,a] = butter(2,50/P.SR,'high'); ResponseF = filter(b,a,D.ResponseCut-D.ResponseCut(1));
VLoudnessMeasured = LF_signalAmplitude(ResponseF,P);
cdBSPL = C_VolumeConversion(VLoudnessMeasured,'V2dB',P.Microphone);
cPa = C_VolumeConversion(cdBSPL,'dB2Pa');
PaRef = C_VolumeConversion(P.dBSPLRef,'dB2Pa');
VRef  = C_VolumeConversion(P.dBSPLRef,'dB2V',P.Microphone);
CorrectionFactor = PaRef/cPa;
fprintf([' => Volume : \t',num2str(cdBSPL,2),' dB SPL (= ',num2str(cPa,2),' Pa = ',num2str(VLoudnessMeasured,2),' V)\n']);
fprintf([' => Reference  : \t',num2str(P.dBSPLRef,2),' dB SPL (= ',num2str(PaRef,2),' Pa = ',num2str(VRef,2),' V)\n']);
fprintf([' => CorrectionFactor : ',num2str(CorrectionFactor,2),'\n']);
P.VoltageOutRMS60dB = P.NoiseStd*CorrectionFactor;

% PREPARE FIGURE
if P.FIG>0
  P.DC = axesDivide([1,1,1],[.8,1,1],[.08,0.12,0.88,.8],[.4],[.8,.5]);
  P.AxisOpt = {'FontSize',7,'FontName','Helvetica Neue','XGrid','on','YGrid','on','Box','on'};
  P.AxisLabelOpt = {'FontSize',8,'FontName','Helvetica Neue'};
  P.XTick = [100,1000,10000]; P.XTickLabel = {'100','1000','10000'};
  figure(P.FIG); clf; set(P.FIG,'Name',...
    ['Speaker: ',P.Speaker,' (SR=',n2s(P.SR),'Hz)'],...
    'MenuBar','none','Toolbar','figure');
end
  
% ESTIMATE TRANSFERFUNCTION 
[R,P] = LF_estimateTF(D.SignalCut(:,1),D.ResponseCut,R,P);

% ESTIMATE INVERSE TRANSFERFUNCTION
[R,P] = LF_estimateITF(D.SignalCut(:,1),D.ResponseCut,R,P);

if ~P.TestMode 
  AIO = LF_prepareEngines(P);

  % ADJUST VOLUME INTERACTIVELY & TEST TRANSFERFUNCTION
  R = LF_testCalibration(AIO,R,P);
  
  % SAVE RESULTS
  LF_saveResults(R,P);
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function P = LF_loudnessParameters(P)
switch P.LoudnessMethod
  case 'MaxLocalStd';
    P.LoudnessParameters = struct('Duration',0.1,'SignalMatlab60dB',1); % Estimate over 100ms
  case {'MinMax'};
    P.LoudnessParameters = struct('SignalMatlab60dB',5);
  case {'Std'};
    P.LoudnessParameters = struct('SignalMatlab60dB',1);
  otherwise error('Error : Method for measuring Signal Amplitude not known.');
end

function A = LF_signalAmplitude(S,P)

switch P.LoudnessMethod
  case 'MaxLocalStd';    A = maxLocalStd(S,P.LoudnessParameters.Duration,P.SR);
  case 'GlobalStd';         A = std(S);
  case 'MinMax';            A = max(abs(S));
  otherwise error('Error : Method for measuring Signal Amplitude not known.');
end

function AIO = LF_prepareEngines(P)

% INITIALIZE ONE INPUT AND ONE OUTPUT DEVICE
AIO = daq.createSession('ni');
Channels = {'Output',P.ChOut;'Input',P.ChIn};
CH(1) = addAnalogOutputChannel(AIO,P.DeviceOut,P.ChOut,'Voltage');
CH(2) = addAnalogInputChannel(AIO,P.DeviceIn,P.ChIn,'Voltage');
CH(2).TerminalConfig = 'SingleEnded';

% GET AVAILABLE SAMPLING RATES
AIO.Rate = P.SR;
SRSet = AIO.Rate;
if (P.SR~=SRSet) error('Suggested Sampling Rate cannot be produced.'); end;
   
function [Signal,P] = LF_prepareSignal(P)
P.NSteps = round(P.LStim*P.SR);
switch P.Signal
  case 'Noise';
    Signal = [randn(1,P.NSteps,1)]';
    Signal = P.NoiseStd*Signal/LF_signalAmplitude(Signal,P);
  case 'Delta';
    Signal = zeros(P.NSteps,1);
    Signal(P.SR/2:P.SR/2:end) = P.NoiseStd;
  case 'Sine';
    Signal = sin(2*pi*500*[1:P.NSteps]'/P.SR);
end
    
function D = LF_getData(AIO,Signal,P)
% SEND SIGNAL AND GET DATA
% IF SIGNAL HAS ONLY ONE ENTRY INTERPRET AS THE RECORDING TIME

FinalSignal = [zeros(P.PreSteps,size(Signal,2));Signal;zeros(P.PostSteps,size(Signal,2))];
AcqLength = size(FinalSignal,1)/P.SR;

queueOutputData(AIO,FinalSignal(:,1));
fprintf('\nStarting Audio Output & Acquisition ... \n');
Response = startForeground(AIO);
Response = Response - mean(Response(round(P.PreSteps/2):P.PreSteps));
Range = [P.PreSteps:P.PreSteps+size(Signal,1)];
D.SignalCut = FinalSignal(Range,:); D.ResponseCut = Response(Range);

function [Signal,Response] = LF_createData(Signal,P)
Sigma = 30;
Kernel = exp(-([0:1000]).^2/(2*Sigma.^2)).*sin(2*pi*1000*[0:1000]/P.SR);
Response = conv(Signal,Kernel); 
Range = [1:length(Signal)];
Response = Response(Range);

function [R,P] = LF_estimateTF(Signal,Response,R,P)
fprintf('   >> Computing Forward Transformations\n')

P.FNyquist = P.SR/2;
P.NSpectrum = P.NFFT/2+1; Window = P.NFFT;
R.Fs = P.FNyquist*(1:P.NSpectrum-1)/(P.NSpectrum-1); % spectral frequencies in Hz (except 0)

% SPECTRA OF INPUT AND OUTPUT
SignalSpec = pwelch(Signal/std(Signal),Window,floor(Window/2),P.NFFT,P.SR);
ResponseSpec = pwelch(Response/std(Response),Window,floor(Window/2),P.NFFT,P.SR);
SignalSpecdB = LF_x2dBScale(abs(SignalSpec(2:P.NSpectrum,:)),1,10);
ResponseSpecdB = LF_x2dBScale(abs(ResponseSpec(2:P.NSpectrum,:)),1,10);

P.SpecAxis = axes('Po',HF_fusePos(P.DC{1,1:2}),P.AxisOpt{:}); hold on;
plot(R.Fs,SignalSpecdB,'Color',P.Colors.Signal);
plot(R.Fs,ResponseSpecdB,'Color',P.Colors.Response);
xlabel('F [Hz]',P.AxisLabelOpt{:});
ylabel('[dB (V^2/Hz)]',P.AxisLabelOpt{:});
title('Power Spectral Densities',P.AxisLabelOpt{:}); 
set(gca,'XTick',[P.XTick,P.FNyquist],'XTickLabel',{P.XTickLabel{:},n2s(P.FNyquist)} ,'XScale','log');
   
% TRANSFER FUNCTION
R.TF = tfestimate(Signal,Response,2*P.NFFT,[],P.NFFT,'twosided'); 
R.TF(P.NSpectrum) = 0;

% GAIN
P.TFGainAxis = axes('Po',P.DC{2,1},P.AxisOpt{:}); hold on
R.TFdB = LF_x2dBScale(abs(R.TF(1:P.NSpectrum-1)),1,20);
plot(R.Fs(1:P.NSpectrum-2),R.TFdB(2:P.NSpectrum-1),...
  'Color',P.Colors.Filter);
title('Transfer Function (Gain)',P.AxisLabelOpt{:});
set(gca,'XTick',P.XTick,'XTickLabel',P.XTickLabel ,'XScale','log');
ylabel('A [dB]',P.AxisLabelOpt{:});

% PHASE
P.TFPhiAxis = axes('Po',P.DC{2,2},P.AxisOpt{:}); hold on;
PhaseUnwrap = unwrap(angle(R.TF(1:P.NSpectrum-1)))/(2*pi);
plot(R.Fs(1:P.NSpectrum-2),PhaseUnwrap(2:P.NSpectrum-1),...
  'Color',P.Colors.Filter);  axis tight;
title('Transfer Function (Phase)',P.AxisLabelOpt{:}); 
set(gca,'XTick',P.XTick,'XTickLabel',P.XTickLabel ,'XScale','log');
ylabel('\phi [2\pi]',P.AxisLabelOpt{:}); 

% COMPUTE IMPULSE-RESPONSE
R.IR = real(ifft(R.TF));  P.dt = 1/P.SR; 
R.Time = [0:P.dt:(P.NFFT-1)*P.dt];

axes('Po',HF_fusePos(P.DC{2,3}),P.AxisOpt{:}); hold on
plot(R.Time*1000,R.IR,'Color',P.Colors.Filter); axis tight;
title('Impulse Response',P.AxisLabelOpt{:}); 
ylabel('V [Volts]',P.AxisLabelOpt{:}); 

% RECOMPUTE TRANSFER FUNCTION (IR ZEROED ABOVE IMPRESPDUR)
IndCutIR = floor(P.ImpRespDur*P.SR); 
R.IR(IndCutIR+1:P.NFFT) = 0;
R.TF = fft(R.IR);  R.TF(P.NSpectrum) = 0;

axes(P.TFGainAxis);
R.TFdB = LF_x2dBScale(abs(R.TF(1:P.NSpectrum-1)),1,20);
plot(R.Fs(1:P.NSpectrum-2),R.TFdB(2:P.NSpectrum-1),...
  'Color',HF_whiten(P.Colors.Filter,.5));  axis tight
Labels = {'TF from Spectra',['TF from IR (0-',n2s(P.ImpRespDur*1000),'ms)']};
Colors = {P.Colors.Filter,HF_whiten(P.Colors.Filter,.5)};
for i=1:length(Labels) 
  text(0.05,.9-(i-1)*0.15,Labels{i},'Units','normalized','Horiz','Left',...
    'Color',Colors{i},'FontWeight','bold','FontSize',8); 
end

axes(P.TFPhiAxis);
R.PhaseUnwrap = unwrap(angle(R.TF(1:P.NSpectrum-1)))/(2*pi);
semilogx(R.Fs(1:P.NSpectrum-2),R.PhaseUnwrap(2:P.NSpectrum-1),...
  'Color',HF_whiten(P.Colors.Filter,0.5)); axis tight

function [R,P] = LF_estimateITF(Signal,Response,R,P)
% The inverse TF is calculated by dividing the TF 
% of an FIR bandpass filter (low_cf ... high_cf)
% by the TF of the forward system.
fprintf('   >> Computing Inverse Transformations\n')
P.NSamples = round(P.ImpRespDur*P.SR);  
% Make P.NSamples odd.
if rem(P.NSamples,2) == 0   P.NSamples = P.NSamples+1; end

% COMPUTE INVERSE TRANSFER FUNCTION
P.UpperEdge = min([P.HighFreq,0.9*P.FNyquist]);
P.FilterFreq = [0,0.8*P.LowFreq,P.LowFreq,...
  P.UpperEdge,1.1*P.UpperEdge,P.FNyquist]./P.FNyquist;  
P.NFreq = length(P.FilterFreq); P.FreqAmplitudes = [0,0.1,1,1,0.1,0];

axes(P.SpecAxis);
plot(P.FilterFreq*P.FNyquist,LF_x2dBScale(P.FreqAmplitudes,1,20),...
  'Color',P.Colors.Filter); axis tight;
Labels = {'Signal','Response','Filter'};
for i=1:length(Labels) 
  text(0.01,.15+(i-1)*.2,Labels{i},'Units','normalized','Horiz','Left',...
    'Color',P.Colors.(Labels{i}),'FontWeight','bold','FontSize',8); 
end

R.IRfir = fir2(P.NSamples-1,P.FilterFreq,P.FreqAmplitudes,kaiser(P.NSamples,3));  % provides impulse-response for the filter.
R.IRfir = R.IRfir(1:P.NFFT)'; % patched with zeros to full NFFT samples
R.TFfir = fft(R.IRfir);  % go to frequency-representation of the fir-filter
R.TFfir(P.NSpectrum) = 0; % set the middle point to 0
R.iTF = [complex(0,0);R.TFfir(2:P.NSpectrum-1)./R.TF(2:P.NSpectrum-1);complex(0,0)]; % Freq 0..P.FNyquist (1x1025)
R.iTFtot = [R.iTF ; flipud(conj(R.iTF(2:end-1)))];

% COMPUTE INVERSE IMPULSE RESPONSE
R.IIR1 = real(ifft(R.iTFtot));

P.IIRAxis = axes('Po',P.DC{3,3},P.AxisOpt{:}); hold on
plot(R.Time*1000,R.IIR1,'Color',P.Colors.Filter);

% INVERSE TRANSFER FUNCTION
% GAIN
P.ITFGainAxis = axes('Po',P.DC{3,1},P.AxisOpt{:}); hold on;
R.iTFtotdB = LF_x2dBScale(abs(R.iTFtot(2:P.NSpectrum-1)),1,20); 
plot(R.Fs(1:P.NSpectrum-2),R.iTFtotdB,'Color',P.Colors.Filter);  axis tight;
ylabel('A [dB]',P.AxisLabelOpt{:}); 
title('Inverse Transfer Function (Gain)',P.AxisLabelOpt{:});
set(gca,'XTick',P.XTick,'XTickLabel',P.XTickLabel ,'XScale','log');

% PHASE
P.ITFPhiAxis = axes('Po',P.DC{3,2},P.AxisOpt{:}); hold on;
PhaseUnwrap = unwrap(angle(R.iTFtot(2:P.NSpectrum-1)))/(2*pi); 
plot(R.Fs(1:P.NSpectrum-2),PhaseUnwrap,'Color',P.Colors.Filter);  axis tight;
ylabel('\phi [2\pi]',P.AxisLabelOpt{:}); 
title('Inverse Transfer Function (Phase)',P.AxisLabelOpt{:});
set(gca,'XTick',P.XTick,'XTickLabel',P.XTickLabel ,'XScale','log');

% COMPUTE INVERSE IMPULSE RESPONSE (QUENCHED AT END)
% WEIGHTING FUNCTION OF TYPE y(t)=a*t^n*exp(-b*t):
% PEAKS AT PeakTimeRel
P.Gating = 'sinusoidal';
switch lower(P.Gating)
  case 'alpha';
    PeakTimeRel = 0.003/R.Time(P.NFFT-1);
    Time = [0:length(R.IIR1)-1]';
    alpha = 0.02;    kw = 1/PeakTimeRel;
    Exponent = -log(alpha)/(kw-1-log(kw));
    t1 = round(PeakTimeRel*(length(Time)-1));
    bw = t1/Exponent;
    aw = 1/(((Exponent*bw)^Exponent)*exp(-Exponent));
    R.ImpWeight = aw*(Time.^Exponent).*exp(-Time/bw);
  case 'sinusoidal'
    R.ImpWeight = tukeywin(length(R.IIR1),0.1);
end
R.IIR2 = R.ImpWeight.*R.IIR1;

% FILTER OUT HIGH FREQUENCY PART
% NN=P.SR/2; hif=24000;
% order = 4;
% [bLow,aLow] = butter(order,hif/NN,'low');
% R.IIR2 = filter(bLow,aLow,R.IIR2);
R.IIR = R.IIR2;

R.iTFtot2 = fft(R.IIR2);

axes(P.IIRAxis);
plot(R.Time*1000,R.IIR2,'Color',HF_whiten(P.Colors.Filter,.5)); axis tight;
xlabel('t (ms)',P.AxisLabelOpt{:}); 
ylabel('V [Volts]',P.AxisLabelOpt{:});
title('Inv. Impulse Response',P.AxisLabelOpt{:}); 
Labels = {'IIR',['IIR weighted with alpha-Kernel']};
Colors = {P.Colors.Filter,HF_whiten(P.Colors.Filter,.5)};
for i=1:length(Labels) 
  text(0.05,.9-(i-1)*0.15,Labels{i},'Units','normalized','Horiz','Left',...
    'Color',Colors{i},'FontWeight','bold','FontSize',8); 
end

% GAIN (QUENCHED)
axes(P.ITFGainAxis); 
R.iTFtot2dB = LF_x2dBScale(abs(R.iTFtot2(2:P.NSpectrum-1)),1,20); 
plot(R.Fs(1:P.NSpectrum-2),R.iTFtot2dB,'Color',HF_whiten(P.Colors.Filter,.5)); 
xlabel('F [Hz])',P.AxisLabelOpt{:}); 
ylabel('A [dB]',P.AxisLabelOpt{:}); 

% PHASE (QUENCHED)
axes(P.ITFPhiAxis);
PhaseUnwrap = unwrap(angle(R.iTFtot2(2:P.NSpectrum-1)))/(2*pi);
plot(R.Fs(1:P.NSpectrum-2),PhaseUnwrap,'Color',HF_whiten(P.Colors.Filter,.5)); 
xlabel('F [Hz]',P.AxisLabelOpt{:});
ylabel('\phi [2\pi]',P.AxisLabelOpt{:}); 

% CONVOLVE THE FORWARD AND INVERSE IMPULSE RESPONSES
R.IIR2 = R.IIR2(1:min([round(2*P.ImpRespDur*P.SR),length(R.IIR2)]));
R.truncIR = R.IR(1:floor(P.ImpRespDur*P.SR));  
R.convIR = conv(R.IIR2,R.truncIR);
R.IRdelayPos = find(max(R.convIR)==R.convIR); 
if length(R.IRdelayPos)>1
  warning('Maximum found at multiple points in convolved impulse response'); 
  R.IRdelayPos = R.IRdelayPos(1);
end
R.IRdelayTime = R.IRdelayPos/P.SR;
Time = [0:P.dt:(length(R.convIR)-1)*P.dt];
axes('Po',P.DC{1,3},P.AxisOpt{:}); hold on;
plot(Time*1000,R.convIR,'Color',P.Colors.Filter); axis tight;
xlabel('t [ms]',P.AxisLabelOpt{:});
title('Convolved IRs',P.AxisLabelOpt{:});
text(0.05,0.9,['Peak at ',n2s(R.IRdelayPos),'steps = ',...
  n2s(R.IRdelayTime*1000),'ms'],'Units','Normalized',P.AxisLabelOpt{:})

function R = LF_testCalibration(AIO,R,P)
% TEST THE ESTIMATED CALIBRATION WITH RESPECT TO LOUDNESS AND SPECTRUM
fprintf(['  >> Collecting Data for Loudness, Delay & Spectrum Test (',n2s(P.TestDur),'s)']);

% Set the maximal amplitude of the noise to be the 60dB limit in Matlab (= P.SignalMatlab60dB)
STIMORIG = randn(round(P.TestDur*P.SR),1);
STIMORIG = P.LoudnessParameters.SignalMatlab60dB/LF_signalAmplitude(STIMORIG,P)*STIMORIG;
R.IIR60dB = 0.01*R.IIR;

for iC = 1:2
  % CONVOLVE STIMULUS WITH SCALED IIR 
  STIM = conv(STIMORIG,R.IIR60dB);
  
  VoltageOutMax = max(abs(STIM));
  if VoltageOutMax > P.Vmax
    error('Increase the Volume of the Amp to achieve a Signal, which fits inside [-10,10] Volts, i.e. output of the DAQ card.');
  end
  
  % COLLECT DATA FOR FIRST REESTIMATE OF LOUDNESS
  D = LF_getData(AIO,[STIM,[STIMORIG;zeros(length(STIM)-length(STIMORIG),1)]],P);

  % FILTER TO AVOID TRANSIENTS
  [b,a] = butter(2,50/P.SR,'high'); ResponseF = filter(b,a,D.ResponseCut-D.ResponseCut(1));

  % REESTIMATE LOUDNESS
  VLoudnessMeasured = LF_signalAmplitude(ResponseF,P);
  cdBSPL = C_VolumeConversion(VLoudnessMeasured,'V2dB',P.Microphone);
  cPa = C_VolumeConversion(cdBSPL,'dB2Pa');
  PaRef = C_VolumeConversion(P.dBSPLRef,'dB2Pa');
  CorrectionFactor = PaRef/cPa;
  fprintf([' => Volume : \t',num2str(cdBSPL,2),' dB SPL (= ',num2str(cPa,2),' Pa)\n']);
  fprintf([' => Reference  : \t',num2str(P.dBSPLRef,2),' dB SPL (= ',num2str(PaRef,2),' Pa)\n']);
  fprintf([' => CorrectionFactor : ',num2str(CorrectionFactor,2),'\n']);

  R.A60dB = PaRef/cPa;
  R.IIR60dB = R.A60dB*R.IIR60dB;
end

% COMPUTE DELAY
MaxDelay = round(0.2*P.SR);
X_SD = xcorr(ResponseF,D.SignalCut(:,2),MaxDelay);
[MAX,R.ConvDelaySteps] = max(X_SD);
R.ConvDelaySteps = R.ConvDelaySteps - MaxDelay -1;
R.ConvDelay = R.ConvDelaySteps/P.SR;
fprintf(['   => Convolution Delay\t:\t',num2str(R.ConvDelay),' s (',num2str(R.ConvDelaySteps),' samples)\n'])
if R.ConvDelaySteps<0 
  warning('Noncausal Delay : Setting to 0.'); 
  R.ConvDelaySteps = 0; R.ConvDelay = R.ConvDelaySteps/P.SR; 
end

% SPECTRAL CHECK
R.ConvDelaySteps = max(R.ConvDelaySteps,1);
STIMORIGtrimmed = D.SignalCut(1:end-R.ConvDelaySteps+1,2);
ResponseFshifted = ResponseF(R.ConvDelaySteps:end);
ResponseFshifted = ResponseFshifted(1:length(STIMORIGtrimmed));
R.TFCalib = tfestimate(STIMORIGtrimmed,ResponseFshifted,2*P.NFFT,[],P.NFFT,'twosided');
R.TFCalib(P.NSpectrum) = 0;
R.TFCalibdB = LF_x2dBScale(abs(R.TFCalib(1:P.NSpectrum-1)),1,20);
PhaseUnwrap = unwrap(angle(R.TFCalib(1:P.NSpectrum-1)))/(2*pi);

% PLOT RESULTS
figure(P.FIG+1); clf; set(gcf,'NumberTitle','off','Name','White Noise Testing (Spectrum, Delay & Loudness)'); NPlot = 5;
DC = axesDivide(1,NPlot,[.08,0.06,.85,.9],[],[.7]);
for i=1:NPlot AH(i) = axes('Po',DC{i},P.AxisOpt{:}); hold on; end

T = [1:length(STIMORIG)]/P.SR;
axes(AH(1)); title('Stimulus for Calibration'); plot(T,STIMORIG); axis tight; grid on
T = [1:length(ResponseF)]/P.SR;
axes(AH(2)); title('Response for Calibration'); plot(T,ResponseF); axis tight; grid on
axes(AH(3)); title('Crosscorrelation'); plot([-MaxDelay:MaxDelay]/P.SR,X_SD);  grid on
axes(AH(4));  title('Transfer Function (Gain)',P.AxisLabelOpt{:});
plot(R.Fs(1:P.NSpectrum-2),R.TFCalibdB(2:P.NSpectrum-1),...
  'Color',P.Colors.Filter); axis tight;
set(gca,'XTick',P.XTick,'XTickLabel',P.XTickLabel ,'XScale','log');
ylabel('A [dB]',P.AxisLabelOpt{:});

axes(AH(5)); title('Transfer Function (Phase)',P.AxisLabelOpt{:});
plot(R.Fs(1:P.NSpectrum-2),PhaseUnwrap(2:P.NSpectrum-1),...
  'Color',P.Colors.Filter);  axis tight;
set(gca,'XTick',P.XTick,'XTickLabel',P.XTickLabel ,'XScale','log');
ylabel('\phi [2\pi]',P.AxisLabelOpt{:});

%% TEST TONAL CALIBRATION
LStim = 10; fbase = P.LowFreq; Xges = log2(P.HighFreq/P.LowFreq);
[ZAPORIG,TZAP,FZAP] = LF_createZAP(LStim,Xges,fbase,P.SR);
ZAPORIG = P.LoudnessParameters.SignalMatlab60dB...
  /LF_signalAmplitude(ZAPORIG,P)*ZAPORIG'; % Brings ZAP Stimulus to +/-V peak to peak
ZAP = conv(ZAPORIG,R.IIR60dB);

VoltageOutMax = max(abs(ZAP));
if VoltageOutMax > P.Vmax
   fprintf(['\nERROR: Desired Voltage : ',n2s(VoltageOutMax),'\n'...
     'Increase the Volume of the Amp to achieve a Signal, which fits inside [-10,10] Volts, i.e. output of the DAQ card.\n\n']);
  return;
end

DZAP = LF_getData(AIO,[ZAP,[ZAPORIG;zeros(length(R.IIR)-1,1)]],P);
ZAPResponseF = filter(b,a,DZAP.ResponseCut-DZAP.ResponseCut(1));

TZAP = [1:length(ZAPResponseF)]/P.SR;
if length(DZAP.ResponseCut)<=length(FZAP);
  FZAP =  FZAP(1:length(ZAPResponseF));
else
  FZAP(end+1:length(ZAPResponseF)) = 0;
end
BinBounds = round(linspace(0,length(ZAPResponseF),200));
for i=1:length(BinBounds)-1
  PaMeasured = LF_signalAmplitude(ZAPResponseF(BinBounds(i)+1:BinBounds(i+1)),P);
  VolZAP(i) = C_VolumeConversion(PaMeasured,'Pa2dB');
  FVolZAP(i) = mean(FZAP(BinBounds(i)+1:BinBounds(i+1)));
end

% PLOT RESULTS FOR FREQUENCY SWEEP
figure(P.FIG+2); clf; set(gcf,'NumberTitle','off','Name','Frequency Sweep Testing');
[DC,AH] = axesDivide([1],[1,2],[0.1,0.1,0.8,0.8],[],0.8,'c');
axes(AH(1)); title('Tonal Amplitude Response');
hold off;
[AX,H1,H2] = plotyy(FZAP,ZAPResponseF,FVolZAP,VolZAP,'semilogx','semilogx');
set(AX,'XLim',[P.LowFreq,P.HighFreq]); grid on
xlabel('Frequency [Hz]');

axes(AH(2)); title('Spectrum of Response');
[S,F,T] = spectrogram(ZAPResponseF,1024,512,1024,P.SR); 
imagesc(T,F/1000,abs(S)); set(gca,'YDir','normal');
xlabel('Time [s]'); ylabel('Frequency [kHz]');
colormap hsv;

function LF_saveResults(Rall,P);
% GET BAPHY PATH
Sep = filesep; Path = which('Controller');
Path = Path(1:find(Path==Sep,1,'last')); 
Path = [Path,'Modules',Sep,'AudioOut',Sep,'Speakers',Sep];
FileName = [Path,'SpeakerCalibration_',P.Speaker,'_',P.Microphone,'.mat'];
Rall.SR = P.SR; Rall.dBSPLRef = P.dBSPLRef;
Rall.Loudness.Method = P.LoudnessMethod;
Rall.Loudness.Parameters = P.LoudnessParameters;

% COLLECT VARIABLES NECESSARY FOR SPEAKER CORRECTION
Vars = {'SR','IIR60dB','ConvDelay','dBSPLRef','Loudness','Fs','TFCalibdB'};
for i=1:length(Vars) eval(['R.(Vars{i}) = Rall.',Vars{i},';']); end

fprintf(['\n ===== Saving Calibration =====\n  >> File\t:\t',escapeMasker(FileName),'\n']);
save(FileName,'R');

function [STIM,T,F] = LF_createZAP(Lstim,Xges,fbase,SRHz)

dt = 1/SRHz;
T = [0:1/SRHz:Lstim]; X =  linspace(0,Xges,length(T));  F = fbase*2.^X;  

phaseinc = dt.*F;
phases = cumsum(phaseinc); 
STIM = sin(2*pi*phases);

function dB = LF_x2dBScale(x,base,fact)
dB = fact*log10(x/base);
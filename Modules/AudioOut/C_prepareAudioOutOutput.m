function C_prepareAudioOutput(ID)
%% Setup an audio streamer for a certain set of channels 
% This requires to discover devices, check that channels are not in use
% already
% 
global CG;

cS = CG.Sessions.Audio.S(ID);
NSamples = CG.Sessions.Audio(ID).ControlSamples;
cData = randn(NSamples,1)/20;
queueOutputData(cS,cData);


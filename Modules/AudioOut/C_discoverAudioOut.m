function C_discoverAudio(Force)

global CG

if ~exist('Force','var') Force = 0; end

% CHECK IF AUDIO ALREADY DISCOVERED
if isfield(CG,'Devices') && isfield(CG.Devices,'Audio') && ~Force
  % DEVICES ALREADY DISCOVERED
else % DISCOVER DEVICES
  switch architecture
    case 'PCWIN';
      
      D = daq.getDevices; iI = 0; iO = 0;
      ModelsToCheck = {'OCTA-CAPTURE','Primary Sound'};
      ModelNamesInternal = {'OctaCapture','SoundCard'};
      Models = get(D,'Model');
      for iM=1:length(ModelsToCheck)
        ModelToCheck = ModelsToCheck{iM};
        Ind = strfind(Models,ModelToCheck);
        Ind = find(~cellfun(@isempty,Ind));
        if isempty(Ind) continue; end
        cD = D(Ind); clear Types;
        for iD=1:length(cD)
          Types{iD} = cD(iD).Subsystems.SubsystemType;
        end
        KnownTypes = {'AudioInput','AudioOutput'};
        Directions = {'Input','Output'};
        for iT = 1:length(KnownTypes)
          clear Channels Handles DevicesByChannel tmp;
          cInd = strcmp(Types,KnownTypes{iT});
          ccD = cD(cInd);
          for iD=1:length(ccD)
            switch ModelToCheck
              case 'OCTA-CAPTURE';
                cDescription = get(ccD(iD),'Description');
                Pos = find(cDescription==' ');
                cDescription = cDescription(Pos(1)+1:Pos(2)-1);
                Pos = find(cDescription=='-');
                if ~isempty(Pos)
                  Channels(iD,:) = [str2num(cDescription(1:Pos-1)),str2num(cDescription(Pos+1:end))];
                end
              case 'Primary Sound';
                % NO OTHER CHANNELS ASSUMED HERE
                ChNames = ccD(iD).Subsystems.ChannelNames;
                for i=1:length(ChNames) Channels(iD,i) = str2num(ChNames{i}); end
            end
          end
          % SORT DEVICES BY CHANNEL NUMBER
          [tttmp,SortInd] = sort(Channels(:,1));
          Channels = Channels(SortInd,:);
          ccD = ccD(SortInd);
          for i=1:length(ccD)
            tmp(i).Device = ccD(i);
            tmp(i).Channels = Channels(i,:);
            tmp(i).ID = ccD(i).ID;
          end
          for i=1:max(Channels(:))
            [i1,i2] = find(Channels==i);
            DevicesByChannel(i).ID = ccD(i1).ID;
            DevicesByChannel(i).LocalChannel = i2;
            DevicesByChannel(i).Pos = i1;
          end
          CG.Devices.Audio.(ModelNamesInternal{iM}).(Directions{iT}).Handles = tmp;
          CG.Devices.Audio.(ModelNamesInternal{iM}).(Directions{iT}).DevicesByChannel = DevicesByChannel;
        end
        
      end
    case 'MAC'; % THIS IS JUST FOR GUI TESTING, BUT COULD MAYBE BE ACTIVATED
      CG.Devices.Audio.SoundBlaster.Out = struct('Handles',[],'DevicesByChannel',1);
  end
end

function Controller(varargin)
%% Open Controller GUI with possibility to load a setup
% 
% TODO :
% - Check what happens if animal switches to wrong side and does not cross? Maybe accumulate evidence on one side that the animal is there. 
% - Popup to not interact with the computer
% - Change or add Saving Tag for some old recordings (e.g. mouse4_R4);
% - Warning for low framerate
% - Buttons in the Paradigm GUI
%   - stop trial early
% - Speed up Computer to avoid lost frames
% -> Change Timing of the Ultrasound recording grab
% - Check the defined Reduction Windows for Camera
%
% - Reduce data in current data to just that last few Trials
% - Convert Acquired Data to int16 or single at least
% - Downsample sensor data before saving on some channels
%    (all platform sensor channels, i.e. ai0-9, simple take every tenth step)
%   Or name certain channels as trigger channels
%
% - Implement possibility to save and load a configuration,
%    or use the Baphy mechanism (saved parameters on each computer) 
%    to load the last settings per paradigm
% - Add start and stop of acquisition as events for the individual modules
% - Program a grdoc aphical Arduino Control Module (for testing Setups)

global CG Verbose;

% SET THE PATH & CHECK FOR REQUIRED TOOLBOXES
C_setPath; if length(varargin)==1 && strcmp(varargin{1},'PathOnly') return; end;
if C_checkToolboxes== -1 return;  end;

fprintf('Starting Controller ... \n');
C_cleanUp; global CG Verbose;  try; dbquit; catch; end

% PARSE INITIALIZATION ARGUMENTS
P = parsePairs(varargin);
for i=1:length(varargin)/2 eval(['CG.',varargin{(i-1)*2+1},' = varargin{i*2};']); end
if ~isfield(CG,'Paradigm')  CG.Paradigm = 'Independent'; end

C_setupEnvironment;

% ASSIGN THE BASIC SETTINGS
C_initialize; evalin('base','global CG Verbose');

%% CREATE THE GRAPHICAL USER INTERFACE 
C_createGUI;

C_orderFields;

% ADD THE CONTROLLER PATH TO THE GLOBAL PATH
function C_setPath(Path)
global CG;
File = which('Controller'); Path = File(1:find(File==filesep,1,'last'));
CG.Files.CodePath = Path;
CG.Files.ConfigPath = [CG.Files.CodePath,'Configurations',filesep];
LF_addpathWithoutVC(Path);

function LF_addpathWithoutVC(Path)

Architecture = computer;
switch Architecture(1:2)
  case 'PC'; Delimiter = ';';
  otherwise Delimiter = ':';
end

Paths=''; 
FullPath = genpath(Path);
Pos = [0,find(FullPath==Delimiter),length(FullPath)+1]; 
PathsAll=cell(size(Pos));
C = computer; 
for i=1:length(Pos)-1 PathsAll{i} = FullPath(Pos(i)+1:Pos(i+1)-1); end
for ii=1:length(PathsAll)
  AddPath = 1;
  if ~isempty(findstr('.svn',PathsAll{ii})) AddPath = 0; end;
  if ~isempty(findstr('.git',PathsAll{ii})) AddPath = 0; end;
  if isempty(PathsAll{ii}) AddPath = 0; end;
  if ~isempty(strfind(PathsAll{ii},[filesep,'Database',filesep])) && isempty(strfind(PathsAll{ii},['Database',filesep,C])); AddPath = 0; end
  if AddPath  Paths=[Paths,Delimiter,PathsAll{ii}]; end
end
addpath(Paths(2:end));
function ParsS = C_convPars2Struct(Pars)
 
  for iP=1:size(Pars,1)
    ParsS{iP} = struct('Name',{Pars{iP,1}},'Value',{Pars{iP,2}},'Type',{Pars{iP,3}},'Choices',{Pars{iP,4}});
  end
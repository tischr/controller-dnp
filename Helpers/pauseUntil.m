function pauseUntil(ContinueTime,PauseInterval)

if nargin < 2; PauseInterval = 0.25; end
SecondConv = 86400;
cTime = now*SecondConv;
while cTime <= ContinueTime
  pause(PauseInterval);
  cTime = now*SecondConv;
end
  
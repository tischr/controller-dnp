function Hostname = HF_getHostname

switch architecture
  case 'PCWIN'; Hostname = getenv('COMPUTERNAME');
  case 'MAC'; [tmp,Hostname] = system('scutil --get ComputerName'); Hostname = Hostname(1:end-1);
  otherwise [tmp,Hostname] = system('hostname'); Hostname = Hostname(1:end-1);
end
Pos = find(Hostname == '.');
if ~isempty(Pos) Hostname = Hostname(1:Pos-1); end

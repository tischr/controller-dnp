function OUT = C_parseID(IN)

if isnumeric(IN{1})
  ANum = floor(IN{1});
  RS = num2str(IN{1}-ANum);
  RNum = str2num(RS(3:end));
  OUT = {'Animal',['mouse',num2str(ANum)],'Recording',RNum}; 
  if length(IN)>1 OUT = [OUT(:)',IN(2:end)]; end
else 
  OUT = IN;
end
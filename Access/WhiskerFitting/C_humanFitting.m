function C_humanFitting(varargin)

P = parsePairs(varargin);
checkField(P,'Paradigm','Interaction');
checkField(P,'RedrawProp',0.1);
checkField(P,'RecID',[]);
checkField(P,'Verbose',0);
  
% SELECT ALL MATCHING RECORDINGS FROM DATABASE
% GET RECORDING INFO
SQL = ['SELECT * FROM recordings WHERE '...
  'NOT animal=''test'' AND paradigm=''',P.Paradigm,''' AND bad=0 '];
DB = mysql(SQL);

% RANDOMLY CHOOSE ONE FROM LEAST ANALYZED ONES
HumanCount = [DB.humantrackingcount];
if isempty(P.RecID)
  MinCount = min(HumanCount);
  MinInd = HumanCount==MinCount;
  
  % invert selection between set that has to catch up and set that does not
  cRand = rand;
  if cRand<P.RedrawProp    cInd = ~MinInd;
  else                                      cInd =   MinInd;
  end
  LinInd = find(cInd);
  Pos = randi([1,length(LinInd)],[1,1]);
  RecInd = LinInd(Pos);
else
  RecIDs = [DB.id];
  RecInd = find(RecIDs == P.RecID);
end
cDB = DB(RecInd);
cCount = cDB.humantrackingcount;

% COPY DATA LOCALLY TO A SINGLE FILE
Path = C_makeLocal('Animal',cDB.animal,'Recording',cDB.recording,...
  'Modules',{'PointGrey','NIDAQ'},'Verbose',P.Verbose);
[Path,Paths] = C_getDir('Animal',cDB.animal,'Recording',cDB.recording);

% LOAD DATA
R = C_loadRecording('Animal',cDB.animal,'Recording',cDB.recording,'Modules',{'NIDAQ','PointGrey'});

% SAVE DATA TO SERVER
% use the analysis_count to generate a filename
newCount = cCount + 1;
try mkdir([Paths.DB,'Results']); end
try mkdir([Paths.DB,'Results',filesep,'Tracking']); end
FileName = [Paths.DB,'Results',filesep,'Tracking',filesep,'HumanTracking_',num2str(newCount),'.mat'];

% OPEN WITH VIDEOVIEWER TO DO VIDEO TRACKING
% check handling of gaps in the video
VideoViewer(...
  'Data',R.PointGrey.Data.Data.Frames,...
  'Time',R.PointGrey.Data.Data.Time,...
  'FileNameAnnotations',FileName,...
  'Transpose',1);

% WRITE COUNT TO DATABASE
pause(0.1);
disp('Updating Database ...');
SQL = ['UPDATE recordings SET ',...
            ' humantrackingcount=',num2str(newCount),' '...
            ' WHERE id=',num2str(cDB.id)];
Res = mysql(SQL);
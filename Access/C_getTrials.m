function R = C_getTrials(varargin)
%% PARSE ARGUMENTS
varargin = C_parseID(varargin);
P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'Modules','all')

%% DETERMINE CORRECT DATA PATH
[P.Path,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);  

if isequal(P.Modules,'all')
  ModulesCand = dir(P.Path);
  ModulesCand = ModulesCand([ModulesCand.isdir]);
  ModulesCand = {ModulesCand.name};
  P.Modules = setdiff(ModulesCand,{'Results','.','..'});
end


for iM = 1:length(P.Modules)
  R.(P.Modules{iM}) = [];
  cPath = [P.Path,P.Modules{iM},filesep];
  Files = dir([cPath,'Data_*.mat']);
  for iF=1:length(Files)
    R.(P.Modules{iM})(iF) = str2num(Files(iF).name(6:end-4));
  end
  R.(P.Modules{iM}) = sort(R.(P.Modules{iM}));
end
function [LocalPath,Paths] = C_makeLocal(varargin)
% Transfer Recordings to the local drive 

global CG
varargin = C_parseID(varargin);
P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'Modules','all'); if ischar(P.Modules) P.Modules = {P.Modules}; end
checkField(P,'Verbose',1)
checkField(P,'Force',0)
checkField(P);

[Path,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);

if strcmp(P.Modules{1},'all') % COPY ALL MODULES
  if ~exist(Paths.Local) | P.Force
    if P.Verbose
      disp(['Transferring ',Paths.DB,'  ===>  ',Paths.Local]);
    end
    mkdirAll(Paths.Local);
    copyfile(Paths.DB,Paths.Local);
  end
else % COPY SELECTION OF MODULES
  if ~exist(Paths.Local) | P.Force
    mkdir(Paths.Local);
    copyfile([Paths.DB,filesep,'General.mat'],Paths.Local);
    copyfile([Paths.DB,filesep,'*.txt'],Paths.Local);
  end
  % COPY MODULES
  for iM=1:length(P.Modules) 
    cPath = [Paths.Local,filesep,P.Modules{iM}];
    if ~exist(cPath)
      if P.Verbose
        disp(['Transferring ',Paths.DB,' ( ',P.Modules{iM},' )  ===>  ',Paths.Local]);
      end
      copyfile([Paths.DB,filesep,P.Modules{iM}],cPath);
    end
  end
end
LocalPath = Paths.Local;
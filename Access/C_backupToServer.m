function C_backupToServer(varargin)
% BACKUP DATA FROM A LOCAL MACHINE TO THE SERVER
%
% Special Case for EPhysModule
% - Controller creates a symbolic link to the remote folder locally
%    !mklink /D Link Target (start Matlab always as administrator: Properties -> Compatibility)
%    List SYMLINK only: dir RootName* | find "SYMLINKD"  (and then check if result empty or not)
% - Backup resolves this link by transfering the files
%

P = parsePairs(varargin);
checkField(P,'Animal',[]);
checkField(P,'Recording',[]);
checkField(P,'ExcludeTest',1);
checkField(P,'Simulation',0);
% MAP NETWORK DRIVE IF NECESSARY
C_mountServer;
BytesPerSecond = 10e6;

%% GET ALL RECORDINGS THAT NEED TO BE BACKUPED
Hostname = upper(HF_getHostname);
SQL = ['SELECT id,animal,recording,trials FROM recordings WHERE ',...
  'hostname=''',Hostname,''' AND bad=0 AND onserver=0'];
Recordings = mysql(SQL);

%% EXCLUDE TEST
Animals = {Recordings.animal};
if P.ExcludeTest;  Recordings =  Recordings(find(~strcmp(Animals,'test'))); end

%% FILTER FOR SELECTED ANIMALS
if ~isempty(P.Animal)
  Animals = {Recordings.animal}; cInd = [];
  if ~iscell(P.Animal) P.Animal = {P.Animal}; end
  for iA=1:length(P.Animal)
    cInd = [cInd,find(strcmp(Animals,P.Animal(iA)))];
  end
  Recordings = Recordings(cInd);
end

%% FILTER FOR SELECTED RECORDINGS
if ~isempty(P.Recording)
  RecNums = [Recordings.recording]; cInd = [];
  for iR = 1:length(P.Recording)
    cInd = [cInd,find(RecNums==P.Recording(iR))];
  end
  Recordings = Recordings(cInd);
end

%% OUTPUT THE RECORDINGS FOUND
fprintf(['\n\n [  Found  [[ ',num2str(length(Recordings)),' ]] to backup from this machine "',Hostname,'"  ] \n']);
Animals = {Recordings.animal}; UAnimals = unique(Animals);
for iA=1:length(UAnimals)
  cInd = find(strcmp(Animals,UAnimals{iA}));
  RecNums = [Recordings(cInd).recording];
  fprintf([' > ',UAnimals{iA},' : ',sprintf('%d ',RecNums),'\n']);
end

if P.Simulation return; end

%% CREATE SAFE ABORT FIGURE
figure(1001); clf; set(gcf,'position',[10,600,200,100],'toolbar','none','menubar','none');
uicontrol('style','pushbutton','units','n','position',[0,0,1,1],'String','Safely Abort Backup',...
  'Callback','global AbortBackup; AbortBackup = 1;');
global AbortBackup; AbortBackup = 0;

%% BACKUP THE RECORDINGS
for iR = 1:length(Recordings)
  Recordings(iR).Name = [Recordings(iR).animal,' R',num2str(Recordings(iR).recording)];
  cR = Recordings(iR);   cAnimal = cR.animal;   cRecording = cR.recording;
  
  fprintf(['\n==  Processing   :   ',cR.Name,' (ID : ',num2str(cR.id),' , Trials : ',num2str(cR.trials),') =========================\n']);
  
  % GET RECORDING PATH 
  [RecordingPath,Paths] = C_getDir('Animal',cAnimal,'Recording',cRecording);
  
  % CHECK FOR RECORDING DIRECTORY
  if ~isdir(RecordingPath) 
    fprintf('Recording Directory does not exist locally! Skipping...\n\n'); continue; 
  end
  
  % GET THE PATHS FOR EACH MODULE
  [ModulePaths,Modules] = C_getModulePaths(RecordingPath); 
  
  % COMPRESS THE DATA FILES (LOAD AND SAVE WITH -v7.3) (LOCALLY)
  TotalBytes = 0;
  for iM = 1:length(ModulePaths)
    fprintf([' > Module : ',Modules{iM},' : ']);
    switch Modules{iM}
      case 'PointGrey'; fprintf('  [ Conversion not necessary ] '); PreFix = 'Data_';
      case 'EPhys'; 
        PreFix = '';
        [S1,S2] = system(['dir ',ModulePaths{iM}(1:end-1),'* | find "SYMLINKD"']);
        isSymLink =  S1==0 && ~isempty(S2);
        if isSymLink
          fprintf('  [ Copying Data to Local Drive ]');
          EPhysPathTmp = S2(find(S2=='[')+1:end-2);
          Files = dir([EPhysPathTmp,'_*.continuous']);
          system(['rmdir ',ModulePaths{iM}]);
          copyfile(EPhysPathTmp,ModulePaths{iM});
          Files = C_findFilesEPhys('Directory',ModulePaths{iM});
          % DELETE UNNECESSARY FILES BEFORE BACKUP
          for iF = 1:length(Files.AUX) delete(Files.AUX(iF).Name); end
          ChNums = [1,4,5,6,7,8];
          try 
            for iF = 1:length(Files.AnalogIn)
              if sum(Files.AnalogIn(iF).ChNum==ChNums)
                delete(Files.AnalogIn(iF).Name);
              end
            end
          catch warning('Analog Files Are Missing!');
          end
        end
        
      otherwise
        PreFix = 'Data_';
        Files = dir([ModulePaths{iM},'Data_*.mat']);
        for iF = 1:length(Files)
          fprintf('.');
          FileName = [ModulePaths{iM},Files(iF).name];
          D = load(FileName);
          TmpName = [FileName(1:end-4),'_tmp.mat'];
          save(TmpName,'-struct','D','-v7.3');
          copyfile(TmpName,FileName);  delete(TmpName);
        end; 
    end;
   fprintf([' (',num2str(length(Files)),' Trials) \n']);
    AllFiles = dir([ModulePaths{iM},PreFix,'*']);  TotalBytes = TotalBytes + sum([AllFiles.bytes]);
  end; fprintf('\n\n')
  
  % LATER INSERT SPIKE SORTING & VIDEO ANALYSIS HERE
  
  
  % TRANSFER FILES TO THE SERVER
  EstSeconds = TotalBytes/BytesPerSecond; EstHours = floor(EstSeconds/3600);
  EstMinutes = floor((EstSeconds - EstHours*3600)/60);  FinishTime = datestr(now+EstSeconds/86400);
  fprintf(['Transferring Data ... ( ',num2str(TotalBytes/1.024e9,3),'GB  =  ',num2str(EstHours),'h ',num2str(EstMinutes),'min  =>  ',FinishTime,') \n']);
  Origin = [Paths.Local,'*'];  Destination = Paths.DB;
  mkdirAll(Destination);  tic;   copyfile(Origin,Destination); DT = toc;
  BytesPerSecond = TotalBytes/DT; Hours = floor(DT/3600); Minutes = floor((DT - Hours*3600)/60);
  fprintf(['Transfer finished (',num2str(Hours),'h ',num2str(Minutes),'min at ',num2str(BytesPerSecond/1e6),'MB.s  =>  ',datestr(now),') \n']);
  
  % MARK RECORDING AS BACKUPED
  mysql(['UPDATE recordings SET onserver = 1 WHERE id=',num2str(cR.id)]);
  fprintf('\n');
  if AbortBackup
    disp('Abort Backup received .... safely breaking out of backop look.'); break;
  end
end
disp('========= DONE ==========');
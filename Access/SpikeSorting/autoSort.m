function FIG = autoSort(varargin)  
% R = autoSort(Data,varargin)
%
% The following results will be extracted:
%  SPs: Spiketimes for each unit
%  mWaves : Average Waveforms for each of the Delays
%  SNRs : averge Signal-to-Noise ratio for each unit
%
% AUTOMATIC SORTING
% - Similarity estimated by dynamics (time from min to max, halfwidths, shape of PSTH, e.g. time of peak)
%
% SEE ALSO:
% testAutoSort, C_findSpikes, C_getResponse

% Coding Comments:
% - NClust : Number of Clusters formed (a cell can be composed of multiple clusters)
% - NCells : Number of Cells Selected

% ToDo
% - Remove odd shapes during sorting
% - Limit total width to remove double triggers, and clusters which trigger
%   again at the end (check for deviation before the trigger point)
% - Maximum criterion?
% - Maximal number of separated cells?? Maybe on the order of 5?
% - Revisit Fusion for small size clusters and shapes
% - Maybe write a better test set of waveform to test fusion


%% SET VARIABLES
global QG Verbose Plotting;
P = parsePairs(varargin);
checkField(P,'Data',[]);
checkField(P,'SR',30000);
checkField(P,'Mode','Supervised'); % Choose between supervised and unsupervised sorting

% CONFIGURE SORT
checkField(P,'Threshold',-3.6);
checkField(P,'ThreshType','Amplitude');
checkField(P,'AlignMethod','WeightedMean');
checkField(P,'RejectionThreshold',30); % Threshold (in SD) for excluding large events
checkField(P,'RejectionRange',0.01); % Exclude 10ms before and after
checkField(P,'ClustSel',NaN); 
checkField(P,'NClust',5);
checkField(P,'NCellMax',P.NClust);
checkField(P,'NClustDims',5);
checkField(P,'PermInd',[ ]);
checkField(P,'Linkage','ward');
checkField(P,'Nmax',2000);
checkField(P,'Projector',[]);
checkField(P,'FusionThreshold',0.08);
checkField(P,'DistMixing',0.5);
checkField(P,'Mode','Supervised')
% SELECT TIMING 
checkField(P,'ISIDur',0.0008);
checkField(P,'PreDur',0.0007); 
checkField(P,'PostDur',0.0012);
% DESCRIBE RECORDING
checkField(P,'TrialIndices',1);
checkField(P,'Electrodes',[]);
checkField(P,'Sorter',[]);
checkField(P,'Identifier',[]);
checkField(P,'Recordings',1);
checkField(P,'StimStart',0); 
checkField(P,'StimStop',0);
checkField(P,'Range',[]);
checkField(P,'FIG',mod(round(now*1e6),1e9));
checkField(P,'Verbose',0);
checkField(P);

%if strcmpi(Data,'test') [Data,P.TrialIndices] = LF_createTestData; end

try import java.awt.Robot; QG.Mouse = Robot; end

% CREATE ID
FID = ['F',n2s(P.FIG)];

% ADD RECORDING INFO AND FID TO QG
if isfield(QG,FID) QG = rmfield(QG,FID); end
QG.(FID).P = P; QG.(FID).FIG = P.FIG;

% ADD DATA FROM INPUT IF NOT EMPTY
if ~isempty(P.Data)
  if isempty(P.Range)
    QG.(FID).Data = P.Data;
  else
    disp('Warning: limiting range!');
    iStart = round(P.Range(1)*P.SR)+1;
    iStop = round(P.Range(2)*P.SR)+1;
    QG.(FID).Data = P.Data(iStart:iStop,:);
  end  
  P = rmfield(P,'Data');
end

% NORMALIZE DATA INTO SD UNITS
NSteps = size(QG.(FID).Data,1);
SelInd = round(linspace(1,NSteps,10000));
SD = 1.48*mad(QG.(FID).Data(SelInd,:),1); % Relation between MAD and SD is a constant scaling
QG.(FID).Data = bsxfun(@rdivide,QG.(FID).Data,SD);  

QG.(FID).LData = NSteps/P.SR; 
QG.(FID).NElectrodes = size(QG.(FID).Data,2);
NElectrodes = QG.(FID).NElectrodes;
if length(P.Threshold)==1 P.Threshold =  repmat(P.Threshold,1,NElectrodes); end
QG.(FID).P.Threshold = P.Threshold;

LF_excludeLargeEvents(FID);

% AVOID DISCRETIZATION PLATEAUS BY ADDING A TINY BIT OF NOISE TO THE DATA
cSteps = round(NElectrodes*NSteps/10);
for i=1:10
    cInd = [(i-1)*cSteps+1 : min(i*cSteps,numel(QG.(FID).Data))];
    QG.(FID).Data(cInd) = QG.(FID).Data(cInd)+1e-10*rand(1,length(cInd));
end

% ASSIGN GLOBAL
LF_assignParameters(FID)

%% BUILD GUI
% PREPARE FIGURE
figure(P.FIG); clf; QG.(FID).GUI.SS = get(0,'ScreenSize');
set(P.FIG,'MenuBar','none','Toolbar','figure',...
    'Position',[10,QG.(FID).GUI.SS(4)-850,1200,800],...
    'DeleteFcn',{@LF_closeFig,FID},'NumberTitle','off',...
    'Name',['QuickSort : ',P.Identifier]);

% ADD DATA PLOTS
% PREPARE AXES
DM = axesDivide([.2,.8],1,[0.01,0.01,0.95,0.98],[.07],[]);
DM{2} = DM{2}.*[1,1,1,0.95];
tmp = axesDivide([1],[1.5,1],DM{2}+[0,.12,0,-0.1],[],[.4]);
DCtmp = axesDivide([1.4,1],[1],tmp{1},[.3],[]);
DC(1) = DCtmp(2);
DC([2:4]) = axesDivide([1,1,0.5],[1],tmp{2},[.3],[]);
DC{end+1} = [DC{2}(1),0.04,DC{2}(3),DC{2}(2)-0.06];
DC{end+1} = [DC{3}(1),0.04,DC{3}(3),DC{3}(2)-0.06];
DC(end+1:end+NElectrodes) = axesDivide([ceil(sqrt(NElectrodes))],[ceil(sqrt(NElectrodes))],DCtmp{1},[0.2],[0.2])';

for i=1:numel(DC)
    QG.(FID).GUI.Axes(i) = axes('Pos',DC{i},QG.FigOpt.AxisOpt{:}); hold on;
    if i==6 set(QG.(FID).GUI.Axes(i),'Visible','Off'); end
    %if i>=7 set(QG.(FID).GUI.Axes(i),'XTickLabel',{}); end
end

set(QG.(FID).GUI.Axes(2),'ButtonDownFcn',{@LF_CBF_Raster,FID});
QG.(FID).GUI.Clusters = QG.(FID).GUI.Axes(1);
QG.(FID).GUI.Raster = QG.(FID).GUI.Axes(2);
QG.(FID).GUI.Trace = QG.(FID).GUI.Axes(3);
QG.(FID).GUI.ISI = QG.(FID).GUI.Axes(4);
QG.(FID).GUI.PSTH = QG.(FID).GUI.Axes(5);
QG.(FID).GUI.TraceZoom = QG.(FID).GUI.Axes(6);
QG.(FID).GUI.Waves = QG.(FID).GUI.Axes(7:end);

set(QG.(FID).GUI.Clusters,'ButtonDownFcn',{@Rotator});
set(P.FIG,'WindowButtonUpFcn','global Rotating_ ; Rotating_ = 0;');

% ADD TRACE STEPPING
Strings = {'<','>'}; cPos = DC{3};
Pos = {[cPos(1),cPos(2)+1.05*cPos(4),cPos(3)/8,cPos(4)/8],...
    [cPos(1)+7/8*cPos(3),cPos(2)+1.05*cPos(4),cPos(3)/8,cPos(4)/8]};
for i=1:2
    QG.(FID).GUI.Linkage = uicontrol('style','pushbutton',...
        'Units','normalized','Position',Pos{i},'FontSize',7,...
        'String',Strings{i},'CallBack',{@LF_CBF_stepTrace,FID,Strings{i}});
end

% ADD THRESHOLD CHANGING
set(QG.(FID).GUI.Waves,'ButtonDownFcn',{@LF_CBF_axisClick,FID});

% ADD MOVIE SLIDER
SliderPos = [DC{1}(1)+1.05*DC{1}(3),DC{1}(2),.02,DC{1}(4)];
QG.(FID).GUI.ClusterSlider = uicontrol('style','slider',...
    'Units','norm','Pos',SliderPos,...
    'Callback',{@LF_CBF_SliderPos,FID},...
    'Value',0,'BackGroundColor',[1,1,1]);
% ADD PLAY BUTTON
Pos = [SliderPos(1),SliderPos(2)+1.01*SliderPos(4),SliderPos(3),0.04];
QG.(FID).GUI.MoviePlayer = uicontrol('style','pushbutton',...
    'Units','normalized','Position',Pos,'FontSize',7,...
    'String','>','CallBack',{@LF_CBF_MoviePlayer,FID});
  
% ADD CONTROLS
DC = axesDivide([1],[15,3,1,1,1.5],DM{1},[],[.1]);
QG.(FID).GUI.ClusterPos = DC{1};

% ADD CORRELATION MATRIX
QG.(FID).GUI.ClusterSeparation = axes('Pos',DC{2}+[0.01,0.02,0,0]);
QG.(FID).GUI.ClusterSeparationH = imagesc(1); colorbar;

DC2 = axesDivide([3,1.3],[1],DC{3},[0.5],[]);
% ADD THRESHOLD SELECTOR
QG.(FID).GUI.Threshold = LF_addEdit(P.FIG,...
    DC2{1},sprintf('%2.1f ',P.Threshold),{@LF_CBF_setThreshold,FID},'Set Threshold (in S.D.)');
% ADD FUSION THRESHOLD SELECTOR
QG.(FID).GUI.FusionThreshold = LF_addEdit(P.FIG,...
    DC2{2},sprintf('%1.3f ',P.FusionThreshold),{@LF_CBF_setFusionThreshold,FID},'Set Fusion Threshold (between 0 and 1)');

  DC2 = axesDivide([1.3,0.8,3],[1],DC{4},[0],[]);
% ADD NVEC SELECTOR
QG.(FID).GUI.NClust = LF_addEdit(P.FIG,...
    DC2{1},n2s(P.NClust),{@LF_updateFit,FID},'Set Number of Clusters');
% ADD LINKAGE SELECTOR
QG.(FID).Linkages = {'average','centroid','complete','median','single','ward','weighted'};
QG.(FID).LinkageInd = find(strcmp(P.Linkage,QG.(FID).Linkages));
QG.(FID).GUI.Linkage = LF_addDropdown(P.FIG,...
    DC2{3}-[0,.01,0,0],QG.(FID).Linkages,QG.(FID).LinkageInd,...
    {@LF_CBF_setLinkage,FID},'','Choose Linkage Style for Clustering');

DC2 = axesDivide([1,1,0.2],[1],DC{5},[.3,0.3],[]);
% ADD REFIT BUTTON
QG.(FID).GUI.Fit = LF_addPushbutton(P.FIG,...
    DC2{1},'Cluster',{@LF_updateFit,FID},'Rerun clustering ...');
% ADD SAVING BUTTON
QG.(FID).GUI.Save = LF_addPushbutton(P.FIG,...
  DC2{2},'Save',{@LF_saveResults,FID},'Save fitted cells ...');
% ADD PERMIND CHECK
QG.(FID).GUI.UsePermInd = LF_addCheckbox(P.FIG,...
    DC2{3},1,{@LF_CBF_changePermInd,FID},'Unscramble by condition or show in temporal sequence');
QG.(FID).UsePermInd = 1;

QG.(FID).NewThreshold = 1; QG.(FID).NewSorting =1;
QG.(FID).Threshold = P.Threshold; QG.(FID).NClust = P.NClust;
QG.(FID).CurrentRep = 1;
QG.(FID).CurrentElectrode = 1;
QG.(FID).WaveOpt = 'sd';
QG.(FID).WaveToggle = 0;

% ADD KEYBOARD CONTROL
switch P.Mode
  case 'Supervised';
    set(P.FIG,'KeyPressFcn',{@LF_KeyPress,FID});
end

%% INITIALIZE FIT
% calls fitting function which updates the current fitting state
% with new NClust, Threshold and other parameters
LF_updateFit([],[],FID)

FIG = P.FIG;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function LF_KeyPress(handle,event,FID)

global QG
CC = get(handle,'CurrentCharacter');
switch CC
    case 'p'; set(QG.(FID).GUI.UsePermInd,'Value',~get(QG.(FID).GUI.UsePermInd,'Value'));
        LF_CBF_changePermInd(QG.(FID).GUI.UsePermInd,[],FID);
    case 'f';
        MaxSize = get(0,'ScreenSize') + [0,40,0,-90]; cPosition = get(gcf,'Position');
        if cPosition == MaxSize   set(gcf,'Position',QG.(FID).OldPosition);
        else QG.(FID).OldPosition = cPosition; set(gcf,'Position',MaxSize);
        end
    case 'c'; LF_updateFit([],[],FID);
    case 's'; LF_saveResults([],event,FID);
    case 'w'; % toggle display of individual or sd waves
        if strcmp(QG.(FID).WaveOpt,'sd') QG.(FID).WaveOpt = 'individual'; else QG.(FID).WaveOpt = 'sd'; end
        QG.(FID).WaveToggle = 1;
        LF_plotWaves(FID);
        LF_collectHandles(FID);
    otherwise
        Num = str2num(CC); NClust = QG.(FID).NClust;
        if ~isempty(Num)
            set(QG.(FID).FIG, 'SelectionType','normal');
            if isfield(QG.(FID),'ShowState')
              QG.(FID).ShowState = ~QG.(FID).ShowState;
            else
              QG.(FID).ShowState = 0; 
            end
            if QG.(FID).ShowState==0 State = 'on'; else State='off'; end
            if Num==0
                for i=1:NClust 
                  LF_CBF_showCluster(QG.(FID).GUI.ClusterLabels(i),event,FID,i,State); 
                end
            elseif Num>0 && Num<=NClust
                LF_CBF_showCluster(QG.(FID).GUI.ClusterLabels(Num),event,FID,Num);
            end
        end
end

function LF_assignParameters(FID)
% ASSIGN GLOBAL VARIABLES
global QG

P = QG.(FID).P;
QG.(FID).PreSteps = fix(P.SR*P.PreDur);
QG.(FID).PostSteps = fix(P.SR*P.PostDur);
QG.(FID).ISIDurSteps = fix(P.SR*P.ISIDur);
QG.(FID).WaveTime = [-QG.(FID).PreSteps:QG.(FID).PostSteps]/P.SR;
QG.(FID).BackwardSteps = round(P.SR*0.0003);
QG.(FID).ForwardSteps = round(P.SR*0.0005);
Center = QG.(FID).PreSteps+1;
QG.(FID).ClustRange = ...
    [Center-QG.(FID).BackwardSteps:Center+QG.(FID).ForwardSteps];
QG.(FID).NTrials = length(QG.(FID).P.TrialIndices);
if isempty(QG.(FID).P.PermInd) | isnan(QG.(FID).P.PermInd)
    QG.(FID).P.PermInd = [1:QG.(FID).NTrials];
end
QG.(FID).TrialBounds = zeros(QG.(FID).NTrials,2);
QG.(FID).P.TrialIndices = vertical(QG.(FID).P.TrialIndices);
IndTmp = [QG.(FID).P.TrialIndices;size(QG.(FID).Data,1)];
for i=1:QG.(FID).NTrials
    QG.(FID).TrialBounds(i,1:2) = IndTmp([i,i+1])';
end
QG.(FID).PlotXMax = max(diff(IndTmp))/P.SR;
if ~isfield(QG.(FID).P,'Indices')
  QG.(FID).P.Indices = ones(1,QG.(FID).NTrials);
end
QG.(FID).RecordingBounds(:,1) = [1,find(diff(round(QG.(FID).P.Indices)))+1]';
QG.(FID).RecordingBounds(:,2) = [QG.(FID).RecordingBounds(2:end,1)-1;QG.(FID).NTrials];
QG.(FID).CurrentRecording = 'all';

% SET PLOTTING OPTIONS
QG.Colors.ColorOn = [1,1,1];
QG.Colors.ColorOff = [1,0.9,0.9];
QG.Colors.Panel = [1,1,1];
QG.Colors.Font = [0,0,0];
QG.FigOpt.LineWidth = 3;
QG.FigOpt.LineCol = [.6,.6,.6];
QG.FigOpt.AxisOpt = {'FontSize',8,'Box','on','XGrid','on','YGrid','on'};
QG.FigOpt.AxisLabelOpt = {'FontSize',9};
QG.FigOpt.LineCol = [.6,.6,.6];
QG.FigOpt.FontName = 'Dialog';

% ANALYSIS FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function LF_updateFit(obj,event,FID)
% UPDATE THE FITTING
global QG;

LF_CBF_setNClust(QG.(FID).GUI.NClust,[],FID);
set(QG.(FID).GUI.Fit,'ForeGroundColor',[1,0,0]);

% COLLECT EVENTS (IF NECESSARY)
if QG.(FID).NewThreshold
  tic; LF_collectEvents(FID);  fprintf(['Events collected in ',num2str(toc),'s\n']);
  QG.(FID).NewSorting = 1;
end

% RUN SORTING AND PLOT (IF NECESSARY)
if QG.(FID).NewSorting
    tic; LF_runSorting(FID);   fprintf(['Events sorted in ',num2str(toc),'s\n']);
    LF_computeSNR(FID);
    LF_showClusterSelect(FID);
    LF_plotFits(FID);
    LF_collectHandles(FID);
    tic; LF_assignCells([],[],FID);   fprintf(['Events assigned in ',num2str(toc),'s\n']);
    LF_setClusterColors(FID);
end

QG.(FID).NewThreshold = 0; QG.(FID).NewSorting = 0;

set(QG.(FID).GUI.Fit,'ForeGroundColor',[0,0,0]);

drawnow;

switch QG.(FID).P.Mode
  case 'Unsupervised'; LF_saveResults([],[],FID);
end

%LF_defocus(FID);

function LF_collectEvents(FID)
% START COLLECTING SPIKES
global QG Verbose
FIG = QG.(FID).FIG;
UD = get(FIG,'UserData');
Threshold = QG.(FID).Threshold;
SR = QG.(FID).P.SR;

NSteps = size(QG.(FID).Data,1);
NElectrodes = size(QG.(FID).Data,2);
MinSearchSteps =   QG.(FID).ISIDurSteps;

% COLLECT THRESHOLD CROSSINGS
for iE = 1:NElectrodes
  cData = QG.(FID).Data(:,iE);
  % THRESHOLD SPIKES
  switch QG.(FID).P.ThreshType
    case 'Amplitude';      if Threshold(iE) > 0  ind = find(cData>Threshold(iE));
      else ind = find(cData<Threshold(iE)); end
    case 'Rise';
      Threshold = std(diff(cData))*Threshold(iE);
      if Threshold(iE) > 0  ind = find(diff(cData)>Threshold(iE));
      else ind = find(diff()<Threshold(iE)); end
    otherwise error('ThreshType not implemented!');
  end
  if isempty(ind) % Possibility to exit with no spikes or change Threshold
    warning('No Spikes found! Choose a lower threshold!'); return;
  end
  if length(ind) > 1
    dind = diff(ind);  ind2 = find(dind>1);
    SP{iE} = zeros(1,length(ind2)+1); SP{iE}(1) = ind(1);  SP{iE}(2:end) = ind(ind2+1);
  else
    SP{iE} = ind; 
  end
  Nspikes = length(SP{iE});
  if Verbose 
    fprintf(['El.',num2str(iE),' : [ ',n2s(Nspikes),' ] triggers at threshold ',n2s(Threshold(iE)),'\n'])
  end
end
clear ind dind;

% % EXCLUDE VERY CLOSE TRIGGERS WITHIN AN ELECTRODE
TriggerJitter = 10;
for iE=1:NElectrodes
  ITIs = diff(SP{iE});
  IndLongDists = find(ITIs>=TriggerJitter);
  SP{iE} = SP{iE}(IndLongDists);
end

% FUSE TRIGGERS ACROSS ELECTRODES AND EXCLUDE MULTIPLE TRIGGERS
[SP,iSel] = unique([SP{:}]);
% ITIs = diff(SP); 
% IndLongDists = find(ITIs>=TriggerJitter);
% SP = [SP(1),SP(IndLongDists+1)];

% CHECK SPIKE TIMES
Nspikes = length(SP); Ncrit = 5;
if Nspikes < Ncrit 
  fprintf(['Only ',n2s(Nspikes),' were found. Proceed with caution!\n']);
end
iStart = length(find(SP<=QG.(FID).PreSteps))+1;
Walign = zeros(Nspikes,1);
Nspikes = find(SP<length(QG.(FID).Data)-QG.(FID).PostSteps,1,'last');
RemoveInd = [1:iStart-1]; % Remove PreSteps violations
PreSteps = round(0.0004*SR);
PostSteps = round(0.0004*SR);

Kernel = exp(-[-PreSteps:PostSteps]'.^2/5^2);
Kernel = repmat(Kernel,1,4);

% PERFORM ALIGNMENT BASED ON MINIMUM OR WEIGHTED MEAN
switch QG.(FID).P.AlignMethod
  case 'Minimum'; MinAlignFlag = 1;
  case 'WeightedMean'; MinAlignFlag = 0;
end
for i=iStart:Nspikes
  Snippet = double(QG.(FID).Data(SP(i)-PreSteps:SP(i)+PostSteps,:));
  if Threshold(1) > 0    Snippet = -Snippet; end
  if MinAlignFlag % Use Absolute Minimum for Alignment
    [MIN,ElecMin] = min(min(Snippet));
    Snippet = Snippet(:,ElecMin);
    [MIN,MinPos] = min(Snippet); 
  else % Use Center of Mass for Alignment
    ASnippet = bsxfun(@times,Kernel,Snippet.^2);
    MinPosAll = sum(ASnippet./repmat(sum(ASnippet),size(ASnippet,1),1) .* repmat([1:length(ASnippet)]',1,size(ASnippet,2)));
    SDSnippet = sqrt(sum(ASnippet.^2));
    MinPos = (SDSnippet./sum(SDSnippet))*MinPosAll';
  end
  Walign(i) = SP(i) + MinPos - PreSteps - 1;
end
RemainInd = setdiff([1:Nspikes],RemoveInd); Walign = Walign(RemainInd);
Walign = unique(Walign); % remove doubles

% REMOVE SPIKES WHICH ARE TOO CLOSE TO EACH OTHER
AlignJitter = round(0.0003*SR);
ITIs = diff(Walign);
IndLongDists = find(ITIs>=AlignJitter);
Walign = Walign(IndLongDists);

Nspikes = length(Walign);

% COLLECT INDIVIDUAL WAVEFORMS
QG.(FID).Waves=zeros(Nspikes,NElectrodes,QG.(FID).PreSteps+QG.(FID).PostSteps+1,'single');
Steps = [1:QG.(FID).PreSteps+QG.(FID).PostSteps+1];
for iS=1:Nspikes
  % CUT OUT INTEGER PART
  IntPos = floor(Walign(iS));
  tmp = QG.(FID).Data(IntPos-QG.(FID).PreSteps:IntPos+QG.(FID).PostSteps,:);
  % ALIGN BETWEEN SAMPLES
  QG.(FID).Waves(iS,:,:) = interp1(Steps,tmp,Steps+Walign(iS)-IntPos,'linear','extrap')';
end
QG.(FID).Walign = Walign;
QG.(FID).Nspikes = Nspikes;

function LF_plotFits(FID)
global QG

LF_plotWaves(FID);
LF_plotClusters(FID);
LF_plotRaster(FID);
LF_plotPSTH(FID);
LF_plotTrace(FID,1);
LF_plotISIhist(FID);
LF_plotClusterSeparation(FID);

function LF_defocus(FID)
global QG
try
    import java.awt.event.*;
    
    FigPos = get(QG.(FID).FIG,'Position');
    QG.Mouse.mouseMove(FigPos(1)+5,FigPos(2)-10);
    QG.Mouse.mousePress(InputEvent.BUTTON1_MASK);
    QG.Mouse.mouseRelease(InputEvent.BUTTON1_MASK)
end

function LF_showClusterSelect(FID)
global QG

NClust = QG.(FID).NClust;
try
    delete(QG.(FID).GUI.ClusterLabels);
    delete(QG.(FID).GUI.ClusterSelects);
    delete(QG.(FID).GUI.ClusterFusion);
    QG.(FID).GUI.ClusterLabels =  zeros(NClust,1);
    QG.(FID).GUI.ClusterSelects =  zeros(NClust,1);
catch; end
DC = axesDivide([0.25,0.75],1,QG.(FID).GUI.ClusterPos,[0.01],[]);
NMaxNormal = max(14,NClust);
NewHeight = NClust/NMaxNormal*DC{1}(4);
DC{1}(2) = DC{1}(2)+DC{1}(4)-NewHeight; DC{1}(4) = NewHeight;

% PLOT THE CLUSTER SEPARATION PLOT
QG.(FID).GUI.ClusterFusion = axes('Position',DC{1}); axis off

% CREATE
for i=1:NClust
    if QG.(FID).GUI.ShowState(i) cBackColor = QG.Colors.ColorOn; else cBackColor = QG.Colors.ColorOff; end
    cFrame = [DC{2}(1),1-DC{2}(4)*i/NMaxNormal,DC{2}(3),DC{2}(4)/(1.2*NMaxNormal)];
    cDC = axesDivide([1,3],[1],cFrame,[.2],[]);
    if isfield(QG.(FID),'ColorsByCluster')
      cColor = QG.(FID).ColorsByCluster{i}; 
    else cColor = [0,0,0];
    end
    
    QG.(FID).GUI.ClusterSelects(i) = uicontrol('style','edit',...
        'Units','normalized',...
        'Position',cDC{1}.*[1,1,1,1.2],...
        'String','','ForeGroundColor',[0,0,0],...
        'FontSize',9,...
        'HorizontalAlignment','center',...
        'FontName','Dialog','BackGroundColor',[1,1,1],...
        'Callback',{@LF_updateCells,FID,i});
    LS{i} = ['C',n2s(i),' (',sprintf('%1.1f | ',QG.(FID).SNRs(i)),n2s(length(QG.(FID).Inds{i})),')'];
    QG.(FID).GUI.ClusterLabels(i) = uicontrol('style','text',...
        'Units','normalized','Enable','inactive',...
        'Position',cDC{2},...
        'String',LS{i},'ForeGroundColor',cColor,...
        QG.FigOpt.AxisLabelOpt{:},'FontWeight','bold',...
        'HorizontalAlignment','left','FontSize',7,...
        'FontName','Dialog','BackGroundColor',cBackColor,...
        'ButtonDownFcn',{@LF_CBF_showCluster,FID,i});
end

% SET CLUSTERS IN GUI
for iC = 1:NClust
  if ~isnan(QG.(FID).CellByCluster(iC))
    cString = num2str(QG.(FID).CellByCluster(iC));
  else 
    cString = '';
  end
  set(QG.(FID).GUI.ClusterSelects(iC),'String',cString);
end


function LF_assignCells(obj,event,FID)
global QG

% COLLECT ALL ASSIGNED CELLS AND ORDER BY CELL
QG.(FID).ClustersByCell = {};
QG.(FID).UnassignedClusters = [];
for iC=1:length(QG.(FID).GUI.ClusterSelects)
  cObj =  QG.(FID).GUI.ClusterSelects(iC);
  cCell =  str2num(get(cObj,'String'));
  if ~isempty(cCell)
    QG.(FID).CellByCluster(iC) = cCell;
    if length(QG.(FID).ClustersByCell)<cCell
      QG.(FID).ClustersByCell{cCell} = iC;
    else
      QG.(FID).ClustersByCell{cCell}(end+1) = iC;
    end
  else 
    QG.(FID).UnassignedClusters(end+1) = iC;
  end
end
QG.(FID).NCells = length(QG.(FID).ClustersByCell);

LF_setClusterColors(FID);

function LF_updateCells(obj,event,FID,cClust)
global QG

LF_assignCells([],[],FID);

% PLOT THE SEPARATION BETWEEN CLUSTERS
NClust = length(QG.(FID).CellInds);
D = QG.(FID).ClusterSeparation;  k=0;
Dtmp = D; Dtmp(isinf(D)) = NaN;
MaxSep = nanmax(Dtmp(:)); MinSep = nanmin(Dtmp(:));
D(isnan(D))=0; D = D+D';
axes(QG.(FID).GUI.ClusterFusion);
set(QG.(FID).GUI.ClusterFusion,'YDir','reverse'); cla; hold on;
for i=[1:cClust-1,cClust+1:NClust]
    ScaledSep = 1-(D(cClust,i)-MinSep)/(MaxSep-MinSep);
    if ScaledSep > 0.95 k=k+1;
        plot(QG.(FID).GUI.ClusterFusion,[-k,-k],[i,cClust],'Color',[ScaledSep,0,0],'LineWidth',(ScaledSep+1));
    end
end;
if k>0 axis([-(k+0.5),-0.5,0.8,NClust+0.8]); end
axis off;

function LF_excludeLargeEvents(FID)
% REJECT PASSAGES IN THE DATA
% including a broadening

global QG
SR = QG.(FID).P.SR;
NElectrodes = QG.(FID).NElectrodes;
Range = [0:round(QG.(FID).P.RejectionRange*SR)];
for iE = 1:NElectrodes
  RejInd = abs(QG.(FID).Data(:,iE))>QG.(FID).P.RejectionThreshold;
  DiffRejInd = diff(RejInd);
  StartPos = find(DiffRejInd==1);
  StopPos = find(DiffRejInd==-1);
  for i=1:length(StartPos)
    cPos = StartPos(i)+1;
    if cPos - Range(end)>=1
      QG.(FID).Data(cPos-Range,:) = 0;
    else
      QG.(FID).Data(1:cPos,:) = 0;
    end
  end
  for i=1:length(StartPos)
    cPos = StartPos(i)+1;
    QG.(FID).Data(cPos+Range,:) = 0;
  end
end

function LF_clusterSeparation(FID)
global QG

NClust = QG.(FID).NClust;
Means = zeros(3,NClust);
ClusterSep = NaN * zeros(NClust);
Delays = [-10:10]; DistMethod = 'Mixed';
WaveSteps = length(QG.(FID).mWaves{1});
NChannels = size(QG.(FID).mWaves,2); 
Ind = bsxfun(@plus,[1:WaveSteps]',[0:WaveSteps:(NChannels-1)*WaveSteps]); Ind = Ind(:);

WaveSizes = zeros(1,NClust);
for iC=1:NClust 
   mWave = mat2vec([QG.(FID).mWaves{iC,:}]);
   WaveSizes(iC) = max(mWave) - min(mWave);
end
MAXWaveSize = max(WaveSizes);
 
for i=1:NClust-1
  for j=i+1:NClust
    % TAKE THE SIZE OF THE CROSSCORRELATION BETWEEN CLUSTER
    mWave1 = mat2vec([QG.(FID).mWaves{i,:}]);
    mWave2 = mat2vec([QG.(FID).mWaves{j,:}]);
    mWave1 = mWave1(Ind);
    mWave2 = mWave2(Ind);
    NSteps = length(mWave1);
    cDist = zeros(size(Delays));
    for iT = 1:length(Delays)
      cDelay = Delays(iT);
      Ind1 = [1:NSteps-abs(cDelay)];
      Ind2 = [abs(cDelay)+1:NSteps];
      if cDelay>0
        D1 = mWave1(Ind1); D2 = mWave2(Ind2);
      else
        D1 = mWave1(Ind2); D2 = mWave2(Ind1);
      end
      switch DistMethod
        case 'Corr';
          cDist(iT)= 1-corr(D1,D2);
          cDist(iT) = min([cDist(iT),1]);
        case 'Norm';
          cDist(iT) = norm(D1-D2)/(norm(D1) + norm(D2));
        case 'Mixed';
          CorrDist(iT)= 1-corr(D1,D2);
          CorrDist(iT) = min([CorrDist(iT),1]);
          NormDist(iT) = norm(D1-D2)/(norm(D1) + norm(D2))/5;
          MixingParameter = 1/(1+exp(-(((WaveSizes(i)+WaveSizes(j))/(2*MAXWaveSize))-0.5)/0.1));
          cDist(iT) = MixingParameter*CorrDist(iT) + (1-MixingParameter)*NormDist(iT);
      end
    end
    % USE MINIMAL DISTANCE ACROSS TIME SHIFTS 
    ClusterSep(i,j) =  min(cDist);
  end
end

ClusterSepBase = ClusterSep;

% EXCLUDE CLUSTERS BASED ON SHAPE
Excluded = logical(zeros(1,NClust));
CenterSteps = round(QG.(FID).P.PreDur*QG.(FID).P.SR);

for iC=1:NClust
  Unipolar = 0; Wide = 0;
  % FIND CHANNEL OF MAXIMUM AMPLITUDE
  mWaves = cell2mat(QG.(FID).mWaves(iC,:));
  [maxAmp,maxChannel] = max(max(mWaves));
  
  % CHECK FOR BIPOLAR SHAPE
  mWave = QG.(FID).mWaves{iC,maxChannel};
  Ind = logical((QG.(FID).WaveTime>-0.0002) .* (QG.(FID).WaveTime<0.0005)); % Remi changed the start time from -0.0001 to -0.0002
  if max(mWave(Ind))<0.5 || min(mWave(Ind))>-0.5;     Excluded(iC) = 1;  Unipolar = 1;  end

  % CHECK FOR WIDTH
  mWave = QG.(FID).mWaves{iC,maxChannel};
  NOutside = sum(abs(mWave)>1);
  ThresholdWidth = round(0.0012*QG.(FID).P.SR);
  if NOutside > ThresholdWidth;     Excluded(iC) = 1;  Wide = 1;  end

  % CHECK FOR ELONGATED NEGATIVE OR POSITIVE SHAPE AHEAD OF NEGATIVE TRIGGER
  if QG.(FID).P.Threshold < 0
    mWave = QG.(FID).mWaves{iC,maxChannel};
    StopPos = round((QG.(FID).P.PreDur-0.0002)*QG.(FID).P.SR);
    NOutside = sum(abs(mWave(1:StopPos)) > 1) ;
    ThresholdWidth = round(0.0003*QG.(FID).P.SR);
    if NOutside > ThresholdWidth;     Excluded(iC) = 1;  Wide = 1;  end
  end
      
  % CHECK FOR ELONGATED NEGATIVE SHAPE AFTER NEGATIVE TRIGGER
  if QG.(FID).P.Threshold < 0
    mWave = QG.(FID).mWaves{iC,maxChannel};
    StartPos = round((QG.(FID).P.PreDur+0.0002)*QG.(FID).P.SR);
    NOutside = sum(mWave(StartPos:end) < -1) ;
    ThresholdWidth = round(0.001*QG.(FID).P.SR);
    if NOutside > ThresholdWidth;     Excluded(iC) = 1;  Wide = 1;  end
  end
  
  % Remove late absolute maximum
  
  % Speed criterion: if spike is too slow, remove
  
  if Excluded(iC)
    fprintf(['Excluded Cluster #',num2str(iC),' ( Unipolar: ',num2str(Unipolar),' , Wide = ',num2str(Wide),' )\n']);
  end
end


% EXCLUDE THE CLUSTERS BY SETTING DISTANCE TO 1
ClusterSep(Excluded,:) = 1; ClusterSep(:,Excluded) = 1;
ClusterSep = triu(ClusterSep) + tril(NaN*zeros(size(ClusterSep)),-1);

% ASSIGN CLUSTERS TO CELLS BASED ON DISTANCES
CBC = NaN*zeros(NClust,1); cCell = 0; Threshold = QG.(FID).P.FusionThreshold;
for iC=1:NClust
  MIN = nanmin(ClusterSep(:));
  if MIN>Threshold break; end
  [i1,i2] = find(ClusterSep==MIN);
  ClusterSep(i1,i2) = 1;
  if ~isnan(CBC(i1)) && ~isnan(CBC(i2))
    [cCell,Pos] = min([CBC(i1),CBC(i2)]);
    if Pos==1
      cCell2 = CBC(i2);
      i2 = CBC==cCell2;
    else
      cCell1 = CBC(i1);
      i1 = CBC==cCell1;
    end
  elseif ~isnan(CBC(i1));  cCell = CBC(i1);
  elseif ~isnan(CBC(i2)) cCell = CBC(i2);
  else cCell = cCell + 1;
  end
  CBC(i1) = cCell;
  CBC(i2) = cCell;
end

for iC=1:NClust
  if isnan(CBC(iC)) && ~Excluded(iC)
    cCell = cCell + 1;
    CBC(iC) = cCell;
  end
end
QG.(FID).CellByCluster = CBC;
QG.(FID).ClusterSeparation = ClusterSepBase;

function LF_setClusterColors(FID)
% ASSIGN COLORS
global QG;

% ASSIGN COLORS BY CELL
if QG.(FID).NCells
  CellNums = unique(QG.(FID).CellByCluster);
  CellNums = CellNums(~isnan(CellNums));
  for iC=1:length(CellNums)
    QG.(FID).ColorsByCell{iC} = hsv2rgb([1-eps-(iC-1)/length(CellNums),1,1]);
  end
  
  % ASSIGN COLORS BY CLUSTER FOR EACH CELL
  QG.(FID).ColorsByCluster = cell(QG.(FID).NClust,1);
  for iC=1:length(QG.(FID).ColorsByCluster) QG.(FID).ColorsByCluster{iC} = [0,0,0]; end
  
  for iC=1:QG.(FID).NCells
    for iN = 1:length(QG.(FID).ClustersByCell{iC})
      cClust = QG.(FID).ClustersByCell{iC}(iN);
      QG.(FID).ColorsByCluster{cClust} = ...
        QG.(FID).ColorsByCell{iC}*(iN+4)/(length(QG.(FID).ClustersByCell{iC}) + 4);
    end
  end
end

% ASSIGN COLORS TO UNASSIGNED CLUSTERS
for iC = 1:length(QG.(FID).UnassignedClusters)
  cClust =QG.(FID).UnassignedClusters(iC);
  QG.(FID).ColorsByCluster{cClust} =   [0.5,0.5,0.5]*iC/length(QG.(FID).UnassignedClusters);
end

% ACTUALLY CHANGE THE COLOR OF THE GRAPHICS OBJECTS
if isfield(QG.(FID).GUI,'hSet')
  for iC = 1:QG.(FID).NClust
    set(QG.(FID).GUI.hSet{iC},'Color',QG.(FID).ColorsByCluster{iC});
    set(QG.(FID).GUI.ClusterLabels(iC),'ForeGroundColor',QG.(FID).ColorsByCluster{iC});
  end
end


function LF_runSorting(FID)
% START SPIKESORTING
global QG

ClustRange = QG.(FID).ClustRange;
ClustWaves = QG.(FID).Waves(:,:,ClustRange);
SW = size(ClustWaves);
QG.(FID).WavesCC = reshape(ClustWaves,[SW(1),SW(2)*SW(3)]);

NClust = QG.(FID).NClust; Nspikes = QG.(FID).Nspikes;
if Nspikes > QG.(FID).P.Nmax
  QG.(FID).PCAInd = unique(round(linspace(1,Nspikes,QG.(FID).P.Nmax)));
  QG.(FID).iPCAInd = setdiff([1:Nspikes],QG.(FID).PCAInd);
else
  QG.(FID).PCAInd = [1:Nspikes];
  QG.(FID).iPCAInd = [ ];
end


% PRINCIPAL COMPONENT ANALYSIS
Basis = 'waveform';
switch lower(Basis)
  case 'waveform';
    mWave = mean(QG.(FID).WavesCC);
    cData = QG.(FID).WavesCC - repmat(mWave,Nspikes,1);
  case 'properties';
    CutOuts = QG.(FID).Waves(:,QG.(FID).ClustRange);
    cData(:,1) = max(CutOuts,[],2);
    cData(:,2) = min(CutOuts,[],2);
    cData(:,3) = max(diff(CutOuts,1,2),[],2);
    cData(:,4) = min(diff(CutOuts,1,2),[],2);
    cData(:,5) = var(CutOuts,1,2);
  case 'wavelet';
    % Horrible code
    QG.(FID).Wavelet_WavesCC= permute(ClustWaves, [1, 3, 2]);
    QG.(FID).Wavelet_WavesCC= reshape(QG.(FID).Wavelet_WavesCC, [SW(1),SW(2)*SW(3)]);
    [nbSpikes, l] = size(QG.(FID).WavesCC);
    [C, L]= wavedec(QG.(FID).Wavelet_WavesCC(1, :), 8, 'db8');
    cData = zeros(size(C, 2), nbSpikes);
    for i = 1:nbSpikes
      [C, L]= wavedec(QG.(FID).Wavelet_WavesCC(i, :), 8, 'db8');
      cData(: , i)=C;
    end
    cData = cData';
end

if isempty(QG.(FID).P.Projector)

  CovM = double(cov(cData)); [EVec,EVal] = eigs(CovM,QG.(FID).P.NClustDims); % Covariance Matrix
  QG.(FID).Projector = EVec';
else
   QG.(FID).Projector = QG.(FID).P.Projector; 
end

PCProj = double(QG.(FID).Projector*cData');
% CLUSTERING (faster than clustvec)
Distances = pdist(PCProj(:,QG.(FID).PCAInd)','euclid');
BinTree = linkage(Distances,QG.(FID).Linkages{QG.(FID).LinkageInd});
ClustVec = cluster(BinTree,'maxclust',NClust);
Means = zeros(NClust,(QG.(FID).ForwardSteps+QG.(FID).BackwardSteps+1)*4);
Strengths = zeros(NClust,1);
for i=1:NClust
  Inds{i}=find(ClustVec==i); WInds{i} = QG.(FID).PCAInd(Inds{i})';
    Means(i,:)=mean(QG.(FID).WavesCC(WInds{i},:),1);
    Strengths(i)=std(Means(i,:))*length(Inds{i});
end;    
    
[Strengths,SInd] = sort(Strengths,'descend');
Inds = Inds(SInd); WInds = WInds(SInd);

QG.(FID).PCProj = PCProj;
QG.(FID).Inds = Inds;
QG.(FID).WInds = WInds;

% ASSIGN ALL SPIKES
AllClust = {}; for i=1:NClust AllClust{i} = i; end
LF_assignSpikes(FID,AllClust,'MapClusters');

% CHOOSE DISPLAYING EVENTS
QG.(FID).SelInd = cell(NClust,1);
for i=1:NClust
    LI=length(QG.(FID).WInds{i});
    if LI>300 QG.(FID).SelInd{i}=[1:100,101:round(LI/300):LI];
    else QG.(FID).SelInd{i}=[1:LI]; end
end

QG.(FID).YMax = 15;%max(QG.(FID).Waves(:));
QG.(FID).YMin = -15;%min(QG.(FID).Waves(:));
QG.(FID).PlotYMin = QG.(FID).YMin;
QG.(FID).PlotYMax = QG.(FID).YMax;

QG.(FID).GUI.ShowState = ones(NClust,1);
try QG.(FID) = rmfield(QG.(FID),'ColorsByCluster'); end

function LF_computeSNR(FID)
% COLLECT NOISE BASELINE
global QG

AvSteps = 10000; tmp = QG.(FID).Data(1:100000);
AllSPs = sort(cell2mat(QG.(FID).SPs));  cInd = AvSteps;
for i=1:length(AllSPs)
    cInd = round([AllSPs(i)-QG.(FID).PreSteps:AllSPs(i)+QG.(FID).PostSteps]);
    tmp(cInd) = NaN; if cInd(1)>AvSteps break; end
end
QG.(FID).StdNoise = nanstd(tmp(1:AvSteps));

% COMPUTE SNRs
QG.(FID).SNRs = max(abs(cellfun(@min,QG.(FID).mWaves))/QG.(FID).StdNoise,[ ],2); 

function LF_assignSpikes(FID,ClustSel,Option)
% MAPS ALL SPIKES TO THE CLOSEST ANALYZED SPIKE
% Arguments : 
% - ClustSel : Provides a cell of cluster numbers, which indicate which
%                 Clusters are fused (This is used only at the end when saving)
% - Option : Either 'MapCells' or 'MapClusters'
%       'MapClusters' : No fusion of clusters, just map the spikes for each cluster
%       'MapCells'      : Fusion across clusters, when combining clusters to cells
%         Important : In this last step many variable switch from a cluster to a cell representation
%                          This should be changed for clarity.

global QG

% 
NCells = length(ClustSel); NClust = QG.(FID).NClust;
NElectrodes = QG.(FID).NElectrodes;
AllSelected = cell2mat(ClustSel); CellInd = cell(NCells,1);
for iC=1:NCells
    CellInd{iC} = cell2mat(QG.(FID).WInds(ClustSel{iC})');
end
Ntmp = cellfun(@length,CellInd,'Un',0); Ntmp = [0,Ntmp{:}];
NonCellInd = sort(cell2mat(QG.(FID).WInds(setdiff([1:NClust],AllSelected))'))';
ClustInd = [cell2mat(CellInd);NonCellInd'];
Closest = dsearchn(QG.(FID).PCProj(:,ClustInd)',...
    delaunayn(QG.(FID).PCProj(:,ClustInd)'),...
    QG.(FID).PCProj(:,QG.(FID).iPCAInd)');

%
QG.(FID).SPs = cell(NCells,1);
QG.(FID).NAPs = zeros(NCells,1);
QG.(FID).WavesByCell = cell(NCells,NElectrodes);
QG.(FID).mWaves = cell(NCells,NElectrodes);
QG.(FID).CellInds = cell(NCells,1);
for iE=1:NElectrodes
  for iC=1:NCells
    AddCellInd = find((Closest > sum(Ntmp(1:iC))).*(Closest<=sum(Ntmp(1:iC+1))) );
    cCellInd = sort([CellInd{iC} ; QG.(FID).iPCAInd(AddCellInd)']);
    QG.(FID).CellInds{iC} = cCellInd;
    QG.(FID).SPs{iC} = QG.(FID).Walign(cCellInd);
    QG.(FID).NAPs(iC) = length(cCellInd);
    QG.(FID).WavesByCell{iC,iE} = single(QG.(FID).Waves(cCellInd,iE,:));
    QG.(FID).mWaves{iC,iE} = squeeze(mean(QG.(FID).WavesByCell{iC,iE},1));
  end
end

NTrials = QG.(FID).NTrials;
QG.(FID).Trials = cell(NTrials,1);
P.TrialIndices = [QG.(FID).P.TrialIndices;inf];
for j=1:NTrials
    QG.(FID).Trials{j} = cell(NCells,1);
    for iC=1:NCells
        Ind = find((QG.(FID).SPs{iC}>=P.TrialIndices(j)) .* (QG.(FID).SPs{iC}<P.TrialIndices(j+1)));
        if ~isempty(Ind)
            QG.(FID).Trials{j}{iC} = ((QG.(FID).SPs{iC}(Ind)-P.TrialIndices(j) + 1)/QG.(FID).P.SR);
        else
            QG.(FID).Trials{j}{iC} = zeros(0,1);
        end
    end
end

switch Option
  case 'MapClusters';
    LF_sortTrials(FID);
    LF_clusterSeparation(FID)
  case 'MapCells';
end

function LF_sortTrials(FID)
% RESORT THE TRIALS FOR DISPLAYING
global QG

if QG.(FID).UsePermInd
    QG.(FID).SortedTrials = QG.(FID).Trials(QG.(FID).P.PermInd);
    QG.(FID).SortedTrialBounds =  QG.(FID).TrialBounds(QG.(FID).P.PermInd,:);
else
    QG.(FID).SortedTrials = QG.(FID).Trials;
    QG.(FID).SortedTrialBounds = QG.(FID).TrialBounds;
end

% CALLBACK FUNCTIONS%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function LF_CBF_showCluster(obj,event,FID,Index,State)

global QG;

cFIG = QG.(FID).FIG;
SelType = get(cFIG, 'SelectionType');
UD = get(cFIG,'UserData');
switch SelType
    case {'normal'}; button = 1; % left
    case {'alt'}; button = 2; % right
    case {'extend'}; button = 3; % middle
    case {'open'}; button = 4; % with shift
    otherwise error('Invalid mouse selection.')
end
switch button
    case 1 % Show/Hide
        h = QG.(FID).GUI.hSet{Index};
        if ~exist('State','var') State = get(h(1),'Visible');
        elseif strcmp(State,'on') State = 'off'; else State ='on'; end % invert the desired state
        if strcmp(State,'on') % invert current state to desired state
            ShowState = 0; NewState = 'off'; NewColor = QG.Colors.ColorOff;
        else
            ShowState = 1; NewState = 'on'; NewColor = QG.Colors.ColorOn;
        end
        QG.(FID).GUI.ShowState(Index) = ShowState;
        set(h,'Visible',NewState);
        set(obj,'BackgroundColor',NewColor);
        LF_updateCells(obj,event,FID,Index)
        
    case 2 % Show only this cluster
        OtherInd = setdiff([1:length(QG.(FID).GUI.hSet)],Index);
        QG.(FID).GUI.ShowState(:) = 0;
        QG.(FID).GUI.ShowState(Index) = 1;
        h = cell2matObj(QG.(FID).GUI.hSet(OtherInd));
        set(h,'Visible','off');
        set(QG.(FID).GUI.ClusterLabels,'BackgroundColor',QG.Colors.ColorOff);
        h = QG.(FID).GUI.hSet{Index};
        set(h,'Visible','on');
        set(obj,'BackgroundColor',[1,1,1]);
        LF_updateCells(obj,event,FID,Index)
end

function LF_CBF_changePermInd(obj,event,FID)
global QG
QG.(FID).UsePermInd = get(obj,'Value');
LF_sortTrials(FID);
LF_plotRaster(FID);
LF_plotPSTH(FID);
LF_plotTrace(FID,0);
LF_collectHandles(FID);

function LF_CBF_Raster(obj,event,FID)
% CHANGE THE DISPLAYED TRACE
global QG

D = get(obj,'CurrentPoint');
SelType = get(gcf, 'SelectionType');
switch SelType
    case {'normal'}; button = 1; % left
    case {'alt'}; button = 2; % right
    case {'extend'}; button = 3; % middle
    case {'open'}; button = 4; % with shift
    otherwise error('Invalid mouse selection.')
end
switch button
    case 1 % CHOOSE TRACE
        tmp = round(D(1,2));
        if tmp <= 0 tmp = 1; end
        QG.(FID).CurrentRep = min([length(QG.(FID).P.TrialIndices),tmp]);
        LF_plotTrace(FID,1);
        LF_collectHandles(FID);
    case 2 % CHOOSE RECORDING FOR PSTH
        cTrial = round(D(1,2));
        if cTrial<0 QG.(FID).CurrentRecording = 'all';
        else QG.(FID).CurrentRecording = find(cTrial>QG.(FID).RecordingBounds(:,1),1,'last');
        end
        LF_plotPSTH(FID);
end

function LF_CBF_axisClick(obj,event,FID)
% SET THE THRESHOLD GRAPHICALLY
global QG;

D = get(obj,'CurrentPoint');
SelType = get(gcf, 'SelectionType');
switch SelType
    case {'normal','open'}; button = 1; % left
    case {'alt'}; button = 2; % right
    case {'extend'}; button = 3; % middle
    case {'open'}; button = 4; % with shift
    otherwise error('Invalid mouse selection.')
end
switch button
    case 1 % Zoom in or out
        if D(1,2) > 0 % Zoom in
            Factor = 0.5;
        else %Zoom out
            Factor = 2;
        end
        set(obj,'YLim',Factor*get(obj,'YLim'));
    case 2 % Set Threshold
        cThreshold = D(1,2); Ind = find(obj==QG.(FID).GUI.Waves);
        QG.(FID).Threshold(Ind) = cThreshold;
        set(QG.(FID).GUI.Threshold,'String',sprintf('%2.1f ',QG.(FID).Threshold));
        LF_CBF_setThreshold(QG.(FID).GUI.Threshold,[],FID)
end

function LF_CBF_stepTrace(obj,event,FID,mode)
% CHANGE THE DISPLAYED ELECTRODE
global QG;

switch mode
    case '<'; tmp = QG.(FID).CurrentElectrode - 1;
    case '>'; tmp = QG.(FID).CurrentElectrode +1;
end
if tmp <= 0 tmp = 1; end
QG.(FID).CurrentElectrode = min([QG.(FID).NElectrodes,tmp]);
LF_plotTrace(FID,0);
LF_collectHandles(FID);

function LF_saveResults(obj,event,FID)
% COLLECT RESULTS

global QG Verbose

Triggering = 0; ClustSelStr = ''; ClustSel = {};
NClust = QG.(FID).NClust;
for i=1:NClust
    cSelect = get(QG.(FID).GUI.ClusterSelects(i),'String');
    if isempty(cSelect) | unique(cSelect)==' ' cSelect = '0'; end
    ClustInd(i) = str2num(cSelect);
end
for i=1:NClust
    if ~isempty(find(ClustInd==i)) ClustSel{end+1} = find(ClustInd==i); end
end

for i=1:length(ClustSel) ClustSelStr = [ClustSelStr,' { ',sprintf('%d ',ClustSel{i}),'}']; end
if isempty(ClustSel) ClustSelStr = 'none'; end
fprintf(['Number of Clusters: ',n2s(NClust),'  -  Chosen Clusters: ',n2s(ClustSelStr),'\n']);
if ~isempty(ClustSel)
    LF_assignSpikes(FID,ClustSel,'MapCells');
    [QG.(FID).SortedSPs,SortInd] = sort(cell2mat(QG.(FID).SPs),'ascend');
    QG.(FID).SortedWaves = cell2mat(QG.(FID).WavesByCell);
    QG.(FID).SortedWaves = QG.(FID).SortedWaves(SortInd,:);
else
    QG.(FID).SPs = {}; QG.(FID).WavesByCell = {};
    QG.(FID).mWaves = {}; QG.(FID).NAPs = 0;
    disp('DONE SORTING!');
    close(QG.(FID).FIG);
    return;
end
% COMPUTE SNRs
LF_computeSNR(FID);

STRING  = sprintf([' < ',n2s(QG.(FID).NAPs'),' > spikes collected at [ ',...
  sprintf('%1.2f ',QG.(FID).NAPs'/QG.(FID).LData),...
  '] Hz and max. SNRs [ ',sprintf('%1.2f ',QG.(FID).SNRs),'].\n']);
fprintf(STRING);
cR.SortString = STRING;

P = QG.(FID).P;
cR.Parameters = P;
cR.Parameters = rmfield(cR.Parameters,'Data');
cR.Parameters.Threshold = QG.(FID).Threshold;
cR.Parameters.Linkage = QG.(FID).Linkages{QG.(FID).LinkageInd};
cR.Parameters.ClustSel = ClustSel;
cR.Parameters.Projector = QG.(FID).Projector;

% MAKE A COPY OF THE FINAL SORT FOR CHECKING
if ~isempty(QG.(FID).P.Identifier)
  cR.FileNames = [tempdir,QG.(FID).P.Identifier,'.png'];
  set(QG.(FID).FIG,'PaperUnits','centimeters','PaperSize',[50,29],'PaperPosition',[0,0,50,29])
  print(QG.(FID).FIG,cR.FileNames,'-dpng','-r100');
end

% SAVE RESULTS FOR EACH RECORDING
for iRec = 1:length(P.Recordings)
    
    %cRecInd holds the trial indexes for each recording
    cRecInd = find((QG.(FID).P.Indices>=iRec).*(QG.(FID).P.Indices<iRec+1));
    
    %MinStep is the first index 
    MinStep = P.TrialIndices(cRecInd(1));
    
    %MaxStep is the limit of the last trial index for this recording
    if iRec==length(P.Recordings)
      MaxStep = length(QG.(FID).Data);
    else
      MaxStep = P.TrialIndices(cRecInd(end))-1;
    end
    
    %cR.SPs holds the spike times for each sorted cell
    for iC=1:length(QG.(FID).SPs)
      cR.SPs{iC,1} = QG.(FID).SPs{iC}(logical((QG.(FID).SPs{iC}>=MinStep).*(QG.(FID).SPs{iC}<=MaxStep)))-MinStep+1;
    end
    
    cR.mWaves = QG.(FID).mWaves;
    cR.SNRs = QG.(FID).SNRs;
    
    %cInd holds the indexes of sorted spike times within the data points from this recording 
    %cInd = logical((QG.(FID).SortedSPs>=MinStep).*(QG.(FID).SortedSPs<=MaxStep));   
    %collect trial indexes from this recording and then make the indexes start from 0
    %cR.TrialIndices = P.TrialIndices(cRecInd) - MinStep;
    
    assignin('base','TmpResultsAutoSort',cR);
end

disp('DONE SORTING!');
close(QG.(FID).FIG);

function LF_CBF_setThreshold(obj,event,FID)
global QG
Threshold = str2num(get(obj,'String'));
QG.(FID).NewThreshold = 1;
QG.(FID).Threshold = Threshold;

function LF_CBF_setFusionThreshold(obj,event,FID)
global QG
FusionThreshold = str2num(get(obj,'String'));
QG.(FID).P.FusionThreshold = FusionThreshold;

function LF_CBF_setNClust(obj,event,FID)
global QG
NClust = str2num(get(obj,'String'));
QG.(FID).NewSorting = 1;
QG.(FID).NClust = NClust;

function LF_CBF_setLinkage(obj,event,FID)
global QG
Index = get(obj,'Value');
QG.(FID).NewSorting = 1;
QG.(FID).LinkageInd = Index;

function LF_CBF_Trace(obj,event,FID)
global QG

Electrode = QG.(FID).CurrentElectrode;

cAxis = QG.(FID).GUI.TraceZoom;
CP = get(obj,'CurrentPoint');
TSelect = CP(1,1);

SR = QG.(FID).P.SR;
iStartTrial = QG.(FID).SortedTrialBounds(QG.(FID).CurrentRep,1);
iStart = iStartTrial + round((TSelect-0.02)*SR); iStart = max(1,iStart);
iEnd = iStart + round(0.04*SR); iEnd = min(iEnd,length(QG.(FID).Data));
Trace = QG.(FID).Data(iStart:iEnd,Electrode);

% PLOT TRACE
Indices = [iStart:iEnd]; Time = (Indices-iStartTrial)/SR;
set(obj,'XTick',[]); xlabel(obj,'');
delete(get(cAxis,'Children'));
set(cAxis,'NextPlot','Add','Visible','on');
plot(cAxis,Time,Trace,'k');
plot(cAxis,Time([1,end]),repmat(QG.(FID).Threshold,2,1));
MINMAX = [min([Trace;-5]),max([Trace;5])];
axis(cAxis,[Time([1,end]),MINMAX]);
set(cAxis,'YTick',MINMAX);

% PLOT TRIGGERS
for i=1:QG.(FID).NClust
    cSPs = QG.(FID).SortedTrials{QG.(FID).CurrentRep}{i};
    if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
    QG.(FID).GUI.hTraceZoom{i} = plot(cAxis,1,1,'Marker','.','LineStyle','none','MarkerSize',40,'HitTest','off');
    set(QG.(FID).GUI.hTraceZoom{i},...
        'XData',cSPs,...
        'YData',repmat(4,length(cSPs),1),...
        'Color',QG.(FID).ColorsByCluster{i},'Visible',State,'MarkerSize',30);
end

function LF_CBF_MoviePlayer(obj,event,FID)
global QG

NFrames = 100;
NClust = QG.(FID).NClust;

for i=1:NFrames
    set(QG.(FID).GUI.ClusterSlider,'Value',i/NFrames)
    LF_CBF_SliderPos(QG.(FID).GUI.ClusterSlider,event,FID)
    pause(0.025);
end

pause(1);
delete(QG.(FID).GUI.hClusterTime);
QG.(FID).GUI = rmfield(QG.(FID).GUI,'hClusterTime')
for i=1:NClust
    if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
    set(QG.(FID).GUI.hCluster{i},'Visible',State);
end

function LF_CBF_SliderPos(obj,event,FID)
global QG

cAxis = QG.(FID).GUI.Clusters;
set(cell2matObj(QG.(FID).GUI.hCluster),'Visible','off');
SliderVal = get(obj,'Value');
NClust = QG.(FID).NClust;
if isfield(QG.(FID).GUI,'hClusterTime') try delete(QG.(FID).GUI.hClusterTime); end; end
QG.(FID).GUI.hClusterTime = [];
Walign = QG.(FID).Walign;
NSteps = floor(QG.(FID).LData*QG.(FID).P.SR);

for i=1:NClust
    %  if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
    State = 'on';
    cWalign = Walign(QG.(FID).WInds{i}(QG.(FID).SelInd{i}));
    cInd = find((cWalign>=0.9*SliderVal*NSteps).*(cWalign<=1.1*SliderVal*NSteps));
    if ~isempty(cInd)
        QG.(FID).GUI.hClusterTime(end+1) = plot3(cAxis,QG.(FID).PCProj(1,QG.(FID).WInds{i}(QG.(FID).SelInd{i}(cInd))),...
            QG.(FID).PCProj(2,QG.(FID).WInds{i}(QG.(FID).SelInd{i}(cInd))),...
            QG.(FID).PCProj(3,QG.(FID).WInds{i}(QG.(FID).SelInd{i}(cInd))),...
            '.','Color',QG.(FID).ColorsByCluster{i},'Visible',State,'MarkerSize',15);
    end
end

function LF_CBF_excludeLargeEvents(obj,event,FID)
global QG
QG.(FID).LargeTreshold = str2num(get(obj,'String'));
LF_excludeLargeEvents(FID);
LF_updateFit([],[],FID);

% PLOTTING FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function LF_plotWaves(FID)
% PLOT INDIVIDUAL AND AVERAGE WAVES
global QG

NClust = QG.(FID).NClust; 
NElectrodes = QG.(FID).NElectrodes;
% LINKING THE X-AXIS OF ALL WAVEFORMS PLOTS
linkaxes(QG.(FID).GUI.Waves, 'xy');
for iE=1:NElectrodes
  cAxis = QG.(FID).GUI.Waves(iE);
  NewPlot = 0; 
  if ~isfield(QG.(FID).GUI,'hWaves') | length(QG.(FID).GUI.hWaves)<iE | QG.(FID).WaveToggle
    try delete(cell2matObj(QG.(FID).GUI.hWaves{iE})); end
    if ~QG.(FID).WaveToggle
      NewPlot = 1;
      QG.(FID).GUI.hmWave{iE} = cell(NClust,1);
      set(cAxis,'NextPlot','add');
      if iE==NElectrodes-1 || NElectrodes==1
        xlabel(cAxis,'Time [ms] '); 
        ylabel(cAxis,'Voltage [S.D.] ');
      end
      grid(cAxis,'on');
      box on;
      QG.(FID).GUI.hWavesStat(iE,1,1) = plot(cAxis,1,1,'Color',QG.FigOpt.LineCol,'HitTest','off');
      QG.(FID).GUI.hWavesStat(iE,2,1) = plot(cAxis,1,1,'Color',[.2,.2,.2],'HitTest','off');
    end
    QG.(FID).GUI.hWaves{iE} = cell(NClust,1);
  else % Newplot = 0; Toggle = 0;
    if NClust < size(QG.(FID).GUI.hWaves{iE},1)
      delete(cell2matObj(QG.(FID).GUI.hWaves{iE}(NClust+1:end)));
      delete(cell2matObj(QG.(FID).GUI.hmWave{iE}(NClust+1:end)));
      QG.(FID).GUI.hWaves{iE} = vertical(QG.(FID).GUI.hWaves{iE}(1:NClust));
      QG.(FID).GUI.hmWave{iE} = vertical(QG.(FID).GUI.hmWave{iE}(1:NClust));
    end
  end
  title(cAxis,['Electrode ',num2str(iE)],'FontSize',12);
  
  % PLOT STANDARD DEVIATIONS OR WAVES (LATER IS SLOWER)
  for iC=1:NClust
    if isfield(QG.(FID),'ColorsByCluster')
      cColor = QG.(FID).ColorsByCluster{iC};
    else
      cColor = [0,0,0];
    end
    
    if QG.(FID).GUI.ShowState(iC) State='on'; else State='off'; end
    switch lower(QG.(FID).WaveOpt)
      case 'sd';
        if NewPlot || QG.(FID).WaveToggle || iC>size(QG.(FID).GUI.hWaves{iE},1)
          QG.(FID).GUI.hWaves{iE}{iC,1} = plot(cAxis,[1,2;1,2],[1,1;1,1],'HitTest','off','LineWidth',QG.FigOpt.LineWidth/2);
        end
        SD = squeeze(std(QG.(FID).Waves(QG.(FID).WInds{iC}(QG.(FID).SelInd{iC}),iE,:)));
        set(QG.(FID).GUI.hWaves{iE}{iC}(1),...
          'XData',QG.(FID).WaveTime*1000,...
          'YData',QG.(FID).mWaves{iC,iE} + SD,...
          'Color',cColor,'Visible',State,'Hittest','off');
        set(QG.(FID).GUI.hWaves{iE}{iC}(2),...
          'XData',QG.(FID).WaveTime*1000,...
          'YData',QG.(FID).mWaves{iC,iE} - SD,...
          'Color',cColor,'Visible',State,'Hittest','off');
      case 'individual';
        try delete(QG.(FID).GUI.hWaves{iE}{iC}); end
        QG.(FID).GUI.hWaves{iE}{iC,1} = ...
          plot(cAxis,repmat(QG.(FID).WaveTime*1000,length(QG.(FID).SelInd{iC}),1)',...
          squeeze(QG.(FID).Waves(QG.(FID).WInds{iC}(QG.(FID).SelInd{iC}),iE,:))',...
          'Color',cColor,'Visible',State,'Hittest','off');
    end
  
    % PLOT MWAVES   
    if QG.(FID).GUI.ShowState(iC) State='on'; else State='off'; end
    if NewPlot ||  iC>size(QG.(FID).GUI.hmWave{iE},1)
      QG.(FID).GUI.hmWave{iE}{iC,1} = plot(cAxis,1,1,'LineWidth',QG.FigOpt.LineWidth,'HitTest','off');
    end
    set(QG.(FID).GUI.hmWave{iE}{iC},...
      'XData',QG.(FID).WaveTime*1000,...
      'YData',QG.(FID).mWaves{iC,iE},...
      'Color',cColor,'Visible',State);
  end;
  
  % PLOT DECORATIONS (THRESHOLD, ZERO)
  set(QG.(FID).GUI.hWavesStat(iE,1),...
    'XData',QG.(FID).WaveTime*1000,...
    'YData',repmat(QG.(FID).Threshold(iE),size(QG.(FID).WaveTime)));
  set(QG.(FID).GUI.hWavesStat(iE,2),...
    'XData',QG.(FID).WaveTime*1000,...
    'YData',repmat(0,size(QG.(FID).WaveTime)));
  set(cAxis,'XLim',[-QG.(FID).P.PreDur,QG.(FID).P.PostDur]*1000,...
    'YLim',[QG.(FID).PlotYMin,QG.(FID).PlotYMax]);
  
  % BRING MWAVES IN FRONT OF REST
  Children = get(cAxis,'Children');
  Plots = [mat2vec(QG.(FID).GUI.hWavesStat(iE,:));cell2matObj(QG.(FID).GUI.hmWave{iE});cell2matObj(QG.(FID).GUI.hWaves{iE})];
  set(cAxis,'Children',[setdiff(Children,Plots);Plots])
end
QG.(FID).WaveToggle = 0;

function LF_plotClusters(FID)
% PLOT PROJECTIONS ONTO EIGENVECTORS
global QG

NClust = QG.(FID).NClust;
cAxis = QG.(FID).GUI.Clusters;
if ~isfield(QG.(FID).GUI,'hCluster') NewPlot = 1;
    QG.(FID).GUI.hCluster = cell(NClust,1);
    set(cAxis,'NextPlot','add');
    title(cAxis,'Largest P.C.s','FontSize',12); grid(cAxis,'on');
    view(cAxis,[45,60]);
else NewPlot = 0;
    if NClust < length(QG.(FID).GUI.hCluster)
        delete([QG.(FID).GUI.hCluster{NClust+1:end}]);
        QG.(FID).GUI.hCluster = QG.(FID).GUI.hCluster(1:NClust);
    end
end

for i=1:NClust
    if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
    if NewPlot || i>length(QG.(FID).GUI.hCluster)
        QG.(FID).GUI.hCluster{i} = plot3(cAxis,1,1,1,'Marker','.','LineStyle','none','MarkerSize',15,'HitTest','off');
    end
    set(QG.(FID).GUI.hCluster{i},...
        'XData',QG.(FID).PCProj(1,QG.(FID).WInds{i}(QG.(FID).SelInd{i})),...
        'YData',QG.(FID).PCProj(2,QG.(FID).WInds{i}(QG.(FID).SelInd{i})),...
        'ZData',QG.(FID).PCProj(3,QG.(FID).WInds{i}(QG.(FID).SelInd{i})),...
        'Color',[0,0,0],'Visible',State);
end;
Abs = [min(QG.(FID).PCProj(1:3,:),[],2),max(QG.(FID).PCProj(1:3,:),[],2)]';
Abs = Abs(:);
set(cAxis,'XLim',Abs(1:2),'YLim',Abs(3:4),'ZLim',Abs(5:6));

function LF_plotRaster(FID)
% PLOT THE SPIKETIMES OVER STIMULUS REPETITIONS
global QG

NTrials = QG.(FID).NTrials; NClust = QG.(FID).NClust;
cAxis = QG.(FID).GUI.Raster;
if ~isfield(QG.(FID).GUI,'hRaster') NewPlot = 1;
    QG.(FID).GUI.hRaster = cell(NClust,1);
    set(cAxis,'NextPlot','add','XTick',[]);
    title(cAxis,'Raster & PSTH');
    ylabel(cAxis,'Indices [#]')
    grid(cAxis,'on');
else
    NewPlot = 0;
    if NClust < length(QG.(FID).GUI.hRaster)
        delete([QG.(FID).GUI.hRaster{NClust+1:end}]);
        QG.(FID).GUI.hRaster = QG.(FID).GUI.hRaster(1:NClust);
    end
end

QG.(FID).APsOverTrials = cell(NClust,1);
for i=1:NClust
    k=0;
    QG.(FID).APsOverTrials{i} = zeros(length(QG.(FID).SPs(i)),2);
    for j=1:NTrials
        cNAP = length(QG.(FID).SortedTrials{j}{i});
        QG.(FID).APsOverTrials{i}(k+1:k+cNAP,:) ...
            = [QG.(FID).SortedTrials{j}{i},repmat(j,cNAP,1)];
        k = k+cNAP;
    end
end

cAxis = QG.(FID).GUI.Raster;
for i=1:NClust
    if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
    MarkerSize = 6;
    %  if length(QG.(FID).APsOverTrials{i}) > 10000 MarkerSize = 6; else MarkerSize=6; end
    if NewPlot || i>length(QG.(FID).GUI.hRaster)
        QG.(FID).GUI.hRaster{i} = plot(cAxis,1,1,'Marker','.','LineStyle','none','MarkerSize',MarkerSize,'HitTest','off');
    end
    set(QG.(FID).GUI.hRaster{i},...
        'XData',QG.(FID).APsOverTrials{i}(:,1),...
        'YData',QG.(FID).APsOverTrials{i}(:,2),...
        'Color',[0,0,0],'Visible',State);
end
if NewPlot
    if ~isempty(QG.(FID).P.StimStart)
        plot(cAxis,[QG.(FID).P.StimStart,QG.(FID).P.StimStart],[0,NTrials],...
            'Color',QG.FigOpt.LineCol,'HitTest','Off');
    end;
    if ~isempty(QG.(FID).P.StimStop)
        plot(cAxis,[QG.(FID).P.StimStop,QG.(FID).P.StimStop],[0,NTrials],...
            'Color',QG.FigOpt.LineCol,'HitTest','Off');
    end;
end;
axis(cAxis,[0,QG.(FID).PlotXMax,0,NTrials+1]);
SortedInd = QG.(FID).P.Indices(QG.(FID).P.PermInd);
[Indices,Ticks] = unique(SortedInd,'first');
set(cAxis,'YTick',Ticks,'YTickLabel',Indices);

function LF_plotClusterSeparation(FID)
  
global QG;
NClust = QG.(FID).NClust; grid on;
set(QG.(FID).GUI.ClusterSeparationH,'CData',QG.(FID).ClusterSeparation,'XData',[1:NClust],'YData',[1:NClust]);
set(QG.(FID).GUI.ClusterSeparation,'XLim',[0.5,NClust+0.5],'YLim',[0.5,NClust+0.5],'XTick',[1:NClust],'YTick',[1:NClust]);
colormap(HF_colormap({[0,0,0],[0,0,1],[0,0,1],[1,1,1],[1,0,0]},[-0.1,-0.01,0.99*QG.(FID).P.FusionThreshold,QG.(FID).P.FusionThreshold,1]));
caxis(QG.(FID).GUI.ClusterSeparation,[-0.1,1]);

function LF_plotPSTH(FID)
% PLOT THE SPIKETIMES OVER STIMULUS REPETITIONS
global QG

NTrials = QG.(FID).NTrials; NClust = QG.(FID).NClust;
cAxis = QG.(FID).GUI.PSTH;
if ~isfield(QG.(FID).GUI,'hPSTH') NewPlot = 1;
    QG.(FID).GUI.hPSTH = cell(NClust,1);
    set(cAxis,'NextPlot','add');
    grid(cAxis,'on');
    box off; set(cAxis,'YTick',[]);
else
    NewPlot = 0;
    if NClust < length(QG.(FID).GUI.hPSTH)
        delete([QG.(FID).GUI.hPSTH{NClust+1:end}]);
        QG.(FID).GUI.hPSTH = QG.(FID).GUI.hPSTH(1:NClust);
    end
end

BinBounds = [0:0.01:QG.(FID).PlotXMax]';
BinCenters = BinBounds(1:end-1) + 0.005;
for i=1:NClust
    if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
    if NewPlot || i>length(QG.(FID).GUI.hPSTH)
        QG.(FID).GUI.hPSTH{i} = plot(cAxis,1,1,'HitTest','off');
    end
    % SELECT FOR ONE OR ALL RECORDINGS
    if strcmp(QG.(FID).CurrentRecording,'all')
        H = histc(QG.(FID).APsOverTrials{i}(:,1),BinBounds);
    else
        Bounds = QG.(FID).RecordingBounds(QG.(FID).CurrentRecording,:);
        [tmp,cInd] = ismember(QG.(FID).APsOverTrials{i}(:,2),Bounds(1):Bounds(2));
        H = histc(QG.(FID).APsOverTrials{i}(find(cInd),1),BinBounds);
    end
    set(QG.(FID).GUI.hPSTH{i},...
        'XData',BinCenters,...
        'YData',H(1:end-1)/max(H),...
        'Color',[0,0,0],'Visible',State);
end

% PLOT STIMULUS START AND END
if NewPlot
    if ~isempty(QG.(FID).P.StimStart)
        plot(cAxis,[QG.(FID).P.StimStart,QG.(FID).P.StimStart],[0,1],...
            'Color',QG.FigOpt.LineCol,'HitTest','Off');
    end;
    if ~isempty(QG.(FID).P.StimStop)
        plot(cAxis,[QG.(FID).P.StimStop,QG.(FID).P.StimStop],[0,1],...
            'Color',QG.FigOpt.LineCol,'HitTest','Off');
    end;
end
axis(cAxis,[0,QG.(FID).PlotXMax,0,1.1]);

function LF_plotTrace(FID,ChangeScale)
% PLOT THE TRACE FOR ONE REPETITION
global QG

NClust = QG.(FID).NClust;
Electrode = QG.(FID).CurrentElectrode;

cAxis = QG.(FID).GUI.Trace;
if ~isfield(QG.(FID).GUI,'hTrace') NewPlot = 1;
    QG.(FID).GUI.hTrace = cell(NClust,1);
    set(cAxis,'NextPlot','add','ButtonDownFcn',{@LF_CBF_Trace,FID});
    xlabel(cAxis,'Time [s] ','FontSize',12);
    ylabel(cAxis,'Voltage [S.D.] ','FontSize',12);
    grid(cAxis,'on');
    box on;
    QG.(FID).GUI.hTraceStat(1) = plot(cAxis,1,1,'k','HitTest','off');
    QG.(FID).GUI.hTraceStat(2) = plot(cAxis,1,1,'-','Color',[.5,.5,.5],'HitTest','off');
else
    NewPlot = 0;
    if NClust < length(QG.(FID).GUI.hTrace)
        delete([QG.(FID).GUI.hTrace{NClust+1:end}]);
        QG.(FID).GUI.hTrace = QG.(FID).GUI.hTrace(1:NClust);
    end
end
title(cAxis,['Electrode ',n2s(Electrode),' - Trial ',n2s(QG.(FID).CurrentRep),''],'FontSize',10);

cMax = 0; SR = QG.(FID).P.SR;
iStart = QG.(FID).SortedTrialBounds(QG.(FID).CurrentRep,1);
iEnd = QG.(FID).SortedTrialBounds(QG.(FID).CurrentRep,2);
TStart = iStart/SR; TEnd = iEnd/SR;

% PLOT TRACE
iEnd = min([iEnd,size(QG.(FID).Data,1)]);
Indices = [iStart:iEnd]; Time = (Indices)/SR;
set(QG.(FID).GUI.hTraceStat(1),...
    'XData',Time,...
    'YData',QG.(FID).Data(Indices,Electrode));

% PLOT TRIGGERS
for i=1:NClust
  if isfield(QG.(FID),'ColorsByCluster')
    cColor = QG.(FID).ColorsByCluster{i};
  else
    cColor = [0,0,0];
  end
  
  cSPs = QG.(FID).SortedTrials{QG.(FID).CurrentRep}{i} + QG.(FID).TrialBounds(QG.(FID).CurrentRep,1)/SR;
  if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
 if NewPlot || i>length(QG.(FID).GUI.hTrace)
    QG.(FID).GUI.hTrace{i} = plot(cAxis,1,1,'Marker','.','LineStyle','none','MarkerSize',20,'HitTest','off');
  end
  set(QG.(FID).GUI.hTrace{i},...
    'XData',cSPs,...
    'YData',repmat(0,length(cSPs),1),...
    'Color',cColor,'Visible',State);
end

set(QG.(FID).GUI.hTraceStat(2),...
    'XData',[0,TEnd],...
    'YData',[QG.(FID).Threshold(Electrode),QG.(FID).Threshold(Electrode)]);

if NewPlot
    if ~isempty(QG.(FID).P.StimStart)
        plot(QG.(FID).GUI.Trace,[QG.(FID).P.StimStart,QG.(FID).P.StimStart],...
            1.5*[QG.(FID).YMin,QG.(FID).YMax],'Color',QG.FigOpt.LineCol,'HitTest','off'); end;
    if ~isempty(QG.(FID).P.StimStop)
        plot(QG.(FID).GUI.Trace,[QG.(FID).P.StimStop,QG.(FID).P.StimStop],...
            1.5*[QG.(FID).YMin,QG.(FID).YMax],'Color',QG.FigOpt.LineCol,'HitTest','off');
    end;
end
if ChangeScale
  axis(cAxis,[[TStart,TEnd],[QG.(FID).PlotYMin,QG.(FID).PlotYMax]]);
end

function LF_plotISIhist(FID)
% PLOT ISI HISTOGRAMS
global QG

cAxis = QG.(FID).GUI.ISI;
NClust = QG.(FID).NClust;
SR = QG.(FID).P.SR;
cMax = 0; 
Bins = logspace(log10(0.1),log10(50),30);
if ~isfield(QG.(FID).GUI,'hISI') 
  NewPlot = 1;
  QG.(FID).GUI.hISI = cell(NClust,1);
  set(cAxis,'NextPlot','add','xscale','log');
  title(cAxis,['ISI Hist']);
  xlabel(cAxis,'Time [ms] ');
  set(cAxis,'xtick',[0.1,0.5,1,5,10,20,50]);
  axis(cAxis,[0.1,50,0,1.1]);
  grid(cAxis,'on');
  box on;
else
  NewPlot = 0;
  if NClust < length(QG.(FID).GUI.hISI)
    delete([QG.(FID).GUI.hISI{NClust+1:end}]);
    QG.(FID).GUI.hISI = QG.(FID).GUI.hISI(1:NClust);
  end
end

H = cell(NClust,1);
for i=1:NClust
  if length(QG.(FID).SPs{i}) > 1
    H{i} = vertical(histc(diff(QG.(FID).SPs{i}/SR*1000),Bins));
    H{i} = H{i}(1:end-1);
    H{i} = H{i}./diff(Bins)';
    if max(H{i}) H{i} = H{i}/max(H{i}); end
  end
end

for i=1:NClust
  if QG.(FID).GUI.ShowState(i) State='on'; else State='off'; end
  if isempty(H{i}) H{i} = zeros(length(Bins)-1,1); end
  if NewPlot || i>length(QG.(FID).GUI.hISI)
    QG.(FID).GUI.hISI{i} = plot(cAxis,1,1,'HitTest','off');
  end
  set(QG.(FID).GUI.hISI{i},...
    'XData',Bins(1:end-1),...
    'YData',H{i},...
    'Color',[0,0,0],'Visible',State);
end

function LF_collectHandles(FID)
  global QG

QG.(FID).GUI.hSet = cell(QG.(FID).NClust,1);
for iV=1:QG.(FID).NClust
  for iE = 1:QG.(FID).NElectrodes
    QG.(FID).GUI.hSet{iV} = [QG.(FID).GUI.hSet{iV}; ...
      QG.(FID).GUI.hWaves{iE}{iV};QG.(FID).GUI.hmWave{iE}{iV};...
      QG.(FID).GUI.hCluster{iV};QG.(FID).GUI.hRaster{iV};QG.(FID).GUI.hTrace{iV};...
      QG.(FID).GUI.hISI{iV};QG.(FID).GUI.hPSTH{iV}];
  end
end

function LF_closeFig(obj,event,FID)
global QG

try QG = rmfield(QG,FID); catch end

% GUI HELPERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h = LF_addCheckbox(Panel,Pos,Val,CBF,Tooltip,Tag,Color);
global QG Verbose
if ~exist('Color','var') | isempty(Color) Color = QG.Colors.Panel; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist ('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
h=uicontrol('Parent',Panel,'Style','checkbox',...
    'Value',Val,'Callback',CBF,'Units','normalized',...
    'Tag',Tag,'Position',Pos,'BackGroundColor',Color,'Tooltip',Tooltip);

function h = LF_addDropdown(Panel,Pos,Strings,Val,CBF,UD,Tooltip,Tag,Color,FontSize);
global QG
if ~exist('Color','var') | isempty(Color) Color = [1,1,1]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
if ~exist('FontSize','var') | isempty(FontSize) FontSize = 8; end
if ~exist('UD','var') | isempty(UD) UD = []; end
h=uicontrol('Parent',Panel,'Style','popupmenu',...
    'Val',Val,'String',Strings,'FontName',QG.FigOpt.FontName,...
    'Callback',CBF,'Units','normalized',...
    'FontSize',FontSize,...
    'Tag',Tag,'Position',Pos,'BackGroundColor',Color,'TooltipString',Tooltip);
set(h,'UserData',UD)

function h = LF_addEdit(Panel,Pos,String,CBF,Tooltip,Tag,Color,FontSize);
global QG
if ~exist('Color','var') | isempty(Color) Color = [1,1,1]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
if ~exist('FontSize','var') | isempty(FontSize) FontSize = 8; end
if ~isstr(String) String = n2s(String); end
h=uicontrol('Parent',Panel,'Style','edit',...
    'String',String,'FontName',QG.FigOpt.FontName,...
    'Callback',CBF,'Units','normalized','Horiz','center',...
    'FontSize',FontSize,...
    'Tag',Tag,'Position',Pos,'BackGroundColor',Color,'ToolTipString',Tooltip);

function h = LF_addPanel(Parent,Title,TitleSize,TitleColor,Color,Position,TitlePos)
global QG
if ~exist('TitlePosition','var') TitlePos = 'centertop'; end
h = uipanel('Parent',Parent,'Title',Title,...
    'FontSize',TitleSize,'FontName',QG.FigOpt.FontName,...
    'ForeGroundColor',TitleColor,'TitlePosition',TitlePos,'BackGroundColor',Color,...
    'Units','normalized','Position',Position,'BorderType','line');

function h = LF_addPushbutton(Panel,Pos,String,CBF,Tooltip,Tag,FGColor,BGColor);
global QG Verbose
if ~exist('BGColor','var') | isempty(BGColor) BGColor = [1,1,1]; end
if ~exist('FGColor','var') | isempty(FGColor) FGColor = [0,0,0]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
h=uicontrol('Parent',Panel,'Style','pushbutton',...
    'String',String,'FontName',QG.FigOpt.FontName,...
    'Callback',CBF,'Units','normalized','Position',Pos,'Tag',Tag,...
    'ToolTipString',Tooltip,'ForegroundColor',FGColor,'BackGroundColor',BGColor);

function h = LF_addText(Panel,Pos,String,Tooltip,Tag,FGColor,BGColor);
global QG
if ~exist('BGColor','var') | isempty(BGColor) BGColor = get(Panel,'BackGroundColor'); end
if ~exist('FGColor','var') | isempty(FGColor) FGColor = [1,1,1]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
h=uicontrol('Parent',Panel,'Style','text',...
    'String',String,'FontName',QG.FigOpt.FontName,...
    'Units','normalized','Position',Pos,...
    'Tag',Tag,'ToolTipString',Tooltip,'HorizontalAlignment','left',...
    'ForegroundColor',FGColor,'BackGroundColor',BGColor);QG.(FID).LinkageInd = find(strcmp(P.Linkage,QG.(FID).Linkages));

function [Data,TrialIdx] = LF_createTestData

NSteps = 10000; NTrials = 100; SR = 25000; FSpike = 1000; NSpikes = 10;
Data = zeros(NTrials,NSteps);
K = sin(2*pi*FSpike*[1:25]/SR);
for i=1:NTrials
    Frac = i/NTrials+0.5;
    for j=1:NSpikes Data(i,700*j) = Frac*mod(j,2)+1; end;
    tmp = conv(Data(i,:),K); Data(i,:) = tmp(1:NSteps);
end
TrialIdx = [1:NSteps:(NTrials-1)*NSteps+1]';
Data = Data'; Data = Data + randn(size(Data))/20;
Data = Data(:);

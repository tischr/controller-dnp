function R = C_findSpikes(varargin)
% TODO: 
% - Add SNR, Waveforms to output, maybe distances between clusters?
% - Make some parameters accessible from the outside (Fusion threshold,...)

P = parsePairs(varargin);
checkField(P,'Data');
checkField(P,'SR');
checkField(P,'Time',[]);
checkField(P,'Identifier','');
checkField(P,'Threshold',-4); % USE NEGATIVE 4 S.D. for the Threshold
checkField(P,'NClust',12);
checkField(P,'TrialIndices',1); % BOUNDARIES BETWEEN TRIALS
checkField(P,'Electrodes',1:size(P.Data,2));
checkField(P,'Mode','Unsupervised');
checkField(P,'Verbose',1)

IdentifierSort = [P.Identifier,'_E',sprintf('_%d',P.Electrodes)];

fprintf(['\n\n===== [ SORTING : ',IdentifierSort,' ] ====== \n']);

% OPEN GUI OR AUTOMATIC SORTING
FIG = autoSort('Data',P.Data,'SR',P.SR,'TrialIndices',P.TrialIndices,'NClust',P.NClust,...
  'Identifier',IdentifierSort,'Electrodes',P.Electrodes,'Mode',P.Mode);
switch P.Mode
  case 'Supervised';
    uiwait(FIG);
end

% SINCE AUTO SORT IS INTERACTIVE, IT WRITES THE DATA TO BASE WORKSPACE FOR TRANSFER
% WE THEN PICK IT UP HERE.
if evalin('base','exist(''TmpResultsAutoSort'')'); 
  Results = evalin('base','TmpResultsAutoSort'); evalin('base','clear TmpResultsAutoSort;');

  NCells = length(Results.SPs);
  for iC = 1:NCells
    % ASSIGN SPIKE TIMES
    if ~isempty(P.Time)
      R.Time{iC} = P.Time(round(Results.SPs{iC}));
    else
      R.Time{iC} = ind/P.SR;
    end
    % ASSIGN RESPONSE VALUES
    R.Response{iC} = ones(size(R.Time{iC}));
    NSpikes = length(Results.SPs{iC});
    R.NSpikes(iC) = NSpikes;
    R.mWaves = Results.mWaves;
    R.SNRs = Results.SNRs;
    R.Parameters = Results.Parameters;
    if isfield(Results,'FileNames') R.SortFiles{1} = Results.FileNames; end
  end
else
  R = struct('Time',[],'Response',[],'NSpikes',[],'mWaves',[],'SNRs',[],'Parameters',[]);
end
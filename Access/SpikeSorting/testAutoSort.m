function Results = testAutoSort(Wait)
% CREATE DATA TO TEST THE AUTO SORT
if ~exist('Wait','var') Wait = 0; end
% SET PARAMETERS
NElectrodes = 4;
NTrials = 10;
NCells = 4;
NSpikes = 5;
SR = 30000;
SpikeTime = 0.1;
TrialLength = 1;
NSteps = SR*TrialLength;
R = zeros(NSteps,NTrials,NElectrodes);
ISI = [0.01,0.015,0.02,0.025]; 
ICI = 0.2; % Intercell Interval (spaces out cells for simpler evaluation)

% SET SCALINGS ACROSS ELECTRODES
rand('seed',now);
for iC=1:NCells
  ScalingsElecByCell(iC,:) = (2*rand(1,NElectrodes) + 1)/3;
end

% SET KERNELS FOR EACH CELL
KTime = 0.0015; KSteps = round(KTime*SR);
FreqByCell = [200,1300,1600];
for iC = 1:NCells-1
  Time = [0:floor(SR/FreqByCell(iC))];
  KernelsByCell{iC} = -sin(2*pi*FreqByCell(iC)*Time/SR).*exp(-Time/20);
end

% ADD A KERNEL WHICH IS NOT A CELL
KernelsByCell{end+1} = -[linspace(0,1,45),linspace(1,0,45)]/2;

% POSITION SPIKES AT REGULAR POSITIONS
for iC = 1:NCells
  % PLACE SPIKES
  tmp = zeros(NSteps,1);
  for iS=1:NSpikes
    cPos = round((SpikeTime + (iC-1)*ICI + (iS-1)*ISI(2) )*SR);
    tmp(cPos) = 1;
  end
  % DISTRIBUTE SPIKES OVER ELECTRODES & TRIALS
  for iE = 1:NElectrodes
    tmp2 = conv(tmp,ScalingsElecByCell(iC,iE)*KernelsByCell{iC});
    for iT=1:NTrials
      R(:,iT,iE) = R(:,iT,iE) + tmp2(1:NSteps);
    end
  end
end

fprintf(['Spikes generated : ',num2str(NCells*NSpikes*NTrials),'\n']);

TrialIndices = [1 : NSteps : (NTrials-1)*NSteps + 1]';
R = single(R + randn(size(R))/10);
R(end-10:end,:,:) = 0;
R = reshape(R,NSteps*NTrials,NElectrodes);
[b,a] = butter(2,[7000]/(SR/2));
R = filter(b,a,R);

FIG = autoSort('Data',R,'SR',SR,'TrialIndices',TrialIndices,...
  'NClust',2*NCells + 1,'NCellMax',5,'FIG',10,'Threshold',-3.6,...
  'Identifier','Test Data','Electrodes',[1:4]);
if Wait
  uiwait(FIG);
  Results = evalin('base','TmpResultsAutoSort'); evalin('base','clear TmpResultsAutoSort;');
  for iC=1:NCells-1
    NCorrect = length(Results.SPs{iC})/(NTrials*NSpikes);
    fprintf(['Fraction Correct : ',num2str(NCorrect),'\n']);
  end
end
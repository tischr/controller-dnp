function R = C_estimateAnimalPosition(varargin)

global CG;
P = parsePairs(varargin);
checkField(P,'Data',[]);
checkField(P,'Animal',[]);
checkField(P,'Recording',[]);
checkField(P,'Raster',1);
checkField(P,'TimeWindows',1);
checkField(P,'BetweenSensors',1); % 
checkField(P,'SR',100); % Sampling Rate for the Raster 

if ~isempty(P.Animal) & isempty(P.NIDAQ)
  % LOAD RECORDING 
end

% GET THE ANIMAL SENSORS
HW = P.Data.General.Paradigm.HW;
AnimalSensors  = HW.AnimalSensorsPhys;
LastTime = P.Data.NIDAQ.Data(end).Data.Time(end,1);

% PARSE EVENTS TO EXTRACT TIME WINDOWS
DAQID = P.Data.General.Paradigm.HW.IDs.DAQ;
E = P.Data.General.Events.NI(DAQID).Events;
Names = {E.Name}; Data = [E.Data]; 
Times = reshape([E.Time],3,length(E))'; Times = Times(:,1);
for iS=1:length(AnimalSensors)
  cInd = Data==AnimalSensors(iS);
  cNames = Names(cInd);
  cTimes = Times(cInd);
  cPos = find(cInd)';
  cSwitches = NaN*zeros(size(cPos));
  cSwitches(strcmp(cNames,'AI_to_Low')) = -1;
  cSwitches(strcmp(cNames,'AI_to_High')) = 1;
  if any(isnan(cSwitches)) error('Unexpected Event'); end
  % COMBINE EVENTS WHICH ARE IN THE SAME DIRECTION
  ccInd = [1;find(diff(cSwitches) ~= 0) + 1];
  cTimes = cTimes(ccInd);
  cSwitches = cSwitches(ccInd);
  if cSwitches(1) == 1 
    cSwitches = [-1;cSwitches]; 
    cTimes = [0;cTimes];
  end
  if cSwitches(end) == -1 
    cSwitches = [cSwitches;1]; 
    cTimes = [cTimes;LastTime];
  end
  SensorsWindows{iS} = reshape(cTimes,2,length(cTimes)/2);  
  NWindows(iS) = size(SensorsWindows{iS},2);
  Channels{iS} = iS*ones(1,NWindows(iS)*2);
  switch iS
    case 1; NInd = [0,2];
    case length(AnimalSensors); NInd = length(AnimalSensors)+[-1,1];
    otherwise NInd = iS+[-1,1];
  end
   Neighbors{iS} = NInd;
 end

% CREATE COMPOSITE REPRESENTATION OF CHANGES
Times = [SensorsWindows{:}];
Switches = repmat([-1,1],size(Times,2),1)';
Channels = [Channels{:}]';
NChanges = length(Channels);
SensorChanges = [Times(:),Switches(:),Channels,repmat(1,NChanges,1)];
[~,SortInd] = sort(SensorChanges(:,1));
SensorChangesS = SensorChanges(SortInd,:);

% STEP THROUGH CHANGES AND CHECK NEIGHBORS FOR 
cSize = size(SensorChanges);
InterChanges = zeros(cSize.*[2,1]);
CheckPos = find(SensorChangesS(:,2)==1); iPos = 0;
InterWindows =cell(1,length(AnimalSensors)-1);
for iC = 1:length(CheckPos)
  cPos = CheckPos(iC); dPos = 0;
  cChannelIndex = SensorChangesS(cPos,3);
  cTime = SensorChangesS(cPos,1);
  while dPos < NChanges - cPos
    dPos = dPos+1;
    NextTime = SensorChangesS(cPos+dPos,1) - 1e-8;
    NextSwitch = SensorChangesS(cPos+dPos,2);
    NextChannelIndex = SensorChangesS(cPos+dPos,3);
    Matches = NextChannelIndex == Neighbors{cChannelIndex};
    if NextSwitch == -1
      if cChannelIndex == NextChannelIndex % RETURN TO SAME SENSOR
        if cChannelIndex < length(AnimalSensors)
          iPos = iPos + 1;  InterChanges(iPos,:) = [cTime,-1,cChannelIndex+0.5,0.5];
          iPos = iPos + 1;  InterChanges(iPos,:) = [NextTime,1,cChannelIndex+0.5,0.5];
          if P.BetweenSensors
            InterPos = cChannelIndex;
            InterWindows{InterPos}(1:2,end+1) = [cTime;NextTime];
          end
        end
        if cChannelIndex > 1
          iPos = iPos + 1;  InterChanges(iPos,:) = [cTime,-1,cChannelIndex-0.5,0.5];
          iPos = iPos + 1;  InterChanges(iPos,:) = [NextTime,1,cChannelIndex-0.5,0.5];
          if P.BetweenSensors
            InterPos = cChannelIndex-1;
            InterWindows{InterPos}(1:2,end+1) = [cTime;NextTime];
          end
        end
        break;
      elseif any(Matches) % STEP TO NEIGHBOR SENSOR
        diffPos = find(Matches) - 1.5;
        iPos = iPos + 1;  InterChanges(iPos,:) = [cTime,-1,cChannelIndex+diffPos,1];
        iPos = iPos + 1;  InterChanges(iPos,:) = [NextTime,1,cChannelIndex+diffPos,1];
        if P.BetweenSensors
          InterPos = round(cChannelIndex+(diffPos<0)*2*diffPos);
          InterWindows{InterPos}(1:2,end+1) = [cTime;NextTime];
        end
        break;
      else 
        break;
      end
    else
      if any(Matches) % FOUND A NEIGHBOR THAT UNCOVERS, BEFORE ANOTHER NEIGHBOR COVERS
        break;
      end
    end
  end
  
end
% REDUCE TO ACTUAL SIZE
InterChanges = InterChanges(1:iPos,:);

AllChanges = [SensorChanges;InterChanges];
[~,SortInd] = sort(AllChanges(:,1));
AllChangesS = AllChanges(SortInd,:);
NBins = 2*length(AnimalSensors)-1;

if P.TimeWindows
  if ~P.BetweenSensors
    R.Windows = SensorsWindows;
    R.Sensors = [-3:-1,1:3];
  else
    R.Windows(1:2:NBins) = SensorsWindows;
    R.Windows(2:2:NBins-1) = InterWindows;
    R.Sensors = [-3:0.5:-1,0,1:0.5:3];
  end
end

% TRANSFORM TO A MATRIX WITH DIMENSIONS POSITION VS TIME
if P.Raster
  NSteps = round(LastTime * P.SR);
  R.AnimalPositions = zeros(NBins,NSteps);
  for iB = 1:NBins
    cInd = find(round(2*(AllChangesS(:,3)-1)+1)==iB);
    for iW = 1:2:length(cInd)-1
      ccPos1 = cInd(iW);
      ccPos2 = cInd(iW+1);
      ccInd = [ceil(AllChangesS(ccPos1,1)*P.SR):ceil(AllChangesS(ccPos2,1)*P.SR)-1];
      if isempty(ccInd) ccInd = ceil(AllChangesS(ccPos1,1)*P.SR); end
      ccInd(ccInd==0) = 1;
      R.AnimalPositions(iB,ccInd) =  AllChangesS(ccPos1,4);
    end
  end
  R.Time = [1:size(R.AnimalPositions,2)]/P.SR + P.Data.NIDAQ.Data(1).Data.Time(1,1);
end
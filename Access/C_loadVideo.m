function R = C_loadVideo(varargin)
% LOAD VIDEO DATA

P = parsePairs(varargin);
checkField(P,'File')
checkField(P,'LoadVideoData',1);

R = load([P.File,'.mat']);
if P.LoadVideoData
  FID = fopen([P.File,'.dat'],'r');
  N = prod(R.Data.Dims);
  R.Data.Frames = reshape(fread(FID,N,'*uint8'),R.Data.Dims);
  fclose(FID);
end

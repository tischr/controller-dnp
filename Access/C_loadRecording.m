function R = C_loadRecording(varargin)
% MAIN ACCESS FUNCTION FOR DATA RECORDED WITH CONTROLLER
% 
% Add single trial loading : check temporal alignment of video/ephys
% Add loading of Ephys as an automatic option.

varargin = C_parseID(varargin);
P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'Modules','all');
checkField(P,'Electrodes','all');
checkField(P,'Reference','all');
checkField(P,'LoadVideoData',1);
checkField(P,'Trials','all');
checkField(P,'Verbose',1);
checkField(P,'LFP',0);
checkField(P,'AlignHost',0);


% FIND THE BASE PATH (COMPARE TO C_initialize)
[RecordingPath,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);

% LOAD BASIC INFORMATION
T = load([RecordingPath,'General.mat']); R.General = T.CGSave; clear T;

%% LOAD DIFFERENT PARTS OF THE RESPONSE
if P.Verbose fprintf('\n = = = = = = = LOADING DATA = = = = = = = \n'); end
[ModulesAvailable,TypesAvailable] = LF_getModulesAvailable(R,RecordingPath);

% SELECT MODULES
[Modules,Types] = LF_selectModules(P.Modules,ModulesAvailable,TypesAvailable);

% START LOADING THE DATA
for iM=1:length(Modules)
  clear Trials;
  cModule = Modules{iM};
  if P.Verbose fprintf(['Processing Module : ',cModule]); end
  ModulePath = [RecordingPath,cModule,filesep];
 
  % PRELOAD EPHYS ACROSS TRIALS
  switch cModule
    case 'EPhys';
      REPhys = C_loadEPhys('Animal',P.Animal,'Recording',P.Recording,...
        'Electrodes',P.Electrodes,'Spike',1, 'LFP',P.LFP,'Raw',0,...
        'Trials',P.Trials,'SeparateByTrial',1,'IncludeIntertrial',1,'Alignment','TrialStop',...
        'Verbose',P.Verbose,'Reference',P.Reference);
  end
  
  % DETERMINE NUMBER OF TRIALS 
  switch cModule
    case 'EPhys'; 
      NTrials = length(REPhys.TrialInfo);
      Trials = P.Trials;
      fprintf('\n');
    otherwise
      DataFiles = dir([ModulePath,'Data_*.mat']);
      KeepInd = [];
      for iF=1:length(DataFiles) 
        if ~isempty(regexp(DataFiles(iF).name,'Data_[0-9]+.mat'))
          KeepInd(end+1) = iF;
        end
      end
      DataFiles = DataFiles(KeepInd);
      NTrials = length(DataFiles);
      if NTrials == 0; fprintf('\n'); continue; end
      for iF=1:NTrials  Trials(iF) = str2num(DataFiles(iF).name(6:end-4)); end
  end
  
  if strcmp(P.Trials,'all') P.Trials = [1:NTrials]; end;
  Trials = intersect(P.Trials,Trials);
  if P.Verbose && length(Trials) fprintf('\n > Trial : '); end
  R.(cModule).Trials = Trials;
  
  % LOOP OVER TRIALS TO LOAD DATA
  for iT=1:length(Trials)
    cTrial = Trials(iT);
    if P.Verbose fprintf([' ',num2str(cTrial)]); end
    FileNameRoot = [ModulePath,'Data_',num2str(cTrial)];
    cFileName = [FileNameRoot,'.mat'];
    switch cModule
      case 'PointGrey';
        if exist(cFileName,'file') R.(cModule).Data(iT) = C_loadVideo('File',FileNameRoot,'LoadVideoData',P.LoadVideoData);   else fprintf(['M']); end
      case 'EPhys'
        R.(cModule).Data(iT) = REPhys.Trials(iT);
      otherwise
        if exist(cFileName,'file') R.(cModule).Data(iT) = load(cFileName); else fprintf(['Missing ']); end 
    end
  end
  for iT=1:length(Trials)  R.(cModule).Data(iT).Trial = Trials(iT); end
  fprintf('\n')
  
  % ADD ADDITIONAL INFORMATION  BY MODULE
  switch cModule
    case 'EPhys';
      R.(cModule).SRAI = REPhys.SR;
  end
  
end

%% SYNCHRONIZE THE DIFFERENT CHANNELS
% - Synchronization pulse sent to one of the Arduinos, and received on all other Arduinos on DI0 (Sync. Port.)
% - For Synchronization : 
%     > Arduinos :        Searched for DI0 => 1
%     > Camera :         Synchronization happens at the beginning of the first pulse
%     > AudioIn/Out :   Wire synchronization into an AI which is recorded in parallel to the AudioInOut
%     > Physiology :      Wire synchronization pulse into an DI/AI aside from the other channels
if P.Verbose fprintf('\n = = = = = = = ALIGNING DATA = = = = = = = \n'); end

% GET TRIAL SYNC TIMES FROM NIDAQ
SyncTimes = C_getTrialSync('Animal',P.Animal,'Recording',P.Recording,'Path',RecordingPath);

for iM=1:length(Modules)   % LOOP OVER MODULES
  cModule = Modules{iM};  cType = Types{iM};
  if P.Verbose fprintf(['Processing Module : ',cModule]); end
  if isfield(R,cModule)
    fprintf('\n > Trial : ');
    cNTrials =  length(R.(cModule).Trials);
    for iT = 1:cNTrials % LOOP OVER TRIALS
      cTrial = R.(cModule).Trials(iT);      
      
      % CHECK FOR SYNCING IN OTHER MODULES
      if P.Verbose fprintf([' ',num2str(cTrial)]); end
      
      switch cType
        case {'Arduino'};
          if ~isempty(R.(cModule).Data(iT).Data.Digital)
            ChangePos = find(diff(bitget(R.(cModule).Data.(iT).Data.Digital,1))~=0);
            ChangeVal = bitget(R.(cModule).Data(iT).Data.Digital(ChangePos,1),1);
            ChangeTime = R.(cModule).Data(iT).Data.Time(ChangePos,:); % as datenum
          else
            ChangePos =[]; ChangeVal = []; ChangeTime = [];
          end
          
        case {'NI'}
          if ~isempty(R.(cModule).Data(iT).Data.Time)
            switch R.(cModule).Data(iT).Session.Saving
              case 'Trial'; % DAQ OBJECT RUNS CONTINUOUSLY, TRIALS ARE CHUNKED  e.g NIDAQ
                switch cModule
                  case 'NIDAQ'; % SYNC PULSE ON CHANNEL
                    ChangeTime = SyncTimes(cTrial).Trial;
                    ChangeVal = [1,0];
                    R.NIDAQ.Data(iT).TrialStartTime = ChangeTime(1);
                    R.NIDAQ.Data(iT).TrialStopTime = ChangeTime(2);
                    if isfield(SyncTimes,'Camera')
                      R.NIDAQ.Data(iT).CameraStartTime = SyncTimes(cTrial).Camera(1);
                    end
                    
                  case 'AnalogIn';
                    if ~isempty(R.(cModule).Data(iT).Data)
                      ChangePos = 1; ChangeVal = 1;
                      ChangeTime = R.NIDAQ.Data(iT).Sync.SyncTimeRel;
                      R.AnalogIn.Data(iT).Data.Time = LF_fixTime(R.AnalogIn.Data(iT).Data.Time);
                    else
                      ChangeTime = []; ChangePos = []; ChangeVal = [];
                    end
                end
                
              case 'All';    % SAVE ALL ACQUIRED DATA AS TRIAL (E.G. AnalogIn)
                ChangePos = 1;
                ChangeVal = 1;
                % MICROPHONE SWITCH FROM ROLAND TO AVISOFT ON 6.2.2015
                % FOR SOME REASON THE SOUND IS ALMOST IDENTICAL LEFT AND RIGHT BEFORE
                %
                % CORRECT MICROPHONE SWITCH BEFORE 17.2.2015 (PROBABLY MISWIRED)
                % THE EXACT DATE IS NOT CLEAR!
                switch cModule
                  case 'AnalogIn';
                    CorrectMicrophoneTime = datenum([2015,2,17,0,0,1]);
                    %RolandToNITime = datenum([2015,2,6,0,0,1]);
                    ThisTime = datenum(R.General.Paradigm.StartTime);
                    if ThisTime < CorrectMicrophoneTime
                      if iT==1 disp(['  NOTE : Switching Microphone Channels of AnalogIn.']); end
                      R.AnalogIn.Data(iT).Data.Analog =  R.AnalogIn.Data(iT).Data.Analog(:,[2,1]);
                    end
                end
                
                if ~isempty(R.(cModule).Data(iT).Data.Time)
                  ChangeTime = R.NIDAQ.Data(iT).Sync.SyncTimeRel;
                  % FIX TIME PROBLEMS
                  switch cModule
                    case 'AnalogIn';
                      R.AnalogIn.Data(iT).Data.Time = LF_fixTime(R.AnalogIn.Data(iT).Data.Time);
                      
                  end
                else
                  ChangeTime = NaN;
                end
            end
          else
            ChangeTime = []; ChangePos = []; ChangeVal = [];
          end
          
        case {'AudioIn','AudioOut'};
          ChangePos = R.(cModule).Data(iT).Sync.ChangePos;
          ChangeVal = R.(cModule).Data(iT).Sync.ChangeVal;
          ChangeTime = R.(cModule).Data(iT).Sync.ChangeTime;
          
        case 'Video';
          if ~isempty(R.(cModule).Data(iT).Data)
            ChangePos = 1; ChangeVal = 1;
            
            ChangeTime = R.(cModule).Data(iT).Data.Time(ChangePos,1);
            R.(cModule).Data(iT).Data.Time(:,1) = R.(cModule).Data(iT).Data.Time(:,1) - ChangeTime;
          else
            ChangeTime = []; ChangePos = []; ChangeVal = [];
          end
          
        case 'EPhys'
          if ~isempty(R.(cModule).Data(iT).Data.Spike)
            ChangePos = R.(cModule).Data(iT).TrialStartPos;
            ChangeVal = 1;
            ChangeTime = R.(cModule).Data(iT).Data.Time(ChangePos,:);
            R.(cModule).Data(iT).Data.Time(:,1) = R.(cModule).Data(iT).Data.Time(:,1) - ChangeTime;
          else
            ChangePos =[]; ChangeVal = []; ChangeTime = [];
          end
          
        otherwise error('Module Type not implemented!');
      end
      R.(cModule).Data(iT).Sync.ChangeVal = ChangeVal;
      R.(cModule).Data(iT).Sync.ChangeTimes = ChangeTime;
      
      % CORRECT TIME VECTOR TO A UNIQUE REFERENCE FRAME
      % TIME IS REPRESENTED RELATIVE TO START OF PARADIGM
      if ~isempty(R.NIDAQ.Data(iT).TrialStartTime)
        % FIND INITIALIZING SYNC PULSE
        switch cModule
          case {'AnalogIn','EPhys'}
            R.(cModule).Data(iT).Sync.SyncTimeRel = SyncTimes(cTrial).Trial(1);
          case 'PointGrey';
            R.(cModule).Data(iT).Sync.SyncTimeRel =  SyncTimes(cTrial).Camera(1); % R.NIDAQ.Data(1).CameraStartTime(1);
          otherwise
            R.(cModule).Data(iT).Sync.SyncTimeRel = 0;
        end
        
        if ~isempty( R.(cModule).Data(iT).Data.Time)
          R.(cModule).Data(iT).Data.Time(:,1) = R.(cModule).Data(iT).Data.Time(:,1) + double(R.(cModule).Data(iT).Sync.SyncTimeRel) ;
        end
      end
    end
  end
  fprintf('\n');
end

if P.AlignHost
    R = getHostPlatforms(R);
    for t = 1:numel(R.General.Paradigm.Trials)
        if strcmp(R.General.Paradigm.Trials(t).Host,'right')
            TrIdx = find(R.PointGrey.Trials==t);
            R.PointGrey.Data(TrIdx).Data.Frames=R.PointGrey.Data(TrIdx).Data.Frames(end:-1:1,:,:,:);
        end
    end
end


function [Modules,Types] = LF_getModulesAvailable(R,RecordingPath);

% LOCATE THE MODULES WHICH HAVE BEEN TRANSFERRED LOCALLY
Files =dir(RecordingPath);
Files = Files([Files.isdir]);
Files = Files(~strcmp('Results',{Files.name}));
Files = Files(~strcmp('.',{Files.name}));
Files = Files(~strcmp('..',{Files.name}));
ModulesOnDisk = {Files.name};
Modules = ModulesOnDisk;

% ASSIGN TYPE FOR EACH MODULE (GENERAL ALSO CONTAINS THIS EXCEPT FOR EPHYS)
for iM = 1:length(Modules)
  switch Modules{iM}
    case {'NIDAQ','AnalogIn','AnalogOut','AnalogIO'}; cType = 'NI';
    case 'PointGrey'; cType = 'Video';
    case 'EPhys'; cType = 'EPhys';
    case {'AudioIn'}; cType =  'AudioIn';
    case {'AudioOut'}; cType = 'AudioOut';
    case {'ArduinoCAM','ArduinoPlatform','ArduinoFeedback'}; cType = 'Arduino';
    otherwise error(['Type for Module ',Modules{iM},'not defined.']);
  end
  Types{iM} = cType;
end;

function [Modules,Types] = LF_selectModules(ModulesSelected,ModulesAvailable,TypesAvailable);

% COMPARE AVAILABLE AND SELECTED MODULES:
% TAKE ONLY SELECTED, WARN ABOUT UNAVAILABLE
if isequal(ModulesSelected,'all')  ModulesSelected = ModulesAvailable; end

% NIDAQ HAS TO ALWAYS BE SELECTED FOR ALIGNMENT
[~,Ind] = setdiff(ModulesSelected,'NIDAQ');
ModulesSelected = ['NIDAQ',ModulesSelected(Ind)];
[~,Ind] = setdiff(ModulesAvailable,'NIDAQ');
ModulesAvailable = ['NIDAQ',ModulesAvailable(Ind)];
TypesAvailable = ['NI',TypesAvailable(Ind)];

AllInd = logical(zeros(size(ModulesAvailable)));
for iM = 1:length(ModulesSelected)
  Ind = strcmp(ModulesSelected{iM},ModulesAvailable);
  if ~any(Ind) fprintf(['Warning : Module ',ModulesAvailable{iM},' not available on disk\n']);
  else   AllInd = AllInd | Ind;
  end
end
Modules = ModulesAvailable(AllInd);
Types      = TypesAvailable(AllInd);

fprintf('Modules (chosen) : '); ChoiceChars = {'O','X'};
for iM = 1:length(ModulesAvailable)
  fprintf([' [ ',ModulesAvailable{iM},' ( ',ChoiceChars{AllInd(iM)+1},' ) ] ']);
end;  fprintf('\n');

function Time = LF_fixTime(Time)

Start =double(Time(1,1));
NTimes = size(Time,1);
End = Start; Pos = NTimes;

while End <= Start;   Pos = Pos - 1;  End =double(Time(Pos,1)); end

End = End + (NTimes-Pos)*(End-Start)/Pos;
Time = [Start:(End-Start)/(NTimes-1):End]';

function R = getHostPlatforms(R)
%only for selected trials, other just get an empty field.
Names   = {R.General.Paradigm.HW.Inputs.Name};
Left    = find(~cellfun(@isempty,strfind(Names,'AniPosP1')));
Rite    = find(~cellfun(@isempty,strfind(Names,'AniPosP2')));
Trig    = strcmpi(Names,'CamStart');
Thres   = [R.General.Paradigm.HW.Inputs.Threshold];

rmList = [];
Tr = R.NIDAQ.Trials;
for t = 1:numel(Tr);
    if ~isempty(R.NIDAQ.Data(t).TrialStartTime);
        camStart = find(R.NIDAQ.Data(t).Data.Analog(:,Trig) > 2.5,1,'first');
        if ~isempty(camStart)
          if any(R.NIDAQ.Data(t).Data.Analog(camStart,Left) < Thres(Left));
            R.General.Paradigm.Trials(Tr(t)).HostIndex = 1; % mouse enters from the left
            R.General.Paradigm.Trials(Tr(t)).Host = 'left';
          else any(R.NIDAQ.Data(t).Data.Analog(camStart,Rite) < Thres(Rite));
            R.General.Paradigm.Trials(Tr(t)).HostIndex = -1; % mouse enters from the right
            R.General.Paradigm.Trials(Tr(t)).Host = 'right';
          end
        else
          warning('no camStart, skip aligning')
          R.General.Paradigm.Trials(Tr(t)).HostIndex = [];; % no video's in trial
          R.General.Paradigm.Trials(Tr(t)).Host = '';
        end
    else
        warning(['Incomplete NIDAQ data in trial %d, can''t align. Removing said trials now! If this doesn''t occur allot, do manual Aligning!\n',...
            R.PointGrey.Trials(t)])
        rmList(end+1) = t;
    end
end
    if ~isempty(rmList); % for now remove these trials, maybe in the future manual checking? 
        R.PointGrey.Trials(rmList)=[];
        R.PointGrey.Data(rmList)=[];
    end

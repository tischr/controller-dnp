function R = C_selectRecordings(varargin)
 % SELECT SUBSET OF RECORDINGS ACCORDING TO CRITERIA
global CG

% PARSE ARGUMENTS
P = parsePairs(varargin);
checkField(P,'Animal','');
checkField(P,'Recording','');
checkField(P,'Paradigm','');
checkField(P,'EPhys',[0,1]);
checkField(P,'Bad',[0])
checkField(P,'StartDate','');

if ~C_checkDatabase error('Database could not be opened!'); end

TestFields = {'StartDate','Animal','Recording','Paradigm','EPhys','Bad'};

Opts = ''; Selection = 0;
for iF=1:length(TestFields)
  if ~isempty(P.(TestFields{iF}))
    switch  TestFields{iF}
      case 'StartDate';
        cOpt = [' date>=STR_TO_DATE(''',P.StartDate,''', ''%Y-%m-%d %H:%i:%s'')'];
      case 'Animal';
        if ~iscell(P.Animal) P.Animal = {P.Animal}; end
        S = ''; for i=1:length(P.Animal) S = [S,'''',P.Animal{i},''',']; end
        S = S(1:end-1); cOpt = ['animal in (',S,') '];
      case 'Paradigm';
        if ~iscell(P.Paradigm) P.Paradigm = {P.Paradigm}; end
        S = ''; for i=1:length(P.Paradigm) S = [S,'''',P.Paradigm{i},''',']; end
        S = S(1:end-1); cOpt = ['paradigm in (',S,') '];
      case 'Recording';
        cOpt = ['recording in (',sprintf('%d,',P.Recording)]; cOpt(end) = ')';
      case 'EPhys';
        cOpt = ['ephys in (',sprintf('%d,',P.EPhys)]; cOpt(end) = ')';
      case 'Bad',
        cOpt = ['bad in (',sprintf('%d,',P.Bad)]; cOpt(end) = ')';        
      otherwise error('Field not defined!');
    end  
    Opts = [Opts , cOpt,' AND '];
    Selection = 1;
  end
end
if Selection Opts = Opts(1:end-5) ; end

% COLLECT ALL REQUIRED FIELDS, UNLESS THEY HAVE BEEN SET DIRECTLY
if ~isempty(Opts) Opts = ['WHERE ',Opts]; end
QUERY = ['SELECT * FROM recordings ',Opts];
fprintf([QUERY,'\n']);
R = mysql(QUERY);

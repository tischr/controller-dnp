function  R = C_loadEPhys(varargin)
% LOAD ELECTROPHYSIOLOGY DATA RECORDED WITH OPENEPHYS
% In relation to the clock on NI, the sampling rate of the OpenEphys system appears to be
% 30000.483 Hz. Hence, it makes sense to correct the sampling rate here during loading to achieve 
% near perfect alignment (~1us for 600s of recording).
% This only requires rescaling the time vector by dividing it by : 1.0000161

%% PARSE ARGUMENTS
P = parsePairs(varargin); if isempty(P) P = struct([]); end
checkField(P,'Directory',[])
checkField(P,'Animal',[]);
checkField(P,'Recording',[]);
checkField(P,'Channels','all');
checkField(P,'Electrodes',[]);
checkField(P,'Trials','all');
checkField(P,'SeparateByTrial',0);
checkField(P,'Reference',[]);
checkField(P,'Raw',1);
checkField(P,'Spike',0);
checkField(P,'LFP',0);
checkField(P,'Trial',0);
checkField(P,'Camera',0);
checkField(P,'Humbug',0);
checkField(P,'SpikeBand',[300,6000]);
checkField(P,'Range','all')
checkField(P,'LFPBand',[1,300]);
checkField(P,'TriggerChannels',[2,3]);
checkField(P,'TriggerNames',{'Trial','Camera'});
checkField(P,'Loader','fast');
checkField(P,'Verbose',1);
checkField(P,'IncludeIntertrial',1);
checkField(P,'Alignment','TrialStart');
checkField(P,'CorrectTime',1); SRCorrectionFactor = 1.0000161;
checkField(P);

%% CHECK FOR DATA SELECTION
if ~isempty(P.Animal)
  [RecordingPath,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);
  P.Directory = [RecordingPath,'EPhys',filesep];
end

%% SYNCHRONIZE ELECTRODES AND CHANNELS
P.Implant = 'EIB64'; P.Recorder = 'Intan64'; P.Geometry = '8x2';
[P,CM] = C_syncChannelSelection(P);
NChannels = length(P.Channels);

%% FIND ALL FILENAMES
Files = C_findFilesEPhys('Directory',P.Directory);

%% LOAD THE OVERVIEW FILE
[D,T,I] = load_open_ephys_data(Files.General);
R.SR = I.header.sampleRate;
% CORRECT SR TO MATCH TIMING IN NI  (see documentation above)
if P.CorrectTime  R.SR = R.SR * SRCorrectionFactor; end

%% LOAD THE TRIGGERING DATA (ADC)
for iC = 1:length(P.TriggerChannels)
  cChannel = P.TriggerChannels(iC);
  cChannelName = P.TriggerNames{iC};
  cFileName = Files.AnalogIn(cChannel).Name;
  switch P.Loader
    case 'fast';
      [tmp,tmpTime,I] = loadOEP(cFileName);
    case 'original'; 
      [tmp,tmpTime,I] = load_open_ephys_data(cFileName);
  end  
  % CHECK SELECTED RANGE
  if iC==1
    if ~strcmp(P.Range,'all')
      RangeBounds = floor(P.Range*R.SR); 
      RangeBounds(1) = max([RangeBounds(1),1]);
      RangeInd = [RangeBounds(1) : RangeBounds(2)];
    else
      RangeInd = [];
    end
  end
  if ~isempty(RangeInd) 
    tmp = tmp(RangeInd); tmpTime = tmpTime(RangeInd);  
  end
  % ASSIGN RESULT
  R.(cChannelName) = logical(tmp>2.5);
  if iC ==1 R.Time = tmpTime; end
end

% CORRECT TIME TO MATCH NI SAMPLING RATE (see documentation above)
if P.CorrectTime  R.Time = R.Time / SRCorrectionFactor;  end

%% FIND THE TRIAL STARTS
R.TrialInfo = LF_findTrials(R.Trial,R.SR);
R.NSamples = length(R.Trial);

%% LOAD THE ELECTRODE DATA (BY CHANNEL)
Wbefore = whos('R'); tic; fprintf('\n');
for iE = 1:length(P.Electrodes)
  cElectrode = P.Electrodes(iE);
  cChannel = P.Channels(iE);
  cFileName = Files.Data(cChannel).Name;
  if P.Verbose;
    fprintf(['Pos: ',num2str(iE),' :\t El.',num2str(cElectrode),' \t(Ch. ',num2str(cChannel),') \t',escapeMasker(cFileName),'\n']);  
  else
    if ~mod(iE,10) fprintf('%d',cElectrode); else fprintf('.'); end
  end
  switch P.Loader
    case 'fast';          tmp = single(loadOEP(cFileName));
    case 'original';   tmp = single(load_open_ephys_data(cFileName));
  end
  if ~isempty(RangeInd)  tmp = tmp(RangeInd); end
  R.Raw(:,iE) = tmp;
end
cFile = dir(cFileName); R.Date = cFile.date;
Wafter = whos('R'); Time = toc;
MB = (Wafter.bytes-Wbefore.bytes)/1024.^2/2; % /2 since int16 is loaded
if P.Verbose fprintf(['  [ OK  (',sprintf('%3.0f MB, %3.1f s, %3.1f MB/s',MB,Time,MB/Time),') ]\n']); end

%% FILTER DATA 
% COMPUTE FILTERS
if P.Humbug || P.LFP || P.Spike
  Filter = LF_computeFilters(P.SpikeBand,P.LFPBand,R.SR,P.Humbug);
end

% SUBTRACT COMMON MEAN OVER A SET OF REFERENCE CHANNELS
if  ~isempty(P.Reference)
  if strcmp(P.Reference,'all') P.Reference = [1:length(CM.Electrodes)]; end
  Reference = C_computeEPhysReference('Animal',P.Animal,'Recording',P.Recording,...
    P.UserSelection,P.Reference,'Verbose',P.Verbose);
  R.Raw = R.Raw-repmat(Reference,1,size(R.Raw,2));
end

% FIRST FILTER THE LINE NOISE
if P.Humbug
  if P.Verbose fprintf('Filtering Humbug-band...\n'); end
  R.Raw = filter(Filter.Humbug.Hd,R.Raw);
end

% FILTER DIFFERENT SIGNALS
if P.Spike
  if P.Verbose fprintf('Filtering Spike-band...\n'); end
  R.Spike = filter(Filter.Spike.Low.b,   Filter.Spike.Low.a,    R.Raw);
  R.Spike = filter(Filter.Spike.High.b,  Filter.Spike.High.a,   R.Spike);
end

if P.LFP
  if P.Verbose fprintf('Filtering LFP-band...\n'); end
  R.LFP     = filter(Filter.LFP.Low.b,    Filter.LFP.Low.a,        R.Raw);
  R.LFP     = filter(Filter.LFP.High.b,   Filter.LFP.High.a,       R.LFP);
end

if ~P.Raw  R = rmfield(R,'Raw'); end
if ~P.Trial  R = rmfield(R,'Trial'); end
if ~P.Camera R = rmfield(R,'Camera'); end

%% SEPARATE INTO TRIALS
if P.SeparateByTrial
  for iT=1:length(R.TrialInfo)
    % FIND THE INDICES OF EACH TRIAL
    if P.IncludeIntertrial
      switch P.Alignment
        case 'TrialStart';        
          StartPos = R.TrialInfo(iT).Start.Pos;
          if iT < length(R.TrialInfo)
            EndPos(iT) = R.TrialInfo(iT+1).Start.Pos-1;
          else
            EndPos(iT) = R.NSamples;
          end
          cTrialStartRel = 1;
        case 'TrialStop';
          EndPos(iT) = R.TrialInfo(iT).Stop.Pos;
          if iT==1
            StartPos = 1;
            cTrialStartRel = R.TrialInfo(iT).Start.Pos;
          else
            StartPos = R.TrialInfo(iT-1).Stop.Pos+1; 
            cTrialStartRel = R.TrialInfo(iT).Start.Pos-EndPos(iT-1);
          end
      end
    else
      EndPos(iT) = R.TrialInfo(iT).Stop.Pos;
    end
    TrialInd{iT} = [StartPos:EndPos(iT)];
    TrialStartRel(iT) = cTrialStartRel;
  end
  
  % APPLY SELECTION TO ALL FIELDS
  FN = fieldnames(R);
  Fields = {'Time','Raw','Spike','LFP','Camera'};
  for iF = 1:length(Fields)
    cField = Fields{iF};
    if isfield(R,cField)
      for iT=1:length(R.TrialInfo)
        R.Trials(iT).Data.(cField) = R.(cField)(TrialInd{iT},:);
        if iF ==1;  R.Trials(iT).TrialStartPos = TrialStartRel(iT); end
      end
      R = rmfield(R,cField);
    end
  end
end
% EVENTUALLY RESTRICT THE OUTPUT TO SELECTED TRIALS
if ~isequal(P.Trials, 'all')
  R.Trials = R.Trials(P.Trials);
  R.TrialInfo = R.TrialInfo(P.Trials);    
end


%% PRECOMPUTE THE FILTERS FOR BOTH TRACE AND LFP FILTERING
function Filter = LF_computeFilters(SpikeBand,LFPBand,SR,Humbug)

  % SPIKE BAND
  SpikeOrder = 2;
  [Filter.Spike.High.b,Filter.Spike.High.a] = ...
    butter(SpikeOrder,SpikeBand(1)/(SR/2),'high');
  [Filter.Spike.Low.b,Filter.Spike.Low.a] = ...
    butter(SpikeOrder,SpikeBand(2)/(SR/2),'low');
  
  % LFP BAND
  LFPOrder = 2;
  [Filter.LFP.Low.b,Filter.LFP.Low.a] = ...
    butter(SpikeOrder,LFPBand(2)/(SR/2),'low');
  if LFPBand(1) > 0
    [Filter.LFP.High.b,Filter.LFP.High.a] = ...
      butter(LFPOrder,LFPBand(1)/(SR/2),'high');
  end
  
  % NOTCH FILTER
  if Humbug
    Filter.Humbug.Design  = fdesign.notch('N,F0,Q,Ap',6,50/(SR/2),10,1);
    Filter.Humbug.Hd = design(Filter.Humbug.Design);
  end
  
%% FIND THE TRIAL ONSETS
function T = LF_findTrials(TrialChannel,SR)
  
  DT = diff(TrialChannel);
  TrialStarts = find(DT==1);
  TrialStops = find(DT==-1);
  
  if isempty(TrialStarts)
    TrialStarts = 1; TrialStops = length(TrialChannel);
  end
  
  for iT=1:length(TrialStarts)
    T(iT).Start.Pos           = TrialStarts(iT);
    T(iT).Start.Time         = T(iT).Start.Pos/SR;
    T(iT).Stop.Pos            = TrialStops(find(TrialStops>TrialStarts(iT),1,'first'));
    T(iT).Stop.Time         = T(iT).Stop.Pos/SR;
    T(iT).Duration.Pos    = T(iT).Stop.Pos - T(iT).Start.Pos + 1;
    T(iT).Duration.Time  = T(iT).Duration.Pos/SR;
  end

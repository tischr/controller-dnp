function [P,CM] = C_syncChannelSelection(P)

CM = C_mapChannel('Implant',P.Implant,'Recorder',P.Recorder,'Geometry',P.Geometry);
if ischar(P.Channels) & strcmp(P.Channels,'all') P.Channels = [1:length(CM.Channels)]; end
if ischar(P.Electrodes) & strcmp(P.Electrodes,'all') P.Electrodes = [1:length(CM.Electrodes)]; end
if ~isempty(P.Electrodes)
  P.Channels = [CM.Electrodes(P.Electrodes).Channel]; P.UserSelection = 'Electrodes';
else
  P.Electrodes = [CM.Channels(P.Channels).Electrode]; P.UserSelection = 'Channels';
end

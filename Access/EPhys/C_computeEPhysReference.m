function Reference = C_computeEPhysReference(varargin)
% COMPUTE THE COMMON REFERENCE FOR A SELECTION OF CHANNELS 

%% PARSE ARGUMENTS
P = parsePairs(varargin); if isempty(P) P = struct([]); end
checkField(P,'Directory',[])
checkField(P,'Animal',[]);
checkField(P,'Recording',[]);
checkField(P,'Channels',[]);
checkField(P,'Electrodes',[]);
checkField(P,'Loader','fast');
checkField(P,'Force',0); 
checkField(P,'Verbose',1);
checkField(P);

%% CHECK FOR DATA SELECTION
if ~isempty(P.Animal)
  [RecordingPath,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);
  P.Directory = [RecordingPath,'EPhys',filesep];
end

%% SYNCHRONIZE ELECTRODES AND CHANNELS
P.Implant = 'EIB64'; P.Recorder = 'Intan64'; P.Geometry = '8x2';
if ~isempty(P.Channels) || ~isempty(P.Electrodes)
  [P,CM] = C_syncChannelSelection(P);
else
  P.Channels = 'existing'; P.Electrodes = 'existing';
end
NChannels = length(P.Channels);

% CREATE REFERENCE FILE NAME
RefName = [P.Directory,'CommonReference.mat'];

% CHECK IF REFERENCE EXISTS
ReferenceFound = 0;
if exist(RefName,'file')
  S = load(RefName,'Channels');
  if isequal(P.Channels,'existing') || isequal(S.Channels,P.Channels) % REFERENCE HAS ALREADY BEEN COMPUTED
    if P.Verbose fprintf('  Loading precomputed Reference\n'); end
      D = load(RefName);
      Reference = D.Reference;
      ReferenceFound = 1;
  end
end

if ~ReferenceFound
  if P.Verbose fprintf('  Recomputing Reference...\n'); end

    %% FIND ALL FILENAMES
    Files = C_findFilesEPhys('Directory',P.Directory);
    
    %% LOAD THE OVERVIEW FILE
    [D,T,I] = load_open_ephys_data(Files.General);
    
    %% LOAD THE ELECTRODE DATA (BY CHANNEL)
    for iE = 1:length(P.Channels)
      cChannel = P.Channels(iE);
      cElectrode = P.Electrodes(iE);
      cFileName = Files.Data(cChannel).Name;
      if P.Verbose;
        fprintf(['Pos: ',num2str(iE),' :\t El.',num2str(cElectrode),' \t(Ch. ',num2str(cChannel),') \t',escapeMasker(cFileName),'\n']);
      else
        if ~mod(iE,10) fprintf('%d',cElectrode); else fprintf('.'); end
      end
      switch P.Loader
        case 'fast';          tmp = single(loadOEP(cFileName));
        case 'original';   tmp = single(load_open_ephys_data(cFileName));
      end
      if iE == 1
        Reference = tmp;
      else
        Reference = (iE-1)/iE*Reference + tmp/iE;
      end
    end
    Channels = P.Channels;
    save(RefName,'Reference','Channels');
end

function R = C_findFilesEPhys(varargin)

P = parsePairs(varargin);
checkField(P,'Directory');
if P.Directory(end)~=filesep P.Directory(end+1) = filesep; end

% FIRST DETERMINE THE POSTFIX
Files = dir([P.Directory,'all_channels*.events']);
if isempty(Files) error('Main Events File not found.'); end

Pos = find(Files(1).name=='.');
if Pos == 13; 
  PostFix = ''; Num = 0; 
else
  PostFix = Files(1).name(13:Pos-1);
  Num = str2num(PostFix(2:end));
end

R.General = [P.Directory,Files(1).name];

Files = dir([P.Directory,'*.continuous']);

for iF=1:length(Files)
  cName = Files(iF).name;
  DotPos = find(cName=='.');
  UnderscorePos = find(cName=='_');
  % CHECK FOR FILE TYPE
  switch cName(UnderscorePos(1)+[1:2])
    case 'AD'; FileTag = 'ADC'; DataType = 'AnalogIn'; 
    case 'AU'; FileTag = 'AUX'; DataType = 'AUX';
    case 'CH'; FileTag = 'CH'; DataType = 'Data';
    otherwise error('FileType not determined.');
  end
    
  % FIND FILE NUMBER 
  if length(UnderscorePos)==2
    EndPos = UnderscorePos(2)-1;
  else
    EndPos = DotPos -1;
  end
  cNum = str2num(cName(UnderscorePos(1)+1+length(FileTag):EndPos));
  
  R.(DataType)(cNum).Name = [P.Directory,cName];
  R.(DataType)(cNum).ChNum = cNum;
end


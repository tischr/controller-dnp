function Resp = C_batchResponse(varargin)
% MAIN ACCESS FUNCTION FOR NEURAL RESPONSES COMPUTED FROM ANALOG DATA
% Principles of Operation: 
% - Sorts should be organized by Electrode Sets
%    - i.e. for Multiunit it would be just one for each channel 
%    - for tetrode sorting it would be a set of 4 Electrodes with a match of  
%    - 
% - Response Data Format : 
%    - Different Kinds of Sorting (all saved in the same file, so that it can be easily updated)
%    - Requires loading and saving, instead, just save different types of sorts with different names?
%    - Vector of Times 
%    - Vector of Response Values (for each time)
% - Different Types of Sorting
%
% - Saving Locations : 
%     Data amounts should be small, hence, try to save local and on server (also to keep these synched)

P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'Paradigm');
checkField(P,'ResponseType','Multiunit');
checkField(P,'Mode','Unsupervised')
checkField(P,'Verbose',1);


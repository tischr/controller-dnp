function Resp = C_getResponse(varargin)
% MAIN ACCESS FUNCTION FOR NEURAL RESPONSES COMPUTED FROM ANALOG DATA
% Principles of Operation: 
% - Sorts should be organized by Electrode Sets
%    - i.e. for Multiunit it would be just one for each channel 
%    - for tetrode sorting it would be a set of 4 Electrodes with a match of  
%    - 
% - Response Data Format : 
%    - Different Kinds of Sorting (all saved in the same file, so that it can be easily updated)
%    - Requires loading and saving, instead, just save different types of sorts with different names?
%    - Vector of Times 
%    - Vector of Response Values (for each time)
% - Different Types of Sorting
%
% - Saving Locations : 
%     Data amounts should be small, hence, try to save local and on server (also to keep these synched)

P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'Trials','all');
checkField(P,'Electrodes','all');
checkField(P,'Reference','all');
checkField(P,'Range','all');
checkField(P,'ResponseType','Multiunit');
checkField(P,'ParametersMultiunit',{'Threshold',-4})
checkField(P,'ParametersSingleunit',{'Threshold',-3.7})
checkField(P,'ParametersHighgamma',{ });
checkField(P,'ParametersSpikes',{});
checkField(P,'Reference','all');
checkField(P,'Recompute',0);
checkField(P,'Mode','Unsupervised')
checkField(P,'Verbose',1);
checkField(P);

% FIND THE BASE PATH
I = C_getRecInfo('Animal',P.Animal,'Recording',P.Recording);
if ~I.EPhys disp('Recording does not have Electrophysiology!'); return; end

%% CHECK FOR ALREADY ANALYZED DATA
cResponseFileLocal = [I.Local.Response.File,'_',P.ResponseType,'.mat'];
cResponseFileDB = [I.DB.Response.File,'_',P.ResponseType,'.mat'];
if exist(cResponseFileLocal,'file')
  R = load(cResponseFileLocal); R = R.R;
elseif exist(cResponseFileDB,'file')
  R = load(cResponseFileDB); R = R.R;
else
  R = [];
end
NewComputed = 0; SortFiles = {};

%% AUTOMATICALLY DIVIDE ELECTRODES INTO SETS FOR THE CHOSEN METHOD
ElectrodeSets = LF_divideElectrodes(I,P);

%% COMPUTE THE RESPONSE IF IT HAS NOT BEEN FOUND
for iS=1:length(ElectrodeSets)
  cElectrodeSet = ElectrodeSets{iS};
  [cSetInd,Exists] = LF_checkElectrodeSet(cElectrodeSet,R);
  
  if ~Exists | P.Recompute
    NewComputed = 1;
    
    if ~exist('Data','var')
      % LOAD THE RAW DATA
      Data = C_loadRecording('Animal',P.Animal,'Recording',P.Recording,...
        'Modules',{'NIDAQ','EPhys'},...
        'Electrodes',P.Electrodes,'Trials',P.Trials,'Raw',0,'Spike',1,'Reference',P.Reference);
    end
    
    [~,cElectrodeInd] = intersect(P.Electrodes,cElectrodeSet);
    
    % FUSE DATA ACROSS TRIALS
    cData = []; cTime = [];
    for iT=1:length(Data.EPhys.Data)
      TrialIndices(iT) = length(cData)+1;
      cData = [cData ; Data.EPhys.Data(iT).Data.Spike(:,cElectrodeInd)];
      cTime = [cTime ; Data.EPhys.Data(iT).Data.Time];
    end
    SR = Data.EPhys.SRAI;
    
    % SEND TO SPIKE SORTING 
    switch lower(P.ResponseType)
      case 'multiunit';
        cR = C_findMultiunit('Data',cData,'Time',cTime,'SR',SR,'Electrodes',cElectrodeSet,...
          P.ParametersMultiunit{:});
      case 'singleunit';
        cR = C_findSpikes('Data',cData,'Time',cTime,'SR',SR,'Electrodes',cElectrodeSet,...
          'TrialIndices',TrialIndices,P.ParametersSpikes{:},P.ParametersSingleunit{:},'Mode',P.Mode,...
          'Identifier',I.Identifier);
      case 'highgamma';
        cR = C_findHighGamma('Data',cData,'Time',cTime,'SR',SR,'Electrodes',cElectrodeSet,...
          P.Parameters.HighGamma);
      otherwise error('Selected ResponseType not implemented.');
    end
    
    % REMEMBER ADDITIONAL FILES FOR SORTING
    if isfield(cR,'SortFiles')
      SortFiles(end+1:end+length(cR.SortFiles)) = cR.SortFiles;
      cR = rmfield(cR,'SortFiles');
    end
    
    % PREPARE RESPONSE FILE FOR SAVING
    % Find matching sets of electrodes
    cR.ElectrodeSet = cElectrodeSet;
    if isempty(R) R = cR; else R(cSetInd) = cR; end 
    
    % SORT SETS IN ASCENDING ORDER
    SortInd = LF_sortChannels(R);
    R = R(SortInd);
    
  end
  
end

% FIND SET OF INDICES AFTER SORTING
for iS=1:length(ElectrodeSets)
  cElectrodeSet = ElectrodeSets{iS};
  SetInds(iS) = LF_checkElectrodeSet(cElectrodeSet,R);
end
Resp = R(SetInds);

if NewComputed
  %% SAVE RESPONSE BOTH LOCALLY AND REMOTE
  mkdirAll(cResponseFileLocal);
  save(cResponseFileLocal,'R'); % SAVE SEPARATELY THE FIELDS (This will allow separate loading later)
  try mkdirAll(cResponseFileDB); end
  try save(cResponseFileDB,'R'); end % SAVE SEPARATELY THE FIELDS (This will allow separate loading later)  
  % COPY ALL OTHER CREATED FILES TO THESE DIRECTORIES
  for iF=1:length(SortFiles)
    copyfile(SortFiles{iF} , I.Local.Response.Path);
    copyfile(SortFiles{iF}, I.DB.Response.Path);
    delete(SortFiles{iF});
  end
  
  if strcmp(P.ResponseType,'Singleunit')
    switch P.Mode
      case 'Unsupervised'; HF_sendEmail('Subject',['Spikesorting of [ ',I.Identifier,' ]  finished!']);
    end
  end
  
end

%% DIVIDE THE ELECTRODES INTO SETS ACCORDING TO ARRAY GEOMETRY
function ElectrodeSets = LF_divideElectrodes(I,P)
  
  switch P.ResponseType
    case 'Multiunit';
      for iE=1:length(P.Electrodes)
        ElectrodeSets{iE} = P.Electrodes(iE);
      end
    case 'Singleunit'; 
      ElectrodesAll = [1:length(I.Implant.Info.Electrodes)];
      GroupsAll = [I.Implant.Info.Electrodes.Group];
      [Electrodes,iA,iB] = intersect(ElectrodesAll,P.Electrodes);
      Groups = GroupsAll(iA);
      UGroups = unique(Groups);
      for iS=1:length(UGroups)
        cInd = Groups == UGroups(iS);
        ElectrodeSets{iS} = P.Electrodes(cInd);
      end     
    otherwise
  end
  
%% CHECK WHETHER THE ELECTRODE SETS HAVE ALREADY BEEN COMPUTED
function [SetInd,Exists] = LF_checkElectrodeSet(cElectrodeSet,Responses)
  
  SetInd = 1;  Exists = 0; % Default Value
  % CHECK THE ALREADY EXISTING SETS TO FIND IF ONE HAS THE CURRENT SET
  if ~isempty(Responses) 
    for iS=1:length(Responses)
      if isequal(cElectrodeSet,Responses(iS).ElectrodeSet)
        SetInd = iS; Exists = 1;
      end
    end
    if ~Exists SetInd = iS+1; end
  end
  
 
  %% SORT THE ELECTRODESETS IN ASCENDING ORDER
  function SortInd = LF_sortChannels(Responses);
    
    for iS=1:length(Responses)
      MeanElectrodes(iS) = mean(Responses(iS).ElectrodeSet);
    end
    [tmp,SortInd] = sort(MeanElectrodes); 

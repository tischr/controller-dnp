function R = C_findMultiunit(varargin)
% Compute Multiunit Response By Simply Thresholding
% Only performed for a single channel

P = parsePairs(varargin);
checkField(P,'Data');
checkField(P,'SR');
checkField(P,'Time',[]);
checkField(P,'ThreshType','Amplitude');
checkField(P,'Threshold',-4); % USE NEGATIVE 4 S.D. for the Threshold
checkField(P,'Rejection',NaN); % REJECT EVENTS THAT ARE BIGGER THAN X S.D.
checkField(P,'RejectionRange',1); % REJECT ONE MILLISECOND
checkField(P,'Electrodes',[])
checkField(P,'Verbose',1)

if isempty(P.Electrodes) P.Electrodes = 1:size(P.Data,2); end 

for iE = 1:size(P.Data,2)
  cElectrode = P.Electrodes(iE);
  R.Time{iE} = []; R.Response{iE} = []; R.NSpikes(iE) = 0;

  % SCALE DATA BY S.D.
  SelInd = [1:min([100000,size(P.Data,1)])];
  SD = mean(abs(prctile(P.Data(SelInd,iE),[16,84])));
  P.ThresholdAbs = P.Threshold*SD;
  P.RejectionAbs = P.Rejection*SD;
  
  % REJECT PASSAGES IN THE DATA
  % including a broadening
  if ~isnan(P.Rejection)
    RejInd = P.Data(:,iE)>P.Rejection;
    Range = round(P.RejectionRange*P.SR);
    RejInd = ...
      [zeros(Range,1);RejInd(Range+1:end-Range);zeros(Range,1);] | ...
      [zeros(2*Range,1);RejInd(1:end-2*Range)] | ...
      [RejInd(2*Range+1:end);zeros(2*Range,1)];
    P.Data(RejInd,iE) = NaN;
  end
  
  % THRESHOLD SPIKES
  switch P.ThreshType
    case 'Amplitude';
      if P.ThresholdAbs > 0  ind = find(P.Data(:,iE)>P.ThresholdAbs);
      else ind = find(P.Data(:,iE)<P.ThresholdAbs); end
    case 'Rise';
      P.ThresholdAbs = std(diff(P.Data(:,iE)))*P.Threshold;
      if P.ThresholdAbs > 0  ind = find(diff(P.Data(:,iE))>P.ThresholdAbs);
      else ind = find(diff(P.Data(:,iE))<P.ThresholdAbs); end
    otherwise
      error('ThreshType not implemented!');
  end
  
  % IF THERE ARE NO SPIKES, EXIT
  if length(ind) > 1
    dind = diff(ind);  ind2 = find(dind>1);
    SP = zeros(1,length(ind2)+1); SP(1) = ind(1);  SP(2:end) = ind(ind2+1);
  else
    SP = ind;
  end
  
  NSpikes = length(SP);
  fprintf(['El.',num2str(cElectrode),' :\t ',n2s(NSpikes),' spikes at threshold ',n2s(P.Threshold),' S.D. (',num2str(round(P.ThresholdAbs)),' uV)\n']);
  
  % ASSIGN SPIKE TIMES
  if ~isempty(P.Time) 
    R.Time{iE} = P.Time(SP);
  else 
    R.Time{iE} = ind/P.SR;
  end
  
  % ASSIGN RESPONSE VALUES
  R.Response{iE} = ones(size(R.Time{iE}));
  R.NSpikes(iE) = NSpikes;
end

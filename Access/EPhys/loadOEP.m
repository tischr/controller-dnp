function [Data, Timestamps, Info] = loadOEP(Filename)
% [Data, Timestamps, Info] = loadOEP(filename)
%   Loads continuous, event, or spike data files into Matlab.
%
%   Input:
%     filename: path to file
%
%   Outputs:
%     data: either an array continuous samples (in microvolts),
%           a matrix of spike waveforms (in microvolts),
%           or an array of event channels (integers)
%     timestamps: in seconds
%     info: structure with header and other information

% CONSTANTS
HeaderBytes = 1024;
RecordSamples = 1024;
RecordMarker = [0 1 2 3 4 5 6 7 8 255]';
NTime16 = 4; 
NRecordMarker16 = 5;
RecordLength16 = RecordSamples + NTime16 +NRecordMarker16 + 2;

% PROCESS HEADER
FID = fopen(Filename);
HeaderChar = fread(FID, HeaderBytes, 'char*1');
eval(char(HeaderChar'));
Info.header = header;

% PROCESS DATA
D16 = fread(FID,inf,'int16',0,'b')*Info.header.bitVolts;
NData16 = length(D16); 
NRecords = NData16/RecordLength16;
if mod(NRecords,1)~=0   error('Record Length is different from expected, check!');
end
  
% CONVERT TO RECORDS
DR = reshape(D16,[RecordLength16,NRecords]);

% CHECK A FEW CONDITIONS
Start16 = NTime16  + 2 + 1;
End16 = RecordLength16 - NRecordMarker16;
Data = DR(Start16:End16,:); 
Data = Data(:);
NSamples = length(Data);

% UNTIL WE HAVE A BETTER CONVERSION FROM
% Timestamps: little endian int64
% Data : big endian int16
% use dec2bin,bin2dec
%Timestamps = DR(1:NTime16,:);
if nargout > 1
  fseek(FID,HeaderBytes,'bof');
  Timestamps = fread(FID,inf,'int64',0,'l');
  Timestamps = Timestamps(1: RecordLength16 :end)';
  StepSize = 4*RecordSamples;
  Timestamps = interp1([1:StepSize:StepSize*length(Timestamps)]',Timestamps,[1:NSamples]','linear','extrap');
  Timestamps = Timestamps./Info.header.sampleRate;
end
fclose(FID);

function filesize = getfilesize(fid)  
  fseek(fid,0,'eof'); filesize = ftell(fid); fseek(fid,0,'bof');

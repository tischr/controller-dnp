function RecordingViewer(varargin)

% PARSE ARGUMENTS
P = parsePairs(varargin); if isempty(P) P = struct([]); end
global RG; R_Defaults;
checkField(P,'Data');
checkField(P,'Channels','all');
checkField(P,'Electrodes',[]);
checkField(P,'Reference',[]);
checkField(P,'Tiling','linear');
checkField(P,'ColorStyle','Tetrode');
checkField(P,'Range',[])
checkField(P,'ShowInfo',0);

% SYNCHRONIZE ELECTRODES AND CHANNELS
P.Implant = 'EIB64'; P.Recorder = 'Intan64'; P.Geometry = '8x2';
CM = C_mapChannel('Implant',P.Implant,'Recorder',P.Recorder,'Geometry',P.Geometry);
if ~isempty(P.Electrodes)
  P.Channels = [CM.Electrodes(P.Electrodes).Channel];
else
  P.Electrodes = [CM.Channels(P.Channels).Electrode];
end

% 
cFIG = round(2^30*rand)+1; 
UD.FID = ['F',n2s(cFIG)]; RG.P = P;
if isfield(RG,'Data') & isfield(RG.Data,UD.FID) RG.Data = rmfield(RG.Data,UD.FID); end

% LOAD DATA OR CONVERT INTO RIGHT FORMAT
if ischar(P.Data) % DATA INDICATES A STRING TO A DIRECTORY
  R_loadData(P.Data,P.Channels,cFIG,UD.FID,P.Range);
else % DATA ALREADY LOADED
  RG.Data.(UD.FID) = P.Data;
end

% GATHER DATA FOR CURRENT PLOT
UD.NChannels = length(P.Channels);

% TILINGS : SELECT & LOAD
UD.Tiling = P.Tiling;
if ischar(UD.Tiling)
  switch UD.Tiling
    case 'linear';
      NChannels = length(P.Channels);
      UD.Tiling = [4,ceil(NChannels/4)];
  end
end

RG.Data.(UD.FID).SR  = RG.DAQ.SR;

UD.YLim = [-200,200];

% SET TITLE AND USERDATA
SS = get(0,'ScreenSize');  FW = 1200; FH = 500;
MainPos = [50,600,100,300];
Position = [MainPos(1)+MainPos(3),SS(4)-FH-2*RG.GUI.MenuOffset,FW,FH];
Name = ['',...
  ' (Dur: ',n2s(RG.Data.(UD.FID).Ndata/RG.Data.(UD.FID).SR),' s, Date: ',...
  datestr(RG.Data.(UD.FID).Date),')'];
figure(cFIG); clf; set(cFIG,'NumberTitle','off','Name',Name,'UserData',UD,...
  'Toolbar','figure','MenuBar','none','Position',Position,...
  'WindowScrollWheelFcn',{@R_CBF_axisWheel},'DeleteFcn',{@R_CloseFig,UD.FID});

% CREATE FILTERED VERSIONS
UD.Reference = RG.P.Reference;
set(gcf,'UserData',UD);
R_filterData(UD.FID)

% BUILD TOOLS
% PLAYTOOLS, STEPSIZE, POSITION, LENGTH, FILTERING
UD.Interval = [0,RG.Disp.SegDur]; R_addToUD(cFIG,'Interval',UD.Interval);
WindowSize = UD.Interval(2); R_addToUD(cFIG,'WindowSize',WindowSize);
StepSize = WindowSize/2; R_addToUD(cFIG,'StepSize',StepSize);
R_buildTools(cFIG);

% PREPARE DISPLAY
R_prepareDisplay(cFIG);

% SHOW DATA
R_CBF_showData(cFIG,UD.Interval);

% HELPERS FOR DISPLAYS %%%%%%%%%%%%%%%%%%%%%%%
function R_buildTools(FIG)
global RG
UD = get(FIG,'UserData');

% USE FIXED SIZES FOR POSITIONING
PanelHeight = 0.04; if ismac PanelHeight = 1.5*PanelHeight; end
Panel = uipanel('Parent',FIG,'Title','','BackGroundColor',RG.Colors.Back,...
  'Units','normalized','Position',[0,0,1,PanelHeight],'BorderType','line');

BW = 30; EW = 20; CW = 35; LW = 20;
DivX = 1./[BW,BW,BW,EW,BW,BW,EW,0.7*EW,0.7*EW,...
  CW,LW,CW,LW,CW,LW,CW,LW,CW,LW/1.7,CW,LW/1.7,0.3*EW,30]; 
DivX = [DivX,1-sum(DivX)];
DC = axesDivide(1.5*DivX,1,[.02,.07,0.9,.86],[1/100],[]); i=1;

% WINDOWSIZE
UD.GUI.WindowSize = LF_addEdit(Panel,DC{i},RG.Disp.SegDur,...
  {@R_CBF_changeWindow,'Size'},'Set Windowsize'); i=i+1;

% JUMP TO START
UD.GUI.JumpStart = LF_addPushbutton(Panel,DC{i},'I<',...
  {@R_CBF_changeWindow,'Start'},'Jump to Start'); i=i+1;

% STEP BACK
UD.GUI.StepBack = LF_addPushbutton(Panel,DC{i},'<',...
  {@R_CBF_changeWindow,'Back'},'Step Back'); i=i+1;

% STEPSIZE
UD.GUI.StepSize = LF_addEdit(Panel,DC{i},RG.Disp.SegDur/2,...
  {@R_CBF_StepSize},'StepSize'); i=i+1;

% STEP FORWARD
UD.GUI.StepForward = LF_addPushbutton(Panel,DC{i},'>',...
  {@R_CBF_changeWindow,'Forward'},'Step Forward'); i=i+1;

% JUMP TO END
UD.GUI.JumpEnd = LF_addPushbutton(Panel,DC{i},'>I',...
  {@R_CBF_changeWindow,'End'},'Jump to End'); i=i+1;

% SPARSENING
UD.GUI.Sparse = LF_addEdit(Panel,DC{i},'auto',...
  {@R_CBF_setSparse},'Set sparsening step'); i=i+1;

% XLimits
UD.GUI.XLim = LF_addEdit(Panel,DC{i},[n2s(UD.Interval(1)),' ',n2s(UD.Interval(2))],...
  {@R_CBF_changeWindow,'XLim'},'Set X Limits'); i=i+1;

% YLimits
UD.GUI.YLim = LF_addEdit(Panel,DC{i},[n2s(UD.YLim(1)),' ',n2s(UD.YLim(2))],...
  {@R_CBF_setYLim},'Set Y Limits'); i=i+1;

% Raw
Vars = {'Raw','Trace','LFP','Spike','Spectrum','Reference'}; 
Letters = {'R','T','L','S','F','C'};
for iV=1:length(Vars)
  UD.(Vars{iV}) = RG.Disp.(Vars{iV});
  UD.GUI.(Vars{iV}) = LF_addCheckbox(Panel,DC{i},UD.(Vars{iV}),...
    {@R_CBF_setVisibility,Vars{iV}},['Set ',Vars{iV},' Visibility']); i=i+1;
  LF_addText(Panel,DC{i},Vars{iV}); i=i+1;
end

% REFERENCE
String = HF_list2colon(RG.P.Reference);
UD.GUI.RefChannels = LF_addEdit(Panel,DC{i},String,...
  {@R_CBF_changeReference},'Choose Reference Channels'); i=i+1;

% FILTERING
% CALLBACK SHOULD ONLY ACT, IF THE PROPERTIES ARE CHANGED
set(Panel,'Units','Pixels')
UD.Panel = Panel;
set(FIG,'UserData',UD);

function R_CloseFig(obj,event,FID)
global RG

try RG.Data = rmfield(RG.Data,FID); catch end

function R_setBusy(obj,event,State)
  UD = get(gcf,'UserData');
  if State; Color = [0,0,1]; else Color = [1,0,0]; end
  set(UD.GUI.Panel,'BackgroundColor',Color)
    

function R_CBF_setYLim(obj,event)
global RG
UD = get(gcf,'UserData');
YLim = str2num(get(obj,'String'));
if isempty(YLim) fprintf('WARNING : Invalid sparse step specified!'); return; end
set(UD.AH,'YLim',YLim);

function R_CBF_changeReference(obj,event,State)
global RG
UD = get(gcf,'UserData');
if nargin<3 State = get(UD.GUI.Reference,'Value'); end
ReferenceChannels = str2num(get(UD.GUI.RefChannels,'String'));
if State
  UD.Reference = ReferenceChannels;
  String = HF_list2colon(UD.Reference);
  set(UD.GUI.RefChannels,'String',String);
else
  UD.Reference = [];
end
set(gcf,'UserData',UD);
R_filterData(UD.FID);
R_CBF_showData(gcf);

function R_addToUD(FIG,Field,Value)
UD = get(FIG,'UserData'); UD.(Field) = Value; set(FIG,'UserData',UD);

function R_CBF_setVisibility(obj,event,Var)
global RG
UD = get(gcf,'UserData');
State = get(obj,'Value');
if  State Value = 'on'; else Value = 'off'; end
UD.(Var) = State;
set(gcf,'UserData',UD);
switch Var
  case 'Spike'; R_showSpike(State); 
  case 'Spectrum'; R_showSpectrum(State); 
  case 'Reference'; R_CBF_changeReference([],[],State);
  otherwise set(UD.([Var(1),'PH']),'Visible',Value);
end
R_CBF_showData(gcf);

function R_CBF_changeWindow(obj,event,Opt);
global RG
FIG = gcf; UD = get(FIG,'UserData');
Interval = UD.Interval; SR = RG.Data.(UD.FID).SR;
WindowSize = UD.WindowSize; StepSize = UD.StepSize;
Ndata = RG.Data.(UD.FID).Ndata; Ldata = Ndata/SR;
switch Opt
  case 'Start'; Interval = [0,WindowSize]; 
  case 'End'; Interval = [Ldata-WindowSize,Ldata];
  case 'Forward'; Interval = Interval + StepSize;
  case 'Back'; Interval = Interval - StepSize;
  case 'Size'; 
    WindowSize = str2num(get(UD.GUI.WindowSize,'String'));
    Interval = Interval(1) + [0,WindowSize];
    set(UD.GUI.StepSize,'String',WindowSize/2);
    UD.StepSize = WindowSize/2;
  case 'XLim';
    Interval = str2num(get(obj,'String'));
    if isempty(Interval) fprintf('WARNING : Invalid interval specified!'); return; end
  case 'Replot'; % don't change
  otherwise fprintf('WARNING : Option not implemented');
end

if Interval(1)<0 Interval = [0,min([WindowSize,Ldata])]; end
if Interval(2)>Ldata Interval = [max([0,Ldata-WindowSize]),Ldata]; end
WindowSize = diff(Interval); 
UD.Interval = Interval; UD.WindowSize = WindowSize;
set(FIG,'UserData',UD); set(UD.GUI.XLim,'String',[n2s(UD.Interval(1)),' ',n2s(UD.Interval(2))]);
R_CBF_showData(FIG,Interval);

function Sparse = R_compSparse(UD)
global RG
PointsDisplayed = RG.Data.(UD.FID).SR*diff(UD.Interval);
FPos = get(gcf,'Position'); AxPos = get(UD.AH(1),'Position'); 
PointsAvail = FPos(3)*AxPos(3);
Sparse = 1;  %min([5,max([1,ceil(PointsDisplayed/(10*PointsAvail))])]);

function Sparse = R_CBF_setSparse(obj,event);
global RG
FIG = gcf; UD = get(FIG,'UserData');
String = get(obj,'String'); 
if strcmp('auto',String)
  Sparse = R_compSparse(UD);
  set(obj,'ToolTip',num2str(Sparse));
else 
  Sparse = str2num(String);
end
if isempty(Sparse) fprintf('WARNING : Invalid sparse step specified!'); return; end

UD.Sparse = Sparse; set(FIG,'UserData',UD);
R_CBF_showData(FIG,[],Sparse);

function R_CBF_StepSize(obj,event);
global RG
FIG=gcf; UD = get(FIG,'UserData');
UD.StepSize = str2num(get(obj,'String'));
set(FIG,'UserData',UD);

function R_CBF_showData(FIG,Interval,Sparse,Index)
global RG
UD = get(FIG,'UserData'); NPlot = numel(UD.AH);
if ~exist('Interval','var') | isempty(Interval) Interval = UD.Interval; end
if ~exist('Sparse','var') | isempty(Sparse) Sparse = R_CBF_setSparse(UD.GUI.Sparse); end
if ~exist('Index','var') | isempty(Sparse) Indices = [1:NPlot];  else Indices = Index; end

SR = RG.Data.(UD.FID).SR; Ndata = RG.Data.(UD.FID).Ndata;

Inds = [max([1,round(Interval(1)*SR)]) : min([round(Interval(2)*SR),Ndata])];
Inds = Inds(1:Sparse:end);

% COMPUTE SPECTRUM
if UD.Spectrum
  F = abs(fft(RG.Data.(UD.FID).RawRef(Inds,:),RG.Disp.NFFT));
  F = F(1:UD.SpecSteps,:); F(1,:) = 0; % delete constant offset
  F = F./max(F(:)); 
end

% TRIGGER SPIKES
Spikes = zeros(UD.SpikeSteps,RG.Disp.NSpikes,NPlot);
if UD.Spike
  for i=Indices
    SP = [ ];
    if UD.AutoThresh.State  % AUTO THRESHOLD?
      UD.AutoThresholds = ...
        -4*std(RG.Data.(UD.FID).Trace(Inds,:));
      UD.Thresholds(UD.AutoThreshBool) = ...
        UD.AutoThresholds(UD.AutoThreshBool);
    end
    if UD.Thresholds(i)>0
      Ind = find(RG.Data.(UD.FID).Trace(Inds,i)>UD.Thresholds(i));
    else
      Ind = find(RG.Data.(UD.FID).Trace(Inds,i)<UD.Thresholds(i));
    end
    if ~isempty(Ind) % IF SPIKES FOUND
      dInd = diff(Ind);  Ind2 = find(dInd>UD.ISISteps);
      SP = [Ind(1),Ind(Ind2+1)'];
      SP = SP(logical((SP>UD.PreSteps).*(SP<(size(RG.Data.(UD.FID).Trace(Inds,i),1)-UD.PostSteps))));
      Ind = [1:min([length(SP),RG.Disp.NSpikes])];
      SPInd = bsxfun(@plus,UD.SpikeInd(:,Ind),SP(Ind)-UD.PreSteps);
      Spikes(:,Ind,i) = reshape(RG.Data.(UD.FID).Trace(Inds(SPInd(:)),i),UD.SpikeSteps,length(Ind));
    end
    if RG.P.ShowInfo
      set(UD.FR(i),'String',[sprintf('%5.1f Hz',length(SP)/(size(RG.Data.(UD.FID).Trace,1)/RG.DAQ.SR))]);
    end
  end
end

% PLOT DATA
TickInds = round(linspace(1,length(Inds),5));
set(UD.AH,'XLim',Inds([1,end])/SR,'XTick',[]);
set(UD.AH(UD.AxisShow),'XTick',[Inds(TickInds)]/SR);
for i=Indices
  if UD.Raw set(UD.RPH(i),'xdata',Inds/SR,'ydata',RG.Data.(UD.FID).RawRef(Inds,i)); end
  if UD.Trace 
    set(UD.TPH(i),'xdata',Inds/SR,'ydata',RG.Data.(UD.FID).Trace(Inds,i)); 
    if RG.P.ShowInfo
      set(UD.STH(i),'String',sprintf('%1.2e V',std(RG.Data.(UD.FID).Trace(Inds,i))));
    end
  end
  if UD.LFP set(UD.LPH(i),'xdata',Inds/SR,'ydata',RG.Data.(UD.FID).LFP(Inds,i)); end
  if UD.Spectrum  set(UD.FPH(i),'YData',F(:,i)); end
  if UD.Spike
    for j=1:size(Spikes,2) set(UD.SPH(i,j),'YData',Spikes(:,j,i)); end
    set(UD.ThPH(i),'YData',[UD.Thresholds(i),UD.Thresholds(i)]);
  end
end

function R_prepareDisplay(FIG)
% REPLOT THE DATA BY REPLACING THE X/Y-DATA OF THE PLOT
global RG Verbose
UD = get(FIG,'UserData');

Opts = {'ALimMode','manual','CLimMode','manual','FontSize',RG.Disp.AxisSize};
NPlot = UD.NChannels;
UD.AutoThresh.State = RG.Disp.Autothresh.State;

% CHECK FOR CONSISTENT CHANNELLAYOUT
if isfield(UD,'ChannelsXY')
  ZerosFound = any(UD.ChannelsXY(:)==0);
  DoublePos = size(unique(UD.ChannelsXY,'rows'),1) ~= size(UD.ChannelsXY,1);
  BadLength = size(UD.ChannelsXY,1) ~= NPlot;
  UseAutomaticXY = ZerosFound | DoublePos | BadLength;
else UseAutomaticXY = 1;
end

% USER HAS NOT SET POSITIONS (PROPERLY) 
if UseAutomaticXY 
  if Verbose fprintf(['\nWARNING : Assigning channels not based on user input!\n']); end
  cInd = [1:NPlot]; UD.ChannelsXY = zeros(NPlot,2);
  UD.ChannelsXY(cInd,2) = modnonzero(cInd,UD.Tiling(1)); % set iX
  UD.ChannelsXY(cInd,1) = ceil(cInd/UD.Tiling(1)); % set iY
end

DCAll = axesDivide(UD.Tiling(1),UD.Tiling(2),[0.02,0.1,0.96,0.88],0.2,0.3);
DCAll = DCAll';

DC = cell(NPlot,1);
for i=1:NPlot DC{i} = DCAll{UD.ChannelsXY(i,2),UD.ChannelsXY(i,1)};  end
UD.DC.Trace = DC;

UD.AH = zeros(NPlot,1);
UD.RPH = zeros(NPlot,1); UD.ZPH = zeros(NPlot,1); 
UD.TPH = zeros(NPlot,1); UD.LPH = zeros(NPlot,1); 
UD.STH = zeros(NPlot,1);
Ts = [0:1000]/RG.Data.(['F',n2s(FIG)]).SR; Init = zeros(size(Ts));

%% PREPARE SPECTRUM DISPLAY
UD.DC.Spectrum = DC;
UD.FAH = zeros(NPlot,1);
UD.FPH = zeros(NPlot,1);
UD.SpecSteps = RG.Disp.NFFT/2^4;
Fs = RG.DAQ.SR/2*linspace(0,1/2^3,UD.SpecSteps);
UD.SpecInit = zeros(UD.SpecSteps,1);
UD.AxesAlterInd = logical(zeros(1,NPlot));

%% PREPARE SPIKE DISPLAY
UD.DC.Spike = DC;
UD.SAH = zeros(NPlot,1);
UD.ISISteps = round(RG.Disp.ISIDur*RG.DAQ.SR);
UD.PreSteps = round(RG.Disp.PreDur*RG.DAQ.SR);
UD.PostSteps = round(RG.Disp.PostDur*RG.DAQ.SR);
UD.SpikeSteps = UD.PreSteps+UD.PostSteps+1;
UD.SpikeDur = UD.SpikeSteps/RG.DAQ.SR;
UD.ThPH = zeros(NPlot,1);
UD.FR = [];
SpikeTime = [-UD.PreSteps:UD.PostSteps]/RG.DAQ.SR*1000; % in ms
UD.SpikeInit = zeros(length(SpikeTime),RG.Disp.NSpikes);
UD.SpikeInd = repmat([0:length(SpikeTime)-1]',1,RG.Disp.NSpikes);
UD.Thresholds =UD.YLim(2)*ones(1,NPlot)/2;
UD.AutoThreshBool = logical(ones(NPlot,1));
UD.AutoThreshBoolSave = UD.AutoThreshBool;

% START PLOTTING
for i = 1:NPlot
  iX = UD.ChannelsXY(i,1);
  iY = UD.ChannelsXY(i,2);
  switch RG.P.ColorStyle
    case 'Alternate';
      UD.AxesAlterInd(i) = mod(iX+iY,2);
    case 'Tetrode';
      UD.AxesAlterInd(i) = mod(ceil(RG.P.Electrodes(i)/4),2);
  end
  
  UD.AH(i) = axes('Position',UD.DC.Trace{i}); hold on;
  UD.ZPH(i) = plot([0,1e6],[0,0],'Color',[.7,.7,.7]);
  UD.RPH(i) = plot(Ts,Init,'Color',RG.Colors.Raw,...
    'LineWidth',0.5,'HitTest','off','Visible',HF_BoolToOnOff(RG.Disp.Raw))';
  UD.TPH(i) = plot(Ts,Init,'Color',RG.Colors.Trace,...
    'LineWidth',0.5,'HitTest','off','Visible',HF_BoolToOnOff(RG.Disp.Trace))';
  UD.LPH(i) = plot(Ts,Init,'Color',RG.Colors.LFP,...
    'LineWidth',0.5,'HitTest','off','Visible',HF_BoolToOnOff(RG.Disp.LFP))';
  set(UD.AH(i),'ButtonDownFcn',{@R_CBF_axisClick,i});
  String = ['El ',sprintf('%d',RG.P.Electrodes(i)),...
    ' Ch ',sprintf('%d',RG.P.Channels(i)),''];
  text(1,.9,String,'Units','n','Horiz','r','FontSize',8);
  
  % SPECTRUM PLOTTING
  UD.FAH(i) = axes('Position',UD.DC.Spectrum{i}); hold on;
  UD.FPH(i) = plot(Fs,UD.SpecInit,'Color',RG.Colors.Spectrum,'LineWidth',0.5,'HitTest','off');
  
  % SPIKE PLOTTING
  UD.SAH(i) = axes('Position',UD.DC.Spike{i}); hold on;
  UD.SPH(i,:) = plot(SpikeTime,UD.SpikeInit,'Color',RG.Colors.Trace,'LineWidth',0.5,'HitTest','Off')';
  UD.ThPH(i) = plot(SpikeTime([1,end]),[UD.Thresholds(i),UD.Thresholds(i)],'Color',RG.Colors.Threshold);
  set(UD.SAH(i),'ButtonDownFcn',{@R_CBF_axisClick,i});
  if RG.P.ShowInfo
    UD.FR(i) = text(1,0.9,'0 Hz','Units','n','Horiz','r','FontSize',8);
    UD.STH(i) = text(.02,.9,['0 V'],'Units','n','Horiz','l','FontSize',8);
  end
  if iX<max(UD.ChannelsXY(:,1)) | iY>1 
    set([UD.SAH(i),UD.AH(i)],'XTick',[],'YTick',[]); 
  else
    UD.AxisShow = i;
  end
end
set(UD.AH,'FontSize',RG.Disp.AxisSize,'XLim',Ts([1,end]),'Ylim',UD.YLim);
set([UD.AH(UD.AxesAlterInd),UD.SAH(UD.AxesAlterInd),...
  UD.FAH(UD.AxesAlterInd)],'Color',[1,1,.7]);
set(UD.SAH,Opts{:},'XLim',SpikeTime([1,end]),'Ylim',1.01*UD.YLim,'YTick',[]);
set(UD.FAH,Opts{:},'XLim',Fs([1,end]),'Ylim',[0,1]);

set(FIG,'UserData',UD);
R_showSpike(UD.Spike);
R_showSpectrum(UD.Spectrum);

function R_CBF_axisClick(obj,event,Index)
% ZOOM IN AND OUT BASED ON CLICKING ON THE YAXIS
% ALSO SELECT THRESHOLD FOR SPIKES
global RG Verbose

D = get(obj,'CurrentPoint');
SelType = get(gcf, 'SelectionType');
switch SelType 
  case {'normal'}; button = 1; % left
  case {'alt'}; button = 2; % right
  case {'extend'}; button = 3; % middle
  case {'open'}; button = 4; % with shift
 otherwise error('Invalid mouse selection.')
end
switch button
  case  1 % Change Scale
    cYLim = get(obj,'YLim');
    if D(1,2) > (cYLim(1)+cYLim(2))/2
      set(obj,'YLim',0.5*cYLim); % Zoom out
    else
      set(obj,'YLim',2*cYLim); % Zoom in
    end
  case 2 % Set Threshold
    UD = get(gcf,'UserData');
    UD.Thresholds(Index) = D(1,2);
    UD.AutoThreshBool(Index) = logical(0);
    set(gcf,'UserData',UD);
    R_CBF_showData(gcf,[],[],Index);
    
  case 3 % Set Scale to match data
    UD = get(gcf,'UserData');
    if UD.Raw             PH = UD.RPH(Index,:);
    elseif UD.LFP      PH = UD.LPH(Index,:);
    elseif UD.Trace   PH = UD.TPH(Index,:);
    end
    Data = get(PH,'YData');
    cYLim = zeros(1,2);
    cYLim(1) = -max(abs(Data(:)));
    cYLim(2) = -cYLim(1);
    set(obj,'YLim',cYLim); % Zoom in
end

function R_CBF_axisWheel(obj,event,Index)
% ZOOM IN AND OUT BASED ON CLICKING ON THE YAXIS
% ALSO SELECT THRESHOLD FOR SPIKES
global RG Verbose
UD = get(gcf,'UserData');
D = get(obj,'CurrentPoint');
YLims = cell2matObj(get([UD.AH,UD.SAH],'YLim'));
 YLims = YLims(:,2); 
 [b,m,n] = unique(YLims);
 H = histc(n,[.5:1:length(b)+.5]);
[MAX,Ind] = max(H); YLim = YLims(Ind);
set([UD.AH,UD.SAH],'YLim',2^(event.VerticalScrollCount/4)*[-YLim,YLim]);

function R_showSpike(State)
% This file is part of MANTA licensed under the GPL. See MANTA.m for details.
global MG Verbose 
UD = get(gcf,'UserData');

R_rearrangePlots;

if State Setting = 'on'; else Setting = 'off'; end
if prod(double(isfield(UD,{'SAH','SPH','FR','ThPH'})))
  H = [UD.SAH(:);UD.SPH(:);UD.FR(:);UD.ThPH(:)];
  set(H,'Visible',Setting);
end

if State
  UD.Trace = 1; set(UD.GUI.Trace,'Value',1);
  try, set(UD.TPH,'Visible',Setting); catch end
end

function R_showSpectrum(State)
% This file is part of MANTA licensed under the GPL. See MANTA.m for details.
global MG Verbose 
UD = get(gcf,'UserData');

R_rearrangePlots;

if State Setting = 'on'; else Setting = 'off'; end
if prod(double(isfield(UD,{'FAH'})))
  H = [UD.FAH(:),UD.FPH(:)];
  set(H,'Visible',Setting);
end

function R_rearrangePlots
global RG Verbose
UD = get(gcf,'UserData');
Slack = 1.1;
for i=1:numel(UD.AH) % ITERATE OVER PLOTS
  % COMPUTE NEW POSITION OF TRACE PLOT
  TracePos = UD.DC.Trace{i};
  SpikePos = UD.DC.Spike{i};
  SpecPos = UD.DC.Spectrum{i};
    
  if UD.Spike % SCALE TRACE & SPIKE IN X DIRECTION
    dX = RG.Disp.SpikeFrac*TracePos(3);
    TracePos(3)= TracePos(3) - Slack*dX;
    SpikePos(1)= SpikePos(1) + SpikePos(3) - dX/Slack;
    SpikePos(3)= dX/Slack;
  end
  
  if UD.Spectrum % SCALE SPECTRUM & TRACE IN Y DIRECTION
    dY = RG.Disp.SpecFrac*TracePos(4);
    TracePos(2) = TracePos(2) + Slack*dY;
    SpikePos(2) = SpikePos(2) + Slack*dY;
    TracePos(4) = TracePos(4) - dY/Slack;
    SpikePos(4) = SpikePos(4) - dY/Slack;
    SpecPos(4) = dY/Slack^2;
  end
      
  % REPOSITION AXES
  set(UD.AH(i),'Pos',TracePos);
  set(UD.SAH(i),'Pos',SpikePos);
  set(UD.FAH(i),'Pos',SpecPos);
end

function R_loadData(BaseName,Channels,FIG,FID,Range)
global RG
tic;  fprintf('Loading Data : ');
RG.Data.(FID).Raw = single([]);
Wbefore = whos('RG');

[BasePath,Name] = fileparts(BaseName);
Dirs = dir([BaseName,'*']);
cDir = [BasePath,filesep,Dirs(1).name,filesep];
Files = dir([cDir,'*_CH*.continuous']);
Pos  = find(Files(1).name=='_');
PreFix = Files(1).name(1:Pos-1);

if ischar(Channels) 
  switch Channels
    case 'all'; Channels = 1:length(Files);
  end
end

for iE = 1:length(Channels)
  cElectrode = Channels(iE); 
  if ~mod(iE,10) fprintf('%d',cElectrode); else fprintf('.'); end
  cFile = [cDir,PreFix,'_CH',num2str(cElectrode)];
  if exist([cFile,'.continuous'],'file') 
    cFileName = [cFile,'.continuous'];
  else
    cFiles = dir([cFile,'_*.continuous']);
    if length(cFiles) == 1
      cFileName = [cDir,cFiles.name];
    else 
      error('File does not exist, or multiple files exist.');
    end
  end
  tmp =  loadOEP(cFileName);
  if ~isempty(Range)
    SR = 30000;
    RangeInd = [round(Range(1)*SR):round(Range(2)*SR)] + 1;
    tmp = tmp(RangeInd);
  end
  [RG.Data.(FID).Raw(:,iE)] = single(tmp);
  %[RG.Data.(FID).Raw(:,iE)] = load_open_ephys_data(cFileName);
end
RG.Data.(FID).Range = max(abs(RG.Data.(FID).Raw(:)));
RG.Data.(FID).Ndata = size(RG.Data.(FID).Raw,1);
cFile = dir(cFileName);
RG.Data.(FID).Date = cFile.date;
Wafter = whos('RG');
Time = toc;
MB = (Wafter.bytes-Wbefore.bytes)/1024.^2/2; % /2 since int16 is loaded
fprintf(['  [ OK  (',sprintf('%3.0f MB, %3.1f s, %3.1f MB/s',MB,Time,MB/Time),') ]\n']);

function R_filterData(FID)
global RG
UD = get(gcf,'UserData');
% COMPUTE FILTERS
Vars = {'Trace','LFP'};
for i=1:length(Vars)
  if RG.Disp.Filter.(Vars{i}).Highpass >0
    [RG.Disp.Filter.(Vars{i}).b,RG.Disp.Filter.(Vars{i}).a] = ...
      butter(RG.Disp.Filter.(Vars{i}).Order,...
      min([RG.Disp.Filter.(Vars{i}).Highpass,RG.Disp.Filter.(Vars{i}).Lowpass]/(RG.DAQ.SR/2),0.99));
  else
    [RG.Disp.Filter.(Vars{i}).b,RG.Disp.Filter.(Vars{i}).a] = ...
      butter(RG.Disp.Filter.(Vars{i}).Order,...
      min([RG.Disp.Filter.(Vars{i}).Lowpass]/(RG.DAQ.SR/2),0.99),'low');
  end
end

% SUBTRACT COMMON MEDIAN OVER A SET OF REFERENCE CHANNELS
if  ~isempty(UD.Reference)
  for i=1:length(UD.Reference)
    cInd(i) = find(UD.Reference(i)==RG.P.Electrodes);
  end
  RG.Data.(FID).RawRef = RG.Data.(FID).Raw-repmat(mean(RG.Data.(FID).Raw(:,cInd),2),1,size(RG.Data.(FID).Raw,2));
else 
  RG.Data.(FID).RawRef = RG.Data.(FID).Raw;
end

% FIRST FILTER THE LINE NOISE
%RG.Data.(FID).Raw = single(filter(RG.Disp.Filter.Humbug.b,RG.Disp.Filter.Humbug.a,RG.Data.(FID).Raw));
%RG.Data.(FID).Raw = single(filter(RG.Disp.Filter.Stop300.b,RG.Disp.Filter.Stop300.a,RG.Data.(FID).Raw));

% FILTER DIFFERENT SIGNALS
RG.Data.(FID).Trace = filter(RG.Disp.Filter.Trace.b,RG.Disp.Filter.Trace.a,RG.Data.(FID).RawRef);
RG.Data.(FID).LFP = filter(RG.Disp.Filter.LFP.b,RG.Disp.Filter.LFP.a,RG.Data.(FID).RawRef);

% GUI HELPERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h = LF_addCheckbox(Panel,Pos,Val,CBF,Tooltip,Tag,Color);
global RG Verbose
if ~exist('Color','var') | isempty(Color) Color = RG.Colors.Panel; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist ('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
h=uicontrol('Parent',Panel,'Style','checkbox',...
  'Value',Val,'Callback',CBF,'Units','normalized',...
  'Tag',Tag,'Position',Pos,'BackGroundColor',Color,'Tooltip',Tooltip);

function h = LF_addDropdown(Panel,Pos,Strings,Val,CBF,UD,Tooltip,Tag,Color);
global RG
if ~exist('Color','var') | isempty(Color) Color = [1,1,1]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
if ~exist('UD','var') | isempty(UD) UD = []; end
h=uicontrol('Parent',Panel,'Style','popupmenu',...
  'Val',Val,'String',Strings,'FontName',RG.GUI.FontName,...
  'Callback',CBF,'Units','normalized',...
  'Tag',Tag,'Position',Pos,'BackGroundColor',Color,'TooltipString',Tooltip);
set(h,'UserData',UD)

function h = LF_addEdit(Panel,Pos,String,CBF,Tooltip,Tag,Color);
global RG
if ~exist('Color','var') | isempty(Color) Color = [1,1,1]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
if ~isstr(String) String = n2s(String); end
h=uicontrol('Parent',Panel,'Style','edit',...
  'String',String,'FontName',RG.GUI.FontName,...
  'Callback',CBF,'Units','normalized','Horiz','center',...
  'Tag',Tag,'Position',Pos,'BackGroundColor',Color,'ToolTipString',Tooltip);

function h = LF_addPanel(Parent,Title,TitleSize,TitleColor,Color,Position,TitlePos)
global RG
if ~exist('TitlePosition','var') TitlePos = 'centertop'; end
h = uipanel('Parent',Parent,'Title',Title,...
  'FontSize',TitleSize,'FontName',RG.GUI.FontName,...
  'ForeGroundColor',TitleColor,'TitlePosition',TitlePos,'BackGroundColor',Color,...
  'Units','normalized','Position',Position,'BorderType','line');

function h = LF_addPushbutton(Panel,Pos,String,CBF,Tooltip,Tag,FGColor,BGColor);
global RG Verbose
if ~exist('BGColor','var') | isempty(BGColor) BGColor = [1,1,1]; end
if ~exist('FGColor','var') | isempty(FGColor) FGColor = [0,0,0]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
h=uicontrol('Parent',Panel,'Style','pushbutton',...
  'String',String,'FontName',RG.GUI.FontName,...
  'Callback',CBF,'Units','normalized','Position',Pos,'Tag',Tag,...
  'ToolTipString',Tooltip,'ForegroundColor',FGColor,'BackGroundColor',BGColor);

function h = LF_addText(Panel,Pos,String,Tooltip,Tag,FGColor,BGColor);
global RG
if ~exist('BGColor','var') | isempty(BGColor) BGColor = get(Panel,'BackGroundColor'); end
if ~exist('FGColor','var') | isempty(FGColor) FGColor = [1,1,1]; end
if ~exist('Tag','var') | isempty(Tag) Tag = ''; end
if ~exist('CBF','var') | isempty(CBF) CBF=''; end
if ~exist('Tooltip','var') | isempty(Tooltip) Tooltip = ''; end
h=uicontrol('Parent',Panel,'Style','text',...
  'String',String,'FontName',RG.GUI.FontName,...
  'Units','normalized','Position',Pos,...
  'Tag',Tag,'ToolTipString',Tooltip,'HorizontalAlignment','left',...
  'ForegroundColor',FGColor,'BackGroundColor',BGColor);

% GENERAL HELPERS
function field = R_fieldMasker(field)
field(find(field=='.')) = '_';

function [Dirs,DirNames] = HF_getDirs(Path,Exclude)
Files = dir(Path); 
Dirs = Files([Files.isdir]); DirNames = {Dirs.name};
RealInd = ~(strcmp('.',DirNames) | strcmp('..',DirNames) );
if exist('Exclude','var')
  RealInd = RealInd & cellfun(@isempty,strfind(DirNames,Exclude));
end
Dirs =  Dirs(RealInd); DirNames = DirNames(RealInd);

function Sep = HF_getSep;
if strfind('PCWIN',computer) Sep = '\'; else Sep = '/'; end

function String = HF_BoolToOnOff(Bool)
if Bool String='on'; else String='off'; end 

function R_Defaults
global RG;

RG.Mode = 'Local';
RG.Reload = 0;
RG.DAQ.SR = 30000;
RG.DAQ.InputRange = [-10,10];
RG.Colors.Font = [1,1,1];
RG.Colors.Back = [0,0,.8];
RG.Colors.Panel = [0,0,1];
RG.Colors.Raw = [0,0,0];
RG.Colors.Trace = [0,0,1];
RG.Colors.LFP = [1,0,0];
RG.Colors.Threshold = [1,0,0];
RG.Colors.Spectrum = [0,0,1];
RG.Disp.AxisSize = 8;
RG.Disp.SegDur = 0.5;
RG.Disp.Raw = 0;
RG.Disp.Trace = 1;
RG.Disp.LFP = 0;
RG.Disp.Reference = 1;
RG.Disp.Spike = 1;
RG.Disp.Spectrum = 0;
RG.Disp.Autothresh.State = 1;
RG.GUI.MenuOffset = 45;
RG.GUI.FontName = 'Arial';
RG.GUI.FIG = 200000;
% HUMBUG FILTER
% designed using fda tool and exportet as SOS/G, converted using sos2tf
RG.Disp.Filter.Humbug.b = [0.997995527211068  -5.987297083916456  14.967228743433322 ...
  -19.955854373444378  14.967228743433322  -5.987297083916456   0.997995527211068];
RG.Disp.Filter.Humbug.a = [1.000000000000000  -5.995310048314492  14.977237236960848 ...
  -19.955846338529373  14.957216231994666  -5.979292154433458   0.995995072333299];
%[RG.Disp.Filter.Stop300.b,RG.Disp.Filter.Stop300.a] =
%butter(4,[200,400]/12500,'stop');
RG.Disp.Filter.Raw.Lowpass = inf;
RG.Disp.Filter.Raw.Highpass = -inf;
RG.Disp.Filter.Raw.Order = 0;
RG.Disp.Filter.Trace.Lowpass = 6000;
RG.Disp.Filter.Trace.Highpass = 300;
RG.Disp.Filter.Trace.Order = 2;
RG.Disp.Filter.LFP.Lowpass = 300;
RG.Disp.Filter.LFP.Highpass = 0;
RG.Disp.Filter.LFP.Order = 2;
RG.Disp.NSpikes = 100;
RG.Disp.PreDur = 0.002;
RG.Disp.PostDur = 0.005;
RG.Disp.ISIDur = 0.002;
RG.Disp.NFFT = 1024;
RG.Disp.SpikeFrac = 0.4;
RG.Disp.SpecFrac = 0.35;

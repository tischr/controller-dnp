 function C_computeTuning(varargin)
% GENERALIZE TO MULTIPLE ELECTRODES/CELLS
% ADD SPONTANEOUS RATE 
% ADD CELL INFO, Number of trials, recording info, etc.
% LABEL CORRECTLY
 
% PARSE ARGUMENTS
varargin = C_parseID(varargin);
P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'Electrodes');
checkField(P,'Reference','all');
checkField(P,'Recompute',0);
checkField(P,'FIG',1);

% LOAD RECORDING (NIDAQ & EPHYS)
Resp = C_getResponse('Animal',P.Animal,'Recording',P.Recording,'Electrodes',P.Electrodes,'Reference',P.Reference,'Recompute',P.Recompute);
% LOAD DATA FOR SYNCTIMES (COULD BE MORE EFFICIENT!)
R = C_loadRecording('Animal',P.Animal,'Recording',P.Recording,'Modules',{'NIDAQ','EPhys'},'Electrodes',1,'Reference',P.Reference);

% RECREATE STIMULUS
Trials = R.General.Paradigm.Trials;
NTrials = length(Trials); 
SRAI = R.General.Parameters.Setup.DAQ.SRAI;
SRAO = R.General.Parameters.Setup.DAQ.SRAO;

% COLLECT SPIKES FOR EACH CONDITION
Fs = R.General.Paradigm.Stimulus.V.Frequencies;
As = R.General.Paradigm.Stimulus.V.Amplitudes;
Tuning = zeros([NTrials,length(As),length(Fs)]);
Durations = zeros(size(Tuning));
NStimuli = R.General.Paradigm.Stimulus.V.NStimuli;

for iT = 1:NTrials
  % START AND STOP TIMES RELATIVE TO TRIAL ONSET
  StartTimes = Trials(iT).Stimulus.StartPos/SRAO;
  StopTimes = Trials(iT).Stimulus.StopPos/SRAO;
  cParSequence = Trials(iT).Stimulus.ParSequence;
  % CORRECT FOR STARTING TIME OF TRIAL
  TrialStart = R.EPhys.Data(iT).Sync.SyncTimeRel;
  cST = Resp.Time{1}-TrialStart;
  cST = cST(cST>0);
  cST = cST(cST<StopTimes(end));
  
  for iS = 1:NStimuli
    cF = cParSequence(iS).Freq;     
    cA = cParSequence(iS).Amp;
    iF = cF==Fs;     iA = cA==As;
    cStart = StartTimes(iS);
    cStop = StopTimes(iS);
    NSpikes = sum((cST>cStart) .* (cST<cStop));
    Tuning(iT,iA,iF) = NSpikes;
    cDur = (cStop-cStart);
    Durations(iT,iA,iF) = cDur;
  end
end
TuningM = permute(sum(Tuning)./sum(Durations),[2,3,1]);
TuningSEM = permute(std(Tuning./Durations),[2,3,1])/sqrt(NTrials);

% SHOW TUNING 
if P.FIG
  figure(P.FIG); clf; 
  switch numel(As)
    case 1;
      errorbar(Fs,TuningM,2*TuningSEM,'.-');
      set(gca,'xscale','log','xlim',Fs([1,end]).*[0.9,1.1],'YLim',[0,1.1*max(TuningM+2*TuningSEM)]);
      box off;
    otherwise
      imagesc(Fs,As,TuningM); colorbar;
      set(gca,'YDir','normal')
  end
end
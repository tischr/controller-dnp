function R = C_mapLines(varargin)
  % PROVIDE GENERAL LINE MAPPING TOOL
  % Arguments
  % Interfaces: Cell
  %    Sequence of Interfaces from Electrodes to Display
  %    One has N Connects, and N+1 Interfaces
  %    Each Connect is a map between two interfaces
  %      Hence, a connect is just an assignment between channels
  %    Each Interface has a number of contacts, and a spatial layout
  
  
  P = parsePairs(varargin);
  checkField(P,'Interfaces',{'EIB64to8x2','EIB64','Headstage'});
  checkField(P,'Plot',0)
  
  % LOOK OVER THE SEQUENCE OF INTERFACES
  for iI = 1:length(P.Interfaces)
    LF_addInterface
    
  end

  % GET MAPPING BASED ON RECORDING SYSTEM
  RR = LF_getRecorder(P.Recorder);

  % GET MAPPING BASED ON IMPLANT
  RI = LF_getImplant(P.Implant,P.Geometry);
  NChannels = numel(RI.ElectrodeByPin);
  
  % COMPUTE THE MAPPING FROM INPUT CHANNEL
  for iC=1:NChannels
    cPin = RR.PinByChannel(iC);
    cElectrode = RI.ElectrodeByPin(cPin);
    R.Channels(iC).Electrode = cElectrode;
    cGroup = RI.GroupByElectrode(cElectrode);
    R.Channels(iC).Group = cGroup;
    R.Channels(iC).Pin = cPin;
    cColor = RI.ColorByGroup(cGroup);
    R.Channels(iC).Color = cColor;
    cCPos = RI.CPosByElectrode(cElectrode,:);
    R.Channels(iC).CPos = cCPos;
    cEPos = RI.EPosByGroup(cGroup,:);
    R.Channels(iC).EPos = cEPos;
    R.Electrodes(cElectrode).Channel = iC;
    R.Electrodes(cElectrode).Group = cGroup;
    R.Electrodes(cElectrode).Color = cColor;
    R.Electrodes(cElectrode).Pin = cPin;
  end
  
  if P.Plot LF_plotArray(R); end
  
end

%% GET IMPLANT
function R =  LF_getImplant(Implant,Geometry)
  
  switch Implant
    case 'EIB64';
      ElectrodeByPin = [...
        7,9,11,14,15,17,19,20,21,23,24,25,26,29,30,32;...
        1,2,3,4,5,6,8,10,12,13,16,18,22,27,28,31;...
        62,60,59,54,50,48,45,44,41,39,38,37,36,35,34,33;...
        64,63,61,58,57,56,55,53,52,51,49,47,46,43,42,40]';
      NElectrodes = numel(ElectrodeByPin);
      CPosA = (-i)^2*exp(-linspace(0.05,0.45,NElectrodes/2).*2*pi*i);
      CPosA = [real(CPosA)',imag(CPosA)'];
      CPosB =  (-i)^2*exp(-linspace(0.55,0.95,NElectrodes/2).*2*pi*i);
      CPosB = [real(CPosB)',imag(CPosB)'];
      CPosByElectrode = [CPosA;CPosB];
      switch Geometry
        case '8x2';
          XPosByGroup = [-0.875:0.25:0.875,-0.875:0.25:0.875 ]';
          YPosByGroup = repmat([-0.125,0.125],8,1);
          EPosByGroup = [XPosByGroup(:),YPosByGroup(:)];
        otherwise error(['Geometry ',Geometry,' not known!']);
      end
      GroupByElectrode = reshape(repmat([1:16],4,1),64,[]);
      ColorByGroup = repmat(['r','g','b','y']',1,4);
    otherwise error(['Recorder ',Implant,' not known!']);
  end
  
  for iC=1:numel(ElectrodeByPin);
    PinByElectrode(iC) = find(ElectrodeByPin==iC);
  end
  
  R.ElectrodeByPin = ElectrodeByPin;
  R.PinByElectrode = PinByElectrode;
  
  R.GroupByElectrode = GroupByElectrode;
  R.CPosByElectrode = CPosByElectrode;
  R.EPosByGroup = EPosByGroup;
  R.ColorByGroup = ColorByGroup(:);
end


%% GET RECORDER 
function R = LF_getRecorder(Recorder)
  % Mapping for all omnetics connectors starts from the right, looking into the headstage contacts
  
  switch Recorder
    case 'Intan64';
      % MAPPING GOES TO THE TOP OMNETICS CONNECTOR (side where the black object is located)
      % 16 . . . . . . . . . . . . . . . 01
      % 32 . . . . . . . . . . . . . . . 17
      % 48 . . . . . . . . . . . . . . . 33
      % 64 . . . . . . . . . . . . . . . 49
      
      % COLUMNS ARE THE CONNECTOR ROWS, LINEAR INDEXING PROVIDES THE 'ByPin' Property
      ChannelByPin = [16:2:46; 17:2:47; 15:-2:1,63:-2:49;14:-2:0,62:-2:48]' + 1;
      
    case 'NanoZ';
      % 20 . . . . . . . . . . . . . . . 01
      % 40 . . . . . . . . . . . . . . . 17
      
      % NOT TESTED
      warning('Placeholder inserted, needs to be tested');
      ChannelByPin = [1:16,21:36];
      
    otherwise error(['Recorder ',Recorder,' not known!']);
  end
  
  for i=1:numel(ChannelByPin)
    PinByChannel(i) = find(ChannelByPin==i);
  end
  
  R.ChannelByPin = ChannelByPin;
  R.PinByChannel = PinByChannel;
end

%% PLOT ARRAY
function LF_plotArray(R)
  figure(1); clf; DC = HF_axesDivide(1,1,[0.1,0.1,0.85,0.8],[0.4],[]);
  
  axes('Pos',DC{1}); hold on;
  for iC=1:length(R.Channels)
    
    plot3(R.Channels(iC).CPos(1),R.Channels(iC).CPos(2),0,'.','Color',R.Channels(iC).Color,'MarkerSize',20);
    text(R.Channels(iC).CPos(1),R.Channels(iC).CPos(2)+0.05,0.1,['C',num2str(iC),' P',num2str(R.Channels(iC).Pin),' E',num2str(R.Channels(iC).Electrode)],'Rotation',90);
    plot3(R.Channels(iC).EPos(1),R.Channels(iC).EPos(2),-1,'.','Color',R.Channels(iC).Color,'MarkerSize',20);
    text(R.Channels(iC).EPos(1),R.Channels(iC).EPos(2)+0.05,-0.9,['C',num2str(iC),' P',num2str(R.Channels(iC).Pin),' E',num2str(R.Channels(iC).Electrode)],'Rotation',90);
  end
  axis([-1.1,1.4,-1,1]);
end
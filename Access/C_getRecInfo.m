function I = C_getRecInfo(varargin)
% Provides General Information on a Recording
% gathered from various sources

P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');

% FIND THE BASE PATH (COMPARE TO C_initialize)
[RecordingPath,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);

% LOAD BASIC INFORMATION
Query = ['SELECT * FROM recordings WHERE animal=''',P.Animal,''' AND recording=',num2str(P.Recording),'']; 
R = mysql(Query);

I.Animal = P.Animal;
I.Recording = P.Recording;
I.Identifier = [P.Animal,'_R',num2str(I.Recording)];
I.Trials = R.trials;
I.Paradigm = R.paradigm;
I.Setup = R.setup;
I.Date = R.date;
I.EPhys = R.ephys;
I.RecID = R.id;

% ASSIGN RECORDING PROPERTIES
if I.EPhys
  I.Implant.Depth = R.depth;
  % Add Implant to Database for each subject
  % Query = ['SELECT * FROM subjects WHERE animal="',P.Animal,'"']; 
  % R = mysql(Query);
  I.Implant.Geometry = '4x4';
  I.Implant.Name = 'EIB64';
  I.Implant.Recorder = 'Intan64';
  I.Implant.Info = C_mapChannel('Implant',I.Implant.Name,...
    'Recorder',I.Implant.Recorder,'Geometry',I.Implant.Geometry);
end

% DEFINE THE NAME OF THE RESPONSE FILE
ResponseSubPath = ['Results',filesep,'Response',filesep];
I.Local.Response.Path = [Paths.Local,ResponseSubPath];
I.Local.Response.File = [I.Local.Response.Path,'Responses'];
I.DB.Response.Path = [Paths.DB,ResponseSubPath];
I.DB.Response.File = [I.DB.Response.Path,'Responses'];


% LOAD THE GENERAL INFORMATION FROM THE RECORDING IF AVAILABLE
if exist(RecordingPath,'file')
  cPath = RecordingPath; IsLocal = 1;
else
  cPath = Paths.DB; IsLocal = 0; C_mountServer;
end

T = load([cPath,'General.mat']); 
General = T.CGSave; clear T;

% FIND ALL MODULES AND THEIR TYPES
Modules = {General.Paradigm.Modules.Name};
Types = {General.Paradigm.Modules.Type};

ModuleDirs = dir(cPath); ModuleDirs = ModuleDirs([ModuleDirs.isdir]);
ModuleDirs = {ModuleDirs.name}; 
ModuleDirs = ModuleDirs(~strcmp(ModuleDirs,'.'));
ModuleDirs = ModuleDirs(~strcmp(ModuleDirs,'..'));
ModuleDirs = ModuleDirs(~strcmp(ModuleDirs,'Results'));
AddModules = setdiff(ModuleDirs,Modules);
for i=1:length(AddModules)
  switch AddModules{i}
    case 'EPhys'; Modules{end+1} =  AddModules{i}; Types{end+1} = 'EPhys';
    otherwise error('New Module found ');
  end
end

% GET PARAMETERS OF THE RECORDING
I.Parameters = General.Parameters;
I.Parameters.Paradigm = General.Paradigm.Parameters;
function [MidDist,SCorr,CorrTime,DeltaTime,CorrMidDist] = VocLocalizer(varargin)
% COLLECT PROPERTIES AND PERFORM INITIAL ANALYSIS OF VOCALIZATIONS  

P = parsePairs(varargin);
checkField(P,'Sounds')
checkField(P,'SR')
checkField(P,'NFFT',250);
checkField(P,'Offset',1);
checkField(P,'HighPass',25000);
checkField(P,'CorrTime',0.0001); % 0.1 ms
checkField(P,'CenterShift',0.009); %10mm of CenterShift based on measured discrepancy between position of left and right microphone
checkField(P,'GainCorrection',1.2);
checkField(P,'LocMethod','Geometric');
checkField(P,'RemoveAmplitude',0);
checkField(P,'CorrMethod','GCC');
checkField(P,'MaxMethod','Max');
checkField(P,'DelayRange',0.0001); %75us
checkField(P,'FreqSelect',1.5);
checkField(P,'FIG',1); % 
checkField(P);

% POTENTIALLY FILTER THE DATA
if P.HighPass
  [b,a]=butter(4,P.HighPass/(P.SR/2),'high');
  for i=1:length(P.Sounds) 
    P.Sounds{i} = filter(b,a,P.Sounds{i}); 
  end
end

if P.RemoveAmplitude
  for i=1:length(P.Sounds)
    H{i} = abs(hilbert(P.Sounds{i}));
    P.Sounds{i} = P.Sounds{i}./H{i}; 
  end
end

if ischar(P.CorrMethod) P.CorrMethod = {P.CorrMethod}; end
CorrSteps = round(P.CorrTime*P.SR);
SCorrFinal = ones(2*CorrSteps+1,1);
for iM = 1:length(P.CorrMethod)
  fprintf(['Estimating Location ( ',P.CorrMethod{iM},' ) : ']);
  switch P.CorrMethod{iM}
    case 'Specgram';
      % COMPUTE SPECTROGRAM OF THE SOUND FROM TWO MICROPHONES
      S1 = HF_specgram(single(P.Sounds{1}),P.NFFT,P.SR,[],P.NFFT-P.Offset,0,0);
      S2 = HF_specgram(single(P.Sounds{2}),P.NFFT,P.SR,[],P.NFFT-P.Offset,0,0);
      
      % SELECT FREQUENCY INDICES TO COMPUTE THE CROSSCORRELATION
      FreqMarginals = mean(abs(S1)+abs(S2),2);
      FreqInds = find( FreqMarginals > P.FreqSelect*median(FreqMarginals));
            
      % COMPUTE CROSSCORRELATIONS
      Corrs= zeros(length(FreqInds),2*CorrSteps+1);
      for iF=1:length(FreqInds)
        cInd = FreqInds(iF);
        Corrs(iF,:) = xcorr(abs(S1(cInd,:)),abs(S2(cInd,:)),CorrSteps,'unbiased');
      end
      Corrs = Corrs./repmat(FreqMarginals(FreqInds),1,size(Corrs,2));
      
      cSCorr  = mean(Corrs)';
      DT = [-CorrSteps:CorrSteps]/P.SR;
      CorrKernel = 0.01*exp(-DT.^2/(2*0.00065^2)) + 1;
      cSCorr = cSCorr / max(cSCorr);
      cSCorr = cSCorr.*CorrKernel';
      
      %SCorr{iM} = abs(hilbert(mean(Corrs)))';
      SCorr{iM} =cSCorr;
      
    case 'GCC';
      P.Sounds{1} = vertical(P.Sounds{1});
      P.Sounds{2} = vertical(P.Sounds{2});
      NFFT = length(P.Sounds{1});
      S1F = fft(double(P.Sounds{1}),NFFT); S2F = fft(double(P.Sounds{2}),NFFT);
      %FLow = 20000; FHigh = 110000;
      F = [1:NFFT]/NFFT*P.SR; F = F(1:round(NFFT/2));
      %ind = (F<FLow).*(F>FHigh);
      %ind = logical([ind,fliplr(ind)]);
      %S1F(ind) = 0; S2(ind) = 0;
      R = S1F.*conj(S2F)./(abs(S1F).*abs(conj(S2F)));
      XCorr = ifft(R);
      SCorr{iM} = abs( hilbert([ XCorr(round(NFFT/2)+1:end) ; XCorr(1:round(NFFT/2))  ] ) );
      SCorr{iM} = SCorr{iM}(round(end/2)+1-CorrSteps:round(end/2)+1+CorrSteps);
      
    case 'IIR';
      P.Sounds{1} = vertical(P.Sounds{1});
      P.Sounds{2} = vertical(P.Sounds{2});
      NFFT = length(P.Sounds{1});
      S1F = fft(double(P.Sounds{1}),NFFT); S2F = fft(double(P.Sounds{2}),NFFT);
      %F = [1:NFFT]/NFFT*P.SR; F = F(1:round(NFFT/2));
      R1 = S2F./S1F;
      R2 = S1F./S2F;
      IIR1 = ifft(R1);
      IIR2 = ifft(R2);
      keyboard

    case 'XCorr';
      SCorr{iM} = xcorr(P.Sounds{1},P.Sounds{2},CorrSteps,'unbiased');
      SCorr{iM} = abs(hilbert(SCorr{iM}));
      
    case 'Micro';
      NSteps = length(P.Sounds{1});
      OversampleFactor = 8;
      % OVERSAMPLE DATA
      for iM=1:2
        Sounds{iM} = interpft(P.Sounds{iM},NSteps*OversampleFactor+1);
      end
      P.SR = P.SR*OversampleFactor;
      
      % REMOVE PHASE
      for i=1:length(Sounds)
        H{i} = abs(hilbert(Sounds{i}));
        Sounds{i} = Sounds{i}./H{i};
      end
      
      % CONSTRUCT HISTOGRAM OF THE PHASE MATCHES
      CorrSteps = round(P.CorrTime*P.SR)/2;
      WindowSteps = P.CorrTime*P.SR;
      NFFT = WindowSteps;
      MaxHist = zeros(2*CorrSteps+1,1);
      Steps = round(linspace(1,NSteps-WindowSteps,100));
      for i=1:length(Steps)
        cStep = Steps(i);
        cInd = cStep+[0:WindowSteps-1];       
        S1 = Sounds{1}(cInd); S2 = Sounds{2}(cInd);        
        X = xcorr(S1,S2,CorrSteps,'biased');
        X(X<max(X)/2) = -inf;
        [Pos,Vals] = findLocalExtrema(X,'max');
        MaxHist(Pos) = MaxHist(Pos) + 1;
      end
      SCorr{iM} = MaxHist;
  end
  
  % COMBINE THE ESTIMATES FROM DIFFERENT METHODS
  SCorrFinal = SCorr{iM}.*SCorrFinal;
end
SCorr = SCorrFinal;

% SMOOTH THE CORRELATION
SCorr = relaxc(SCorr,1);

% SUBSELECT A RANGE FOR CHOOSING THE RIGHT DELAY
CorrTime = [-CorrSteps:CorrSteps]/P.SR;
NotInd = find(abs(CorrTime)>P.DelayRange);
SCorrRange= SCorr;
SCorrRange(NotInd) = -inf;

% EXTRACT CENTER OF CORRELATION
switch P.MaxMethod
  case 'Weighted';
    Pos = sum([1:length(SCorrRange)]'.*SCorrRange./(sum(SCorrRange)));
  case 'Max';
    [M,Pos] = max(SCorrRange);
  otherwise error('Maximum Method not known');
end

DeltaTime = real((Pos-(CorrSteps+1))/P.SR); % DeltatTime is negative, if signal arrives first on the left (channel 1), i.e. position is also on the left

MidDist = LF_translateTime2Space(DeltaTime,P.LocMethod,P.CenterShift,P.GainCorrection);

disp([' ',num2str(MidDist)]);

if abs(imag(MidDist))>0
  MidDist = NaN;
  fprintf('Warning: Out of bounds distance!\n');
end

CorrMidDist = LF_translateTime2Space(CorrTime,P.LocMethod,P.CenterShift,P.GainCorrection);

function MidDist = LF_translateTime2Space(DeltaTime,LocMethod,CenterShift,GainCorrection)

% COMPUTE POSITION FROM TIMING
VSound = 343; %m/s
DeltaDist = VSound * DeltaTime;
switch LocMethod
  case 'Geometric';
    % COMPUTE POSITION BASED ON SETUP GEOMETRY AND CORRELATION
    MicDist = 0.46; %m % Measurements from new setup
    % Estimate of previous distances : 0.38
    MicHeight = 0.314; %m % Measurement of Speaker
    % MicHeight = 0.334; %m % Measurement for Mouse
    % Estimate of previous height: 0.35
    T = DeltaDist;
    H = MicHeight;
    D  = MicDist;
    
    MidDist = 0.5.*T.* sqrt((4*H^2 + D^2 - T.^2)./(D^2-T.^2));   
    MidDist = MidDist - CenterShift;
     
  case 'Empirical';
    MidDist = (DeltaDist - CenterShift)/GainCorrection;
   
end

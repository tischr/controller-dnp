function TestVocLocalizer(varargin)
% TEST VOCALIZATION LOCALIZATION

P = parsePairs(varargin);
checkField(P,'SR',250000)
checkField(P,'Delay',1);
checkField(P,'CorrMethod','GCC');
checkField(P,'FIG',1); % 
checkField(P,'Shifts',[0.0002,0.0002]);
checkField(P,'Duration',0.1);
checkField(P,'DeltaX',0.3);
checkField(P,'FBase',40000);
checkField(P,'Signal2Noise',10);
checkField(P,'NoiseTime',0.01);
checkField(P,'Seed',0);
checkField(P);

if P.Seed
  RR = RandStream('mt19937ar','Seed',P.Seed);
else
  RR = RandStream('mt19937ar');
end

% CREATE TIME SIGNAL
T = [0:1/P.SR:P.Duration];
XUp = linspace(0,P.DeltaX,length(T)/5)';
XDown = linspace(P.DeltaX,0,length(T)/5)';
XMiddle = repmat(P.DeltaX,round([length(T)-2*length(T)/5,1]));
X=[XUp;XMiddle;XDown];
F = P.FBase.*2.^X;
phaseinc = 1./P.SR.*F;
phases = cumsum(phaseinc);
Stimulus = sin(2*pi*phases);
Stimulus = Stimulus / std(Stimulus);

% CREATE TIME SHIFTED SIGNALS WITH NOISE
NMics = 2; Shifts = P.Shifts;
for iM=1:NMics
  NSteps = round(P.SR * (P.Duration + P.NoiseTime));
  Sounds{iM} = zeros(NSteps,1);
  cShiftSteps = round(P.SR*P.Shifts(iM));
  Sounds{iM}(cShiftSteps+1:cShiftSteps + length(Stimulus)) = Stimulus;
  Sounds{iM} = P.Signal2Noise*Sounds{iM} + RR.randn(NSteps,1);
  Time = [1:250]/P.SR;
  IIR{iM} = Time.*exp(-Time/0.00005).*sin(2*pi*(50000+iM*10000)*Time); 
  Sounds{iM} = conv(Sounds{iM},IIR{iM});
end

CorrTime = 0.001;
[MidDist,SCorr,CorrTime,DeltaTime,CorrMidDist] = ...
  VocLocalizer('Sounds',Sounds,'SR',P.SR,'CorrMethod',P.CorrMethod,'CenterShift',0,'CorrTime',CorrTime,'DelayRange',CorrTime);

figure(P.FIG); clf; [DC,AH] = axesDivide(1,[1,1,2],'c');
for iM = 1:NMics
  axes(AH(iM)); plot(Sounds{iM}); axis tight;
end
axes(AH(3));
plot(CorrTime,SCorr);
title(['Method ',P.CorrMethod,' : \DeltaT = ',num2str(DeltaTime*1000),'ms']);
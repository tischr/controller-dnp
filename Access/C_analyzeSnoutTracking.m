function RCam = C_analyzeSnoutTracking(varargin)
% Provides General Information on a Recording
% gathered from various sources

P = parsePairs(varargin);
checkField(P,'Animals');
checkField(P,'Recordings');
checkField(P,'Path','C:\Users\CDL setup\Desktop\AnnotationsSnoutTracking\');
checkField(P,'FIG',2);
checkField(P,'SelectByIntensityDiff',1);
checkField(P,'AudioCorrectionShift',0);
checkField(P,'ColorField','Duration');

figure(P.FIG); clf;
[DC,AH] = axesDivide(2,1,[0.1,0.1,0.8,0.8],[0.4],'c');
XPosAll ={[],[]}; XPosAudioAll = {[],[]};

if ischar(P.Animals) P.Animals = {P.Animals}; end
iK=0; k=0; Sides = {'left','right'};
for iA = 1:length(P.Animals)
  for iR=1:length(P.Recordings{iA})
    fprintf(['Loading Animal ',P.Animals{iA},' Recording ',num2str(P.Recordings{iA}(iR)),' : ']);
    FileNameCam = [P.Path,P.Animals{iA},'_R',num2str(P.Recordings{iA}(iR))];
    tmp = load(FileNameCam);
    iK = iK+1;
    RCam(iK).CurvesByFrame = tmp.CurvesByFrame;
    RCam(iK).SequenceOfInspection = tmp.SequenceOfInspection;
    
    [Path,Paths] = C_getDir('Animal',P.Animals{iA},'Recording',P.Recordings{iA}(iR));
    ResPath = [Paths.DB,'Results'];
    VocPath = [ResPath,filesep,'Vocalizations'];
    FileNameAudio = [VocPath,filesep,'Vocalizations.mat'];
    RAudio = load(FileNameAudio);
    
    %% COLLECT THE CAMERA BASED POSITIONS
    
    % FIND FRAMES WHERE CLICKS WERE RECORDED
    Frames = find(~cellfun(@isempty,RCam(iK).CurvesByFrame));
    NCam=0; 
    for iF=1:length(Frames)
      Clicks = RCam(iK).CurvesByFrame{Frames(iF)};
      NCam = NCam + 1;
      RCam(iK).XPos(NCam,:) = NaN;
      RCam(iK).YPos(NCam,:) = NaN;
      for iT = 1:length(Clicks)
        cSide = Clicks{iT}{4};
        iS = find(strcmp(Sides,cSide));
        RCam(iK).XPos(NCam,iS) = Clicks{iT}{1};
        RCam(iK).YPos(NCam,iS) = Clicks{iT}{2};
        RCam(iK).Time(NCam,1) = Clicks{iT}{5};
        RCam(iK).VocLocation(NCam,1) = Clicks{iT}{6}*1000;
      end
    end
     
    %% COLLECT THE AUDIO BASED POSITIONS
    for iV = 1: length(RAudio.Vocs)
      RAudio.CenterTime(iV) = RAudio.Vocs(iV).Start + RAudio.Vocs(iV).Duration/2 ;
      RAudio.Location(iV) = RAudio.Vocs(iV).Location*1000;
    end
    
    %% FUSE ACROSS POSITIONS
    KeepInd = logical(zeros(1,NCam));
    for iV = 1:NCam
      [MIN,iAudio] = min(abs(RCam(iK).Time(iV) - RAudio.CenterTime));
      if MIN < 0.15 % Corresponding Vocalization found
        RCam(iK).XPosAudio(iV,1) = RAudio.Location(iAudio) + P.AudioCorrectionShift;
        RCam(iK).FRange(iV,1) = RAudio.Vocs(iAudio).FRange;
        RCam(iK).Duration(iV,1) = RAudio.Vocs(iAudio).Duration;
        RCam(iK).Frequency(iV,1) = RAudio.Vocs(iAudio).FMean;
        RCam(iK).MeanEnergy(iV,:) = RAudio.Vocs(iAudio).MeanEnergy;
        RCam(iK).EnergyDiff(iV,1) = (RAudio.Vocs(iAudio).MeanEnergy(2))/(0.74*RAudio.Vocs(iAudio).MeanEnergy(1));
        KeepInd(iV) = 1;
      end
    end
% <<<<<<< HEAD
%     
%     % KEEP SUBSET OF VOCALIZATIONS
%     RCam(iK).XPos = RCam(iK).XPos(KeepInd,:);
%     RCam(iK).YPos = RCam(iK).YPos(KeepInd,:);
%     RCam(iK).XPosAudio = RCam(iK).XPosAudio(KeepInd,:);
%     RCam(iK).Time = RCam(iK).Time(KeepInd,:);
%     RCam(iK).VocLocation = RCam(iK).VocLocation(KeepInd,:);
%     RCam(iK).FRange = RCam(iK).FRange(KeepInd,:);
%     RCam(iK).Duration = RCam(iK).Duration(KeepInd,:);
%     RCam(iK).Frequency = RCam(iK).Frequency(KeepInd,:);
%     RCam(iK).MeanEnergy = RCam(iK).MeanEnergy(KeepInd,:);
%     RCam(iK).EnergyDiff = RCam(iK).EnergyDiff(KeepInd,:);
%     
%     NVoc(iK) = sum(KeepInd);
%     fprintf([' [ ',num2str(NVoc(iK)),' Vocs ] \n']);
%     
%     if sum(KeepInd)
% =======
        
    % PLOT RESULTS
    NCSteps = 256;
    CM = HF_colormap({[1,0,0],[1,0,0],[1,1,1],[0,0,1],[0,0,1]},[-2,-1,0,1,2],NCSteps);
    Sides = {'Left','Right'};
    for i=1:length(Sides);
      axes(AH(i)); hold on; grid on;
      cSide = Sides{i};
      Ind = RCam(iK).([cSide,'Ind']);
      if P.SelectByIntensityDiff
        EnergyDiffs = [RCam(iK).EnergyDiff];
        switch cSide
          case 'Left';    Ind = intersect(Ind,find(EnergyDiffs>1));
          case 'Right';  Ind = intersect(Ind,find(EnergyDiffs<1));
        end
      end
      MSE(i) = sqrt(mean((RCam(iK).XPos(Ind) - RCam(iK).XPosAudio(Ind)).^2));
      if ~isempty(Ind)
        [CC(i),Prob] = corr(RCam(iK).XPos(Ind)',RCam(iK).XPosAudio(Ind)');
      else CC(i) = 0;
      end
      XPosAll{i} = [XPosAll{i},RCam(iK).XPos(Ind)];
      XPosAudioAll{i} = [XPosAudioAll{i},RCam(iK).XPosAudio(Ind)];
      
      MaxVal = max(abs(RCam(iK).(P.ColorField)));

      
      % PLOT RESULTS
      NCSteps = 256;
      CM = HF_colormap({[1,0,0],[1,0,0],[1,1,1],[0,0,1],[0,0,1]},[-2,-1,0,1,2],NCSteps);
      for i=1:length(Sides);
        axes(AH(i)); hold on; grid on;
        cSide = Sides{i};
        if P.SelectByIntensityDiff
          EnergyDiffs = [RCam(iK).EnergyDiff];
          switch cSide
            case 'left';    Ind = find(EnergyDiffs>1); SideInd = 1;
            case 'right';  Ind = find(EnergyDiffs<1); SideInd = 2;
          end
        end
        MSE(i) = sqrt(mean((RCam(iK).XPos(Ind,SideInd) - RCam(iK).XPosAudio(Ind)).^2));
        if ~isempty(Ind)
          [CC(i),Prob] = corr(RCam(iK).XPos(Ind,SideInd),RCam(iK).XPosAudio(Ind));
        else CC(i) = 0;
        end
        XPosAll{i} = [XPosAll{i};RCam(iK).XPos(Ind,SideInd)];
        XPosAudioAll{i} = [XPosAudioAll{i};RCam(iK).XPosAudio(Ind)];
        
        MaxVal = max(abs(RCam(iK).(P.ColorField)));
        
        for iI = 1:length(Ind)
          cTime = RCam(iK).Time(Ind(iI));
          cVal = RCam(iK).(P.ColorField)(Ind(iI));
          switch P.ColorField
            case 'IntensityDiff';
              cVal = cVal/MaxVal;
              cColor  = CM(round(cVal*NCSteps/2 + NCSteps/2),:);
            otherwise cColor = [cVal/MaxVal,0,0];
          end
          plot(RCam(iK).XPos(Ind(iI),SideInd),RCam(iK).XPosAudio(Ind(iI)),'.','Color',cColor,...
            'MarkerSize',16,'ButtonDownFcn',['fprintf([''Recording = ',num2str(P.Recordings{iA}(iR)),',  Time = ',num2str(cTime),'s\n'']);']);
        end
        
        plot([-30,30],[-30,30],'r');
        plot([-30,30],[-25,35],'r--');
        plot([-30,30],[-35,25],'r--');
        axis([-30,30,-100,100]);
        xlabel('Camera [mm]');
        ylabel('Sound [mm]');
        title([cSide,': MSE = ',num2str(MSE(i),3),'mm , CC = ',num2str(CC(i))]);
        set(gca,'DataAspectRatio',[1,1,1])
        text(0.05,0.05,['Color = ',P.ColorField],'Units','norm');
        
      end
      k=k+1;
      MSEs(k,1:2) = MSE;
    end
  end
end

fprintf(['Total Number of Vocalizations : ',num2str(sum(NVoc)),'\n']);

% for i=1:length(Sides)
%   fprintf(['Side : ',Sides{i},'\n']);
%   MSEAll(i) = sqrt(mean((XPosAll{i} - XPosAudioAll{i}).^2))
%   [CCAll(i),Prob] = corr(XPosAll{i}',XPosAudioAll{i}')
% end
  
  

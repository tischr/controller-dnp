function SyncTimes = C_getTrialSync(varargin)
%% PARSE ARGUMENTS
varargin = C_parseID(varargin);
P = parsePairs(varargin);
checkField(P,'Animal',[]);
checkField(P,'Recording',[]);
checkField(P,'Recompute',0);
checkField(P,'Path',[])
checkField(P,'ModuleSync','NIDAQ')
checkField(P,'Verbose',0);
if isempty(P.Path) && (isempty(P.Animal) || isempty(P.Recording))
  error('Either Animal/Recording or Path needs to be specified!');
end

%% DETERMINE CORRECT DATA PATH
if isempty(P.Path)  [P.Path,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);  end

%% CHECK FOR SAVED TIMES
SaveFile = [P.Path,'Results',filesep,'SyncTimes.mat'];
if ~P.Recompute && exist(SaveFile)
  tmp = load(SaveFile);
  SyncTimes = tmp.SyncTimes;
  return;
end

%% LOAD BASIC INFORMATION
T = load([P.Path,'General.mat']); R.General = T.CGSave; clear T;

%% LOAD THE ENTIRE NIDAQ TRIAL DATA
fprintf('Extracting SyncTimes : ');
cPath = [P.Path,P.ModuleSync,filesep];
Files = dir([cPath,'Data_*.mat']);
for iT=1:length(Files);
  FilesFull{iT} = [cPath,'Data_',num2str(iT),'.mat'];
  R.(P.ModuleSync).Data(iT) = load(FilesFull{iT}); fprintf('%d ',iT); 
end; endl;
Recordings = [1:iT]; NRecordings = iT;

Names = {R.General.Paradigm.HW.Inputs.Name};
TrialInd = find(strcmp('Trial',Names));
TrialPos = R.General.Paradigm.HW.Inputs(TrialInd).Index;
CameraInd = find(strcmp('CamStart',Names));
if ~isempty(CameraInd)
  CamAvailable = 1; 
  CameraPos = R.General.Paradigm.HW.Inputs(CameraInd).Index;
else
  CamAvailable = 0;
end

%% FUSE THE DATA ACROSS RECORDINGS
Data = []; Time = []'; 
RecordingEmpty = zeros(NRecordings,1); 
RecordingStops = zeros(NRecordings,1);
switch P.ModuleSync
  case 'NIDAQ';
    for iR = 1:NRecordings
      if CamAvailable
        Data = [Data;R.NIDAQ.Data(iR).Data.Analog(:,[TrialPos,CameraPos])];
      else
        Data = [Data;R.NIDAQ.Data(iR).Data.Analog(:,[TrialPos])];
      end
      Time = [Time;R.NIDAQ.Data(iR).Data.Time(:,1)];
      RecordingEmpty(iR) = isempty(R.NIDAQ.Data(iR).Data.Time);
      RecordingStops(iR) = length(Data);
    end
    TrialData = Data(:,1); 
    if CamAvailable CamData = Data(:,2); end;
    clear Data;
      
  otherwise error('SyncModule not implemented.');
end
NSamples = length(TrialData);

%% FIND TRIAL STARTS
ChangeInd = TrialData>2.5;
ChangePos = find(diff(ChangeInd)~=0)+1;
% CHECK IF LAST STOP RECORDED
if mod(length(ChangePos)/2,1)~=0
  ChangePos(end+1) = NSamples;
  TrialData(end)=0; 
  if CamAvailable;   CamData(end)=0;  end
end
NTrials = length(ChangePos)/2;
% CHECK IF STARTS WITH A POSITIVE CHANGE
if ChangeInd(ChangePos(1))==1
  ChangePos = reshape(ChangePos,2,length(ChangePos)/2)';
else
  error('Change Direction of first trial is not correct');
end

%% CHECK FOR TRIAL CONSISTENCY
% CHECK WHETHER A TRIAL WAS NOT STARTED/NOT RECORDED
if size(ChangePos,1)<NRecordings
  NTrials = size(ChangePos,1);
  ChangePos = ChangePos(1:NTrials,:);
  disp(['Inconsistency between #Trials and #Files (#Trials < # Files). Ignore trials after ',num2str(NTrials)]);
end

% CHECK WHETHER TRIAL STOP WAS ARTEFACTUALLY SHORT
ITI = ChangePos(2:end,1) - ChangePos(1:end-1,2); % Intertrial Interval
SR = 1/(Time(2)-Time(1));
FailInd = ITI<=0.1*SR;
RemoveInd = find(FailInd);
if ~isempty(RemoveInd)
  for i=1:length(RemoveInd)
    cInd = RemoveInd(i);
    cTrials = size(ChangePos,1);
    KeepInd = setdiff([1:numel(ChangePos)],[cInd+1,cTrials + cInd]);
    ChangePos = ChangePos(KeepInd);
    ChangePos = reshape(ChangePos,cTrials-1,2);
    NTrials = NTrials - 1;
  end
end

% CHECK WHETHER A TRIAL WAS SPREAD ACROSS RECORDINGS OR THE OPPOSITE
for iT = 1:NTrials
  if RecordingStops(iT) < ChangePos(iT,1)
    NTrials = iT-1;
    disp(['Inconsistency in recording-times : Ignore trials after ',num2str(NTrials)]);
  end
end

% CHECK IF ACQUISITION FAILED TO START
if sum(RecordingEmpty(1:NTrials))
  NTrials = find(RecordingEmpty(1:NTrials),1,'first')-1;
  disp(['Empty trial found : Ignore trials after ',num2str(NTrials)]);
end
ChangePos = ChangePos(1:NTrials,:);

if ChangePos(end,2)>RecordingStops(NTrials);
  ChangePos(end,2) = RecordingStops(NTrials);
  TrialData(ChangePos(end,2):end) = 0;
 if CamAvailable
   CamData(ChangePos(end,2):end) = 0;
 end
end

%% CHECK FOR TRIAL OVERLAP 
for iT = 1:NTrials
  if ChangePos(iT,2) > RecordingStops(iT);
    NIDAQShift(iT) = ChangePos(iT,2)-RecordingStops(iT);
  else 
    NIDAQShift(iT) = 0;
  end
end

%% FIND CAMERA START
if CamAvailable
  for iT = 1:NTrials
    CamStartInd =  find(CamData(ChangePos(iT,1):end)>2.5,1,'first');
    if ~isempty(CamStartInd)
      CamChangePos(iT,1) = CamStartInd + ChangePos(iT,1)-1;
      CamChangePos(iT,2) = find(CamData(CamChangePos(iT,1):end)<2.5,1,'first') + CamChangePos(iT,1)-1;
      CamChangeTimes(iT,:) = Time(CamChangePos(iT,:),1);
    else
      CamChangePos(iT,:) = [NaN,NaN];
      CamChangeTimes(iT,:) = [NaN,NaN];
    end
  end
end

%% FIND CHANGE TIMES
ChangeTimes(:,1) = Time(ChangePos(:,1),1); ChangeTimes(:,2) = Time(ChangePos(:,2),1);

%% TRANSFER INFORMATION TO OUTPUT
for iT = 1:NTrials
  SyncTimes(iT).Trial = ChangeTimes(iT,:);
  SyncTimes(iT).NIDAQShift = NIDAQShift(iT);
  if CamAvailable; SyncTimes(iT).Camera = CamChangeTimes(iT,:); end
end

%% CORRECT THE NIDAQ DATA (SHIFT DATA BETWEEN TRIALS)
SaveTrials = [];
for iT = 1:NTrials 
  if SyncTimes(iT).NIDAQShift
    ShiftSteps = SyncTimes(iT).NIDAQShift;
    TransInd = [1:ShiftSteps];
    R.NIDAQ.Data(iT).Data.Analog(end+TransInd,:) =  R.NIDAQ.Data(iT+1).Data.Analog(TransInd,:);
    R.NIDAQ.Data(iT).Data.Time(end+TransInd,:) =  R.NIDAQ.Data(iT+1).Data.Time(TransInd,:);
    R.NIDAQ.Data(iT+1).Data.Analog = R.NIDAQ.Data(iT+1).Data.Analog(ShiftSteps+2:end,:);
    R.NIDAQ.Data(iT+1).Data.Time = R.NIDAQ.Data(iT+1).Data.Time(ShiftSteps+2:end,:);
    SaveTrials(end+[1,2]) = [iT,iT+1];
  end
end
SaveTrials = unique(SaveTrials);

% SAVE CORRECTED FILES
if ~isempty(SaveTrials) disp(['Modifying Files : ',num2str(SaveTrials)]); end
for iT = 1:length(SaveTrials)
  cTrial = SaveTrials(iT);
  if P.Verbose disp(['Saving ',FilesFull{cTrial}]); end 
  T = R.NIDAQ.Data(cTrial);
  save(FilesFull{cTrial},'-struct','T','-v7.3');
end
% DELETE REMAINING FILES
if NRecordings>NTrials 
  if  P.Verbose disp(['Deleting Files : ',num2str(NTrials+1:NRecordings)]);  end
  for iT = NTrials+1:NRecordings
    delete(FilesFull{iT});
  end
end

%% SAVE TIMES FOR LATER USAGE
mkdirAll(SaveFile);
save(SaveFile,'SyncTimes');

function C_saveVideoOnly(Frames,varargin)
  
  P = parsePairs(varargin);
  checkField(P,'Time',[]);
  checkField(P,'Filename','');
  
  if isempty(P.Filename) P.Filename = [pwd,filesep,'Data']; end
  if isempty(P.Time) P.Time = [1:size(Frames,4)]; end
  
  Data.TimeAll = P.Time; % SAVE ALL FRAME TIMES
  Data.SaveInd = [1:length(P.Time)];
  Data.MetaData = [];
  
  % ADD DIMENSIONS
  Data.NFrames = length(Data.SaveInd);
  Data.Dims = size(Frames);
  Data.Dims(4) = Data.NFrames;
  Data.Resolution = Data.Dims([1:2]);
  save([P.Filename,'.mat'],'Data','-v6');
  
  % SAVE VIDEO ITSELF
  FID = fopen([P.Filename,'.dat'],'w');
  fwrite(FID,Frames,'uint8');
  fclose(FID);
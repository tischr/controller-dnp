function C_setupEnvironment(varargin)
% Setup Environment on current Computer for 
% - Running Controller for Performing Experiments
% - Working with Data from Controller
  
global CG

% PASE INPUT
P = parsePairs(varargin);
checkField(P,'DataPath',[]);

% ADD CONTROLLER FILES TO CURRENT PATH
Controller('PathOnly');

% SET BASIC PATHS FOR CURRENT COMPUTER
HostNameFile = ['C_Hostname_',lower(HF_getHostname)];
if exist(HostNameFile,'file'); 
  eval(HostNameFile); 
else
  LF_createConfigFile(HostNameFile);
end

% OVERRULE DATA PATH IF SPECIFIED BY USER
if ~isempty(P.DataPath) CG.Files.DataPath = P.DataPath; end

% CHECK DATA BASE ACCESS
if CG.Misc.Database.Use C_checkDatabase; end

evalin('base','global CG');

% CREATE THE CONFIG FILE BASED ON USER INPUT
function LF_createConfigFile(HostNameFile)
  
  global CG
  % FIND THE RIGHT LAB (PATH) FOR THE CURRENT COMPUTER
  Labs = C_getLabs;
  fprintf('Choose your lab : \n')
  for i=1:length(Labs)
    fprintf([num2str(i),' : ',Labs{i},'\n']);
  end
  R = input('Enter the number, or type a new lab abbreviation (no spaces!)) : ','s');
  StartPath = CG.Files.ConfigPath;
  if ~isempty(str2num(R))
    cLab = Labs{str2num(R)};
    fprintf(['Choosing ',cLab,'\n'])
  else 
    cLab = R;
    fprintf(['Creating ',cLab,'\n'])
  end
  LabPath = [StartPath,cLab,filesep];
  if ~exist(LabPath,'dir') mkdir(LabPath); end
    
  % FIND THE RIGHT SETUP
  cSetup = 'Testing';
  
  % FIND THE RIGHT DATA PATH FOR THE CURRENT COMPUTER
  StartPath = tempdir;
  DataPath = [uigetdir(StartPath,'Choose Base Directory for Data Storage'),filesep,'ControllerData',filesep];
  HostNameFileFull = [LabPath,HostNameFile,'.m'];
  FID = fopen(HostNameFileFull,'w');
  fprintf(FID,['function ',HostNameFile,'\n\n'...
    'global CG\n\n',...
    'CG.Files.DataPath = ''',escapeMasker(DataPath),''';\n'...
    'CG.Parameters.General.Lab = ''',cLab,''';\n'...
    'CG.Parameters.General.Setup = ''',cSetup,''';\n'...
    ]);
  fclose(FID);
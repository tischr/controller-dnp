function [Path,Paths] = C_getDir(varargin)
% Gets the data path for a given recording

global CG
P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'Kind','base');
checkField(P,'DB',0);

if isempty(CG) | ~isfield(CG,'Parameters') C_setupEnvironment; end
  
% FIRST BUILD SUBPATH
switch lower(P.Kind)
  case {'base'};  SubPath = [P.Animal,filesep,'R',num2str(P.Recording),filesep];
  otherwise error('Subpath not correctly defined.');
end

% DETERMINE LOCAL DATA PATH
Paths.Local = [CG.Files.DataPath,SubPath];

% DETERMINE DB DATA PATH (LOCAL VERSIOn)
if isfield(CG.Files,'DataPathDB')
  Paths.DB = CG.Files.DataPathDB;
else
  switch architecture
    case 'PCWIN'; Paths.DB = ['S:\ControllerData\'];
    case 'MAC';    Paths.DB = ['/Volumes/Backup/ControllerData/'];
    case 'LINUX';  Paths.DB = ['/auto/Backup/ControllerData/'];
    case 'UNIX'; Paths.DB = ['/cifs/dnp-backup/ControllerData/ControllerData/'];
    otherwise error('Paths not defined!');
  end
  CG.Files.DataPathDB = Paths.DB;
end
Paths.DB =  [Paths.DB,SubPath];

% CHOOSE DESIRED PATH FOR OUTPUT
if P.DB  Path = Paths.DB;  else  Path = Paths.Local; end
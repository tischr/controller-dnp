function R = C_appendVideoTimes(varargin)

P = parsePairs(varargin);
checkField(P,'Animal');
checkField(P,'Recording');
checkField(P,'VideoModule','PointGrey')
checkField(P,'Trials','all');

% LOAD ONLY THE FRAME TIMES
R  = C_loadRecording('Animal',P.Animal,'Recording',P.Recording,...
  'Modules',{'NIDAQ',P.VideoModule},'Trials',P.Trials,'LoadVideoData',0);

% APPEND FRAME-TIMES FOR EACH VIDEO FILE
[RecordingPath,Paths] = C_getDir('Animal',P.Animal,'Recording',P.Recording);
VideoPath = [RecordingPath,P.VideoModule,filesep];

Trials = R.(P.VideoModule).Trials;
NTrials = length(Trials);
fprintf('Appending for Trial : ');
for iT=1:NTrials
  fprintf([' ',num2str(Trials(iT))]);
  % LOAD DATA FILE
  cFile = [VideoPath,'Data_',num2str(Trials(iT)),'.mat'];
  VR = load(cFile);
  VR.Data.TimeAbs = R.(P.VideoModule).Data(Trials(iT)).Data.Time;
  save(cFile,'-struct','VR');
end; fprintf('\n');

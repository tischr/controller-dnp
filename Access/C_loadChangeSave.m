function C_loadChangeSave(File)

R = load(File);
if size(R.Data.Analog,2)>2
  R.Data.Analog = R.Data.Analog(:,1:2);
  save(File,'-struct','R','-v7.3');
else
  disp('Everything fine!');
end
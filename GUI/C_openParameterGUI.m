function C_openParameterGUI(varargin)
%% Open Parameter GUI to set parameters for different 
%
% Should be able to make a call to set the parameters (whenever something is changed)

global CG;

P = parsePairs(varargin);
checkField(P,'MainType');
checkField(P,'Name',[]);
checkField(P,'Type',[]);
checkField(P,'ID',[]);
checkField(P,'Parameters',[]);

switch P.MainType
  case 'Paradigm';
    if isempty(P.Parameters)
      P.Parameters = CG.Paradigm.getParameters;
    end
    cH = CG.GUI.Main.Paradigm;
    P.FIG = CG.GUI.Parameters.Paradigm.FIG;
    P.FigName = ['Parameters of Paradigm'];
    
  case 'Module'
    if isempty(P.Parameters)
      P.Parameters = CG.Paradigm.getParametersModule(P.Name);
    end
    cH = CG.GUI.Main.Modules.(P.Type)(P.ID);
    P.FIG = CG.GUI.Parameters.Modules.(P.Type)(P.ID).FIG;
    P.FigName = ['Parameters of ',P.Name];
  otherwise error('Type not known');
end

% DISABLE THE GUI WHILE PARAMETER SELECTION
set([cH.ModifyButton,cH.ShowButton,cH.StartButton],'Enable','off');

if ~ishandle(P.FIG) C_LF_createFigure(P,cH); end

function FIG = C_LF_createFigure(P,cH)
  global CG;
  
  cPars = P.Parameters;
  MPos = get(CG.GUI.Main.FIG,'Position');
  NPars = length(cPars); SS = get(0,'ScreenSize');
  FH = NPars*40; FW = 280; Border = 0.02;
  FigureSize = [MPos(1)+MPos(3)+10,SS(4)-FH-CG.GUI.Props.MenuOffset,FW,FH];
  
  figure(P.FIG); clf; set(P.FIG,'Position',FigureSize,...
    'NumberTitle','off','Name',P.FigName,'Color',CG.Colors.GUI.Background,...
    'Toolbar','none','Menubar','none',...
    'DeleteFcn',{@C_CBF_closeParameters,P.MainType,P.Type,P.ID,P.Name});
  
  C_addFigure(P.FIG);
  
  Positions = axesDivide([1,1.5],NPars,[Border,Border,1-2*Border,1-2*Border]);
  
  for iP = 1:length(cPars)
    cPar = cPars{iP};
    % NAME (TextField)
    cName = cPar.Name;
    H(iP,1) = uicontrol('style','text','Units','N','BackGroundColor',CG.Colors.GUI.Background,'Position',Positions{iP,1},'String',cName);
    
    cVal = cPar.Value;
    
    % BRANCH BY TYPE
    % Add callback functions for both cases, which assign the values directly
    switch lower(cPar.Type)
      case 'numeric'; % PUT EDIT FIELD
        if numel(cVal)==1
          cString = num2str(cVal,3);
        else
          cString = '';
          for i1=1:size(cVal,1)
            for i2=1:size(cVal,2)
              cString = [cString,num2str(cVal(i1,i2),3),','];
            end
            if i1<size(cVal,1) cString(end) = ';'; end
          end
          cString = cString(1:end-1);
        end
        H(iP,2) = uicontrol('style','edit','Units','N','BackGroundColor',CG.Colors.GUI.Background,...
          'Position',Positions{iP,2},'String',cString,'Callback',{@C_CBF_checkType,'numeric'});
        
      case 'string'; % PUT DROPDOWN OR JUST A STRING
        cSelection = cPar.Choices;
        switch class(cSelection)
          case 'char'; if ~isempty(cSelection) eval(cSelection); cSelection =TMP; end
          case 'cell'; % Assumed to already be correct
        end
        if iscell(cSelection)
          cValue = find(strcmp(cPar.Value,cSelection));
          H(iP,2) = uicontrol('style','popup','Units','N','BackGroundColor',CG.Colors.GUI.Background,...
            'Position',Positions{iP,2},'String',cSelection,'Value',cValue);
          set(H(iP,2),'UserData',cSelection);
        else % SELECTION IS ONLY A STRING
          if iscell(cVal)
            cData = [];
            for i=1:length(cVal) cData = [cData,' ',cVal{i}]; end
          else
            cData = cVal;
          end
          H(iP,2) = uicontrol('style','edit','Units','N','BackGroundColor',CG.Colors.GUI.Background,...
            'Position',Positions{iP,2},'String',cData);
        end
      otherwise error('Type not known');
    end
  end
  set(P.FIG,'UserData',struct('ButtonHandles',cH,'UIHandles',H));

%% CHECK FOR CORRECT PARAMETER TYPE
function C_CBF_checkType(obj,event,Type)
  
cData = get(obj,'String'); FalseType = 1;
switch Type
  case 'numeric'; 
    Val = str2num(cData);
    if ~isempty(Val) & isnumeric(Val) FalseType = 0; end
  case 'string';
end

if FalseType
  set(obj,'ForeGroundColor','red');
else
  set(obj,'ForeGroundColor','black');
end

%% GET PARAMETERS
function Pars = C_getParametersFromGUI(FIG)
  Handles = get(FIG,'UserData');
  for iP=1:size(Handles.UIHandles,1)
    ParName = get(Handles.UIHandles(iP,1),'String');
    switch get(Handles.UIHandles(iP,2),'Style')
      case 'edit';
        ParValue = get(Handles.UIHandles(iP,2),'String');
        tmp = str2num(ParValue);
        if ~isempty(tmp) && isnumeric(tmp) ParValue = tmp; end
      case 'popupmenu';
        Selection = get(Handles.UIHandles(iP,2),'String');
        ParValue = Selection{get(Handles.UIHandles(iP,2),'Value')};
    end
    Pars.(ParName) = ParValue;
  end
  
%% CLOSE GUI AND SET VALUES IN PARADIGM
function C_CBF_closeParameters(obj,event,MainType,Type,ID,ModuleName)
global CG

% GET PARAMETERS FROM GUI
NewPars = C_getParametersFromGUI(obj);

switch MainType
  case 'Paradigm';
    % SET PARAMETERS PARADIGM PARAMETERS
    CG.Paradigm.setParameters(NewPars);
    
  case 'Module';
    Pars = CG.Paradigm.getParametersModule(ModuleName);
    FN = fieldnames(NewPars); for i=1:length(Pars) Pars{i}.Value = NewPars.(FN{i}); end
    % SET PARAMETERS IN MODULE (IN PARADIGM)
    CG.Paradigm.setParametersModule(ModuleName,Pars);
    
    % PREPARE THE CURRENT MODULE WITH THE NEW PARAMETERS
    eval(['C_stop',Type,'(',n2s(ID),',0);']);
    eval(['C_prepare',Type,'(',n2s(ID),');']);
end

% REENABLE THE GUI
H = get(obj,'UserData');
set([H.ButtonHandles.ModifyButton,H.ButtonHandles.ShowButton,H.ButtonHandles.StartButton],'Enable','on');

C_removeFigure(obj);
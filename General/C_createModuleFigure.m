function FIG = C_createModuleFigure(ModuleType,ID,Size)

global CG;

FIG = C_nextFig;
CG.Display.(ModuleType)(ID).FIG = FIG;
if ~isfield(CG.Display.(ModuleType)(ID),'State')
  CG.Display.(ModuleType)(ID).State = 0;
end

% COMPUTE POSITION OF FIGURE
SS = get(0,'ScreenSize');
if isfield(CG,'GUI') & isfield(CG.GUI,'LastXPos') StartXPos = CG.GUI.LastXPos + 10; else StartXPos = 10; end
if StartXPos+Size(1) > SS(3) StartXPos = CG.GUI.Main.Width + 10;  end
FigureSize = [StartXPos,SS(4)-Size(2)-CG.GUI.Props.MenuOffset-25,Size(1),Size(2)];
CG.GUI.LastXPos = StartXPos;
Name = CG.Sessions.(ModuleType)(ID).Name;

% CONFIGURE FIGURE
figure(FIG); clf;
String = CG.Misc.StateStrings{CG.Display.(ModuleType)(ID).State+1};
Title = [Name,' [ ',CG.Sessions.(ModuleType)(ID).Device,' ]'];
set(FIG,'Position',FigureSize,'NumberTitle','off','MenuBar','none','Name',Title,...
  'HitTest','off','Visible',String,'DeleteFcn',{@C_CBF_closeModuleFigure,ModuleType,ID});

C_addFigure(FIG);

function C_CBF_closeModuleFigure(obj,event,ModuleType,ID)

global CG;

CG.Display.(ModuleType)(ID).State = 0;
try set(CG.GUI.Main.Modules.(ModuleType)(ID).ShowButton,'Value',0,'BackgroundColor',[1,1,1]); end

C_removeFigure(obj);
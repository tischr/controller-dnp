function C_cleanUp

global CG Verbose

% REMOVE PREVIOUS IMAGE ACQUISITION SESSIONS
try;  IO = imaqfind; for i=1:length(IO) delete(IO(i)); end; end;
delete(instrfind);
if strcmp(architecture,'PCWIN') daq.reset; end

% CLOSE DISPLAYS FROM PREVIOUS INSTANCE
Displays = {'Video','Audio','Arduino'};
if ~isempty(CG)
  for i=1:length(Displays) 
    try 
      cFIGs = [CG.Display.(Displays{i}).FIG];
      for iF = 1:length(cFIGs)
        close(cFIGs(iF));
      end;
    end; 
  end
end
CGOld = CG;
clear global CG CGold INITIALIZE; global CG

try CG.Misc.Database = CGOld.Misc.Database; end

if isempty(Verbose) Verbose = 0; end
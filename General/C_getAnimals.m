function Animals = C_getAnimals
  
global CG
 
Animals = {'test'};  % Fallback values

if CG.Misc.Database.Use
  C_checkDatabase;

  if CG.Misc.Database.Available
    R = mysql('SELECT name FROM subjects WHERE retired = 0 AND NOT(name=''test'')');
    Animals = {R.name,'test'};
  end
end
function Paradigms = C_getParadigms
  
global CG
  
Path = which('Logic');
Pos = find(Path == filesep);
Path = Path(1:Pos(end-1));
Logics = dir([Path,'*Logic']); k=0;

for iL = 1:length(Logics)
  if ~strcmp(Logics(iL).name,'@Logic')
    
    k = k + 1;
    
    startIndex = 1;
    if Logics(iL).name(1) == '@'
      startIndex = 2;
    end
    
    Paradigms{k} = Logics(iL).name(startIndex:end-5);    
  end
end
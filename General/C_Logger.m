function C_Logger(Caller,String,varargin)
% CENTRALIZED LOGGING OF INFORMATION
% Arguments:
% - Caller : function that sends the logging information
% - String : sprintf compatible string with status information
% - varargin : data to fill dynamic fields in String
 
global CG Verbose 
StringFinal = sprintf(String,varargin{:});
% LOG MESSAGES
if Verbose > -1   CG.Log(end+1,[1:2]) = {Caller,StringFinal}; end
% PRINT MESSAGES
if Verbose > 0     fprintf([Caller,' : ',String],varargin{:});          end

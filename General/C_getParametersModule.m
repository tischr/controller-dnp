function [Pars,PS] = C_getParametersModule(varargin)
%% CENTRALIZE PARAMETER MANAGEMENT FOR ALL MODULES
%
% PARAMETERS (CELL with 4 entries: Name,Default,Type,Choices)
% These can be returned if called with 'ReturnParameters',1
% 'Choices' can be:
% - a vector (possible range of values)
% - a cell (possible values)
% - a string : should evaluate to one of the above (i.e. after eval(string))

global CG

P = parsePairs(varargin);
checkField(P,'ModuleType');
checkField(P,'IDType',0);
checkField(P,'DefaultPars',0);
checkField(P,'Pars',[]);

% GET DEFAULT PARS OF THE CURRENT 
DefaultPars = LF_getDefaultPars(P.ModuleType);

if ~isempty(P.Pars)
  % PARSE PARAMETERS AND RETURN
  for iF=1:size(DefaultPars,1)
    if isfield(P.Pars,DefaultPars{iF,1})
      DefaultPars{iF,2} = P.Pars.(DefaultPars{iF,1});
    end
  end
  Pars = DefaultPars;
else
  % CHECK WHETHER A PARADIGM EXISTS
  ExistParadigm = isfield(CG,'Paradigm') && ~ischar(CG.Paradigm);
  if ExistParadigm && ~P.DefaultPars % PARADIGM EXISTS
    % GET CURRENT PARAMETERS FROM PARADIGM
    Name = CG.Paradigm.getModuleName(P.ModuleType,P.IDType);
    Pars = CG.Paradigm.getParametersModule(Name);
  else % PARADIGM DOES NOT (YET) EXIST
    Pars = DefaultPars;
  end
end

% CONVERT 2D CELL FORMAT TO CELL OF STRUCTS FORMAT
if numel(Pars) > length(Pars) | ~isstruct(Pars{1})
  Pars = C_convPars2Struct(Pars);
end

for i=1:length(Pars) PS.(Pars{i}.Name) = Pars{i}.Value; end


function Pars = LF_getDefaultPars(ModuleType)
global CG;

% RETURN DEFAULT PARAMETERS FOR EACH MODULE
switch ModuleType
  
  % ==============================================
  case 'Arduino';
    ArduinoAvailable = C_discoverArduino('ReturnAvailable',1);
    if ~isempty(ArduinoAvailable)
      cArduino = ArduinoAvailable{1};
    else 
      cArduino = {};
    end
    Pars = {...
      'SerialPort',cArduino,'String',['C_discoverArduino; TMP = CG.Devices.Arduino.Handles;'];...
      'ControlInterval',0.05,'Numeric',inf;...
      };
    
    % ==============================================
  
  case 'NI';
    cNI.ID = 'Dev1';
    Pars = {...
      'Name','NI','String','';...
      'Device',cNI.ID,'String',['TMP = fieldnames(CG.Devices.NI);'];...
      'ChAI',{'ai0','ai1','ai2','ai3','ai4','ai5','ai6','ai7','ai8','ai9','ai10','ai11','ai12','ai13','ai14','ai15'},'String','TMP=[];';...
      'AINames',{'0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15'},'String','';...
      'SRAI',10000,'Numeric',inf;...s
      'ChAO',{'ao0','ao1'},'String','TMP = [];';...
      'AONames',{'0','1'},'String','';...
      'SRAO',10000,'Numeric',inf;...
      'ChDO',{ 'port0/line7' },'String',inf;... % ivanenko
      'TerminalConfig','SingleEnded','String','TMP = {''SingleEnded'',''Differential''};';...
      'ControlInterval',0.05,'Numeric',inf;...
      'DisplayDuration',1,'Numeric',[];...
      'Saving','Trial','String','TMP = {''Trial'',''All''};'};
    
    % ==============================================
  case {'AudioIn'};
    switch architecture
      case 'PCWIN'; PreferredDevice = 'OctaCapture'; % 'SoundCard'
        %        case 'PCWIN'; PreferredDevice = 'SoundCard';
      case 'MAC';    PreferredDevice = 'SoundBlaster';
    end
    Pars = {...
      'Device',PreferredDevice,'String','C_discoverAudio; TMP = fieldnames(CG.Devices.Audio);'; ...
      'Channels',1,'Numeric',inf; ...
      'ControlInterval',0.1,'Numeric',[]; ...
      'DisplayDuration',1,'Numeric',[]; ...
      'SR',192000,'Numeric', []};
    
    % ==============================================
  case {'AudioOut'};
    switch architecture
      case 'PCWIN'; PreferredDevice = 'DirectSound';
      case 'MAC';    PreferredDevice = 'SoundBlaster';
    end
    Pars = {...
      'Device',PreferredDevice,'String','C_discoverAudio; TMP = fieldnames(CG.Devices.Audio);'; ...
      'Channels',[],'Numeric',inf; ...
      'ControlInterval',1,'Numeric',[]; ...
      'SR',192000,'Numeric', []};
    
    % ==============================================
  case 'Keyboard';
    Pars = {'ControlInterval',0.1,'Numeric',inf};
    
    %% ==============================================
  case 'Video';    

    Pars = {};

      
    switch architecture
      case 'PCWIN';       
        PreferredDevice = 'PointGrey';          
        hwi = imaqhwinfo(PreferredDevice);
        availableVideoModes = hwi.DeviceInfo.SupportedFormats;
        cMode = hwi.DeviceInfo.DefaultFormat;
        cMode = CG.Parameters.Setup.Camera.VideoMode;     
         cSR = CG.Parameters.Setup.Camera.FrameRate;          
         cST = CG.Parameters.Setup.Camera.ShutterTime;
        Pars = [ Pars; { 'ShutterTime', cST, 'Numeric', inf; } ];
        
      case 'MAC'; 
          PreferredDevice = 'MacVideo';
          availableVideoModes = { 'YCbCr422_1280x720' };       
          cMode = availableVideoModes{1};
          cSR = 30;
          
    end
    
    videoModesStr = '{';
    for i = 1:numel(availableVideoModes)
        if i ~= 1;             videoModesStr = [ videoModesStr ','];         end
        videoModesStr = [videoModesStr '''' availableVideoModes{i} ''''];
    end
    videoModesStr = [videoModesStr '}'];
    
    Pars = [ {...
      'Device', PreferredDevice, 'String', 'C_discoverVideo; TMP = fieldnames(CG.Devices.Video);'; ...
      'CameraID', 1, 'Numeric', inf; ...
      'BrightnessThreshold', 0.94, 'Numeric', 1; ...
      'ControlInterval', 0.1, 'Numeric', inf; ...
      'VideoMode', cMode,'String', ['TMP = ', videoModesStr, ';']; ...
      'SR', cSR, 'Numeric', []; ...      
      }; Pars ] ;
    
  case 'Phidget'
    Pars = {...
      'ControlInterval',0.05,'Numeric',inf;...
      };
    
  otherwise error('Module ');
end

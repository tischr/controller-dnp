function C_getID(varargin)
%% FIND THE LATEST ID FOR THE CURRENT MODULE TYPE
global CG;

P = parsePairs(varargin);
checkField(P,'ModuleType');

if isfield(CG,'Sessions') && isfield(CG.Sessions,P.ModuleType) && isfield(CG.Sessions.(P.ModuleType),'S')
  ID = length(CG.Sessions.(P.ModuleType)) + 1;
else
  ID = 1;
end

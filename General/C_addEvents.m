function C_addEvents(ModuleType,ID,Events)

global CG; 

LastEvent = length(CG.Events.(ModuleType)(ID).Events);
CG.Events.(ModuleType)(ID).Events(LastEvent+1:LastEvent+length(Events)) = Events;
for iE=1:length(Events)
  C_Logger('C_addEvents',...
    ['Event ',Events(iE).Name,' at ',num2str(Events(iE).Data),' (T=',num2str(Events(iE).Time),')\n']);
end

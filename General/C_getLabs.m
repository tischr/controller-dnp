function Labs = C_getLabs
  
global CG

LabDirs = dir([CG.Files.ConfigPath]);

Labs = {};
for iL=1:length(LabDirs)
  cLab = LabDirs(iL).name;
  switch cLab
    case {'.','..','.DS_Store'}; 
    otherwise
      Labs{end+1} = cLab;
  end
end

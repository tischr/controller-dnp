function StatesStr = C_setFeedbackState(Index)
  
global CG;

ID = CG.Parameters.Setup.Camera.CameraID;
EP = CG.Sessions.Video(ID).ExtractPosition;
PositionChannels = EP.DistanceChannels;
NBits = length(PositionChannels);

if Index > 1 Index = 1; end
if Index <0 Index = 0; end
Index = Index * (2^NBits-1);

StatesStr = fliplr(dec2bin(Index,NBits));
States = int8(StatesStr)-48;
OldStates =     int8(CG.Sessions.Video(ID).ExtractPosition.OldStates);
if any(States-OldStates)
  C_setDigitalNI(CG.Paradigm.HW.IDs.DAQ,[PositionChannels],[States]); 
  CG.Sessions.Video(ID).ExtractPosition.OldStates = States;
end
function Users = C_getUsers
  
global CG

Users = {'unknown'}; % FALLBACK VALUE

if CG.Misc.Database.Use
  C_checkDatabase;
  
  if CG.Misc.Database.Available
    R = mysql('SELECT name,lab FROM users');
    Users = sort({R.name});
  end
end

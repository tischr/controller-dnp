function FIG = C_nextFig

global CG;

FIG = mod(round(now*10000000),1000000000);
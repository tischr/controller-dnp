function Identifier = C_generateIdentifier(Animal,Recording)

if nargin<1
  global CG
  Animal = CG.Parameters.General.Animal;
  Recording = CG.Parameters.General.Recording;
end

Identifier = [Animal,'_R',num2str(Recording)];

CG.Parameters.General.Identifier = Identifier;
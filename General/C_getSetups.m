function Setups = C_getSetups(cLab)
  
global CG

if nargin<1 cLab = CG.Parameters.General.Lab; end

CG.Files.ConfigPath = [CG.Files.CodePath,'Configurations',filesep];
Labs = dir([CG.Files.ConfigPath]);

for iL=1:length(Labs)
  cLabName = Labs(iL).name;
  switch cLabName
    case {'.','..','.DS_Store'};
    otherwise
      cSetups = dir([CG.Files.ConfigPath,cLabName,filesep,'C_Setup_*.m']);
      CG.Misc.Labs.(cLabName).Setups = {};
      for iS = 1:length(cSetups)
        cSetupName = cSetups(iS).name(9:end-2);
        CG.Misc.Labs.(cLabName).Setups{iS} = cSetupName;
      end
  end
end

Setups = CG.Misc.Labs.(cLab).Setups;
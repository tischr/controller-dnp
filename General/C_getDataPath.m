function Path = C_getDataPath

global CG;

% CHECK IF SETUP SET THE PATH
if ~isfield(CG,'Files') || ~isfield(CG.Files,'DataPath') || isempty(CG.Files.DataPath)
  Path = tempdir;
  CG.Files.DataPath = Path;
end

function C_initialize

global CG;

set(0,'DefaultFigureColor',[1,1,1]);

%% INITIALIZE GENERAL PARAMETERS
CG.Log = cell(0,2);
File = which('Controller'); CG.Files.CodePath = File(1:find(File==filesep,1,'last'));

%% DATA BASE ACCESS
if ~isfield(CG.Misc.Database,'Use')  CG.Misc.Database.Use = 1; end 

%% ASSIGN DEFAULT USER
CG.Parameters.General.User = 'unknown';

% ASSIGN COMPUTER AND SETUP DEPENDENCIES
CG.Parameters.General.Hostname = lower(HF_getHostname);
eval(['C_Hostname_',CG.Parameters.General.Hostname,';']);
if isfield(CG.Parameters.General,'Setup')
  eval(['C_Setup_',CG.Parameters.General.Setup,';']);
end
C_getDataPath;
  
%% INITIALIZE MODULE SPECIFIC PARAMETERS
% VIDEO

CG.Parameters.Video.MacVideo.SRMax = 30;

% AUDIO


% ARDUINO
BytesPerPacket = 13; BaudRate = [230400];
CG.Parameters.Arduino = struct(...
  'StepSize',1000,'BaudRate',BaudRate,'InputBufferSize',100*BaudRate,...
  'BytesAvailableFcnMode','byte','TimeInd',BytesPerPacket - [3,2],'BusyInd',BytesPerPacket-4,...
  'BytesPerPacket',BytesPerPacket,'BytesPerCommand',11,'PacketsDisplayed',1000,...
  'StopCharAsInt',[254;255],'AnalogRange',[0,5],'DigitalRange',[0,1],...
  'AbortPin',38); % Change to 255 also in Arduino
% NI
CG.Parameters.NI = struct('AnalogInRange',{[-6,6],[-6,6]},'AnalogOutRange',{[-10,10],[-10,10]},'StepSize',{5,5}); % 5 seconds

 % PHIDGET
 CG.Parameters.Phidget = struct('LibName','phidget21',...
  'BoardSN',163609,'BoardType','01616','NumberOfBits',16);

% KEYBOARD

% PARADIGM
CG.Display.Paradigm.FIG = [];

%% INITIALIZE SOME VARIABLES FOR SPEEDY ACCESS
CG.Misc.StateStrings = {'off','on'};
CG.Misc.UInt8ToBit = uint8(zeros(256,8));
for iI=1:256;  for iB=1:8; CG.Misc.UInt8ToBit(iI,iB) = bitget(uint8(iI),iB); end; end
CG.Misc.DateNum2SecFactor = 24*60*60;

%% INITIALIZE GUI PARAMETERS
CG.GUI.Main.FIG = 10000;
CG.GUI.Main.Width = 220;
CG.GUI.LastXPos = CG.GUI.Main.Width + 10;
CG.GUI.Props.Font = 'Arial';
switch architecture
  case 'PCWIN'; 
    CG.GUI.Props.FontSizePanel = 8; CG.GUI.Props.MenuOffset = 31;
    CG.GUI.Props.WindowOffset = 31;
  case 'MAC'; 
    CG.GUI.Props.FontSizePanel = 12; CG.GUI.Props.MenuOffset = 60;
    CG.GUI.Props.WindowOffset = 31;
end

CG.GUI.AllFigures = [];

%% DEFINES DEFAULT COLORS
if ~isfield(CG,'Colors') || ~isfield(CG.Colors,'Skin') CG.Colors.Skin = 'default'; end
switch lower(CG.Colors.Skin)
  case 'default'
    CG.Colors.GUI.Background = [1,1,1];
    CG.Colors.GUI.TextColor = [0,0,0];
    CG.Colors.GUI.Active = [1,1,0];
    CG.Colors.GUI.Inactive = [0.5,0.5,0.5];
    CG.Colors.GUI.Panel = [1,1,1];
    CG.Colors.GUI.PanelModule = [0.9,0.9,1];
    CG.Colors.GUI.PanelHighlight = [0.5,0.5,0.5];
    CG.Colors.Figure.Background = [1,1,1];
    CG.Colors.Figure.LineColor = [0,0,0];
    CG.Colors.Figure.Trace = [0,0,1];
    CG.Colors.Figure.Threshold = [1,0,0];
end

%% INITIALIZE PARADIGM
CG.Paradigm = eval([CG.Paradigm,'Logic']);
%CG.Paradigm = GapCrossingLogic;



function State = C_mountServer(Force)
% MOUNT THE BACKUP SERVER
global CG;

if nargin<1 Force = 0; end

% TEST AND ESTABLISH THE VPN CONNECTION IF NECESSARY
switch architecture
  case 'PCWIN'
    %rasdial /disconnect
    %rasdial connection_name user pass
    CommandConnect = ['C:\Windows\System32\rasdial "ScienceVPNSec"'];
    system(CommandConnect);
  case 'MAC';
    [S,C] = system('scutil --nc list');
    if length(C)<=70
      disp('Assuming VPN is established...');
    else
      CommandCheck = ['networksetup -showpppoestatus "ScienceVPN"'];
      [R,Status] = system(CommandCheck); Status = Status(1:end-1);
      if ~strcmp(Status,'connected')
        CommandConnect = ['networksetup -connectpppoeservice "ScienceVPN"'];
        system(CommandConnect);
        fprintf(['Connecting to VPN ']);
        while ~strcmp(Status,'connected')
          fprintf('.');
          pause(0.25); % WAIT FOR CONNECTION TO ESTABLISH
          [R,Status] = system(CommandCheck); Status = Status(1:end-1);
        end
        fprintf('established.\n');
      end
    end
end

switch architecture
  case 'PCWIN';
    CommandUnmount = ['net use ',lower(CG.Misc.Database.MappedFolder),' /Delete'];
    CommandMount = ['C:\Windows\System32\net use ',...
      CG.Misc.Database.MappedFolder,...
      '  \\',CG.Misc.Database.Server,'\',CG.Misc.Database.Share,...
      ' /user:',CG.Misc.Database.User,' ',CG.Misc.Database.Password];
  case 'MAC';
    CommandUnmount = ['umount ',CG.Misc.Database.MappedFolder];
    CommandMount = ['mkdir ',CG.Misc.Database.MappedFolder,'; mount_smbfs //',CG.Misc.Database.User,':',...
      CG.Misc.Database.Password,'@',CG.Misc.Database.Server,...
      '/',CG.Misc.Database.Share,' ',CG.Misc.Database.MappedFolder];
  case 'UNIX';
    CommandUnmount = ['umount ',CG.Misc.Database.MappedFolder];
    CommandMount = ['mkdir ',CG.Misc.Database.MappedFolder,'; sudo mount -t cifs ',...
      '//',CG.Misc.Database.Server,...
      '/',CG.Misc.Database.Share,' ',CG.Misc.Database.MappedFolder,...
      ' -o username=',CG.Misc.Database.User,',password=',CG.Misc.Database.Password];
  otherwise error('Mouting not implemented yet.');
end

% TRY TO UNMOUNT THE CURRENT SHARE
if Force; [R,Out] = system(CommandUnmount); end

% MOUNT THE FOLDER 
if ~isdir([CG.Misc.Database.MappedFolder]) | Force
  fprintf('Connecting to Server ...');
  [R,Out] = system(CommandMount);
  if R error(Out); 
  else fprintf(' mounted.\n');
  end
end

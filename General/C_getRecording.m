function NextRecording = C_getRecording(Animal)
  
global CG

if CG.Misc.Database.Use
  C_checkDatabase;
end

% GET RECORDING FROM DISK
RecordingDirs = dir([CG.Files.DataPath Animal '\']);
Recordings = [];
for iR=1:length(RecordingDirs)
  if RecordingDirs(iR).name(1) == 'R'
    Recordings(end+1) = str2num(RecordingDirs(iR).name(2:end));
  end
end
NextRecordingDisk = max(Recordings)+1;

if CG.Misc.Database.Use && CG.Misc.Database.Available
  R = mysql(['SELECT recording FROM recordings WHERE animal=''',Animal,'''']);
  if isempty(R)
    NextRecording = 1;
  else
    NextRecording = max([R.recording])+1;
  end
  
  if NextRecordingDisk>NextRecording
    fprintf('WARNING : \tFound a later recording on the disk than in the DB.\n');
    NextRecording = NextRecordingDisk;
  end
else
  NextRecording = NextRecordingDisk;
end


if isempty(NextRecording)
  NextRecording = 1;
end
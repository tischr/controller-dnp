function [ModulePaths,Modules] = C_getModulePaths(RecordingPath)
% GET MODULE PATHS FOR THE CURRENT RECORDING PATH

ModuleDirs = dir(RecordingPath);
ModuleDirs = ModuleDirs([ModuleDirs.isdir]);
ModuleDirs = ModuleDirs(~strcmp({ModuleDirs.name},'.'));
ModuleDirs = ModuleDirs(~strcmp({ModuleDirs.name},'..'));

for iM=1:length(ModuleDirs) 
  Modules{iM} = ModuleDirs(iM).name; 
  ModulePaths{iM} = [RecordingPath,Modules{iM},filesep];
end

function C_orderFields

global CG

% ORDER MAIN FIELDS
CG = orderfields(CG);

% ORDER DISPLAY FIELDS
CG.Display = orderfields(CG.Display);
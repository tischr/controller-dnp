function C_TrialTimerCallback(H,E,DelayTime)
global CG;

if CG.Paradigm.TrialActive
  fprintf(['=== Timer for Trial has expired (',num2str(DelayTime),'s) =>  NewTrial. ====\n']);
  CG.Paradigm.TrialActive = 0;
  C_setDigitalNI(CG.Paradigm.HW.IDs.DAQ,'Trial',0);
  CG.Paradigm.stopTrial;
  if CG.Paradigm.VideoAvailable
    C_setDigitalNI(CG.Paradigm.HW.IDs.DAQ,'Camera',1);
    C_setDigitalNI(CG.Paradigm.HW.IDs.DAQ,'Camera',0);
    flushdata(CG.Sessions.Video(1).S);
  end;
  CG.Paradigm.prepareTrial;
  pauseUntil(CG.Paradigm.TimeToContinue);
  CG.Paradigm.startTrial;
end
